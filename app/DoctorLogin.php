<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorLogin extends Model
{
    protected $table = 'doctor_login';

    public function doctor(){
        return $this->belongsTo(Doctor::class,'doctor_id','id');
    }
}
