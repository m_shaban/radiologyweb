<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorItemBranches extends Model
{
    protected $table = 'vendoritembranches';
    public function vendor_items(){
        return $this->belongsTo(VendorItems::class,'vendor_item_id','id');
        }
        public function vendor_branches(){
        return $this->belongsTo(VendorBranches::class,'branch_id','id');
        }
        public function request_vendor_branches(){
        return $this->hasMany(RequestVendorBranches::class,'vendor_item_branches_id','id');
        }
}
