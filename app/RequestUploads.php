<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestUploads extends Model
{
    protected $table = 'RequestUploads';
    protected $fillable = ['upload'];
    public $timestamps = false;
     public function request_header(){
        return $this->belongsTo(RequestHeader::class,'request_header_id','id');
     }
}
