<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestQuestion extends Model
{
    protected  $table = 'RequestQuestion';
     protected $fillable = [
        'answer',
        'request_id',
        'vendor_service_question_id',
       
    ];
    public function request_header(){
        return $this->belongsTo(RequestHeader::class,'request_id','id');
        }
        
        public function vendor_service_question(){
        return $this->belongsTo(VendorServiceQuestion::class,'vendor_service_question_id','id');
        }
}
