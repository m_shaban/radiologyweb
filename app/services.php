<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class services extends Model
{

    
        protected $table = 'Services';
    
       public function service_types(){
        return $this->belongsTo(servicetypes::class,'service_type_id','id');
    } 
   public function service_files(){
        return $this->hasMany(servicefiles::class,'service_id','id');
    }
    public function vendor_services(){

        return $this->hasMany(vendorservices::class,'service_id','id');
    }
   
}
