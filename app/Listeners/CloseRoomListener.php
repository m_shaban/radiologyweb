<?php

namespace App\Listeners;

use App\Events\CloseRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class CloseRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CloseRoomEvent  $event
     * @return void
     */
    public function handle(CloseRoomEvent $event)
    {
        Broadcast::channel($event->roomName,function(){return true;});
    }
}
