<?php

namespace App\Listeners;

use App\Events\PatientSendMessageEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PatientSendMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PatientSendMessageEvent  $event
     * @return void
     */
    public function handle(PatientSendMessageEvent $event)
    {
        //
    }
}
