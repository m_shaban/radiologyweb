<?php

namespace App\Listeners;

use App\Events\PatientLeftRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class PatientLeftRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PatientLeftRoomEvent  $event
     * @return void
     */
    public function handle(PatientLeftRoomEvent $event)
    {
        Broadcast::channel($event->roomName, function () { return true;});
    }
}
