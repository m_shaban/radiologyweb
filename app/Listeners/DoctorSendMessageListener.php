<?php

namespace App\Listeners;

use App\Events\DoctorSendMessageEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DoctorSendMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DoctorSendMessageEvent  $event
     * @return void
     */
    public function handle(DoctorSendMessageEvent $event)
    {
        //
    }
}
