<?php

namespace App\Listeners;

use App\Events\PatientJoinedRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class PatientJoinedRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PatientJoinedRoomEvent  $event
     * @return void
     */
    public function handle(PatientJoinedRoomEvent $event)
    {
        Broadcast::channel($event->roomName, function () { return true;});
    }
}
