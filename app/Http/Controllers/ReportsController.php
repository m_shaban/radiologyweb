<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function statusReports()
    {
        $data = collect(DB::select(
                                     "Select PatientName,PatientGender,
                                             Phones,Location,RequestDate,
                                              case when status is null then 'Request under Process'
                                              when status = 'cr'  then 'Client Cancelled the Request'
                                              when status = 'rw'  then 'Waiting for client response'
                                              when status = 'qw'  then 'Client waiting for center approval'
                                              when status = 'qa'  then 'Center approved reservation'
                                              when status = 'qr'  then 'Center reject reservation'
                                              when status = 'pa'  then 'Patient Arrived to Center'
                                              When status = 'pr'  then 'Patient Cancelled Reservation'
                                              when status = 'rd'  then 'Results are delivered'
                                              else 'Unknown' 
                                              end Follow
                                    from v_patientrequest
                                    order by RequestDate desc;"))->toJson();

        return view('reports.statusgroup',compact('data'));
    }
}
