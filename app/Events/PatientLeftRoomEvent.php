<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PatientLeftRoomEvent extends ChannelEvent
{
    use InteractsWithSockets, SerializesModels;
    public $patientId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roomName,$patientId)
    {
        parent::__construct($roomName);
        $this->patientId = $patientId;
    }

    public function broadcastAs()
    {
        return 'patient.left_room';
    }
}
