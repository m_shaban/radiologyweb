<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreateRoomEvent extends ChannelEvent
{
    use InteractsWithSockets, SerializesModels;
    public $patientId;
    public $doctorId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roomName, $patientId, $doctorId)
    {
        parent::__construct($roomName);
        $this->doctorId = $doctorId;
        $this->patientId = $patientId;
    }

    public function broadcastAs()
    {
        return 'room.created';
    }
}
