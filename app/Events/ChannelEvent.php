<?php
/**
 * Created by PhpStorm.
 * User: mhosmy
 * Date: 13/09/17
 * Time: 10:13 ص
 */

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChannelEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $roomName;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roomName)
    {
        $this->roomName = $roomName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel($this->roomName);
    }
}