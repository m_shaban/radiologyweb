<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class confirmation extends Model
{
    protected $table = 'radiologyconfirmed';
    protected $fillable =['center_id','radiology_type_id'];
 public function confirmation(){
        return $this
            ->belongsToMany(confirmation::class,'paitent_request','center_id','response_id')
            ->withPivot(['arrive_datetime','response_id','center_id'])
            ->withTimestamps();
    }

 
}
