<?php

namespace App\Models\Chat;

use App\Doctor;
use App\Patient;
use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    protected $table= 'chat_rooms';
    protected $fillable = ['room_name','doctor_id','patient_id'];

    public function messages(){
        return $this->hasMany(RoomMessage::class,'room_id','id');
    }

    public function doctor(){
        return $this->belongsTo(Doctor::class,'doctor_id','id');

    }
    public function patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');

    }
}
