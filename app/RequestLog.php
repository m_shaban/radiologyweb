<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    protected $table = 'RequestLog';
    public function request_header(){
        return $this->belongsTo(RequestHeader::class,'request_header_id','id');
        }
    public function request_status(){
        return $this->belongsTo(request_status::class,'request_status_id','id');
        }    
}
