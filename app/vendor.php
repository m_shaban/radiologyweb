<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendor extends Model
{

    
        protected $table = 'Vendor';
   
        public function vendor_branches(){
        return $this->hasMany(vendorbranches::class,'vendor_id','id');
        }
 public function vendor_cntacts(){
        return $this->hasMany(vendorcntacts::class,'vendor_branch_id','id');
    }
public function vendor_services(){

        return $this->hasMany(vendorservices::class,'vendor_id','id');
    }
}
