<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendorservices extends Model
{
    public $timestamps = false;
        protected $table = 'VendorServices';
    

    public function vendor(){
        return $this->belongsTo(vendor::class,'vendor_id','id');
    }
     public function services(){
        return $this->belongsTo(services::class,'service_id','id');
    }  

    public function vendor_uploads()
    {
        return $this->hasMany(vendoruploads::class,'vendorservices_id','id');
    }
       public function vendor_services_types()
    {
        return $this->hasMany(VendorServicesTypes::class,'vendor_service_id','id');
    }
    public function vendor_service_question(){
        return $this->hasMany(VendorServiceQuestion::class,'vendor_service_id','id');
        }

}
