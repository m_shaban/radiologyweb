<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVendorLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
        Schema::table('VendorBranches', function (Blueprint $table) {
            $table->boolean('head_quarter');     
            $table->string('map_link');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('VendorBranches', function (Blueprint $table) {
            $table->dropColumn(['head_quarter']);
            $table->dropColumn(['map_link']);
        });
    }
}
