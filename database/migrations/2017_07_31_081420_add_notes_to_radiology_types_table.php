<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotesToRadiologyTypesTable extends Migration
{
    public function up()
    {
        Schema::table('radiology_types', function($table) {
            $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('radiology_types', function (Blueprint $table) {
            $table->dropColumn('notes');
        });
    }
}
