<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestVendorBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestVendorBranches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_details_id')->unsigned();
            $table->integer('vendor_item_branches_id')->unsigned();
            $table->dateTime('arrival_date_time');
            $table->boolean('is_active');
            $table->timestamps();
            $table->foreign('request_details_id')->references('id')->on('RequestDetail');
            $table->foreign('vendor_item_branches_id')->references('id')->on('VendorItemBranches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestVendorBranches');
    }
}
