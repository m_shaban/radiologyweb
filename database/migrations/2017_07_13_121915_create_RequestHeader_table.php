<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestHeader', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('requester_id')->unsigned();
            $table->integer('requester_type');//Doctor Or Patient
            $table->integer('recipient_id');//From 
            $table->boolean('is_home');
            $table->boolean('is_stable');
            $table->boolean('is_emergency');
            $table->decimal('weight');
            $table->foreign('requester_id')->references('id')->on('Users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestHeader');
    }
}
