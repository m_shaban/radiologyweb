<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestQuestion', function (Blueprint $table) {
            $table->increments('id');
             $table->string('answer');
            $table->integer('request_id')->unsigned(); 
            $table->integer('vendor_service_question_id')->unsigned();             
            $table->timestamps();
            $table->foreign('request_id')->references('id')->on('RequestHeader');
            $table->foreign('vendor_service_question_id')->references('id')->on('VendorServiceQuestion');              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestQuestion');
    }
}
