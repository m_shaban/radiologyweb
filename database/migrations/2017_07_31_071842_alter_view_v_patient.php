<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterViewVPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Statement("
                        Alter ALGORITHM = UNDEFINED
                         DEFINER =`root`@`localhost`
                         SQL SECURITY DEFINER VIEW `v_patient` AS
                          SELECT
                            `pd`.`id`                                                                            AS `PatientCode`,
                            `pd`.`name`                                                                          AS `PatientName`,
                            `pd`.`gender`                                                                        AS `PatientGender`,
                            `pd`.`email`                                                                         AS `email`,
                            group_concat(trim(concat(`pp`.`phone`)) SEPARATOR ',')                               AS `Phones`,
                            group_concat(trim(`pa`.`address`) SEPARATOR ',')                                     AS `Addresses`,
                            group_concat(concat('(', `pa`.`latitude`, ',', `pa`.`longitude`, ')') SEPARATOR ',') AS `MapAddresses`
                          FROM ((`patient` `pd` LEFT JOIN `patient_phone` `pp` ON ((`pd`.`id` = `pp`.`patient_id`))) LEFT JOIN
                            `patient_addresses` `pa` ON ((`pd`.`id` = `pa`.`patient_id`)))
                          GROUP BY `pd`.`id`, `pd`.`name`, `pd`.`gender`, `pd`.`email`"
                    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::Statement("drop view v_patient");
    }
}
