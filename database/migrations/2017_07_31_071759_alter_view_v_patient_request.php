<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterViewVPatientRequest extends Migration
{
    public function up()
    {
        DB::Statement("
          Alter ALGORITHM = UNDEFINED
          DEFINER =`root`@`localhost`
          SQL SECURITY DEFINER VIEW `v_patientrequest` AS
          SELECT
            `p`.`PatientCode`                                  AS `PatientCode`,
            `p`.`PatientName`                                  AS `PatientName`,
            `p`.`PatientGender`                                AS `PatientGender`,
            `p`.`email`                                        AS `email`,
            `p`.`Phones`                                       AS `Phones`,
             Case when `r`.`patient_address_id` is not null then `a`.`address`
                  else `p`.`Addresses`
             end                                               AS `Addresses`,
            `p`.`MapAddresses`                                 AS `MapAddresses`,
            `r`.`patient_age`                                  AS `Age`,
            concat(`r`.`patient_weight`, ' KG')                AS `Weight`,
            `r`.`is_at_home`                                   AS `is_at_home`,
            (CASE WHEN (`r`.`is_at_home` = 0)
              THEN 'At Center'
             ELSE 'At Home' END)                               AS `Location`,
            (CASE WHEN (`r`.`is_able_to_be_stable` = 0)
              THEN 'No'
             ELSE 'Yes' END)                                   AS `Stable`,
            date_format(`r`.`created_at`, '%d-%m-%Y %h:%i %p') AS `RequestDate`,
            `u`.`PrescriptionImages`                           AS `Uploads`,
            `r`.`id`                                           AS `RequestNo`,
            `r`.`status`                                       AS `status`,
            `r`.`report`                                       As `report`
          FROM `patient_requests` `r` LEFT JOIN `v_patient` `p`
          ON (`r`.`patient_id` = `p`.`PatientCode`)
          LEFT JOIN `v_patientrequestuploads` `u`
          ON (`r`.`id` = `u`.`request_id`)
          LEFT JOIN `patient_addresses` a
          ON (`a`.`id` = `r`.`patient_address_id`);");
    }

    public function down()
    {
        DB::Statement("drop view V_PatientRequest;");
    }

}
