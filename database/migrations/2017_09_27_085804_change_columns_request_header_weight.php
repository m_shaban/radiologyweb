<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsRequestHeaderWeight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
    {
        Schema::table('RequestHeader', function($table) {
            DB::statement('ALTER TABLE `RequestHeader` CHANGE `weight` `weight` decimal(8,2) NULL;');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('RequestHeader', function($table) {
            $table->dropColumn('weight');
            
        });
    }
}
