<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestLog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_header_id')->unsigned();
            $table->integer('request_status_id')->unsigned();
            $table->timestamps();

            $table->foreign('request_header_id')->references('id')->on('RequestHeader');
            $table->foreign('request_status_id')->references('id')->on('RequestStatus');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestLog');
    }
}
