@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Delete Radiology Type</b> Was Done Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Radiology Type</b> Was Not Deleted.
    </div>
    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Create Radiology Types!</h4>
        <b>You Could Create Radiology Types</b> for the Center.
    </div>

    <a class="btn btn-success" href="{{route('radiologytypecategories.create')}}">
        <i class="fa fa-plus"></i> Add New Radiology Type
    </a>
    <br>
    <hr>
<table id="radiologytypecategories" class="table display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>#</th>
        <th>Radiology Type - English</th>
        <th>Radiology Type - Arabic</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Radiology Type - English</th>
        <th>Radiology Type - Arabic</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </tfoot>
    <tbody>
        @php $i=1; @endphp
        @foreach($radiologyCats as $cat)
            <tr>
                <td>{{$i++}}</td>
                <td><a href="{{route('radiologytypecategories.show',$cat->id)}}">{{$cat->en_name}}</a></td>
                <td><a href="{{route('radiologytypecategories.show',$cat->id)}}">{{$cat->ar_name}}</a></td>
                <td>
                    <a href="{{route('radiologytypecategories.edit',$cat->id)}}" Class="btn btn-info">
                        <i Class="fa fa-lg fa-pencil"></i>
                    </a>
                </td>
                <td>
                    {!!Form::open(['method' => 'delete','route' => ['radiologytypecategories.destroy', $cat->id]])!!}
                    {{ csrf_field() }}
                    <button type="submit" Class="btn btn-danger">
                        <i Class="fa fa-lg fa-trash"></i>
                    </button>
                    {!!Form::close()!!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@stop
@section('script')
<script>
    $(document).ready(function () {

        $('#radiologytypecategories').DataTable();
    });
</script>
@stop