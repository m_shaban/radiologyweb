@extends('layout')
@section('content')

    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Delete Branch</b> Was Done Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Branch</b> Was Not Deleted.
    </div>

    <a class="btn btn-success" href="{{route('centers.create')}}">
        <i class="fa fa-plus"></i> Add New Branch
    </a>
    <br>
    <hr>
    <table id="centers" class="table display" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>#</th>
            <th>Center</th>
            <th>Address</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Center</th>
            <th>Address</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @php $i=1; @endphp
        @foreach($centers as $center)
            <tr>
                <td>{{$i++}}</td>
                <td><a href="{{route('centers.show',$center->id)}}">{{$center->name}}</a></td>
                <td>{{$center->address}}</td>
                <td>
                    <a href="{{route('centers.edit',$center->id)}}" Class="btn btn-info">
                        <i Class="fa fa-lg fa-pencil"></i>
                    </a>
                </td>
                <td>
                    {!!Form::open(['method' => 'delete','route' => ['centers.destroy', $center->id]])!!}
                    {{ csrf_field() }}
                    <button type="submit" Class="btn btn-danger">
                        <i Class="fa fa-lg fa-trash"></i>
                    </button>
                    {!!Form::close()!!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop
@section('script')
    <script>
        $(document).ready(function () {

            $('#centers').DataTable();
        });
    </script>
@stop