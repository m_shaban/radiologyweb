@extends('layout')
@section('content')


<p>{{trans('patientrequests.Dashboard')}}</p>
<div class="col-md-12">
    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Totals</h3>
                <div class="text-right"> <span class="text-muted">Total </span>
                    <h1><sup><i class=" text-success"></i></sup> {{$total_countcurrent}}</h1>
                </div>

                <span class="text-info">Total Confirmed {{$total_countcurrentqa}}</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrent==0) {$total_countcurrent=1;} echo round(($total_countcurrentqa/$total_countcurrent)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>


                <span class="text-success">Total Cancelled {{$total_countcurrentrr}}</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrent==0) {$total_countcurrent=1;} echo round(($total_countcurrentrr/$total_countcurrent)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>


            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Current Status</h3>
                <div class="text-right"> <span class="text-muted">Total Request </span>
                    <h1><sup><i class=" text-success"></i></sup> {{$total_countcurrentdate}}</h1>
                </div>

                <span class="text-success">Confirmed @php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdateqa/$total_countcurrentdate)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdateqa/$total_countcurrentdate)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-danger">Cancelled @php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdaterr/$total_countcurrentdate)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdaterr/$total_countcurrentdate)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-info">Inprogress @php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdateqw/$total_countcurrentdate)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentdate==0) {$total_countcurrentdate=1;} echo round(($total_countcurrentdateqw/$total_countcurrentdate)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

            </div>
        </div>
    </div>

   <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Month Status</h3>
                <div class="text-right"> <span class="text-muted">Total Request </span>
                    <h1><sup><i class=" text-success"></i></sup> {{$total_countcurrentmonth}}</h1>
                </div>



                <span class="text-success">Confirmed @php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthqa/$total_countcurrentmonth)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthqa/$total_countcurrentmonth)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-danger">Cancelled @php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthrr/$total_countcurrentmonth)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthrr/$total_countcurrentmonth)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-info">Inprogress @php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthqw/$total_countcurrentmonth)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentmonth==0) {$total_countcurrentmonth=1;} echo round(($total_countcurrentmonthqw/$total_countcurrentmonth)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>



            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Year Status</h3>
                <div class="text-right"> <span class="text-muted">Total Request </span>
                    <h1><sup><i class=" text-success"></i></sup> {{$total_countcurrentyear}}</h1>
                </div>



                <span class="text-success">Confirmed @php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearqa/$total_countcurrentyear)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearqa/$total_countcurrentyear)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-danger">Cancelled @php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearrr/$total_countcurrentyear)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearrr/$total_countcurrentyear)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

                <span class="text-info">Inprogress @php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearqw/$total_countcurrentyear)*100);  @endphp%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:@php if($total_countcurrentyear==0) {$total_countcurrentyear=1;} echo round(($total_countcurrentyearqw/$total_countcurrentyear)*100);  @endphp%;"> <span class="sr-only">20% Complete</span> </div>
                </div>



            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Total Cancelled Requests</h3>
                <div class="text-right"> <span class="text-muted">Total Canelled</span>
                    <h1><sup><i class=" text-success"></i></sup> {{$count_canceled_current}}</h1>
                </div>
                <span class="text-success">{{$count_canceled_currentprec}}%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Unproccessed</h3>
                <div class="text-right"> <span class="text-muted">Total Statuses</span>
                    <h1><sup><i class=" text-success"></i></sup> {{$count_unprocessed_current}}</h1>
                </div>
                <span class="text-success">{{$count_unprocessed_currentprec}}%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">In Progress</h3>
                <div class="text-right"> <span class="text-muted">Total Statuses</span>
                    <h1><sup><i class=" text-success"></i></sup> {{$count_inprogress_current}}</h1>
                </div>
                <span class="text-success">{{$count_inprogress_currentprec}}%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="white-box">

            <div class="el-card-avatar imagehov">
                <h3 class="box-title">Confirmed</h3>
                <div class="text-right"> <span class="text-muted">Total Confirmed</span>
                    <h1><sup><i class=" text-success"></i></sup> {{$count_confirmed_current}}</h1>
                </div>
                <span class="text-success">{{$count_confirmed_currentprec}}%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<table id="example" class="table display" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>patient name</th>
            <th>request date</th>
            <th>location</th>
            <th>status</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>patient name</th>
            <th>request date</th>
            <th>location</th>
            <th>status</th>

        </tr>
    </tfoot>
    <tbody>

        @if(!empty($requests))
        @foreach($requests as $request)
            @if($request->status == 'cr')
                <?php $class = 'info'; ?>
            @elseif($request->status == 'rw')
                <?php $class = 'warning'; ?>
            @elseif($request->status == 'rr')
              <?php $class = 'danger'; ?>
            @elseif($request->status == 'qw')
                <?php $class = 'warning'; ?>
            @elseif($request->status == 'qa')
                <?php $class = 'success'; ?>
            @elseif($request->status == 'qr')
                <?php $class = 'danger'; ?>
            @else
                <?php $class = 'default'; ?>
        @endif
        <tr class="<?php echo $class; ?>">
            <td>{{$request->PatientName}}</td>
            <td>{{$request->RequestDate}}</td>
            <td>{{$request->Location}}</td>
            @if($request->status == 'cr')
            <td><span class="badge btn-danger"> Client Reject </span></td>
            @elseif($request->status == 'rw')
            <td><span class="badge btn-warning"> Request Waiting </span></td>
            @elseif($request->status == 'rr')
            <td><span class="badge btn-danger"> Request Rejected </span></td>
            @elseif($request->status == 'qw')
            <td><span class="badge btn-warning"> Question Waiting </span></td>
            @elseif($request->status == 'qa')
            <td><span class="badge btn-success"> Question Approved </span></td>
            @elseif($request->status == 'qr')
            <td><span class="badge btn-danger"> Question Rejected </span></td>
            @else
            <td><span class="badge btn-default">Unknown </span></td>
            @endif

        </tr>
        @endforeach
        @endif
    </tbody>
</table>

@stop




@section('script')
<script>
$(document).ready(function () {

    $('#example').DataTable();
});
</script>
@endsection

