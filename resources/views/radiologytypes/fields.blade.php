

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Item (English)</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="flag-icon flag-icon-us"></span>
                </span>
                <input type="text" id="en_name" class="form-control" name="en_name" value="{{(isset($rad->en_name) ? $rad->en_name : '')}}" placeholder="Radiology Type (English)" required>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Item (Arabic)</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="flag-icon flag-icon-eg"></span>
                </span>
                <input type="text" id="ar_name" class="form-control" name="ar_name" value="{{(isset($rad->ar_name) ? $rad->ar_name : '')}}" placeholder="Radiology Type (Arabic)" required>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Type</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="fa fa-rss"></span>
                </span>
                <select class="form-control" name="type_group_id">
                    <option value=""></option>
                @foreach($radiologyCats as $cat)
                    @if(isset($rad->type_group_id) && $rad->type_group_id == $cat->id)
                        <option value="{{$cat->id}}" selected>{{$cat->en_name}}-{{$cat->ar_name}}</option>
                    @else
                        <option value="{{$cat->id}}">{{$cat->en_name}}-{{$cat->ar_name}}</option>
                    @endif
                @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Item Price</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="fa fa-dollar"></span>
                </span>
                <input type="number" min="10" max="10000" id="price" class="form-control" name="price" value="{{(isset($rad->price) ? $rad->price : '')}}" placeholder="Radiology Item Price" required>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Definition</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-tag font-blue"></i>
                </span>
                <textarea type="text" class="form-control" name="definition" placeholder="Add definition" >{{(isset($rad->definition) ? $rad->definition : '')}}</textarea>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Preparation</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-briefcase"></i>
                </span>
                <textarea type="text" class="form-control" name="preparation" placeholder="Add Preparation" >{{(isset($rad->preparation) ? $rad->preparation : '')}}</textarea>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Notes</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-file-text"></i>
                </span>
                <textarea type="text" class="form-control" name="notes" placeholder="Write down Notes" >{{(isset($rad->notes) ? $rad->notes : '')}}</textarea>
            </div>
        </div>
    </div>
</div>