@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Radiology Items!</h4>
        <b>You Could Create New Radiology Item</b> for the Center.
    </div>

    <a class="btn btn-success" href="{{route('radiologytypecategories.create')}}">
        <i class="fa fa-plus"></i> Create New Radiology Item
    </a>
    <br>
    <hr>
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"> Radiology Item Detail </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" style="background-color: whitesmoke;">
                    <div class="panel-body form">

                        @include('radiologytypes.fields')

                        <div class="form-actions">
                            <div class="form-actions">
                                <a href="{{route('radiologytypes.edit',$rad->id)}}" Class="btn btn-info" style="color: white;">
                                    <i Class="fa fa-lg fa-pencil"></i> Edit
                                </a>
                                <a href="{{route('radiologytypes.index')}}" style="color:white;" class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        $(document).ready(function(){
            $("input").prop('disabled', true);
            $("select").prop('disabled', true);
            $("textarea").prop('disabled', true);
        });
    </script>
@stop
