/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : doc2

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-12 09:51:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `address`
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `country_code` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `longtiude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------

-- ----------------------------
-- Table structure for `admins`
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', 'admin', 'admin@example.com', '$2y$10$.sgOyYfQU/bKJv6UlXwuwetHqFFXgy9HwdYe4lWSQ6LZBdW82xMpW', null, '2017-06-05 04:18:32', '2017-06-05 04:18:32');

-- ----------------------------
-- Table structure for `adminsol`
-- ----------------------------
DROP TABLE IF EXISTS `adminsol`;
CREATE TABLE `adminsol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adminsol
-- ----------------------------

-- ----------------------------
-- Table structure for `admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `uri` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '0', '1', 'Index', 'fa-bar-chart', '/', null, null);
INSERT INTO `admin_menu` VALUES ('2', '0', '2', 'Admin', 'fa-tasks', '', null, null);
INSERT INTO `admin_menu` VALUES ('3', '2', '3', 'Users', 'fa-users', 'auth/users', null, null);
INSERT INTO `admin_menu` VALUES ('4', '2', '4', 'Roles', 'fa-user', 'auth/roles', null, null);
INSERT INTO `admin_menu` VALUES ('5', '2', '5', 'Permission', 'fa-user', 'auth/permissions', null, null);
INSERT INTO `admin_menu` VALUES ('6', '2', '6', 'Menu', 'fa-bars', 'auth/menu', null, null);
INSERT INTO `admin_menu` VALUES ('7', '2', '7', 'Operation log', 'fa-history', 'auth/logs', null, null);
INSERT INTO `admin_menu` VALUES ('8', '0', '9', 'Helpers', 'fa-gears', '', null, '2017-06-13 19:48:55');
INSERT INTO `admin_menu` VALUES ('9', '8', '10', 'Scaffold', 'fa-keyboard-o', 'helpers/scaffold', null, '2017-06-13 19:48:55');
INSERT INTO `admin_menu` VALUES ('10', '8', '11', 'Database terminal', 'fa-database', 'helpers/terminal/database', null, '2017-06-13 19:48:55');
INSERT INTO `admin_menu` VALUES ('11', '8', '12', 'Laravel artisan', 'fa-terminal', 'helpers/terminal/artisan', null, '2017-06-13 19:48:55');
INSERT INTO `admin_menu` VALUES ('14', '0', '8', 'Doctors', 'fa-adn', 'auth/doctor', '2017-06-13 19:48:51', '2017-07-05 18:13:38');

-- ----------------------------
-- Table structure for `admin_operation_log`
-- ----------------------------
DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `method` varchar(10) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `input` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1113 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_operation_log
-- ----------------------------
INSERT INTO `admin_operation_log` VALUES ('1', '1', 'admin', 'GET', '127.0.0.1', '[]', '2017-06-03 07:16:53', '2017-06-03 07:16:53');
INSERT INTO `admin_operation_log` VALUES ('2', '1', 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:14', '2017-06-03 07:17:14');
INSERT INTO `admin_operation_log` VALUES ('3', '1', 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:18', '2017-06-03 07:17:18');
INSERT INTO `admin_operation_log` VALUES ('4', '1', 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:21', '2017-06-03 07:17:21');
INSERT INTO `admin_operation_log` VALUES ('5', '1', 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:27', '2017-06-03 07:17:27');
INSERT INTO `admin_operation_log` VALUES ('6', '1', 'admin/auth/permissions', 'POST', '127.0.0.1', '{\"slug\":\"f\",\"name\":\"f\",\"_token\":\"8z2bxl0V0vIiXG5ktuvPi99SlTMy0WA5Ts26UV6r\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2017-06-03 07:17:38', '2017-06-03 07:17:38');
INSERT INTO `admin_operation_log` VALUES ('7', '1', 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2017-06-03 07:17:39', '2017-06-03 07:17:39');
INSERT INTO `admin_operation_log` VALUES ('8', '1', 'admin/auth/roles', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:45', '2017-06-03 07:17:45');
INSERT INTO `admin_operation_log` VALUES ('9', '1', 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:17:57', '2017-06-03 07:17:57');
INSERT INTO `admin_operation_log` VALUES ('10', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 07:18:05', '2017-06-03 07:18:05');
INSERT INTO `admin_operation_log` VALUES ('11', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:46:09', '2017-06-03 18:46:09');
INSERT INTO `admin_operation_log` VALUES ('12', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\doctor\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\doctor\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:52:28', '2017-06-03 18:52:28');
INSERT INTO `admin_operation_log` VALUES ('13', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:52:29', '2017-06-03 18:52:29');
INSERT INTO `admin_operation_log` VALUES ('14', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:53:03', '2017-06-03 18:53:03');
INSERT INTO `admin_operation_log` VALUES ('15', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:53:04', '2017-06-03 18:53:04');
INSERT INTO `admin_operation_log` VALUES ('16', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\",\"controller_name\":\"App\\\\Admin\\\\Controllers\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:53:25', '2017-06-03 18:53:25');
INSERT INTO `admin_operation_log` VALUES ('17', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:53:26', '2017-06-03 18:53:26');
INSERT INTO `admin_operation_log` VALUES ('18', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\",\"controller_name\":\"App\\\\Admin\\\\Controllers\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:55:17', '2017-06-03 18:55:17');
INSERT INTO `admin_operation_log` VALUES ('19', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:55:18', '2017-06-03 18:55:18');
INSERT INTO `admin_operation_log` VALUES ('20', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\",\"controller_name\":\"App\\\\Admin\\\\Controllers\",\"create\":[\"model\",\"controller\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:55:51', '2017-06-03 18:55:51');
INSERT INTO `admin_operation_log` VALUES ('21', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:55:51', '2017-06-03 18:55:51');
INSERT INTO `admin_operation_log` VALUES ('22', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:56:38', '2017-06-03 18:56:38');
INSERT INTO `admin_operation_log` VALUES ('23', '1', 'admin/auth/menu/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:56:50', '2017-06-03 18:56:50');
INSERT INTO `admin_operation_log` VALUES ('24', '1', 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:56:59', '2017-06-03 18:56:59');
INSERT INTO `admin_operation_log` VALUES ('25', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:57:02', '2017-06-03 18:57:02');
INSERT INTO `admin_operation_log` VALUES ('26', '1', 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:57:12', '2017-06-03 18:57:12');
INSERT INTO `admin_operation_log` VALUES ('27', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 18:57:14', '2017-06-03 18:57:14');
INSERT INTO `admin_operation_log` VALUES ('28', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\doctor\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\doctor\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:58:55', '2017-06-03 18:58:55');
INSERT INTO `admin_operation_log` VALUES ('29', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:58:56', '2017-06-03 18:58:56');
INSERT INTO `admin_operation_log` VALUES ('30', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\doctor\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\doctor\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 18:59:44', '2017-06-03 18:59:44');
INSERT INTO `admin_operation_log` VALUES ('31', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 18:59:46', '2017-06-03 18:59:46');
INSERT INTO `admin_operation_log` VALUES ('32', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\doctor\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\doctor\",\"create\":[\"migration\",\"model\",\"controller\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:00:25', '2017-06-03 19:00:25');
INSERT INTO `admin_operation_log` VALUES ('33', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:00:26', '2017-06-03 19:00:26');
INSERT INTO `admin_operation_log` VALUES ('34', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '[]', '2017-06-03 19:01:03', '2017-06-03 19:01:03');
INSERT INTO `admin_operation_log` VALUES ('35', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:refresh\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:01:29', '2017-06-03 19:01:29');
INSERT INTO `admin_operation_log` VALUES ('36', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:status\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:01:46', '2017-06-03 19:01:46');
INSERT INTO `admin_operation_log` VALUES ('37', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:01:58', '2017-06-03 19:01:58');
INSERT INTO `admin_operation_log` VALUES ('38', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:rollback\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:02:09', '2017-06-03 19:02:09');
INSERT INTO `admin_operation_log` VALUES ('39', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:02:21', '2017-06-03 19:02:21');
INSERT INTO `admin_operation_log` VALUES ('40', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":null,\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:02:22', '2017-06-03 19:02:22');
INSERT INTO `admin_operation_log` VALUES ('41', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:02:36', '2017-06-03 19:02:36');
INSERT INTO `admin_operation_log` VALUES ('42', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:03:20', '2017-06-03 19:03:20');
INSERT INTO `admin_operation_log` VALUES ('43', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:03:53', '2017-06-03 19:03:53');
INSERT INTO `admin_operation_log` VALUES ('44', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:rollback\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:06:01', '2017-06-03 19:06:01');
INSERT INTO `admin_operation_log` VALUES ('45', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:09:53', '2017-06-03 19:09:53');
INSERT INTO `admin_operation_log` VALUES ('46', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:09:53', '2017-06-03 19:09:53');
INSERT INTO `admin_operation_log` VALUES ('47', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:10:30', '2017-06-03 19:10:30');
INSERT INTO `admin_operation_log` VALUES ('48', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:10:31', '2017-06-03 19:10:31');
INSERT INTO `admin_operation_log` VALUES ('49', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:rollback\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:11:03', '2017-06-03 19:11:03');
INSERT INTO `admin_operation_log` VALUES ('50', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:reset\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:11:09', '2017-06-03 19:11:09');
INSERT INTO `admin_operation_log` VALUES ('51', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:refresh\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:11:15', '2017-06-03 19:11:15');
INSERT INTO `admin_operation_log` VALUES ('52', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:status\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:11:32', '2017-06-03 19:11:32');
INSERT INTO `admin_operation_log` VALUES ('53', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:11:52', '2017-06-03 19:11:52');
INSERT INTO `admin_operation_log` VALUES ('54', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:11:53', '2017-06-03 19:11:53');
INSERT INTO `admin_operation_log` VALUES ('55', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:12:21', '2017-06-03 19:12:21');
INSERT INTO `admin_operation_log` VALUES ('56', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:12:21', '2017-06-03 19:12:21');
INSERT INTO `admin_operation_log` VALUES ('57', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:13:55', '2017-06-03 19:13:55');
INSERT INTO `admin_operation_log` VALUES ('58', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:13:56', '2017-06-03 19:13:56');
INSERT INTO `admin_operation_log` VALUES ('59', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:refresh\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:16:30', '2017-06-03 19:16:30');
INSERT INTO `admin_operation_log` VALUES ('60', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:reset\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:16:35', '2017-06-03 19:16:35');
INSERT INTO `admin_operation_log` VALUES ('61', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate:status\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:16:40', '2017-06-03 19:16:40');
INSERT INTO `admin_operation_log` VALUES ('62', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"cache:clear\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:16:49', '2017-06-03 19:16:49');
INSERT INTO `admin_operation_log` VALUES ('63', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:16:58', '2017-06-03 19:16:58');
INSERT INTO `admin_operation_log` VALUES ('64', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:16:59', '2017-06-03 19:16:59');
INSERT INTO `admin_operation_log` VALUES ('65', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-06-03 19:17:48', '2017-06-03 19:17:48');
INSERT INTO `admin_operation_log` VALUES ('66', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"cache:clear\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:22:27', '2017-06-03 19:22:27');
INSERT INTO `admin_operation_log` VALUES ('67', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"doctor\",\"model_name\":\"App\\\\Models\\\\doctor\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\doctor\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"gender\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"user_state\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"current_credit\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"profile_img\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"password\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"graduation_year\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_arabic\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"short_bio_english\",\"type\":\"text\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"city\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"country\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"neighborhood\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_level_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"doctor_speciality_id\",\"type\":\"integer\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"front_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"back_card_id\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"referral_number\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"practicing\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:22:48', '2017-06-03 19:22:48');
INSERT INTO `admin_operation_log` VALUES ('68', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:22:49', '2017-06-03 19:22:49');
INSERT INTO `admin_operation_log` VALUES ('69', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:23:16', '2017-06-03 19:23:16');
INSERT INTO `admin_operation_log` VALUES ('70', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"admin:menu\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:24:35', '2017-06-03 19:24:35');
INSERT INTO `admin_operation_log` VALUES ('71', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"admin:install\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:25:08', '2017-06-03 19:25:08');
INSERT INTO `admin_operation_log` VALUES ('72', '1', 'admin/auth/logs', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:25:32', '2017-06-03 19:25:32');
INSERT INTO `admin_operation_log` VALUES ('73', '1', 'admin', 'GET', '127.0.0.1', '[]', '2017-06-03 19:26:08', '2017-06-03 19:26:08');
INSERT INTO `admin_operation_log` VALUES ('74', '1', 'admin/helpers/terminal/database', 'GET', '127.0.0.1', '[]', '2017-06-03 19:26:49', '2017-06-03 19:26:49');
INSERT INTO `admin_operation_log` VALUES ('75', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '[]', '2017-06-03 19:26:52', '2017-06-03 19:26:52');
INSERT INTO `admin_operation_log` VALUES ('76', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:27:07', '2017-06-03 19:27:07');
INSERT INTO `admin_operation_log` VALUES ('77', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:clear\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:27:23', '2017-06-03 19:27:23');
INSERT INTO `admin_operation_log` VALUES ('78', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:27:30', '2017-06-03 19:27:30');
INSERT INTO `admin_operation_log` VALUES ('79', '1', 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:28:25', '2017-06-03 19:28:25');
INSERT INTO `admin_operation_log` VALUES ('80', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:29:02', '2017-06-03 19:29:02');
INSERT INTO `admin_operation_log` VALUES ('81', '1', 'admin/auth/menu/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:29:32', '2017-06-03 19:29:32');
INSERT INTO `admin_operation_log` VALUES ('82', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:29:44', '2017-06-03 19:29:44');
INSERT INTO `admin_operation_log` VALUES ('83', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"Doctors\",\"icon\":\"fa-bars\",\"uri\":\"auth\\/doctor\",\"roles\":[null],\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:30:09', '2017-06-03 19:30:09');
INSERT INTO `admin_operation_log` VALUES ('84', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-06-03 19:30:10', '2017-06-03 19:30:10');
INSERT INTO `admin_operation_log` VALUES ('85', '1', 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"demo\",\"model_name\":\"App\\\\Models\\\\demo\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\demo\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"text\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:54:27', '2017-06-03 19:54:27');
INSERT INTO `admin_operation_log` VALUES ('86', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 19:54:28', '2017-06-03 19:54:28');
INSERT INTO `admin_operation_log` VALUES ('87', '1', 'admin', 'GET', '127.0.0.1', '[]', '2017-06-03 19:55:03', '2017-06-03 19:55:03');
INSERT INTO `admin_operation_log` VALUES ('88', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:55:24', '2017-06-03 19:55:24');
INSERT INTO `admin_operation_log` VALUES ('89', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:55:49', '2017-06-03 19:55:49');
INSERT INTO `admin_operation_log` VALUES ('90', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:cache\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:55:57', '2017-06-03 19:55:57');
INSERT INTO `admin_operation_log` VALUES ('91', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:14', '2017-06-03 19:56:14');
INSERT INTO `admin_operation_log` VALUES ('92', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:25', '2017-06-03 19:56:25');
INSERT INTO `admin_operation_log` VALUES ('93', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:28', '2017-06-03 19:56:28');
INSERT INTO `admin_operation_log` VALUES ('94', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:29', '2017-06-03 19:56:29');
INSERT INTO `admin_operation_log` VALUES ('95', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '[]', '2017-06-03 19:56:31', '2017-06-03 19:56:31');
INSERT INTO `admin_operation_log` VALUES ('96', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:41', '2017-06-03 19:56:41');
INSERT INTO `admin_operation_log` VALUES ('97', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:56:53', '2017-06-03 19:56:53');
INSERT INTO `admin_operation_log` VALUES ('98', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"cache:clear\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:57:20', '2017-06-03 19:57:20');
INSERT INTO `admin_operation_log` VALUES ('99', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"admin:menu\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:57:28', '2017-06-03 19:57:28');
INSERT INTO `admin_operation_log` VALUES ('100', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:57:51', '2017-06-03 19:57:51');
INSERT INTO `admin_operation_log` VALUES ('101', '1', 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"demo\",\"icon\":\"fa-bars\",\"uri\":\"auth\\/demo\",\"roles\":[null],\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 19:58:16', '2017-06-03 19:58:16');
INSERT INTO `admin_operation_log` VALUES ('102', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2017-06-03 19:58:17', '2017-06-03 19:58:17');
INSERT INTO `admin_operation_log` VALUES ('103', '1', 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 19:58:27', '2017-06-03 19:58:27');
INSERT INTO `admin_operation_log` VALUES ('104', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 19:59:51', '2017-06-03 19:59:51');
INSERT INTO `admin_operation_log` VALUES ('105', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:00:22', '2017-06-03 20:00:22');
INSERT INTO `admin_operation_log` VALUES ('106', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:02:27', '2017-06-03 20:02:27');
INSERT INTO `admin_operation_log` VALUES ('107', '1', 'admin', 'GET', '127.0.0.1', '[]', '2017-06-03 20:02:53', '2017-06-03 20:02:53');
INSERT INTO `admin_operation_log` VALUES ('108', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:03:13', '2017-06-03 20:03:13');
INSERT INTO `admin_operation_log` VALUES ('109', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:06:52', '2017-06-03 20:06:52');
INSERT INTO `admin_operation_log` VALUES ('110', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:08:27', '2017-06-03 20:08:27');
INSERT INTO `admin_operation_log` VALUES ('111', '1', 'admin/demo', 'GET', '127.0.0.1', '[]', '2017-06-03 20:09:39', '2017-06-03 20:09:39');
INSERT INTO `admin_operation_log` VALUES ('112', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:10:12', '2017-06-03 20:10:12');
INSERT INTO `admin_operation_log` VALUES ('113', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:16:08', '2017-06-03 20:16:08');
INSERT INTO `admin_operation_log` VALUES ('114', '1', 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:16:44', '2017-06-03 20:16:44');
INSERT INTO `admin_operation_log` VALUES ('115', '1', 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:16:50', '2017-06-03 20:16:50');
INSERT INTO `admin_operation_log` VALUES ('116', '1', 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:17:57', '2017-06-03 20:17:57');
INSERT INTO `admin_operation_log` VALUES ('117', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:17:59', '2017-06-03 20:17:59');
INSERT INTO `admin_operation_log` VALUES ('118', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:18:18', '2017-06-03 20:18:18');
INSERT INTO `admin_operation_log` VALUES ('119', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:20:37', '2017-06-03 20:20:37');
INSERT INTO `admin_operation_log` VALUES ('120', '1', 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 20:22:35', '2017-06-03 20:22:35');
INSERT INTO `admin_operation_log` VALUES ('121', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:clear\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 20:22:43', '2017-06-03 20:22:43');
INSERT INTO `admin_operation_log` VALUES ('122', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:cache\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 20:22:49', '2017-06-03 20:22:49');
INSERT INTO `admin_operation_log` VALUES ('123', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 20:22:56', '2017-06-03 20:22:56');
INSERT INTO `admin_operation_log` VALUES ('124', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 20:29:54', '2017-06-03 20:29:54');
INSERT INTO `admin_operation_log` VALUES ('125', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 20:37:54', '2017-06-03 20:37:54');
INSERT INTO `admin_operation_log` VALUES ('126', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:53:21', '2017-06-03 20:53:21');
INSERT INTO `admin_operation_log` VALUES ('127', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:53:37', '2017-06-03 20:53:37');
INSERT INTO `admin_operation_log` VALUES ('128', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:54:06', '2017-06-03 20:54:06');
INSERT INTO `admin_operation_log` VALUES ('129', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 20:57:52', '2017-06-03 20:57:52');
INSERT INTO `admin_operation_log` VALUES ('130', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:cache\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 21:01:26', '2017-06-03 21:01:26');
INSERT INTO `admin_operation_log` VALUES ('131', '1', 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"route:list\",\"_token\":\"Cp893qfEPxf1WzxAT3TxCAczWEN6FauTQ0VdGMWE\"}', '2017-06-03 21:02:25', '2017-06-03 21:02:25');
INSERT INTO `admin_operation_log` VALUES ('132', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:02:52', '2017-06-03 21:02:52');
INSERT INTO `admin_operation_log` VALUES ('133', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:03:52', '2017-06-03 21:03:52');
INSERT INTO `admin_operation_log` VALUES ('134', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:04:12', '2017-06-03 21:04:12');
INSERT INTO `admin_operation_log` VALUES ('135', '1', 'admin/doctor/create', 'GET', '127.0.0.1', '[]', '2017-06-03 21:05:28', '2017-06-03 21:05:28');
INSERT INTO `admin_operation_log` VALUES ('136', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:05:43', '2017-06-03 21:05:43');
INSERT INTO `admin_operation_log` VALUES ('137', '1', 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2017-06-03 21:06:02', '2017-06-03 21:06:02');
INSERT INTO `admin_operation_log` VALUES ('138', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:06:17', '2017-06-03 21:06:17');
INSERT INTO `admin_operation_log` VALUES ('139', '1', 'admin/doctor/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:06:26', '2017-06-03 21:06:26');
INSERT INTO `admin_operation_log` VALUES ('140', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:06:31', '2017-06-03 21:06:31');
INSERT INTO `admin_operation_log` VALUES ('141', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:09:32', '2017-06-03 21:09:32');
INSERT INTO `admin_operation_log` VALUES ('142', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:09:33', '2017-06-03 21:09:33');
INSERT INTO `admin_operation_log` VALUES ('143', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:10:30', '2017-06-03 21:10:30');
INSERT INTO `admin_operation_log` VALUES ('144', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:10:31', '2017-06-03 21:10:31');
INSERT INTO `admin_operation_log` VALUES ('145', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:10:35', '2017-06-03 21:10:35');
INSERT INTO `admin_operation_log` VALUES ('146', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:10:45', '2017-06-03 21:10:45');
INSERT INTO `admin_operation_log` VALUES ('147', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:10:45', '2017-06-03 21:10:45');
INSERT INTO `admin_operation_log` VALUES ('148', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:12:38', '2017-06-03 21:12:38');
INSERT INTO `admin_operation_log` VALUES ('149', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:12:39', '2017-06-03 21:12:39');
INSERT INTO `admin_operation_log` VALUES ('150', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:12:58', '2017-06-03 21:12:58');
INSERT INTO `admin_operation_log` VALUES ('151', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-03 21:13:33', '2017-06-03 21:13:33');
INSERT INTO `admin_operation_log` VALUES ('152', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:13:49', '2017-06-03 21:13:49');
INSERT INTO `admin_operation_log` VALUES ('153', '1', 'admin/doctor/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:13:54', '2017-06-03 21:13:54');
INSERT INTO `admin_operation_log` VALUES ('154', '1', 'admin/doctor/create', 'GET', '127.0.0.1', '[]', '2017-06-03 21:14:38', '2017-06-03 21:14:38');
INSERT INTO `admin_operation_log` VALUES ('155', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:15:37', '2017-06-03 21:15:37');
INSERT INTO `admin_operation_log` VALUES ('156', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:15:40', '2017-06-03 21:15:40');
INSERT INTO `admin_operation_log` VALUES ('157', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-03 21:15:58', '2017-06-03 21:15:58');
INSERT INTO `admin_operation_log` VALUES ('158', '1', 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:16:05', '2017-06-03 21:16:05');
INSERT INTO `admin_operation_log` VALUES ('159', '1', 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:16:09', '2017-06-03 21:16:09');
INSERT INTO `admin_operation_log` VALUES ('160', '1', 'admin/auth/users/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:16:13', '2017-06-03 21:16:13');
INSERT INTO `admin_operation_log` VALUES ('161', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-03 21:19:23', '2017-06-03 21:19:23');
INSERT INTO `admin_operation_log` VALUES ('162', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '[]', '2017-06-04 05:58:25', '2017-06-04 05:58:25');
INSERT INTO `admin_operation_log` VALUES ('163', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-04 05:58:52', '2017-06-04 05:58:52');
INSERT INTO `admin_operation_log` VALUES ('164', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"per_page\":\"100\",\"_pjax\":\"#pjax-container\"}', '2017-06-04 05:59:03', '2017-06-04 05:59:03');
INSERT INTO `admin_operation_log` VALUES ('165', '1', 'admin/doctor/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-04 05:59:20', '2017-06-04 05:59:20');
INSERT INTO `admin_operation_log` VALUES ('166', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"per_page\":\"100\",\"_pjax\":\"#pjax-container\"}', '2017-06-04 06:05:10', '2017-06-04 06:05:10');
INSERT INTO `admin_operation_log` VALUES ('167', '1', 'admin/doctor', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2017-06-04 06:05:11', '2017-06-04 06:05:11');
INSERT INTO `admin_operation_log` VALUES ('168', '1', 'admin/doctor', 'GET', '127.0.0.1', '[]', '2017-06-04 06:05:14', '2017-06-04 06:05:14');
INSERT INTO `admin_operation_log` VALUES ('169', '1', 'admin/doctor/22/edit', 'GET', '127.0.0.1', '[]', '2017-06-04 06:05:43', '2017-06-04 06:05:43');
INSERT INTO `admin_operation_log` VALUES ('170', '1', 'admin', 'GET', '127.0.0.1', '[]', '2017-06-04 16:53:21', '2017-06-04 16:53:21');
INSERT INTO `admin_operation_log` VALUES ('171', '1', 'admin', 'GET', '41.42.201.12', '[]', '2017-06-06 06:56:42', '2017-06-06 06:56:42');
INSERT INTO `admin_operation_log` VALUES ('172', '1', 'admin/helpers/terminal/database', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:57:47', '2017-06-06 06:57:47');
INSERT INTO `admin_operation_log` VALUES ('173', '1', 'admin/auth/users', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:57:56', '2017-06-06 06:57:56');
INSERT INTO `admin_operation_log` VALUES ('174', '1', 'admin/auth/roles', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:57:59', '2017-06-06 06:57:59');
INSERT INTO `admin_operation_log` VALUES ('175', '1', 'admin/auth/permissions', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:58:01', '2017-06-06 06:58:01');
INSERT INTO `admin_operation_log` VALUES ('176', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:58:04', '2017-06-06 06:58:04');
INSERT INTO `admin_operation_log` VALUES ('177', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '[]', '2017-06-06 06:59:08', '2017-06-06 06:59:08');
INSERT INTO `admin_operation_log` VALUES ('178', '1', 'admin/auth/menu/12/edit', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:59:19', '2017-06-06 06:59:19');
INSERT INTO `admin_operation_log` VALUES ('179', '1', 'admin/auth/menu/12', 'PUT', '41.42.201.12', '{\"parent_id\":\"0\",\"title\":\"Doctors\",\"icon\":\"fa-bars\",\"uri\":\"doctor\",\"roles\":[null],\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-06-06 06:59:37', '2017-06-06 06:59:37');
INSERT INTO `admin_operation_log` VALUES ('180', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '[]', '2017-06-06 06:59:37', '2017-06-06 06:59:37');
INSERT INTO `admin_operation_log` VALUES ('181', '1', 'admin/auth/menu', 'POST', '41.42.201.12', '{\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":12},{\\\"id\\\":13}]\"}', '2017-06-06 06:59:43', '2017-06-06 06:59:43');
INSERT INTO `admin_operation_log` VALUES ('182', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:59:44', '2017-06-06 06:59:44');
INSERT INTO `admin_operation_log` VALUES ('183', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:59:46', '2017-06-06 06:59:46');
INSERT INTO `admin_operation_log` VALUES ('184', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:59:49', '2017-06-06 06:59:49');
INSERT INTO `admin_operation_log` VALUES ('185', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 06:59:51', '2017-06-06 06:59:51');
INSERT INTO `admin_operation_log` VALUES ('186', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '[]', '2017-06-06 06:59:54', '2017-06-06 06:59:54');
INSERT INTO `admin_operation_log` VALUES ('187', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:02:31', '2017-06-06 07:02:31');
INSERT INTO `admin_operation_log` VALUES ('188', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:02:40', '2017-06-06 07:02:40');
INSERT INTO `admin_operation_log` VALUES ('189', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:03:17', '2017-06-06 07:03:17');
INSERT INTO `admin_operation_log` VALUES ('190', '1', 'admin', 'GET', '156.213.8.173', '[]', '2017-06-06 07:03:18', '2017-06-06 07:03:18');
INSERT INTO `admin_operation_log` VALUES ('191', '1', 'admin/auth/users', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:03:54', '2017-06-06 07:03:54');
INSERT INTO `admin_operation_log` VALUES ('192', '1', 'admin/auth/roles', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:03:57', '2017-06-06 07:03:57');
INSERT INTO `admin_operation_log` VALUES ('193', '1', 'admin', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:04:03', '2017-06-06 07:04:03');
INSERT INTO `admin_operation_log` VALUES ('194', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:04:07', '2017-06-06 07:04:07');
INSERT INTO `admin_operation_log` VALUES ('195', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:04:12', '2017-06-06 07:04:12');
INSERT INTO `admin_operation_log` VALUES ('196', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:12:11', '2017-06-06 07:12:11');
INSERT INTO `admin_operation_log` VALUES ('197', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:12:16', '2017-06-06 07:12:16');
INSERT INTO `admin_operation_log` VALUES ('198', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:12:22', '2017-06-06 07:12:22');
INSERT INTO `admin_operation_log` VALUES ('199', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '[]', '2017-06-06 07:14:30', '2017-06-06 07:14:30');
INSERT INTO `admin_operation_log` VALUES ('200', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:14:41', '2017-06-06 07:14:41');
INSERT INTO `admin_operation_log` VALUES ('201', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:15:35', '2017-06-06 07:15:35');
INSERT INTO `admin_operation_log` VALUES ('202', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:16:19', '2017-06-06 07:16:19');
INSERT INTO `admin_operation_log` VALUES ('203', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:17:00', '2017-06-06 07:17:00');
INSERT INTO `admin_operation_log` VALUES ('204', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:17:06', '2017-06-06 07:17:06');
INSERT INTO `admin_operation_log` VALUES ('205', '1', 'admin/doctor', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:17:40', '2017-06-06 07:17:40');
INSERT INTO `admin_operation_log` VALUES ('206', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:17:40', '2017-06-06 07:17:40');
INSERT INTO `admin_operation_log` VALUES ('207', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:17:57', '2017-06-06 07:17:57');
INSERT INTO `admin_operation_log` VALUES ('208', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:19:17', '2017-06-06 07:19:17');
INSERT INTO `admin_operation_log` VALUES ('209', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:34:27', '2017-06-06 07:34:27');
INSERT INTO `admin_operation_log` VALUES ('210', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:36:02', '2017-06-06 07:36:02');
INSERT INTO `admin_operation_log` VALUES ('211', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '[]', '2017-06-06 07:36:07', '2017-06-06 07:36:07');
INSERT INTO `admin_operation_log` VALUES ('212', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:36:15', '2017-06-06 07:36:15');
INSERT INTO `admin_operation_log` VALUES ('213', '1', 'admin/doctor', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:36:36', '2017-06-06 07:36:36');
INSERT INTO `admin_operation_log` VALUES ('214', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:36:36', '2017-06-06 07:36:36');
INSERT INTO `admin_operation_log` VALUES ('215', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:37:29', '2017-06-06 07:37:29');
INSERT INTO `admin_operation_log` VALUES ('216', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:38:15', '2017-06-06 07:38:15');
INSERT INTO `admin_operation_log` VALUES ('217', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:38:39', '2017-06-06 07:38:39');
INSERT INTO `admin_operation_log` VALUES ('218', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:39:43', '2017-06-06 07:39:43');
INSERT INTO `admin_operation_log` VALUES ('219', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:41:39', '2017-06-06 07:41:39');
INSERT INTO `admin_operation_log` VALUES ('220', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:49:52', '2017-06-06 07:49:52');
INSERT INTO `admin_operation_log` VALUES ('221', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:50:22', '2017-06-06 07:50:22');
INSERT INTO `admin_operation_log` VALUES ('222', '1', 'admin', 'GET', '41.42.201.12', '[]', '2017-06-06 07:50:44', '2017-06-06 07:50:44');
INSERT INTO `admin_operation_log` VALUES ('223', '1', 'admin/auth/logs', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:50:55', '2017-06-06 07:50:55');
INSERT INTO `admin_operation_log` VALUES ('224', '1', 'admin/auth/logs', 'GET', '41.42.201.12', '[]', '2017-06-06 07:50:55', '2017-06-06 07:50:55');
INSERT INTO `admin_operation_log` VALUES ('225', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:06', '2017-06-06 07:51:06');
INSERT INTO `admin_operation_log` VALUES ('226', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '[]', '2017-06-06 07:51:07', '2017-06-06 07:51:07');
INSERT INTO `admin_operation_log` VALUES ('227', '1', 'admin/auth/users', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:16', '2017-06-06 07:51:16');
INSERT INTO `admin_operation_log` VALUES ('228', '1', 'admin/auth/roles', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:19', '2017-06-06 07:51:19');
INSERT INTO `admin_operation_log` VALUES ('229', '1', 'admin/auth/permissions', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:22', '2017-06-06 07:51:22');
INSERT INTO `admin_operation_log` VALUES ('230', '1', 'admin/auth/menu', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:24', '2017-06-06 07:51:24');
INSERT INTO `admin_operation_log` VALUES ('231', '1', 'admin/helpers/terminal/database', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:30', '2017-06-06 07:51:30');
INSERT INTO `admin_operation_log` VALUES ('232', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 07:51:34', '2017-06-06 07:51:34');
INSERT INTO `admin_operation_log` VALUES ('233', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:51:39', '2017-06-06 07:51:39');
INSERT INTO `admin_operation_log` VALUES ('234', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:table\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:51:46', '2017-06-06 07:51:46');
INSERT INTO `admin_operation_log` VALUES ('235', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:table\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:51:50', '2017-06-06 07:51:50');
INSERT INTO `admin_operation_log` VALUES ('236', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:table\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:51:52', '2017-06-06 07:51:52');
INSERT INTO `admin_operation_log` VALUES ('237', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:52:00', '2017-06-06 07:52:00');
INSERT INTO `admin_operation_log` VALUES ('238', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 07:52:08', '2017-06-06 07:52:08');
INSERT INTO `admin_operation_log` VALUES ('239', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 07:52:48', '2017-06-06 07:52:48');
INSERT INTO `admin_operation_log` VALUES ('240', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:35:32', '2017-06-06 08:35:32');
INSERT INTO `admin_operation_log` VALUES ('241', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '[]', '2017-06-06 08:36:13', '2017-06-06 08:36:13');
INSERT INTO `admin_operation_log` VALUES ('242', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:46:07', '2017-06-06 08:46:07');
INSERT INTO `admin_operation_log` VALUES ('243', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:46:28', '2017-06-06 08:46:28');
INSERT INTO `admin_operation_log` VALUES ('244', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:47:21', '2017-06-06 08:47:21');
INSERT INTO `admin_operation_log` VALUES ('245', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:48:20', '2017-06-06 08:48:20');
INSERT INTO `admin_operation_log` VALUES ('246', '1', 'admin/auth/users', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 08:48:48', '2017-06-06 08:48:48');
INSERT INTO `admin_operation_log` VALUES ('247', '1', 'admin/helpers/terminal/artisan', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 08:48:58', '2017-06-06 08:48:58');
INSERT INTO `admin_operation_log` VALUES ('248', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:49:04', '2017-06-06 08:49:04');
INSERT INTO `admin_operation_log` VALUES ('249', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:50:26', '2017-06-06 08:50:26');
INSERT INTO `admin_operation_log` VALUES ('250', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:50:28', '2017-06-06 08:50:28');
INSERT INTO `admin_operation_log` VALUES ('251', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:51:10', '2017-06-06 08:51:10');
INSERT INTO `admin_operation_log` VALUES ('252', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"cache:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:20', '2017-06-06 08:51:20');
INSERT INTO `admin_operation_log` VALUES ('253', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:51:25', '2017-06-06 08:51:25');
INSERT INTO `admin_operation_log` VALUES ('254', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:clear\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:31', '2017-06-06 08:51:31');
INSERT INTO `admin_operation_log` VALUES ('255', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:37', '2017-06-06 08:51:37');
INSERT INTO `admin_operation_log` VALUES ('256', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:39', '2017-06-06 08:51:39');
INSERT INTO `admin_operation_log` VALUES ('257', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:40', '2017-06-06 08:51:40');
INSERT INTO `admin_operation_log` VALUES ('258', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:41', '2017-06-06 08:51:41');
INSERT INTO `admin_operation_log` VALUES ('259', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:42', '2017-06-06 08:51:42');
INSERT INTO `admin_operation_log` VALUES ('260', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:cache\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:42', '2017-06-06 08:51:42');
INSERT INTO `admin_operation_log` VALUES ('261', '1', 'admin/helpers/terminal/artisan', 'POST', '41.42.201.12', '{\"c\":\"route:list\",\"_token\":\"NUBEFLEVhcdn8GrYchbRKXMkYsSiEWi5GQnm4rG3\"}', '2017-06-06 08:51:45', '2017-06-06 08:51:45');
INSERT INTO `admin_operation_log` VALUES ('262', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 08:55:53', '2017-06-06 08:55:53');
INSERT INTO `admin_operation_log` VALUES ('263', '1', 'admin/helpers/terminal/database', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 08:56:22', '2017-06-06 08:56:22');
INSERT INTO `admin_operation_log` VALUES ('264', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 08:56:23', '2017-06-06 08:56:23');
INSERT INTO `admin_operation_log` VALUES ('265', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '[]', '2017-06-06 08:56:23', '2017-06-06 08:56:23');
INSERT INTO `admin_operation_log` VALUES ('266', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '[]', '2017-06-06 08:56:27', '2017-06-06 08:56:27');
INSERT INTO `admin_operation_log` VALUES ('267', '1', 'admin/helpers/terminal/database', 'GET', '41.42.201.12', '[]', '2017-06-06 08:56:30', '2017-06-06 08:56:30');
INSERT INTO `admin_operation_log` VALUES ('268', '1', 'admin/doctor', 'GET', '41.42.201.12', '[]', '2017-06-06 09:00:51', '2017-06-06 09:00:51');
INSERT INTO `admin_operation_log` VALUES ('269', '1', 'admin/helpers/scaffold', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 09:01:17', '2017-06-06 09:01:17');
INSERT INTO `admin_operation_log` VALUES ('270', '1', 'admin/doctor', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 09:03:40', '2017-06-06 09:03:40');
INSERT INTO `admin_operation_log` VALUES ('271', '1', 'admin/doctor/11/edit', 'GET', '41.42.201.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 09:03:46', '2017-06-06 09:03:46');
INSERT INTO `admin_operation_log` VALUES ('272', '1', 'admin', 'GET', '156.213.8.173', '[]', '2017-06-06 16:11:28', '2017-06-06 16:11:28');
INSERT INTO `admin_operation_log` VALUES ('273', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:11:40', '2017-06-06 16:11:40');
INSERT INTO `admin_operation_log` VALUES ('274', '1', 'admin/doctor/11/edit', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:11:55', '2017-06-06 16:11:55');
INSERT INTO `admin_operation_log` VALUES ('275', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:12:18', '2017-06-06 16:12:18');
INSERT INTO `admin_operation_log` VALUES ('276', '1', 'admin/doctor/create', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:12:46', '2017-06-06 16:12:46');
INSERT INTO `admin_operation_log` VALUES ('277', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:12:54', '2017-06-06 16:12:54');
INSERT INTO `admin_operation_log` VALUES ('278', '1', 'admin/auth/users', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:12:57', '2017-06-06 16:12:57');
INSERT INTO `admin_operation_log` VALUES ('279', '1', 'admin/auth/roles', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:00', '2017-06-06 16:13:00');
INSERT INTO `admin_operation_log` VALUES ('280', '1', 'admin/auth/roles/create', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:05', '2017-06-06 16:13:05');
INSERT INTO `admin_operation_log` VALUES ('281', '1', 'admin/auth/roles', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:17', '2017-06-06 16:13:17');
INSERT INTO `admin_operation_log` VALUES ('282', '1', 'admin/auth/permissions', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:25', '2017-06-06 16:13:25');
INSERT INTO `admin_operation_log` VALUES ('283', '1', 'admin/auth/permissions/1/edit', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:31', '2017-06-06 16:13:31');
INSERT INTO `admin_operation_log` VALUES ('284', '1', 'admin/auth/permissions', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:35', '2017-06-06 16:13:35');
INSERT INTO `admin_operation_log` VALUES ('285', '1', 'admin/auth/menu', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:38', '2017-06-06 16:13:38');
INSERT INTO `admin_operation_log` VALUES ('286', '1', 'admin/auth/logs', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:43', '2017-06-06 16:13:43');
INSERT INTO `admin_operation_log` VALUES ('287', '1', 'admin/helpers/scaffold', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:13:59', '2017-06-06 16:13:59');
INSERT INTO `admin_operation_log` VALUES ('288', '1', 'admin/helpers/terminal/database', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:02', '2017-06-06 16:14:02');
INSERT INTO `admin_operation_log` VALUES ('289', '1', 'admin/helpers/scaffold', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:05', '2017-06-06 16:14:05');
INSERT INTO `admin_operation_log` VALUES ('290', '1', 'admin/helpers/terminal/database', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:09', '2017-06-06 16:14:09');
INSERT INTO `admin_operation_log` VALUES ('291', '1', 'admin/helpers/terminal/artisan', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:16', '2017-06-06 16:14:16');
INSERT INTO `admin_operation_log` VALUES ('292', '1', 'admin/helpers/terminal/artisan', 'GET', '156.213.8.173', '[]', '2017-06-06 16:14:25', '2017-06-06 16:14:25');
INSERT INTO `admin_operation_log` VALUES ('293', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:27', '2017-06-06 16:14:27');
INSERT INTO `admin_operation_log` VALUES ('294', '1', 'admin', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:14:32', '2017-06-06 16:14:32');
INSERT INTO `admin_operation_log` VALUES ('295', '1', 'admin', 'GET', '156.213.8.173', '[]', '2017-06-06 16:14:41', '2017-06-06 16:14:41');
INSERT INTO `admin_operation_log` VALUES ('296', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:04', '2017-06-06 16:15:04');
INSERT INTO `admin_operation_log` VALUES ('297', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:17', '2017-06-06 16:15:17');
INSERT INTO `admin_operation_log` VALUES ('298', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:20', '2017-06-06 16:15:20');
INSERT INTO `admin_operation_log` VALUES ('299', '1', 'admin/doctor', 'GET', '156.213.8.173', '[]', '2017-06-06 16:15:24', '2017-06-06 16:15:24');
INSERT INTO `admin_operation_log` VALUES ('300', '1', 'admin/helpers/scaffold', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:27', '2017-06-06 16:15:27');
INSERT INTO `admin_operation_log` VALUES ('301', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:29', '2017-06-06 16:15:29');
INSERT INTO `admin_operation_log` VALUES ('302', '1', 'admin/doctor/2/edit', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:15:37', '2017-06-06 16:15:37');
INSERT INTO `admin_operation_log` VALUES ('303', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:16:01', '2017-06-06 16:16:01');
INSERT INTO `admin_operation_log` VALUES ('304', '1', 'admin/doctor/2/edit', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:16:04', '2017-06-06 16:16:04');
INSERT INTO `admin_operation_log` VALUES ('305', '1', 'admin/doctor', 'GET', '156.213.8.173', '{\"_pjax\":\"#pjax-container\"}', '2017-06-06 16:16:09', '2017-06-06 16:16:09');
INSERT INTO `admin_operation_log` VALUES ('306', '1', 'admin/doctor', 'GET', '156.213.8.173', '[]', '2017-06-06 16:26:14', '2017-06-06 16:26:14');
INSERT INTO `admin_operation_log` VALUES ('307', '1', 'admin', 'GET', '66.249.93.192', '[]', '2017-06-12 02:43:20', '2017-06-12 02:43:20');
INSERT INTO `admin_operation_log` VALUES ('308', '1', 'admin/doctor', 'GET', '66.249.93.192', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:43:46', '2017-06-12 02:43:46');
INSERT INTO `admin_operation_log` VALUES ('309', '1', 'admin/doctor', 'GET', '66.249.93.192', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:43:48', '2017-06-12 02:43:48');
INSERT INTO `admin_operation_log` VALUES ('310', '1', 'admin/doctor', 'GET', '66.249.93.223', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:43:53', '2017-06-12 02:43:53');
INSERT INTO `admin_operation_log` VALUES ('311', '1', 'admin/doctor', 'GET', '66.249.93.192', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:43:56', '2017-06-12 02:43:56');
INSERT INTO `admin_operation_log` VALUES ('312', '1', 'admin/doctor', 'GET', '66.249.93.223', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:44:10', '2017-06-12 02:44:10');
INSERT INTO `admin_operation_log` VALUES ('313', '1', 'admin/doctor/2/edit', 'GET', '66.249.93.223', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:44:49', '2017-06-12 02:44:49');
INSERT INTO `admin_operation_log` VALUES ('314', '1', 'admin/doctor', 'GET', '66.249.93.192', '{\"_pjax\":\"#pjax-container\"}', '2017-06-12 02:45:13', '2017-06-12 02:45:13');
INSERT INTO `admin_operation_log` VALUES ('315', '1', 'admin', 'GET', '156.214.139.63', '[]', '2017-06-13 18:55:35', '2017-06-13 18:55:35');
INSERT INTO `admin_operation_log` VALUES ('316', '1', 'admin/doctor', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:56:02', '2017-06-13 18:56:02');
INSERT INTO `admin_operation_log` VALUES ('317', '1', 'admin/doctor/2/edit', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:57:35', '2017-06-13 18:57:35');
INSERT INTO `admin_operation_log` VALUES ('318', '1', 'admin/doctor', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:57:58', '2017-06-13 18:57:58');
INSERT INTO `admin_operation_log` VALUES ('319', '1', 'admin/doctor/2/edit', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:58:01', '2017-06-13 18:58:01');
INSERT INTO `admin_operation_log` VALUES ('320', '1', 'admin/doctor', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:58:28', '2017-06-13 18:58:28');
INSERT INTO `admin_operation_log` VALUES ('321', '1', 'admin/doctor/2/edit', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:58:32', '2017-06-13 18:58:32');
INSERT INTO `admin_operation_log` VALUES ('322', '1', 'admin/doctor', 'GET', '156.214.139.63', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 18:58:43', '2017-06-13 18:58:43');
INSERT INTO `admin_operation_log` VALUES ('323', '1', 'admin', 'GET', '197.246.9.12', '[]', '2017-06-13 19:04:28', '2017-06-13 19:04:28');
INSERT INTO `admin_operation_log` VALUES ('324', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:05:09', '2017-06-13 19:05:09');
INSERT INTO `admin_operation_log` VALUES ('325', '1', 'admin', 'GET', '41.42.94.153', '[]', '2017-06-13 19:48:24', '2017-06-13 19:48:24');
INSERT INTO `admin_operation_log` VALUES ('326', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:48:31', '2017-06-13 19:48:31');
INSERT INTO `admin_operation_log` VALUES ('327', '1', 'admin/auth/menu', 'POST', '41.42.94.153', '{\"parent_id\":\"2\",\"title\":\"Doctors\",\"icon\":\"fa-adn\",\"uri\":\"auth\\/doctor\",\"roles\":[null],\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\"}', '2017-06-13 19:48:51', '2017-06-13 19:48:51');
INSERT INTO `admin_operation_log` VALUES ('328', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '[]', '2017-06-13 19:48:52', '2017-06-13 19:48:52');
INSERT INTO `admin_operation_log` VALUES ('329', '1', 'admin/auth/menu', 'POST', '41.42.94.153', '{\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7},{\\\"id\\\":14}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":12},{\\\"id\\\":13}]\"}', '2017-06-13 19:48:55', '2017-06-13 19:48:55');
INSERT INTO `admin_operation_log` VALUES ('330', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:48:55', '2017-06-13 19:48:55');
INSERT INTO `admin_operation_log` VALUES ('331', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:48:56', '2017-06-13 19:48:56');
INSERT INTO `admin_operation_log` VALUES ('332', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:48:58', '2017-06-13 19:48:58');
INSERT INTO `admin_operation_log` VALUES ('333', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '[]', '2017-06-13 19:49:00', '2017-06-13 19:49:00');
INSERT INTO `admin_operation_log` VALUES ('334', '1', 'admin/auth/doctor', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:06', '2017-06-13 19:49:06');
INSERT INTO `admin_operation_log` VALUES ('335', '1', 'admin/auth/doctor/2', 'DELETE', '41.42.94.153', '{\"_method\":\"delete\",\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\"}', '2017-06-13 19:49:11', '2017-06-13 19:49:11');
INSERT INTO `admin_operation_log` VALUES ('336', '1', 'admin/auth/doctor', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:12', '2017-06-13 19:49:12');
INSERT INTO `admin_operation_log` VALUES ('337', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:17', '2017-06-13 19:49:17');
INSERT INTO `admin_operation_log` VALUES ('338', '1', 'admin/auth/menu/12', 'DELETE', '41.42.94.153', '{\"_method\":\"delete\",\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\"}', '2017-06-13 19:49:23', '2017-06-13 19:49:23');
INSERT INTO `admin_operation_log` VALUES ('339', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:23', '2017-06-13 19:49:23');
INSERT INTO `admin_operation_log` VALUES ('340', '1', 'admin/auth/menu/13', 'DELETE', '41.42.94.153', '{\"_method\":\"delete\",\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\"}', '2017-06-13 19:49:27', '2017-06-13 19:49:27');
INSERT INTO `admin_operation_log` VALUES ('341', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:28', '2017-06-13 19:49:28');
INSERT INTO `admin_operation_log` VALUES ('342', '1', 'admin/auth/menu', 'POST', '41.42.94.153', '{\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7},{\\\"id\\\":14}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]}]\"}', '2017-06-13 19:49:31', '2017-06-13 19:49:31');
INSERT INTO `admin_operation_log` VALUES ('343', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:31', '2017-06-13 19:49:31');
INSERT INTO `admin_operation_log` VALUES ('344', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:33', '2017-06-13 19:49:33');
INSERT INTO `admin_operation_log` VALUES ('345', '1', 'admin/auth/menu', 'GET', '41.42.94.153', '[]', '2017-06-13 19:49:35', '2017-06-13 19:49:35');
INSERT INTO `admin_operation_log` VALUES ('346', '1', 'admin/auth/doctor', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:45', '2017-06-13 19:49:45');
INSERT INTO `admin_operation_log` VALUES ('347', '1', 'admin/auth/doctor/11/edit', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:49:51', '2017-06-13 19:49:51');
INSERT INTO `admin_operation_log` VALUES ('348', '1', 'admin/auth/doctor/11', 'PUT', '41.42.94.153', '{\"name\":\"asmaa123\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-06-13 19:49:58', '2017-06-13 19:49:58');
INSERT INTO `admin_operation_log` VALUES ('349', '1', 'admin/auth/doctor', 'GET', '41.42.94.153', '[]', '2017-06-13 19:49:58', '2017-06-13 19:49:58');
INSERT INTO `admin_operation_log` VALUES ('350', '1', 'admin/auth/doctor/11/edit', 'GET', '41.42.94.153', '{\"_pjax\":\"#pjax-container\"}', '2017-06-13 19:50:23', '2017-06-13 19:50:23');
INSERT INTO `admin_operation_log` VALUES ('351', '1', 'admin/auth/doctor/11', 'PUT', '41.42.94.153', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"TyNpRQ2U0Edjqz9sa9J8BmGORDQAZ2PTieTXgrc3\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-06-13 19:50:29', '2017-06-13 19:50:29');
INSERT INTO `admin_operation_log` VALUES ('352', '1', 'admin/auth/doctor', 'GET', '41.42.94.153', '[]', '2017-06-13 19:50:30', '2017-06-13 19:50:30');
INSERT INTO `admin_operation_log` VALUES ('353', '1', 'admin', 'GET', '196.138.123.11', '[]', '2017-06-14 16:57:06', '2017-06-14 16:57:06');
INSERT INTO `admin_operation_log` VALUES ('354', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '{\"_pjax\":\"#pjax-container\"}', '2017-06-14 16:57:20', '2017-06-14 16:57:20');
INSERT INTO `admin_operation_log` VALUES ('355', '1', 'admin/auth/doctor/13', 'DELETE', '196.138.123.11', '{\"_method\":\"delete\",\"_token\":\"OA3Tw2OpHhz3S8s9ooIL36xVKeW2Gn20gJ838WeT\"}', '2017-06-14 16:57:39', '2017-06-14 16:57:39');
INSERT INTO `admin_operation_log` VALUES ('356', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '{\"_pjax\":\"#pjax-container\"}', '2017-06-14 16:57:40', '2017-06-14 16:57:40');
INSERT INTO `admin_operation_log` VALUES ('357', '1', 'admin/auth/doctor/11/edit', 'GET', '196.138.123.11', '{\"_pjax\":\"#pjax-container\"}', '2017-06-14 16:57:47', '2017-06-14 16:57:47');
INSERT INTO `admin_operation_log` VALUES ('358', '1', 'admin/auth/doctor/11', 'PUT', '196.138.123.11', '{\"name\":\"asmaa12\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"OA3Tw2OpHhz3S8s9ooIL36xVKeW2Gn20gJ838WeT\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-06-14 16:57:55', '2017-06-14 16:57:55');
INSERT INTO `admin_operation_log` VALUES ('359', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '[]', '2017-06-14 16:57:56', '2017-06-14 16:57:56');
INSERT INTO `admin_operation_log` VALUES ('360', '1', 'admin/auth/doctor/11/edit', 'GET', '196.138.123.11', '{\"_pjax\":\"#pjax-container\"}', '2017-06-14 16:58:03', '2017-06-14 16:58:03');
INSERT INTO `admin_operation_log` VALUES ('361', '1', 'admin/auth/doctor/11/edit', 'GET', '196.138.123.11', '{\"_pjax\":\"#pjax-container\"}', '2017-06-14 16:58:03', '2017-06-14 16:58:03');
INSERT INTO `admin_operation_log` VALUES ('362', '1', 'admin/auth/doctor/11', 'PUT', '196.138.123.11', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"OA3Tw2OpHhz3S8s9ooIL36xVKeW2Gn20gJ838WeT\",\"_method\":\"PUT\"}', '2017-06-14 16:58:44', '2017-06-14 16:58:44');
INSERT INTO `admin_operation_log` VALUES ('363', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '[]', '2017-06-14 16:58:44', '2017-06-14 16:58:44');
INSERT INTO `admin_operation_log` VALUES ('364', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '{\"_export_\":\"1\"}', '2017-06-14 16:58:55', '2017-06-14 16:58:55');
INSERT INTO `admin_operation_log` VALUES ('365', '1', 'admin/auth/doctor', 'GET', '196.138.123.11', '{\"_export_\":\"1\"}', '2017-06-14 17:00:12', '2017-06-14 17:00:12');
INSERT INTO `admin_operation_log` VALUES ('366', '1', 'admin', 'GET', '196.219.61.97', '[]', '2017-06-22 18:42:30', '2017-06-22 18:42:30');
INSERT INTO `admin_operation_log` VALUES ('367', '1', 'admin/auth/users', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:42:53', '2017-06-22 18:42:53');
INSERT INTO `admin_operation_log` VALUES ('368', '1', 'admin', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:42:58', '2017-06-22 18:42:58');
INSERT INTO `admin_operation_log` VALUES ('369', '1', 'admin/auth/users', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:05', '2017-06-22 18:43:05');
INSERT INTO `admin_operation_log` VALUES ('370', '1', 'admin/auth/roles', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:11', '2017-06-22 18:43:11');
INSERT INTO `admin_operation_log` VALUES ('371', '1', 'admin/auth/permissions', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:12', '2017-06-22 18:43:12');
INSERT INTO `admin_operation_log` VALUES ('372', '1', 'admin/auth/menu', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:14', '2017-06-22 18:43:14');
INSERT INTO `admin_operation_log` VALUES ('373', '1', 'admin/auth/logs', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:20', '2017-06-22 18:43:20');
INSERT INTO `admin_operation_log` VALUES ('374', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:24', '2017-06-22 18:43:24');
INSERT INTO `admin_operation_log` VALUES ('375', '1', 'admin/auth/doctor/14/edit', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:35', '2017-06-22 18:43:35');
INSERT INTO `admin_operation_log` VALUES ('376', '1', 'admin/auth/doctor/14', 'PUT', '196.219.61.97', '{\"name\":\"chg1213\",\"email\":\"fnv@nm.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2580\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"W2O1eWnUlt35czIvoF1guJSDUN8SVnKojDfxXVdC\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-06-22 18:43:42', '2017-06-22 18:43:42');
INSERT INTO `admin_operation_log` VALUES ('377', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '[]', '2017-06-22 18:43:43', '2017-06-22 18:43:43');
INSERT INTO `admin_operation_log` VALUES ('378', '1', 'admin/auth/doctor/14/edit', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:43:48', '2017-06-22 18:43:48');
INSERT INTO `admin_operation_log` VALUES ('379', '1', 'admin/auth/doctor/14', 'PUT', '196.219.61.97', '{\"name\":\"chg1213\",\"email\":\"fnv@nm.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2580\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"0\",\"doctor_speciality_id\":\"0\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"W2O1eWnUlt35czIvoF1guJSDUN8SVnKojDfxXVdC\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-06-22 18:44:06', '2017-06-22 18:44:06');
INSERT INTO `admin_operation_log` VALUES ('380', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '[]', '2017-06-22 18:44:07', '2017-06-22 18:44:07');
INSERT INTO `admin_operation_log` VALUES ('381', '1', 'admin/auth/doctor/15', 'DELETE', '196.219.61.97', '{\"_method\":\"delete\",\"_token\":\"W2O1eWnUlt35czIvoF1guJSDUN8SVnKojDfxXVdC\"}', '2017-06-22 18:44:13', '2017-06-22 18:44:13');
INSERT INTO `admin_operation_log` VALUES ('382', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:13', '2017-06-22 18:44:13');
INSERT INTO `admin_operation_log` VALUES ('383', '1', 'admin/auth/doctor/32/edit', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:20', '2017-06-22 18:44:20');
INSERT INTO `admin_operation_log` VALUES ('384', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:39', '2017-06-22 18:44:39');
INSERT INTO `admin_operation_log` VALUES ('385', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:43', '2017-06-22 18:44:43');
INSERT INTO `admin_operation_log` VALUES ('386', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:43', '2017-06-22 18:44:43');
INSERT INTO `admin_operation_log` VALUES ('387', '1', 'admin/helpers/scaffold', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:44:54', '2017-06-22 18:44:54');
INSERT INTO `admin_operation_log` VALUES ('388', '1', 'admin/helpers/terminal/database', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:45:01', '2017-06-22 18:45:01');
INSERT INTO `admin_operation_log` VALUES ('389', '1', 'admin/helpers/terminal/database', 'GET', '196.219.61.97', '[]', '2017-06-22 18:45:06', '2017-06-22 18:45:06');
INSERT INTO `admin_operation_log` VALUES ('390', '1', 'admin/helpers/terminal/database', 'POST', '196.219.61.97', '{\"c\":\"db:mysql\",\"q\":\"select * from\",\"_token\":\"W2O1eWnUlt35czIvoF1guJSDUN8SVnKojDfxXVdC\"}', '2017-06-22 18:45:19', '2017-06-22 18:45:19');
INSERT INTO `admin_operation_log` VALUES ('391', '1', 'admin/helpers/terminal/artisan', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:45:32', '2017-06-22 18:45:32');
INSERT INTO `admin_operation_log` VALUES ('392', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\"}', '2017-06-22 18:45:47', '2017-06-22 18:45:47');
INSERT INTO `admin_operation_log` VALUES ('393', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\",\"page\":\"8\"}', '2017-06-22 18:45:51', '2017-06-22 18:45:51');
INSERT INTO `admin_operation_log` VALUES ('394', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\",\"page\":\"5\"}', '2017-06-22 18:45:59', '2017-06-22 18:45:59');
INSERT INTO `admin_operation_log` VALUES ('395', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"_pjax\":\"#pjax-container\",\"page\":\"3\"}', '2017-06-22 18:46:03', '2017-06-22 18:46:03');
INSERT INTO `admin_operation_log` VALUES ('396', '1', 'admin/auth/doctor', 'GET', '196.219.61.97', '{\"page\":\"3\"}', '2017-06-22 18:46:08', '2017-06-22 18:46:08');
INSERT INTO `admin_operation_log` VALUES ('397', '1', 'admin', 'GET', '66.249.93.60', '[]', '2017-06-25 21:16:03', '2017-06-25 21:16:03');
INSERT INTO `admin_operation_log` VALUES ('398', '1', 'admin/auth/users', 'GET', '66.249.93.60', '{\"_pjax\":\"#pjax-container\"}', '2017-06-25 21:17:54', '2017-06-25 21:17:54');
INSERT INTO `admin_operation_log` VALUES ('399', '1', 'admin/auth/doctor', 'GET', '66.249.93.33', '{\"_pjax\":\"#pjax-container\"}', '2017-06-25 21:17:56', '2017-06-25 21:17:56');
INSERT INTO `admin_operation_log` VALUES ('400', '1', 'admin/auth/doctor/32/edit', 'GET', '66.249.93.33', '{\"_pjax\":\"#pjax-container\"}', '2017-06-25 21:18:31', '2017-06-25 21:18:31');
INSERT INTO `admin_operation_log` VALUES ('401', '1', 'admin/auth/users', 'GET', '66.249.93.60', '{\"_pjax\":\"#pjax-container\"}', '2017-06-25 21:18:33', '2017-06-25 21:18:33');
INSERT INTO `admin_operation_log` VALUES ('402', '1', 'admin/auth/doctor/32/edit', 'GET', '66.249.93.60', '[]', '2017-06-25 22:04:04', '2017-06-25 22:04:04');
INSERT INTO `admin_operation_log` VALUES ('403', '1', 'admin', 'GET', '41.236.195.224', '[]', '2017-06-28 00:59:39', '2017-06-28 00:59:39');
INSERT INTO `admin_operation_log` VALUES ('404', '1', 'admin/auth/users', 'GET', '41.236.195.224', '{\"_pjax\":\"#pjax-container\"}', '2017-06-28 00:59:46', '2017-06-28 00:59:46');
INSERT INTO `admin_operation_log` VALUES ('405', '1', 'admin/auth/doctor', 'GET', '41.236.195.224', '{\"_pjax\":\"#pjax-container\"}', '2017-06-28 00:59:51', '2017-06-28 00:59:51');
INSERT INTO `admin_operation_log` VALUES ('406', '1', 'admin/auth/doctor/11/edit', 'GET', '41.236.195.224', '{\"_pjax\":\"#pjax-container\"}', '2017-06-28 00:59:57', '2017-06-28 00:59:57');
INSERT INTO `admin_operation_log` VALUES ('407', '1', 'admin', 'GET', '196.154.141.97', '[]', '2017-07-04 21:36:07', '2017-07-04 21:36:07');
INSERT INTO `admin_operation_log` VALUES ('408', '1', 'admin/auth/logs', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:36:30', '2017-07-04 21:36:30');
INSERT INTO `admin_operation_log` VALUES ('409', '1', 'admin/auth/logs', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:36:31', '2017-07-04 21:36:31');
INSERT INTO `admin_operation_log` VALUES ('410', '1', 'admin/auth/logs', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:36:31', '2017-07-04 21:36:31');
INSERT INTO `admin_operation_log` VALUES ('411', '1', 'admin/auth/logs', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:36:32', '2017-07-04 21:36:32');
INSERT INTO `admin_operation_log` VALUES ('412', '1', 'admin/auth/doctor', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:38:13', '2017-07-04 21:38:13');
INSERT INTO `admin_operation_log` VALUES ('413', '1', 'admin/auth/doctor/11/edit', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:38:19', '2017-07-04 21:38:19');
INSERT INTO `admin_operation_log` VALUES ('414', '1', 'admin/auth/doctor/11/edit', 'GET', '196.154.141.97', '[]', '2017-07-04 21:38:24', '2017-07-04 21:38:24');
INSERT INTO `admin_operation_log` VALUES ('415', '1', 'admin/auth/doctor/11/edit', 'GET', '196.154.141.97', '{\"_pjax\":\"#pjax-container\"}', '2017-07-04 21:38:25', '2017-07-04 21:38:25');
INSERT INTO `admin_operation_log` VALUES ('416', '1', 'admin/auth/doctor', 'GET', '156.212.151.187', '[]', '2017-07-05 03:33:07', '2017-07-05 03:33:07');
INSERT INTO `admin_operation_log` VALUES ('417', '1', 'admin/auth/doctor/14/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:33:13', '2017-07-05 03:33:13');
INSERT INTO `admin_operation_log` VALUES ('418', '1', 'admin/auth/doctor/16/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:33:15', '2017-07-05 03:33:15');
INSERT INTO `admin_operation_log` VALUES ('419', '1', 'admin/auth/doctor/11/edit', 'GET', '156.212.151.187', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 03:50:08', '2017-07-05 03:50:08');
INSERT INTO `admin_operation_log` VALUES ('420', '1', 'admin/auth/doctor/11/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:50:09', '2017-07-05 03:50:09');
INSERT INTO `admin_operation_log` VALUES ('421', '1', 'admin/auth/doctor/20/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:50:21', '2017-07-05 03:50:21');
INSERT INTO `admin_operation_log` VALUES ('422', '1', 'admin/auth/doctor/19/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:50:22', '2017-07-05 03:50:22');
INSERT INTO `admin_operation_log` VALUES ('423', '1', 'admin/auth/doctor/19', 'PUT', '156.212.151.187', '{\"name\":\"bchf\",\"email\":\"gfg@hck.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2580\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"5\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"13mFTYZeY4Pz9dOvDLhjfi5a5TbmMeLxF9uNNFfb\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-05 03:50:37', '2017-07-05 03:50:37');
INSERT INTO `admin_operation_log` VALUES ('424', '1', 'admin/auth/doctor', 'GET', '156.212.151.187', '[]', '2017-07-05 03:50:37', '2017-07-05 03:50:37');
INSERT INTO `admin_operation_log` VALUES ('425', '1', 'admin/auth/doctor/19/edit', 'GET', '156.212.151.187', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 03:50:41', '2017-07-05 03:50:41');
INSERT INTO `admin_operation_log` VALUES ('426', '1', 'admin/auth/doctor/20/edit', 'GET', '156.212.151.187', '[]', '2017-07-05 03:55:52', '2017-07-05 03:55:52');
INSERT INTO `admin_operation_log` VALUES ('427', '1', 'admin/auth/doctor/20', 'PUT', '156.212.151.187', '{\"name\":\"fjjf\",\"email\":\"fhc@vhj.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"0852\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"2\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"13mFTYZeY4Pz9dOvDLhjfi5a5TbmMeLxF9uNNFfb\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-05 03:56:16', '2017-07-05 03:56:16');
INSERT INTO `admin_operation_log` VALUES ('428', '1', 'admin/auth/doctor', 'GET', '156.212.151.187', '[]', '2017-07-05 03:56:17', '2017-07-05 03:56:17');
INSERT INTO `admin_operation_log` VALUES ('429', '1', 'admin/auth/doctor/20/edit', 'GET', '156.212.151.187', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 03:56:21', '2017-07-05 03:56:21');
INSERT INTO `admin_operation_log` VALUES ('430', '1', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:00:40', '2017-07-05 18:00:40');
INSERT INTO `admin_operation_log` VALUES ('431', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:00:49', '2017-07-05 18:00:49');
INSERT INTO `admin_operation_log` VALUES ('432', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:01:23', '2017-07-05 18:01:23');
INSERT INTO `admin_operation_log` VALUES ('433', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:01:46', '2017-07-05 18:01:46');
INSERT INTO `admin_operation_log` VALUES ('434', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:03:11', '2017-07-05 18:03:11');
INSERT INTO `admin_operation_log` VALUES ('435', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:04:44', '2017-07-05 18:04:44');
INSERT INTO `admin_operation_log` VALUES ('436', '1', 'admin/auth/users/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:04:49', '2017-07-05 18:04:49');
INSERT INTO `admin_operation_log` VALUES ('437', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:05:56', '2017-07-05 18:05:56');
INSERT INTO `admin_operation_log` VALUES ('438', '1', 'admin/auth/roles/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:06:14', '2017-07-05 18:06:14');
INSERT INTO `admin_operation_log` VALUES ('439', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:06:32', '2017-07-05 18:06:32');
INSERT INTO `admin_operation_log` VALUES ('440', '1', 'admin/auth/permissions/1/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:07:20', '2017-07-05 18:07:20');
INSERT INTO `admin_operation_log` VALUES ('441', '1', 'admin/auth/permissions/1', 'PUT', '197.246.9.12', '{\"slug\":\"f\",\"name\":\"f\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/permissions\"}', '2017-07-05 18:07:24', '2017-07-05 18:07:24');
INSERT INTO `admin_operation_log` VALUES ('442', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:07:25', '2017-07-05 18:07:25');
INSERT INTO `admin_operation_log` VALUES ('443', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:07:28', '2017-07-05 18:07:28');
INSERT INTO `admin_operation_log` VALUES ('444', '1', 'admin/auth/roles/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:07:34', '2017-07-05 18:07:34');
INSERT INTO `admin_operation_log` VALUES ('445', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:07:44', '2017-07-05 18:07:44');
INSERT INTO `admin_operation_log` VALUES ('446', '1', 'admin/auth/permissions/1/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:07:51', '2017-07-05 18:07:51');
INSERT INTO `admin_operation_log` VALUES ('447', '1', 'admin/auth/permissions/1', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/permissions\"}', '2017-07-05 18:08:30', '2017-07-05 18:08:30');
INSERT INTO `admin_operation_log` VALUES ('448', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:08:30', '2017-07-05 18:08:30');
INSERT INTO `admin_operation_log` VALUES ('449', '1', 'admin/auth/permissions/1/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:08:50', '2017-07-05 18:08:50');
INSERT INTO `admin_operation_log` VALUES ('450', '1', 'admin/auth/permissions/1', 'PUT', '197.246.9.12', '{\"slug\":\"admin\",\"name\":\"admin\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/permissions\"}', '2017-07-05 18:09:03', '2017-07-05 18:09:03');
INSERT INTO `admin_operation_log` VALUES ('451', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:09:04', '2017-07-05 18:09:04');
INSERT INTO `admin_operation_log` VALUES ('452', '1', 'admin/auth/permissions/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:09:07', '2017-07-05 18:09:07');
INSERT INTO `admin_operation_log` VALUES ('453', '1', 'admin/auth/permissions', 'POST', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/permissions\"}', '2017-07-05 18:09:17', '2017-07-05 18:09:17');
INSERT INTO `admin_operation_log` VALUES ('454', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:09:18', '2017-07-05 18:09:18');
INSERT INTO `admin_operation_log` VALUES ('455', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:09:24', '2017-07-05 18:09:24');
INSERT INTO `admin_operation_log` VALUES ('456', '1', 'admin/auth/roles/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:09:29', '2017-07-05 18:09:29');
INSERT INTO `admin_operation_log` VALUES ('457', '1', 'admin/auth/roles', 'POST', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:09:42', '2017-07-05 18:09:42');
INSERT INTO `admin_operation_log` VALUES ('458', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:09:42', '2017-07-05 18:09:42');
INSERT INTO `admin_operation_log` VALUES ('459', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:09:45', '2017-07-05 18:09:45');
INSERT INTO `admin_operation_log` VALUES ('460', '1', 'admin/auth/users/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:09:48', '2017-07-05 18:09:48');
INSERT INTO `admin_operation_log` VALUES ('461', '1', 'admin/auth/users', 'POST', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"ahmeddoc123\",\"password_confirmation\":\"ahmeddoc123\",\"roles\":[\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:10:23', '2017-07-05 18:10:23');
INSERT INTO `admin_operation_log` VALUES ('462', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:10:23', '2017-07-05 18:10:23');
INSERT INTO `admin_operation_log` VALUES ('463', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:10:28', '2017-07-05 18:10:28');
INSERT INTO `admin_operation_log` VALUES ('464', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:11:14', '2017-07-05 18:11:14');
INSERT INTO `admin_operation_log` VALUES ('465', '1', 'admin/auth/menu/14', 'PUT', '197.246.9.12', '{\"parent_id\":\"2\",\"title\":\"Doctors\",\"icon\":\"fa-adn\",\"uri\":\"auth\\/doctor\",\"roles\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 18:11:25', '2017-07-05 18:11:25');
INSERT INTO `admin_operation_log` VALUES ('466', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 18:11:26', '2017-07-05 18:11:26');
INSERT INTO `admin_operation_log` VALUES ('467', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:11:36', '2017-07-05 18:11:36');
INSERT INTO `admin_operation_log` VALUES ('468', '1', 'admin/auth/logout', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:11:52', '2017-07-05 18:11:52');
INSERT INTO `admin_operation_log` VALUES ('469', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:12:06', '2017-07-05 18:12:06');
INSERT INTO `admin_operation_log` VALUES ('470', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:12:14', '2017-07-05 18:12:14');
INSERT INTO `admin_operation_log` VALUES ('471', '2', 'admin/auth/logout', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:12:21', '2017-07-05 18:12:21');
INSERT INTO `admin_operation_log` VALUES ('472', '1', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:13:02', '2017-07-05 18:13:02');
INSERT INTO `admin_operation_log` VALUES ('473', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:07', '2017-07-05 18:13:07');
INSERT INTO `admin_operation_log` VALUES ('474', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:12', '2017-07-05 18:13:12');
INSERT INTO `admin_operation_log` VALUES ('475', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:13', '2017-07-05 18:13:13');
INSERT INTO `admin_operation_log` VALUES ('476', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:18', '2017-07-05 18:13:18');
INSERT INTO `admin_operation_log` VALUES ('477', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:23', '2017-07-05 18:13:23');
INSERT INTO `admin_operation_log` VALUES ('478', '1', 'admin/auth/menu', 'POST', '197.246.9.12', '{\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":14},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]}]\"}', '2017-07-05 18:13:38', '2017-07-05 18:13:38');
INSERT INTO `admin_operation_log` VALUES ('479', '1', 'admin/auth/menu', 'POST', '197.246.9.12', '{\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":14},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]}]\"}', '2017-07-05 18:13:38', '2017-07-05 18:13:38');
INSERT INTO `admin_operation_log` VALUES ('480', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:39', '2017-07-05 18:13:39');
INSERT INTO `admin_operation_log` VALUES ('481', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:13:39', '2017-07-05 18:13:39');
INSERT INTO `admin_operation_log` VALUES ('482', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:15:07', '2017-07-05 18:15:07');
INSERT INTO `admin_operation_log` VALUES ('483', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:15:13', '2017-07-05 18:15:13');
INSERT INTO `admin_operation_log` VALUES ('484', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:15:17', '2017-07-05 18:15:17');
INSERT INTO `admin_operation_log` VALUES ('485', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:15:18', '2017-07-05 18:15:18');
INSERT INTO `admin_operation_log` VALUES ('486', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:15:35', '2017-07-05 18:15:35');
INSERT INTO `admin_operation_log` VALUES ('487', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:16:17', '2017-07-05 18:16:17');
INSERT INTO `admin_operation_log` VALUES ('488', '1', 'admin/auth/menu/14', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Doctors\",\"icon\":\"fa-adn\",\"uri\":\"auth\\/doctor\",\"roles\":[null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 18:16:49', '2017-07-05 18:16:49');
INSERT INTO `admin_operation_log` VALUES ('489', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 18:16:49', '2017-07-05 18:16:49');
INSERT INTO `admin_operation_log` VALUES ('490', '2', 'admin/auth/logout', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:00', '2017-07-05 18:17:00');
INSERT INTO `admin_operation_log` VALUES ('491', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:17:08', '2017-07-05 18:17:08');
INSERT INTO `admin_operation_log` VALUES ('492', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:11', '2017-07-05 18:17:11');
INSERT INTO `admin_operation_log` VALUES ('493', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:13', '2017-07-05 18:17:13');
INSERT INTO `admin_operation_log` VALUES ('494', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:14', '2017-07-05 18:17:14');
INSERT INTO `admin_operation_log` VALUES ('495', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:14', '2017-07-05 18:17:14');
INSERT INTO `admin_operation_log` VALUES ('496', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:15', '2017-07-05 18:17:15');
INSERT INTO `admin_operation_log` VALUES ('497', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:15', '2017-07-05 18:17:15');
INSERT INTO `admin_operation_log` VALUES ('498', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:24', '2017-07-05 18:17:24');
INSERT INTO `admin_operation_log` VALUES ('499', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:17:30', '2017-07-05 18:17:30');
INSERT INTO `admin_operation_log` VALUES ('500', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"1\",\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:17:49', '2017-07-05 18:17:49');
INSERT INTO `admin_operation_log` VALUES ('501', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:17:50', '2017-07-05 18:17:50');
INSERT INTO `admin_operation_log` VALUES ('502', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:17:52', '2017-07-05 18:17:52');
INSERT INTO `admin_operation_log` VALUES ('503', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:18:02', '2017-07-05 18:18:02');
INSERT INTO `admin_operation_log` VALUES ('504', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:18:13', '2017-07-05 18:18:13');
INSERT INTO `admin_operation_log` VALUES ('505', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:18:14', '2017-07-05 18:18:14');
INSERT INTO `admin_operation_log` VALUES ('506', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:18:18', '2017-07-05 18:18:18');
INSERT INTO `admin_operation_log` VALUES ('507', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:18:22', '2017-07-05 18:18:22');
INSERT INTO `admin_operation_log` VALUES ('508', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:18:32', '2017-07-05 18:18:32');
INSERT INTO `admin_operation_log` VALUES ('509', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:18:40', '2017-07-05 18:18:40');
INSERT INTO `admin_operation_log` VALUES ('510', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:18:44', '2017-07-05 18:18:44');
INSERT INTO `admin_operation_log` VALUES ('511', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:18:57', '2017-07-05 18:18:57');
INSERT INTO `admin_operation_log` VALUES ('512', '1', 'admin/auth/permissions/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:19:02', '2017-07-05 18:19:02');
INSERT INTO `admin_operation_log` VALUES ('513', '1', 'admin/auth/roles/2', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:19:23', '2017-07-05 18:19:23');
INSERT INTO `admin_operation_log` VALUES ('514', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:19:23', '2017-07-05 18:19:23');
INSERT INTO `admin_operation_log` VALUES ('515', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:19:28', '2017-07-05 18:19:28');
INSERT INTO `admin_operation_log` VALUES ('516', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:19:30', '2017-07-05 18:19:30');
INSERT INTO `admin_operation_log` VALUES ('517', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:19:50', '2017-07-05 18:19:50');
INSERT INTO `admin_operation_log` VALUES ('518', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 18:20:06', '2017-07-05 18:20:06');
INSERT INTO `admin_operation_log` VALUES ('519', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:20:26', '2017-07-05 18:20:26');
INSERT INTO `admin_operation_log` VALUES ('520', '1', 'admin/auth/menu/14', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Doctors\",\"icon\":\"fa-adn\",\"uri\":\"auth\\/doctor\",\"roles\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 18:20:42', '2017-07-05 18:20:42');
INSERT INTO `admin_operation_log` VALUES ('521', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 18:20:42', '2017-07-05 18:20:42');
INSERT INTO `admin_operation_log` VALUES ('522', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:20:46', '2017-07-05 18:20:46');
INSERT INTO `admin_operation_log` VALUES ('523', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:20:48', '2017-07-05 18:20:48');
INSERT INTO `admin_operation_log` VALUES ('524', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:20:50', '2017-07-05 18:20:50');
INSERT INTO `admin_operation_log` VALUES ('525', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:20:50', '2017-07-05 18:20:50');
INSERT INTO `admin_operation_log` VALUES ('526', '1', 'admin/auth/menu/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:21:04', '2017-07-05 18:21:04');
INSERT INTO `admin_operation_log` VALUES ('527', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:21:41', '2017-07-05 18:21:41');
INSERT INTO `admin_operation_log` VALUES ('528', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:22:21', '2017-07-05 18:22:21');
INSERT INTO `admin_operation_log` VALUES ('529', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:22:21', '2017-07-05 18:22:21');
INSERT INTO `admin_operation_log` VALUES ('530', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:22:22', '2017-07-05 18:22:22');
INSERT INTO `admin_operation_log` VALUES ('531', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:22:30', '2017-07-05 18:22:30');
INSERT INTO `admin_operation_log` VALUES ('532', '1', 'admin/auth/roles/2', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:22:37', '2017-07-05 18:22:37');
INSERT INTO `admin_operation_log` VALUES ('533', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:22:38', '2017-07-05 18:22:38');
INSERT INTO `admin_operation_log` VALUES ('534', '1', 'admin/auth/permissions/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:22:41', '2017-07-05 18:22:41');
INSERT INTO `admin_operation_log` VALUES ('535', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:22:48', '2017-07-05 18:22:48');
INSERT INTO `admin_operation_log` VALUES ('536', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:22:57', '2017-07-05 18:22:57');
INSERT INTO `admin_operation_log` VALUES ('537', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:23:09', '2017-07-05 18:23:09');
INSERT INTO `admin_operation_log` VALUES ('538', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:23:10', '2017-07-05 18:23:10');
INSERT INTO `admin_operation_log` VALUES ('539', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:23:16', '2017-07-05 18:23:16');
INSERT INTO `admin_operation_log` VALUES ('540', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:23:20', '2017-07-05 18:23:20');
INSERT INTO `admin_operation_log` VALUES ('541', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:23:21', '2017-07-05 18:23:21');
INSERT INTO `admin_operation_log` VALUES ('542', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:23:36', '2017-07-05 18:23:36');
INSERT INTO `admin_operation_log` VALUES ('543', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"cache:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 18:23:44', '2017-07-05 18:23:44');
INSERT INTO `admin_operation_log` VALUES ('544', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"config:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 18:24:03', '2017-07-05 18:24:03');
INSERT INTO `admin_operation_log` VALUES ('545', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:24:09', '2017-07-05 18:24:09');
INSERT INTO `admin_operation_log` VALUES ('546', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:24:18', '2017-07-05 18:24:18');
INSERT INTO `admin_operation_log` VALUES ('547', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:24:21', '2017-07-05 18:24:21');
INSERT INTO `admin_operation_log` VALUES ('548', '2', 'admin/auth/setting', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:24:30', '2017-07-05 18:24:30');
INSERT INTO `admin_operation_log` VALUES ('549', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:24:40', '2017-07-05 18:24:40');
INSERT INTO `admin_operation_log` VALUES ('550', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:24:41', '2017-07-05 18:24:41');
INSERT INTO `admin_operation_log` VALUES ('551', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:25:18', '2017-07-05 18:25:18');
INSERT INTO `admin_operation_log` VALUES ('552', '1', 'admin/helpers/scaffold', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:25:26', '2017-07-05 18:25:26');
INSERT INTO `admin_operation_log` VALUES ('553', '1', 'admin/auth/menu/1/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:28:07', '2017-07-05 18:28:07');
INSERT INTO `admin_operation_log` VALUES ('554', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:32:10', '2017-07-05 18:32:10');
INSERT INTO `admin_operation_log` VALUES ('555', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\",\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 18:32:15', '2017-07-05 18:32:15');
INSERT INTO `admin_operation_log` VALUES ('556', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:19', '2017-07-05 18:32:19');
INSERT INTO `admin_operation_log` VALUES ('557', '1', 'admin/auth/doctor/22/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:20', '2017-07-05 18:32:20');
INSERT INTO `admin_operation_log` VALUES ('558', '1', 'admin/auth/doctor/23/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:21', '2017-07-05 18:32:21');
INSERT INTO `admin_operation_log` VALUES ('559', '1', 'admin/auth/doctor/32/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:22', '2017-07-05 18:32:22');
INSERT INTO `admin_operation_log` VALUES ('560', '1', 'admin/auth/doctor/11', 'PUT', '197.246.9.12', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Not confirmed\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor?&_sort%5Bcolumn%5D=phone_confirmation&_sort%5Btype%5D=desc\"}', '2017-07-05 18:32:31', '2017-07-05 18:32:31');
INSERT INTO `admin_operation_log` VALUES ('561', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:31', '2017-07-05 18:32:31');
INSERT INTO `admin_operation_log` VALUES ('562', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:32:56', '2017-07-05 18:32:56');
INSERT INTO `admin_operation_log` VALUES ('563', '1', 'admin/auth/doctor/22', 'PUT', '197.246.9.12', '{\"name\":\"Asmaa Ali\",\"email\":\"asmaa.kohla@yahoo.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Pending\",\"front_card_id\":\"doc_22_2017-04-0414:23:04.jpg\",\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor?&_sort%5Bcolumn%5D=phone_confirmation&_sort%5Btype%5D=desc\"}', '2017-07-05 18:33:18', '2017-07-05 18:33:18');
INSERT INTO `admin_operation_log` VALUES ('564', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 18:33:19', '2017-07-05 18:33:19');
INSERT INTO `admin_operation_log` VALUES ('565', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 18:33:24', '2017-07-05 18:33:24');
INSERT INTO `admin_operation_log` VALUES ('566', '1', 'admin/auth/doctor/11', 'PUT', '197.246.9.12', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Confirmed\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor?&_sort%5Bcolumn%5D=phone_confirmation&_sort%5Btype%5D=desc\"}', '2017-07-05 18:33:32', '2017-07-05 18:33:32');
INSERT INTO `admin_operation_log` VALUES ('567', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 18:33:33', '2017-07-05 18:33:33');
INSERT INTO `admin_operation_log` VALUES ('568', '1', 'admin/auth/doctor/23', 'PUT', '197.246.9.12', '{\"name\":\"Asmaa Ali\",\"email\":\"asmaa@gmail.com\",\"gender\":\"F\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":\"\\u00d9\\u0192\\u00d9\\u201e\\u00d8\\u00a7\\u00d9\\u2026 \\u00d8\\u00b9\\u00d8\\u00b1\\u00d8\\u00a8\\u00d9\\u0160\",\"short_bio_english\":\"english txt\",\"city\":\"1\",\"country\":\"EG\",\"neighborhood\":\"agamy\",\"doctor_level_id\":\"4\",\"doctor_speciality_id\":\"30\",\"phone_confirmation\":\"Not confirmed\",\"front_card_id\":\"doc_23_2017-04-0414:29:32.jpeg\",\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor?&_sort%5Bcolumn%5D=phone_confirmation&_sort%5Btype%5D=desc\"}', '2017-07-05 18:33:39', '2017-07-05 18:33:39');
INSERT INTO `admin_operation_log` VALUES ('569', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 18:33:40', '2017-07-05 18:33:40');
INSERT INTO `admin_operation_log` VALUES ('570', '1', 'admin/auth/doctor/32/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:33:46', '2017-07-05 18:33:46');
INSERT INTO `admin_operation_log` VALUES ('571', '1', 'admin/auth/doctor/32', 'PUT', '197.246.9.12', '{\"name\":\"Mohamed Metwally\",\"email\":\"metwally@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2000\",\"short_bio_arabic\":\"\\u00d8\\u00a7\\u00d9\\u201e\\u00d8\\u00af\\u00d9\\u0192\\u00d8\\u00aa\\u00d9\\u02c6\\u00d8\\u00b1 \\u00d9\\u2026\\u00d8\\u00ad\\u00d9\\u2026\\u00d8\\u00af \\u00d9\\u2026\\u00d8\\u00aa\\u00d9\\u02c6\\u00d9\\u201e\\u00d9\\u0160\",\"short_bio_english\":\"Dr. Mohamed Metwally\",\"city\":\"1\",\"country\":\"EG\",\"neighborhood\":\"Shobra\",\"doctor_level_id\":\"2\",\"doctor_speciality_id\":\"30\",\"phone_confirmation\":\"Confirmed\",\"front_card_id\":\"doc_32_2017-04-2501:13:09.jpg\",\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor?&_sort%5Bcolumn%5D=phone_confirmation&_sort%5Btype%5D=desc\"}', '2017-07-05 18:33:52', '2017-07-05 18:33:52');
INSERT INTO `admin_operation_log` VALUES ('572', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 18:33:53', '2017-07-05 18:33:53');
INSERT INTO `admin_operation_log` VALUES ('573', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:34:19', '2017-07-05 18:34:19');
INSERT INTO `admin_operation_log` VALUES ('574', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:40:49', '2017-07-05 18:40:49');
INSERT INTO `admin_operation_log` VALUES ('575', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:40:53', '2017-07-05 18:40:53');
INSERT INTO `admin_operation_log` VALUES ('576', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"1\",\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:41:06', '2017-07-05 18:41:06');
INSERT INTO `admin_operation_log` VALUES ('577', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:41:06', '2017-07-05 18:41:06');
INSERT INTO `admin_operation_log` VALUES ('578', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:41:09', '2017-07-05 18:41:09');
INSERT INTO `admin_operation_log` VALUES ('579', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:41:20', '2017-07-05 18:41:20');
INSERT INTO `admin_operation_log` VALUES ('580', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 18:41:31', '2017-07-05 18:41:31');
INSERT INTO `admin_operation_log` VALUES ('581', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 18:41:32', '2017-07-05 18:41:32');
INSERT INTO `admin_operation_log` VALUES ('582', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:41:34', '2017-07-05 18:41:34');
INSERT INTO `admin_operation_log` VALUES ('583', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:41:45', '2017-07-05 18:41:45');
INSERT INTO `admin_operation_log` VALUES ('584', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:41:49', '2017-07-05 18:41:49');
INSERT INTO `admin_operation_log` VALUES ('585', '1', 'admin/auth/menu/14', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Doctors\",\"icon\":\"fa-adn\",\"uri\":\"auth\\/doctor\",\"roles\":[null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 18:42:02', '2017-07-05 18:42:02');
INSERT INTO `admin_operation_log` VALUES ('586', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 18:42:02', '2017-07-05 18:42:02');
INSERT INTO `admin_operation_log` VALUES ('587', '1', 'admin/auth/menu/14/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:42:15', '2017-07-05 18:42:15');
INSERT INTO `admin_operation_log` VALUES ('588', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:42:20', '2017-07-05 18:42:20');
INSERT INTO `admin_operation_log` VALUES ('589', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:42:23', '2017-07-05 18:42:23');
INSERT INTO `admin_operation_log` VALUES ('590', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:13', '2017-07-05 18:44:13');
INSERT INTO `admin_operation_log` VALUES ('591', '1', 'admin/auth/permissions/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:17', '2017-07-05 18:44:17');
INSERT INTO `admin_operation_log` VALUES ('592', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:25', '2017-07-05 18:44:25');
INSERT INTO `admin_operation_log` VALUES ('593', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:28', '2017-07-05 18:44:28');
INSERT INTO `admin_operation_log` VALUES ('594', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:33', '2017-07-05 18:44:33');
INSERT INTO `admin_operation_log` VALUES ('595', '1', 'admin/auth/roles/2', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:44:39', '2017-07-05 18:44:39');
INSERT INTO `admin_operation_log` VALUES ('596', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:44:39', '2017-07-05 18:44:39');
INSERT INTO `admin_operation_log` VALUES ('597', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:44:44', '2017-07-05 18:44:44');
INSERT INTO `admin_operation_log` VALUES ('598', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:44:52', '2017-07-05 18:44:52');
INSERT INTO `admin_operation_log` VALUES ('599', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:44:59', '2017-07-05 18:44:59');
INSERT INTO `admin_operation_log` VALUES ('600', '1', 'admin/auth/roles/2', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"1\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:45:06', '2017-07-05 18:45:06');
INSERT INTO `admin_operation_log` VALUES ('601', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:45:06', '2017-07-05 18:45:06');
INSERT INTO `admin_operation_log` VALUES ('602', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:45:11', '2017-07-05 18:45:11');
INSERT INTO `admin_operation_log` VALUES ('603', '1', 'admin/auth/roles/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:45:16', '2017-07-05 18:45:16');
INSERT INTO `admin_operation_log` VALUES ('604', '1', 'admin/auth/roles/2', 'PUT', '197.246.9.12', '{\"slug\":\"callcenter\",\"name\":\"call center\",\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 18:45:24', '2017-07-05 18:45:24');
INSERT INTO `admin_operation_log` VALUES ('605', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 18:45:24', '2017-07-05 18:45:24');
INSERT INTO `admin_operation_log` VALUES ('606', '2', 'admin/auth/logout', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:46:08', '2017-07-05 18:46:08');
INSERT INTO `admin_operation_log` VALUES ('607', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:46:20', '2017-07-05 18:46:20');
INSERT INTO `admin_operation_log` VALUES ('608', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:46:28', '2017-07-05 18:46:28');
INSERT INTO `admin_operation_log` VALUES ('609', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:48:56', '2017-07-05 18:48:56');
INSERT INTO `admin_operation_log` VALUES ('610', '2', 'admin/auth/roles', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:49:03', '2017-07-05 18:49:03');
INSERT INTO `admin_operation_log` VALUES ('611', '2', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:49:07', '2017-07-05 18:49:07');
INSERT INTO `admin_operation_log` VALUES ('612', '2', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 18:49:34', '2017-07-05 18:49:34');
INSERT INTO `admin_operation_log` VALUES ('613', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:49:42', '2017-07-05 18:49:42');
INSERT INTO `admin_operation_log` VALUES ('614', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 18:51:53', '2017-07-05 18:51:53');
INSERT INTO `admin_operation_log` VALUES ('615', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:52:15', '2017-07-05 18:52:15');
INSERT INTO `admin_operation_log` VALUES ('616', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:52:18', '2017-07-05 18:52:18');
INSERT INTO `admin_operation_log` VALUES ('617', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 18:52:19', '2017-07-05 18:52:19');
INSERT INTO `admin_operation_log` VALUES ('618', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:52:22', '2017-07-05 18:52:22');
INSERT INTO `admin_operation_log` VALUES ('619', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:53:20', '2017-07-05 18:53:20');
INSERT INTO `admin_operation_log` VALUES ('620', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 18:59:21', '2017-07-05 18:59:21');
INSERT INTO `admin_operation_log` VALUES ('621', '1', 'admin/helpers/scaffold', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:00:51', '2017-07-05 19:00:51');
INSERT INTO `admin_operation_log` VALUES ('622', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:00:52', '2017-07-05 19:00:52');
INSERT INTO `admin_operation_log` VALUES ('623', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:00:53', '2017-07-05 19:00:53');
INSERT INTO `admin_operation_log` VALUES ('624', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '[]', '2017-07-05 19:01:00', '2017-07-05 19:01:00');
INSERT INTO `admin_operation_log` VALUES ('625', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"cache:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 19:01:08', '2017-07-05 19:01:08');
INSERT INTO `admin_operation_log` VALUES ('626', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:01:14', '2017-07-05 19:01:14');
INSERT INTO `admin_operation_log` VALUES ('627', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:01:16', '2017-07-05 19:01:16');
INSERT INTO `admin_operation_log` VALUES ('628', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:03:19', '2017-07-05 19:03:19');
INSERT INTO `admin_operation_log` VALUES ('629', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:03:58', '2017-07-05 19:03:58');
INSERT INTO `admin_operation_log` VALUES ('630', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:04:13', '2017-07-05 19:04:13');
INSERT INTO `admin_operation_log` VALUES ('631', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:04:42', '2017-07-05 19:04:42');
INSERT INTO `admin_operation_log` VALUES ('632', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:04:48', '2017-07-05 19:04:48');
INSERT INTO `admin_operation_log` VALUES ('633', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"1\",\"2\",null],\"permissions\":[null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 19:05:03', '2017-07-05 19:05:03');
INSERT INTO `admin_operation_log` VALUES ('634', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 19:05:04', '2017-07-05 19:05:04');
INSERT INTO `admin_operation_log` VALUES ('635', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:05:39', '2017-07-05 19:05:39');
INSERT INTO `admin_operation_log` VALUES ('636', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:06:03', '2017-07-05 19:06:03');
INSERT INTO `admin_operation_log` VALUES ('637', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 19:06:08', '2017-07-05 19:06:08');
INSERT INTO `admin_operation_log` VALUES ('638', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 19:07:33', '2017-07-05 19:07:33');
INSERT INTO `admin_operation_log` VALUES ('639', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 19:07:33', '2017-07-05 19:07:33');
INSERT INTO `admin_operation_log` VALUES ('640', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:07:42', '2017-07-05 19:07:42');
INSERT INTO `admin_operation_log` VALUES ('641', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:07:54', '2017-07-05 19:07:54');
INSERT INTO `admin_operation_log` VALUES ('642', '1', 'admin/auth/menu', 'POST', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"test\",\"icon\":\"fa-bars\",\"uri\":\"test\",\"roles\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 19:08:15', '2017-07-05 19:08:15');
INSERT INTO `admin_operation_log` VALUES ('643', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 19:08:15', '2017-07-05 19:08:15');
INSERT INTO `admin_operation_log` VALUES ('644', '1', 'admin/auth/menu', 'POST', '197.246.9.12', '{\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":14},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":15}]\"}', '2017-07-05 19:08:23', '2017-07-05 19:08:23');
INSERT INTO `admin_operation_log` VALUES ('645', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:08:24', '2017-07-05 19:08:24');
INSERT INTO `admin_operation_log` VALUES ('646', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:08:27', '2017-07-05 19:08:27');
INSERT INTO `admin_operation_log` VALUES ('647', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:09:08', '2017-07-05 19:09:08');
INSERT INTO `admin_operation_log` VALUES ('648', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:10:24', '2017-07-05 19:10:24');
INSERT INTO `admin_operation_log` VALUES ('649', '1', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:10:28', '2017-07-05 19:10:28');
INSERT INTO `admin_operation_log` VALUES ('650', '1', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:16:25', '2017-07-05 19:16:25');
INSERT INTO `admin_operation_log` VALUES ('651', '1', 'admin/doctor/32/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:16:40', '2017-07-05 19:16:40');
INSERT INTO `admin_operation_log` VALUES ('652', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:18:28', '2017-07-05 19:18:28');
INSERT INTO `admin_operation_log` VALUES ('653', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:18:32', '2017-07-05 19:18:32');
INSERT INTO `admin_operation_log` VALUES ('654', '2', 'admin/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:18:37', '2017-07-05 19:18:37');
INSERT INTO `admin_operation_log` VALUES ('655', '2', 'admin/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:19:04', '2017-07-05 19:19:04');
INSERT INTO `admin_operation_log` VALUES ('656', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:19:08', '2017-07-05 19:19:08');
INSERT INTO `admin_operation_log` VALUES ('657', '2', 'admin/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:20:01', '2017-07-05 19:20:01');
INSERT INTO `admin_operation_log` VALUES ('658', '1', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:38:40', '2017-07-05 19:38:40');
INSERT INTO `admin_operation_log` VALUES ('659', '1', 'admin/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 19:39:25', '2017-07-05 19:39:25');
INSERT INTO `admin_operation_log` VALUES ('660', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:41:43', '2017-07-05 19:41:43');
INSERT INTO `admin_operation_log` VALUES ('661', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:42:24', '2017-07-05 19:42:24');
INSERT INTO `admin_operation_log` VALUES ('662', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:42:36', '2017-07-05 19:42:36');
INSERT INTO `admin_operation_log` VALUES ('663', '2', 'admin/doctor/11/edit', 'GET', '197.246.9.12', '[]', '2017-07-05 19:42:56', '2017-07-05 19:42:56');
INSERT INTO `admin_operation_log` VALUES ('664', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:48:36', '2017-07-05 19:48:36');
INSERT INTO `admin_operation_log` VALUES ('665', '2', 'admin/doctor/22/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:48:40', '2017-07-05 19:48:40');
INSERT INTO `admin_operation_log` VALUES ('666', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:49:29', '2017-07-05 19:49:29');
INSERT INTO `admin_operation_log` VALUES ('667', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:49:32', '2017-07-05 19:49:32');
INSERT INTO `admin_operation_log` VALUES ('668', '1', 'admin/auth/doctor/11', 'PUT', '197.246.9.12', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Confirmed\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-05 19:49:55', '2017-07-05 19:49:55');
INSERT INTO `admin_operation_log` VALUES ('669', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:49:55', '2017-07-05 19:49:55');
INSERT INTO `admin_operation_log` VALUES ('670', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 19:50:19', '2017-07-05 19:50:19');
INSERT INTO `admin_operation_log` VALUES ('671', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 19:56:22', '2017-07-05 19:56:22');
INSERT INTO `admin_operation_log` VALUES ('672', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:00:07', '2017-07-05 20:00:07');
INSERT INTO `admin_operation_log` VALUES ('673', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:00:37', '2017-07-05 20:00:37');
INSERT INTO `admin_operation_log` VALUES ('674', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:02:51', '2017-07-05 20:02:51');
INSERT INTO `admin_operation_log` VALUES ('675', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:06:55', '2017-07-05 20:06:55');
INSERT INTO `admin_operation_log` VALUES ('676', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:07:28', '2017-07-05 20:07:28');
INSERT INTO `admin_operation_log` VALUES ('677', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:07:40', '2017-07-05 20:07:40');
INSERT INTO `admin_operation_log` VALUES ('678', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:08:42', '2017-07-05 20:08:42');
INSERT INTO `admin_operation_log` VALUES ('679', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:09:04', '2017-07-05 20:09:04');
INSERT INTO `admin_operation_log` VALUES ('680', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:09:55', '2017-07-05 20:09:55');
INSERT INTO `admin_operation_log` VALUES ('681', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:10:09', '2017-07-05 20:10:09');
INSERT INTO `admin_operation_log` VALUES ('682', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:11:11', '2017-07-05 20:11:11');
INSERT INTO `admin_operation_log` VALUES ('683', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:12:23', '2017-07-05 20:12:23');
INSERT INTO `admin_operation_log` VALUES ('684', '1', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:12:35', '2017-07-05 20:12:35');
INSERT INTO `admin_operation_log` VALUES ('685', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:21:55', '2017-07-05 20:21:55');
INSERT INTO `admin_operation_log` VALUES ('686', '1', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:22:10', '2017-07-05 20:22:10');
INSERT INTO `admin_operation_log` VALUES ('687', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:22:24', '2017-07-05 20:22:24');
INSERT INTO `admin_operation_log` VALUES ('688', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:22:29', '2017-07-05 20:22:29');
INSERT INTO `admin_operation_log` VALUES ('689', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 20:22:40', '2017-07-05 20:22:40');
INSERT INTO `admin_operation_log` VALUES ('690', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:22:41', '2017-07-05 20:22:41');
INSERT INTO `admin_operation_log` VALUES ('691', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:23:15', '2017-07-05 20:23:15');
INSERT INTO `admin_operation_log` VALUES ('692', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"1\",\"2\",null],\"permissions\":[\"1\",\"2\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 20:23:26', '2017-07-05 20:23:26');
INSERT INTO `admin_operation_log` VALUES ('693', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:23:27', '2017-07-05 20:23:27');
INSERT INTO `admin_operation_log` VALUES ('694', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:23:34', '2017-07-05 20:23:34');
INSERT INTO `admin_operation_log` VALUES ('695', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 20:23:44', '2017-07-05 20:23:44');
INSERT INTO `admin_operation_log` VALUES ('696', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 20:23:46', '2017-07-05 20:23:46');
INSERT INTO `admin_operation_log` VALUES ('697', '1', 'admin/auth/permissions/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:23:53', '2017-07-05 20:23:53');
INSERT INTO `admin_operation_log` VALUES ('698', '1', 'admin/auth/permissions', 'POST', '197.246.9.12', '{\"slug\":\"Master\",\"name\":\"Master\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/permissions\"}', '2017-07-05 20:24:07', '2017-07-05 20:24:07');
INSERT INTO `admin_operation_log` VALUES ('699', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '[]', '2017-07-05 20:24:07', '2017-07-05 20:24:07');
INSERT INTO `admin_operation_log` VALUES ('700', '1', 'admin/auth/roles/create', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:24:10', '2017-07-05 20:24:10');
INSERT INTO `admin_operation_log` VALUES ('701', '1', 'admin/auth/roles', 'POST', '197.246.9.12', '{\"slug\":\"Master\",\"name\":\"Master\",\"permissions\":[\"1\",\"2\",\"3\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/roles\"}', '2017-07-05 20:24:28', '2017-07-05 20:24:28');
INSERT INTO `admin_operation_log` VALUES ('702', '1', 'admin/auth/roles', 'GET', '197.246.9.12', '[]', '2017-07-05 20:24:29', '2017-07-05 20:24:29');
INSERT INTO `admin_operation_log` VALUES ('703', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:24:37', '2017-07-05 20:24:37');
INSERT INTO `admin_operation_log` VALUES ('704', '1', 'admin/auth/menu/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:24:41', '2017-07-05 20:24:41');
INSERT INTO `admin_operation_log` VALUES ('705', '1', 'admin/auth/menu/2', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Admin\",\"icon\":\"fa-tasks\",\"uri\":null,\"roles\":[\"3\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 20:25:02', '2017-07-05 20:25:02');
INSERT INTO `admin_operation_log` VALUES ('706', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 20:25:02', '2017-07-05 20:25:02');
INSERT INTO `admin_operation_log` VALUES ('707', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:25:06', '2017-07-05 20:25:06');
INSERT INTO `admin_operation_log` VALUES ('708', '1', 'admin/auth/menu/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:25:16', '2017-07-05 20:25:16');
INSERT INTO `admin_operation_log` VALUES ('709', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:25:24', '2017-07-05 20:25:24');
INSERT INTO `admin_operation_log` VALUES ('710', '1', 'admin/auth/menu/8/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:25:28', '2017-07-05 20:25:28');
INSERT INTO `admin_operation_log` VALUES ('711', '1', 'admin/auth/menu/8', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Helpers\",\"icon\":\"fa-gears\",\"uri\":null,\"roles\":[\"3\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 20:25:42', '2017-07-05 20:25:42');
INSERT INTO `admin_operation_log` VALUES ('712', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 20:25:43', '2017-07-05 20:25:43');
INSERT INTO `admin_operation_log` VALUES ('713', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:25:52', '2017-07-05 20:25:52');
INSERT INTO `admin_operation_log` VALUES ('714', '1', 'admin/auth/users/1/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:25:57', '2017-07-05 20:25:57');
INSERT INTO `admin_operation_log` VALUES ('715', '1', 'admin/auth/users/1', 'PUT', '197.246.9.12', '{\"username\":\"admin\",\"name\":\"Administrator\",\"password\":\"$2y$10$z3hOXAMcP8fpJqTZy8B8\\/OtWuiZLcUrqw9CQGjEdAz285B.IAerE.\",\"password_confirmation\":\"$2y$10$z3hOXAMcP8fpJqTZy8B8\\/OtWuiZLcUrqw9CQGjEdAz285B.IAerE.\",\"roles\":[\"1\",\"3\",null],\"permissions\":[null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 20:26:09', '2017-07-05 20:26:09');
INSERT INTO `admin_operation_log` VALUES ('716', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:26:10', '2017-07-05 20:26:10');
INSERT INTO `admin_operation_log` VALUES ('717', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:26:22', '2017-07-05 20:26:22');
INSERT INTO `admin_operation_log` VALUES ('718', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:26:51', '2017-07-05 20:26:51');
INSERT INTO `admin_operation_log` VALUES ('719', '1', 'admin/auth/users/2', 'PUT', '197.246.9.12', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",\"3\",null],\"permissions\":[\"2\",\"3\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-05 20:27:06', '2017-07-05 20:27:06');
INSERT INTO `admin_operation_log` VALUES ('720', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:27:06', '2017-07-05 20:27:06');
INSERT INTO `admin_operation_log` VALUES ('721', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:27:11', '2017-07-05 20:27:11');
INSERT INTO `admin_operation_log` VALUES ('722', '2', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:27:33', '2017-07-05 20:27:33');
INSERT INTO `admin_operation_log` VALUES ('723', '1', 'admin/helpers/scaffold', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:27:48', '2017-07-05 20:27:48');
INSERT INTO `admin_operation_log` VALUES ('724', '1', 'admin/helpers/terminal/database', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:27:55', '2017-07-05 20:27:55');
INSERT INTO `admin_operation_log` VALUES ('725', '1', 'admin/helpers/terminal/database', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:27:57', '2017-07-05 20:27:57');
INSERT INTO `admin_operation_log` VALUES ('726', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:27:59', '2017-07-05 20:27:59');
INSERT INTO `admin_operation_log` VALUES ('727', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"config:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:05', '2017-07-05 20:28:05');
INSERT INTO `admin_operation_log` VALUES ('728', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"admin:menu\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:10', '2017-07-05 20:28:10');
INSERT INTO `admin_operation_log` VALUES ('729', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"admin:make\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:25', '2017-07-05 20:28:25');
INSERT INTO `admin_operation_log` VALUES ('730', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"admin:install\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:34', '2017-07-05 20:28:34');
INSERT INTO `admin_operation_log` VALUES ('731', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"cache:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:46', '2017-07-05 20:28:46');
INSERT INTO `admin_operation_log` VALUES ('732', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"laravel-admin:update\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:28:55', '2017-07-05 20:28:55');
INSERT INTO `admin_operation_log` VALUES ('733', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:29:08', '2017-07-05 20:29:08');
INSERT INTO `admin_operation_log` VALUES ('734', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:cache\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:29:13', '2017-07-05 20:29:13');
INSERT INTO `admin_operation_log` VALUES ('735', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:list\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:29:17', '2017-07-05 20:29:17');
INSERT INTO `admin_operation_log` VALUES ('736', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:30:35', '2017-07-05 20:30:35');
INSERT INTO `admin_operation_log` VALUES ('737', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:list\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:30:39', '2017-07-05 20:30:39');
INSERT INTO `admin_operation_log` VALUES ('738', '1', 'admin/helpers/terminal/database', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:33:55', '2017-07-05 20:33:55');
INSERT INTO `admin_operation_log` VALUES ('739', '1', 'admin/helpers/terminal/artisan', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:33:56', '2017-07-05 20:33:56');
INSERT INTO `admin_operation_log` VALUES ('740', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:cache\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:34:26', '2017-07-05 20:34:26');
INSERT INTO `admin_operation_log` VALUES ('741', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:clear\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:34:33', '2017-07-05 20:34:33');
INSERT INTO `admin_operation_log` VALUES ('742', '1', 'admin/helpers/terminal/artisan', 'POST', '197.246.9.12', '{\"c\":\"route:list\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:34:36', '2017-07-05 20:34:36');
INSERT INTO `admin_operation_log` VALUES ('743', '1', 'admin/auth/logs', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:35:16', '2017-07-05 20:35:16');
INSERT INTO `admin_operation_log` VALUES ('744', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:35:35', '2017-07-05 20:35:35');
INSERT INTO `admin_operation_log` VALUES ('745', '1', 'admin/auth/permissions', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:35:44', '2017-07-05 20:35:44');
INSERT INTO `admin_operation_log` VALUES ('746', '1', 'admin/auth/permissions/3/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:35:48', '2017-07-05 20:35:48');
INSERT INTO `admin_operation_log` VALUES ('747', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:35:56', '2017-07-05 20:35:56');
INSERT INTO `admin_operation_log` VALUES ('748', '1', 'admin/auth/menu/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:00', '2017-07-05 20:36:00');
INSERT INTO `admin_operation_log` VALUES ('749', '1', 'admin/auth/menu/2', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Admin\",\"icon\":\"fa-tasks\",\"uri\":null,\"roles\":[\"1\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 20:36:09', '2017-07-05 20:36:09');
INSERT INTO `admin_operation_log` VALUES ('750', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 20:36:10', '2017-07-05 20:36:10');
INSERT INTO `admin_operation_log` VALUES ('751', '1', 'admin/auth/menu/8/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:13', '2017-07-05 20:36:13');
INSERT INTO `admin_operation_log` VALUES ('752', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:20', '2017-07-05 20:36:20');
INSERT INTO `admin_operation_log` VALUES ('753', '1', 'admin/auth/menu/9/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:24', '2017-07-05 20:36:24');
INSERT INTO `admin_operation_log` VALUES ('754', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:34', '2017-07-05 20:36:34');
INSERT INTO `admin_operation_log` VALUES ('755', '1', 'admin/auth/menu/8/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:38', '2017-07-05 20:36:38');
INSERT INTO `admin_operation_log` VALUES ('756', '1', 'admin/auth/menu/8', 'PUT', '197.246.9.12', '{\"parent_id\":\"0\",\"title\":\"Helpers\",\"icon\":\"fa-gears\",\"uri\":null,\"roles\":[\"1\",null],\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/menu\"}', '2017-07-05 20:36:44', '2017-07-05 20:36:44');
INSERT INTO `admin_operation_log` VALUES ('757', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '[]', '2017-07-05 20:36:45', '2017-07-05 20:36:45');
INSERT INTO `admin_operation_log` VALUES ('758', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:36:49', '2017-07-05 20:36:49');
INSERT INTO `admin_operation_log` VALUES ('759', '2', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:36:54', '2017-07-05 20:36:54');
INSERT INTO `admin_operation_log` VALUES ('760', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:44:15', '2017-07-05 20:44:15');
INSERT INTO `admin_operation_log` VALUES ('761', '1', 'admin/auth/users', 'GET', '197.246.9.12', '[]', '2017-07-05 20:44:19', '2017-07-05 20:44:19');
INSERT INTO `admin_operation_log` VALUES ('762', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:45:37', '2017-07-05 20:45:37');
INSERT INTO `admin_operation_log` VALUES ('763', '1', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:45:41', '2017-07-05 20:45:41');
INSERT INTO `admin_operation_log` VALUES ('764', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:47:54', '2017-07-05 20:47:54');
INSERT INTO `admin_operation_log` VALUES ('765', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:51:54', '2017-07-05 20:51:54');
INSERT INTO `admin_operation_log` VALUES ('766', '2', 'admin', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:51:58', '2017-07-05 20:51:58');
INSERT INTO `admin_operation_log` VALUES ('767', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:52:00', '2017-07-05 20:52:00');
INSERT INTO `admin_operation_log` VALUES ('768', '2', 'admin/auth/doctor/11/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:52:10', '2017-07-05 20:52:10');
INSERT INTO `admin_operation_log` VALUES ('769', '2', 'admin/auth/doctor/11', 'PUT', '197.246.9.12', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"user_state\":\"3\",\"current_credit\":null,\"graduation_year\":\"2011\",\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Pending\",\"front_card_id\":null,\"back_card_id\":null,\"referral_number\":null,\"practicing\":null,\"_token\":\"odk7XgJWkLDR7pRwLQScSJJMPmot01FLqrhRVx4b\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-05 20:52:21', '2017-07-05 20:52:21');
INSERT INTO `admin_operation_log` VALUES ('770', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:52:21', '2017-07-05 20:52:21');
INSERT INTO `admin_operation_log` VALUES ('771', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:52:35', '2017-07-05 20:52:35');
INSERT INTO `admin_operation_log` VALUES ('772', '1', 'admin/auth/menu/15', 'DELETE', '197.246.9.12', '{\"_method\":\"delete\",\"_token\":\"idwVln4k8VBwrPE4FXqGFVajIYecurjkyPaX0rVI\"}', '2017-07-05 20:52:46', '2017-07-05 20:52:46');
INSERT INTO `admin_operation_log` VALUES ('773', '1', 'admin/auth/menu', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:52:46', '2017-07-05 20:52:46');
INSERT INTO `admin_operation_log` VALUES ('774', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:53:14', '2017-07-05 20:53:14');
INSERT INTO `admin_operation_log` VALUES ('775', '2', 'admin/auth/logout', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:53:37', '2017-07-05 20:53:37');
INSERT INTO `admin_operation_log` VALUES ('776', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-05 20:58:25', '2017-07-05 20:58:25');
INSERT INTO `admin_operation_log` VALUES ('777', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 20:58:28', '2017-07-05 20:58:28');
INSERT INTO `admin_operation_log` VALUES ('778', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:58:56', '2017-07-05 20:58:56');
INSERT INTO `admin_operation_log` VALUES ('779', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:59:33', '2017-07-05 20:59:33');
INSERT INTO `admin_operation_log` VALUES ('780', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 20:59:36', '2017-07-05 20:59:36');
INSERT INTO `admin_operation_log` VALUES ('781', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:00:04', '2017-07-05 21:00:04');
INSERT INTO `admin_operation_log` VALUES ('782', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:00:27', '2017-07-05 21:00:27');
INSERT INTO `admin_operation_log` VALUES ('783', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:06:04', '2017-07-05 21:06:04');
INSERT INTO `admin_operation_log` VALUES ('784', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:07:19', '2017-07-05 21:07:19');
INSERT INTO `admin_operation_log` VALUES ('785', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:08:32', '2017-07-05 21:08:32');
INSERT INTO `admin_operation_log` VALUES ('786', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:16:03', '2017-07-05 21:16:03');
INSERT INTO `admin_operation_log` VALUES ('787', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:22:59', '2017-07-05 21:22:59');
INSERT INTO `admin_operation_log` VALUES ('788', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:28:05', '2017-07-05 21:28:05');
INSERT INTO `admin_operation_log` VALUES ('789', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:28:38', '2017-07-05 21:28:38');
INSERT INTO `admin_operation_log` VALUES ('790', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:28:52', '2017-07-05 21:28:52');
INSERT INTO `admin_operation_log` VALUES ('791', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:36:59', '2017-07-05 21:36:59');
INSERT INTO `admin_operation_log` VALUES ('792', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '[]', '2017-07-05 21:37:27', '2017-07-05 21:37:27');
INSERT INTO `admin_operation_log` VALUES ('793', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"},\"_pjax\":\"#pjax-container\"}', '2017-07-05 21:37:33', '2017-07-05 21:37:33');
INSERT INTO `admin_operation_log` VALUES ('794', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2017-07-05 21:37:35', '2017-07-05 21:37:35');
INSERT INTO `admin_operation_log` VALUES ('795', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"},\"_pjax\":\"#pjax-container\"}', '2017-07-05 21:37:36', '2017-07-05 21:37:36');
INSERT INTO `admin_operation_log` VALUES ('796', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:42:41', '2017-07-05 21:42:41');
INSERT INTO `admin_operation_log` VALUES ('797', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:43:05', '2017-07-05 21:43:05');
INSERT INTO `admin_operation_log` VALUES ('798', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:45:03', '2017-07-05 21:45:03');
INSERT INTO `admin_operation_log` VALUES ('799', '1', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:45:42', '2017-07-05 21:45:42');
INSERT INTO `admin_operation_log` VALUES ('800', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:47:14', '2017-07-05 21:47:14');
INSERT INTO `admin_operation_log` VALUES ('801', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:49:28', '2017-07-05 21:49:28');
INSERT INTO `admin_operation_log` VALUES ('802', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:51:08', '2017-07-05 21:51:08');
INSERT INTO `admin_operation_log` VALUES ('803', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:52:08', '2017-07-05 21:52:08');
INSERT INTO `admin_operation_log` VALUES ('804', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_sort\":{\"column\":\"phone_confirmation\",\"type\":\"desc\"}}', '2017-07-05 21:53:32', '2017-07-05 21:53:32');
INSERT INTO `admin_operation_log` VALUES ('805', '2', 'admin', 'GET', '196.145.128.165', '[]', '2017-07-05 22:06:51', '2017-07-05 22:06:51');
INSERT INTO `admin_operation_log` VALUES ('806', '2', 'admin/auth/doctor', 'GET', '196.145.128.165', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:07:17', '2017-07-05 22:07:17');
INSERT INTO `admin_operation_log` VALUES ('807', '2', 'admin/auth/doctor/32/edit', 'GET', '196.145.128.165', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:07:26', '2017-07-05 22:07:26');
INSERT INTO `admin_operation_log` VALUES ('808', '2', 'admin/auth/doctor', 'GET', '196.145.128.165', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:15:45', '2017-07-05 22:15:45');
INSERT INTO `admin_operation_log` VALUES ('809', '2', 'admin/auth/doctor/23/edit', 'GET', '196.145.128.165', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:15:54', '2017-07-05 22:15:54');
INSERT INTO `admin_operation_log` VALUES ('810', '2', 'admin/auth/doctor', 'GET', '196.145.128.165', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:16:03', '2017-07-05 22:16:03');
INSERT INTO `admin_operation_log` VALUES ('811', '2', 'admin', 'GET', '41.232.203.235', '[]', '2017-07-05 22:44:33', '2017-07-05 22:44:33');
INSERT INTO `admin_operation_log` VALUES ('812', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:44:45', '2017-07-05 22:44:45');
INSERT INTO `admin_operation_log` VALUES ('813', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:44:49', '2017-07-05 22:44:49');
INSERT INTO `admin_operation_log` VALUES ('814', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:45:20', '2017-07-05 22:45:20');
INSERT INTO `admin_operation_log` VALUES ('815', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:45:28', '2017-07-05 22:45:28');
INSERT INTO `admin_operation_log` VALUES ('816', '2', 'admin/auth/login', 'GET', '41.232.203.235', '[]', '2017-07-05 22:45:57', '2017-07-05 22:45:57');
INSERT INTO `admin_operation_log` VALUES ('817', '2', 'admin', 'GET', '41.232.203.235', '[]', '2017-07-05 22:45:57', '2017-07-05 22:45:57');
INSERT INTO `admin_operation_log` VALUES ('818', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:46:03', '2017-07-05 22:46:03');
INSERT INTO `admin_operation_log` VALUES ('819', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:46:07', '2017-07-05 22:46:07');
INSERT INTO `admin_operation_log` VALUES ('820', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:46:08', '2017-07-05 22:46:08');
INSERT INTO `admin_operation_log` VALUES ('821', '2', 'admin/auth/login', 'GET', '41.232.203.235', '[]', '2017-07-05 22:46:41', '2017-07-05 22:46:41');
INSERT INTO `admin_operation_log` VALUES ('822', '2', 'admin', 'GET', '41.232.203.235', '[]', '2017-07-05 22:46:41', '2017-07-05 22:46:41');
INSERT INTO `admin_operation_log` VALUES ('823', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:46:52', '2017-07-05 22:46:52');
INSERT INTO `admin_operation_log` VALUES ('824', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:46:53', '2017-07-05 22:46:53');
INSERT INTO `admin_operation_log` VALUES ('825', '2', 'admin/auth/login', 'GET', '41.232.203.235', '[]', '2017-07-05 22:47:17', '2017-07-05 22:47:17');
INSERT INTO `admin_operation_log` VALUES ('826', '2', 'admin', 'GET', '41.232.203.235', '[]', '2017-07-05 22:47:17', '2017-07-05 22:47:17');
INSERT INTO `admin_operation_log` VALUES ('827', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:47:24', '2017-07-05 22:47:24');
INSERT INTO `admin_operation_log` VALUES ('828', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:47:27', '2017-07-05 22:47:27');
INSERT INTO `admin_operation_log` VALUES ('829', '2', 'admin/auth/login', 'GET', '41.232.203.235', '[]', '2017-07-05 22:47:48', '2017-07-05 22:47:48');
INSERT INTO `admin_operation_log` VALUES ('830', '2', 'admin', 'GET', '41.232.203.235', '[]', '2017-07-05 22:47:49', '2017-07-05 22:47:49');
INSERT INTO `admin_operation_log` VALUES ('831', '2', 'admin', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:47:57', '2017-07-05 22:47:57');
INSERT INTO `admin_operation_log` VALUES ('832', '2', 'admin/auth/doctor', 'GET', '41.232.203.235', '{\"_pjax\":\"#pjax-container\"}', '2017-07-05 22:47:58', '2017-07-05 22:47:58');
INSERT INTO `admin_operation_log` VALUES ('833', '2', 'admin/doctor', 'GET', '154.138.43.204', '[]', '2017-07-06 00:01:40', '2017-07-06 00:01:40');
INSERT INTO `admin_operation_log` VALUES ('834', '2', 'admin/doctor', 'GET', '154.138.43.204', '[]', '2017-07-06 00:01:53', '2017-07-06 00:01:53');
INSERT INTO `admin_operation_log` VALUES ('835', '2', 'admin/doctor', 'GET', '154.138.43.204', '[]', '2017-07-06 00:02:35', '2017-07-06 00:02:35');
INSERT INTO `admin_operation_log` VALUES ('836', '2', 'admin/doctor/11/edit', 'GET', '154.138.43.204', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 00:02:42', '2017-07-06 00:02:42');
INSERT INTO `admin_operation_log` VALUES ('837', '2', 'admin/auth/doctor', 'GET', '154.138.43.204', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 00:02:49', '2017-07-06 00:02:49');
INSERT INTO `admin_operation_log` VALUES ('838', '2', 'admin/auth/doctor/11/edit', 'GET', '154.138.43.204', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 00:02:53', '2017-07-06 00:02:53');
INSERT INTO `admin_operation_log` VALUES ('839', '2', 'admin', 'GET', '196.152.102.158', '[]', '2017-07-06 08:33:23', '2017-07-06 08:33:23');
INSERT INTO `admin_operation_log` VALUES ('840', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:33:27', '2017-07-06 08:33:27');
INSERT INTO `admin_operation_log` VALUES ('841', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-06 08:33:37', '2017-07-06 08:33:37');
INSERT INTO `admin_operation_log` VALUES ('842', '2', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:33:55', '2017-07-06 08:33:55');
INSERT INTO `admin_operation_log` VALUES ('843', '2', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:33:58', '2017-07-06 08:33:58');
INSERT INTO `admin_operation_log` VALUES ('844', '2', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:33:59', '2017-07-06 08:33:59');
INSERT INTO `admin_operation_log` VALUES ('845', '2', 'admin/auth/doctor/23', 'PUT', '196.152.102.158', '{\"name\":\"Asmaa Ali\",\"email\":\"asmaa@gmail.com\",\"gender\":\"F\",\"graduation_year\":\"2011\",\"doctor_level_id\":\"4\",\"doctor_speciality_id\":\"30\",\"phone_confirmation\":\"Pending\",\"referral_number\":null,\"_token\":\"blqLo9HdH4sFaP4q5g2n7c4vibnlKCL6n6Qlvorx\",\"_method\":\"PUT\"}', '2017-07-06 08:34:20', '2017-07-06 08:34:20');
INSERT INTO `admin_operation_log` VALUES ('846', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-06 08:34:21', '2017-07-06 08:34:21');
INSERT INTO `admin_operation_log` VALUES ('847', '2', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:34:37', '2017-07-06 08:34:37');
INSERT INTO `admin_operation_log` VALUES ('848', '2', 'admin/auth/logout', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:25', '2017-07-06 08:36:25');
INSERT INTO `admin_operation_log` VALUES ('849', '1', 'admin', 'GET', '196.152.102.158', '[]', '2017-07-06 08:36:30', '2017-07-06 08:36:30');
INSERT INTO `admin_operation_log` VALUES ('850', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:31', '2017-07-06 08:36:31');
INSERT INTO `admin_operation_log` VALUES ('851', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:35', '2017-07-06 08:36:35');
INSERT INTO `admin_operation_log` VALUES ('852', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:42', '2017-07-06 08:36:42');
INSERT INTO `admin_operation_log` VALUES ('853', '1', 'admin/auth/users', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:55', '2017-07-06 08:36:55');
INSERT INTO `admin_operation_log` VALUES ('854', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:36:59', '2017-07-06 08:36:59');
INSERT INTO `admin_operation_log` VALUES ('855', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 08:37:02', '2017-07-06 08:37:02');
INSERT INTO `admin_operation_log` VALUES ('856', '1', 'admin', 'GET', '196.152.102.158', '[]', '2017-07-06 14:43:04', '2017-07-06 14:43:04');
INSERT INTO `admin_operation_log` VALUES ('857', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 14:43:07', '2017-07-06 14:43:07');
INSERT INTO `admin_operation_log` VALUES ('858', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 14:43:10', '2017-07-06 14:43:10');
INSERT INTO `admin_operation_log` VALUES ('859', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 14:43:50', '2017-07-06 14:43:50');
INSERT INTO `admin_operation_log` VALUES ('860', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:43:52', '2017-07-06 14:43:52');
INSERT INTO `admin_operation_log` VALUES ('861', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:43:54', '2017-07-06 14:43:54');
INSERT INTO `admin_operation_log` VALUES ('862', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:43:55', '2017-07-06 14:43:55');
INSERT INTO `admin_operation_log` VALUES ('863', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:44:30', '2017-07-06 14:44:30');
INSERT INTO `admin_operation_log` VALUES ('864', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:51:56', '2017-07-06 14:51:56');
INSERT INTO `admin_operation_log` VALUES ('865', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 14:55:03', '2017-07-06 14:55:03');
INSERT INTO `admin_operation_log` VALUES ('866', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:10:41', '2017-07-06 15:10:41');
INSERT INTO `admin_operation_log` VALUES ('867', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:13:23', '2017-07-06 15:13:23');
INSERT INTO `admin_operation_log` VALUES ('868', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:13:47', '2017-07-06 15:13:47');
INSERT INTO `admin_operation_log` VALUES ('869', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:16:37', '2017-07-06 15:16:37');
INSERT INTO `admin_operation_log` VALUES ('870', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:29:07', '2017-07-06 15:29:07');
INSERT INTO `admin_operation_log` VALUES ('871', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:29:52', '2017-07-06 15:29:52');
INSERT INTO `admin_operation_log` VALUES ('872', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:38:41', '2017-07-06 15:38:41');
INSERT INTO `admin_operation_log` VALUES ('873', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 15:38:49', '2017-07-06 15:38:49');
INSERT INTO `admin_operation_log` VALUES ('874', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:44:44', '2017-07-06 15:44:44');
INSERT INTO `admin_operation_log` VALUES ('875', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:49:28', '2017-07-06 15:49:28');
INSERT INTO `admin_operation_log` VALUES ('876', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:50:18', '2017-07-06 15:50:18');
INSERT INTO `admin_operation_log` VALUES ('877', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:51:53', '2017-07-06 15:51:53');
INSERT INTO `admin_operation_log` VALUES ('878', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:52:59', '2017-07-06 15:52:59');
INSERT INTO `admin_operation_log` VALUES ('879', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:54:08', '2017-07-06 15:54:08');
INSERT INTO `admin_operation_log` VALUES ('880', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:56:19', '2017-07-06 15:56:19');
INSERT INTO `admin_operation_log` VALUES ('881', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:56:21', '2017-07-06 15:56:21');
INSERT INTO `admin_operation_log` VALUES ('882', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:56:23', '2017-07-06 15:56:23');
INSERT INTO `admin_operation_log` VALUES ('883', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:56:39', '2017-07-06 15:56:39');
INSERT INTO `admin_operation_log` VALUES ('884', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:57:08', '2017-07-06 15:57:08');
INSERT INTO `admin_operation_log` VALUES ('885', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:57:51', '2017-07-06 15:57:51');
INSERT INTO `admin_operation_log` VALUES ('886', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:57:52', '2017-07-06 15:57:52');
INSERT INTO `admin_operation_log` VALUES ('887', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:58:55', '2017-07-06 15:58:55');
INSERT INTO `admin_operation_log` VALUES ('888', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:58:58', '2017-07-06 15:58:58');
INSERT INTO `admin_operation_log` VALUES ('889', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 15:59:28', '2017-07-06 15:59:28');
INSERT INTO `admin_operation_log` VALUES ('890', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:02:17', '2017-07-06 16:02:17');
INSERT INTO `admin_operation_log` VALUES ('891', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:02:19', '2017-07-06 16:02:19');
INSERT INTO `admin_operation_log` VALUES ('892', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:02:52', '2017-07-06 16:02:52');
INSERT INTO `admin_operation_log` VALUES ('893', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:02:53', '2017-07-06 16:02:53');
INSERT INTO `admin_operation_log` VALUES ('894', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:03:27', '2017-07-06 16:03:27');
INSERT INTO `admin_operation_log` VALUES ('895', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:03:30', '2017-07-06 16:03:30');
INSERT INTO `admin_operation_log` VALUES ('896', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:03:30', '2017-07-06 16:03:30');
INSERT INTO `admin_operation_log` VALUES ('897', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:03:30', '2017-07-06 16:03:30');
INSERT INTO `admin_operation_log` VALUES ('898', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:03:30', '2017-07-06 16:03:30');
INSERT INTO `admin_operation_log` VALUES ('899', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:04:27', '2017-07-06 16:04:27');
INSERT INTO `admin_operation_log` VALUES ('900', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:05:09', '2017-07-06 16:05:09');
INSERT INTO `admin_operation_log` VALUES ('901', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:05:35', '2017-07-06 16:05:35');
INSERT INTO `admin_operation_log` VALUES ('902', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:07:39', '2017-07-06 16:07:39');
INSERT INTO `admin_operation_log` VALUES ('903', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:08:15', '2017-07-06 16:08:15');
INSERT INTO `admin_operation_log` VALUES ('904', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:11:52', '2017-07-06 16:11:52');
INSERT INTO `admin_operation_log` VALUES ('905', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:12:26', '2017-07-06 16:12:26');
INSERT INTO `admin_operation_log` VALUES ('906', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:13:03', '2017-07-06 16:13:03');
INSERT INTO `admin_operation_log` VALUES ('907', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 16:13:29', '2017-07-06 16:13:29');
INSERT INTO `admin_operation_log` VALUES ('908', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:13:34', '2017-07-06 16:13:34');
INSERT INTO `admin_operation_log` VALUES ('909', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:13:35', '2017-07-06 16:13:35');
INSERT INTO `admin_operation_log` VALUES ('910', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:13:37', '2017-07-06 16:13:37');
INSERT INTO `admin_operation_log` VALUES ('911', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:14:17', '2017-07-06 16:14:17');
INSERT INTO `admin_operation_log` VALUES ('912', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:14:22', '2017-07-06 16:14:22');
INSERT INTO `admin_operation_log` VALUES ('913', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:14:26', '2017-07-06 16:14:26');
INSERT INTO `admin_operation_log` VALUES ('914', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:14:27', '2017-07-06 16:14:27');
INSERT INTO `admin_operation_log` VALUES ('915', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:19:03', '2017-07-06 16:19:03');
INSERT INTO `admin_operation_log` VALUES ('916', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:20:27', '2017-07-06 16:20:27');
INSERT INTO `admin_operation_log` VALUES ('917', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:21:01', '2017-07-06 16:21:01');
INSERT INTO `admin_operation_log` VALUES ('918', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:22:18', '2017-07-06 16:22:18');
INSERT INTO `admin_operation_log` VALUES ('919', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:22:20', '2017-07-06 16:22:20');
INSERT INTO `admin_operation_log` VALUES ('920', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:23:22', '2017-07-06 16:23:22');
INSERT INTO `admin_operation_log` VALUES ('921', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:29:10', '2017-07-06 16:29:10');
INSERT INTO `admin_operation_log` VALUES ('922', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:29:58', '2017-07-06 16:29:58');
INSERT INTO `admin_operation_log` VALUES ('923', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:30:06', '2017-07-06 16:30:06');
INSERT INTO `admin_operation_log` VALUES ('924', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:30:09', '2017-07-06 16:30:09');
INSERT INTO `admin_operation_log` VALUES ('925', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:30:10', '2017-07-06 16:30:10');
INSERT INTO `admin_operation_log` VALUES ('926', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:31:03', '2017-07-06 16:31:03');
INSERT INTO `admin_operation_log` VALUES ('927', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:31:40', '2017-07-06 16:31:40');
INSERT INTO `admin_operation_log` VALUES ('928', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:31:49', '2017-07-06 16:31:49');
INSERT INTO `admin_operation_log` VALUES ('929', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:31:59', '2017-07-06 16:31:59');
INSERT INTO `admin_operation_log` VALUES ('930', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 16:48:26', '2017-07-06 16:48:26');
INSERT INTO `admin_operation_log` VALUES ('931', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 16:57:28', '2017-07-06 16:57:28');
INSERT INTO `admin_operation_log` VALUES ('932', '1', 'admin/auth/logs', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:01:18', '2017-07-06 17:01:18');
INSERT INTO `admin_operation_log` VALUES ('933', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:02:03', '2017-07-06 17:02:03');
INSERT INTO `admin_operation_log` VALUES ('934', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:02:12', '2017-07-06 17:02:12');
INSERT INTO `admin_operation_log` VALUES ('935', '1', 'admin/auth/doctor/23', 'PUT', '196.152.102.158', '{\"name\":\"Asmaa Ali\",\"email\":\"asmaa@gmail.com\",\"gender\":\"F\",\"graduation_year\":\"2011\",\"doctor_level_id\":\"4\",\"doctor_speciality_id\":\"30\",\"phone_confirmation\":\"Not confirmed\",\"Mobile\":\"01226427786\",\"referral_number\":null,\"user_state\":\"2\",\"current_credit\":null,\"short_bio_arabic\":\"\\u00d9\\u0192\\u00d9\\u201e\\u00d8\\u00a7\\u00d9\\u2026 \\u00d8\\u00b9\\u00d8\\u00b1\\u00d8\\u00a8\\u00d9\\u0160\",\"short_bio_english\":\"english txt\",\"city\":\"1\",\"country\":\"EG\",\"neighborhood\":\"agamy\",\"front_card_id\":\"doc_23_2017-04-0414:29:32.jpeg\",\"back_card_id\":null,\"practicing\":null,\"_token\":\"fcMn328CF6P2WL1ehyCoI2hAkJEbFMwSblFT32Kz\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-06 17:02:23', '2017-07-06 17:02:23');
INSERT INTO `admin_operation_log` VALUES ('936', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:02:23', '2017-07-06 17:02:23');
INSERT INTO `admin_operation_log` VALUES ('937', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:03:26', '2017-07-06 17:03:26');
INSERT INTO `admin_operation_log` VALUES ('938', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:05:18', '2017-07-06 17:05:18');
INSERT INTO `admin_operation_log` VALUES ('939', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:05:49', '2017-07-06 17:05:49');
INSERT INTO `admin_operation_log` VALUES ('940', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:05:53', '2017-07-06 17:05:53');
INSERT INTO `admin_operation_log` VALUES ('941', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:07:54', '2017-07-06 17:07:54');
INSERT INTO `admin_operation_log` VALUES ('942', '1', 'admin/auth/doctor/23/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:11:12', '2017-07-06 17:11:12');
INSERT INTO `admin_operation_log` VALUES ('943', '1', 'admin/auth/doctor/23', 'PUT', '196.152.102.158', '{\"name\":\"Asmaa Ali\",\"email\":\"asmaa@gmail.com\",\"gender\":\"F\",\"graduation_year\":\"2011\",\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Pending\",\"referral_number\":null,\"user_state\":\"2\",\"current_credit\":null,\"short_bio_arabic\":\"\\u00d9\\u0192\\u00d9\\u201e\\u00d8\\u00a7\\u00d9\\u2026 \\u00d8\\u00b9\\u00d8\\u00b1\\u00d8\\u00a8\\u00d9\\u0160\",\"short_bio_english\":\"english txt\",\"city\":\"1\",\"country\":\"EG\",\"neighborhood\":\"agamy\",\"front_card_id\":\"doc_23_2017-04-0414:29:32.jpeg\",\"back_card_id\":null,\"practicing\":null,\"_token\":\"fcMn328CF6P2WL1ehyCoI2hAkJEbFMwSblFT32Kz\",\"_method\":\"PUT\"}', '2017-07-06 17:11:22', '2017-07-06 17:11:22');
INSERT INTO `admin_operation_log` VALUES ('944', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-06 17:11:39', '2017-07-06 17:11:39');
INSERT INTO `admin_operation_log` VALUES ('945', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:11:44', '2017-07-06 17:11:44');
INSERT INTO `admin_operation_log` VALUES ('946', '1', 'admin/auth/doctor/11', 'PUT', '196.152.102.158', '{\"name\":\"asmaa\",\"email\":\"asmaa.kohla@gmail.com\",\"gender\":\"M\",\"graduation_year\":\"2011\",\"doctor_level_id\":\"1\",\"doctor_speciality_id\":\"1\",\"phone_confirmation\":\"Not confirmed\",\"referral_number\":null,\"user_state\":\"3\",\"current_credit\":null,\"short_bio_arabic\":null,\"short_bio_english\":null,\"city\":null,\"country\":null,\"neighborhood\":null,\"front_card_id\":null,\"back_card_id\":null,\"practicing\":null,\"_token\":\"fcMn328CF6P2WL1ehyCoI2hAkJEbFMwSblFT32Kz\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/doctor\"}', '2017-07-06 17:11:59', '2017-07-06 17:11:59');
INSERT INTO `admin_operation_log` VALUES ('947', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-06 17:11:59', '2017-07-06 17:11:59');
INSERT INTO `admin_operation_log` VALUES ('948', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:12:38', '2017-07-06 17:12:38');
INSERT INTO `admin_operation_log` VALUES ('949', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:15:58', '2017-07-06 17:15:58');
INSERT INTO `admin_operation_log` VALUES ('950', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 17:16:26', '2017-07-06 17:16:26');
INSERT INTO `admin_operation_log` VALUES ('951', '1', 'admin/auth/doctor/22/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:16:40', '2017-07-06 17:16:40');
INSERT INTO `admin_operation_log` VALUES ('952', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:16:53', '2017-07-06 17:16:53');
INSERT INTO `admin_operation_log` VALUES ('953', '1', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 17:16:57', '2017-07-06 17:16:57');
INSERT INTO `admin_operation_log` VALUES ('954', '2', 'admin', 'GET', '196.152.102.158', '[]', '2017-07-06 20:48:14', '2017-07-06 20:48:14');
INSERT INTO `admin_operation_log` VALUES ('955', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:48:20', '2017-07-06 20:48:20');
INSERT INTO `admin_operation_log` VALUES ('956', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:49:04', '2017-07-06 20:49:04');
INSERT INTO `admin_operation_log` VALUES ('957', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:50:07', '2017-07-06 20:50:07');
INSERT INTO `admin_operation_log` VALUES ('958', '2', 'admin/auth/doctor/32/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:50:20', '2017-07-06 20:50:20');
INSERT INTO `admin_operation_log` VALUES ('959', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:02', '2017-07-06 20:51:02');
INSERT INTO `admin_operation_log` VALUES ('960', '2', 'admin/auth/logout', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:16', '2017-07-06 20:51:16');
INSERT INTO `admin_operation_log` VALUES ('961', '1', 'admin', 'GET', '196.152.102.158', '[]', '2017-07-06 20:51:20', '2017-07-06 20:51:20');
INSERT INTO `admin_operation_log` VALUES ('962', '1', 'admin/auth/doctor', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:26', '2017-07-06 20:51:26');
INSERT INTO `admin_operation_log` VALUES ('963', '1', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:28', '2017-07-06 20:51:28');
INSERT INTO `admin_operation_log` VALUES ('964', '1', 'admin/auth/roles', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:38', '2017-07-06 20:51:38');
INSERT INTO `admin_operation_log` VALUES ('965', '1', 'admin/auth/users', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:49', '2017-07-06 20:51:49');
INSERT INTO `admin_operation_log` VALUES ('966', '1', 'admin/auth/users/2/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-06 20:51:56', '2017-07-06 20:51:56');
INSERT INTO `admin_operation_log` VALUES ('967', '1', 'admin/auth/users/2/edit', 'GET', '196.152.102.158', '[]', '2017-07-06 20:52:01', '2017-07-06 20:52:01');
INSERT INTO `admin_operation_log` VALUES ('968', '1', 'admin/auth/users/2', 'PUT', '196.152.102.158', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"password_confirmation\":\"$2y$10$o\\/hT105royFtaVCtrRrp3eNq5Y5Afr6Q9UJ7\\/k0XBqQ7OKfDN8BS6\",\"roles\":[\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"KiaiYDyUkdYCH4rhgvDwIBcAJ0qBiy6OKIa6xpag\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-06 20:52:12', '2017-07-06 20:52:12');
INSERT INTO `admin_operation_log` VALUES ('969', '1', 'admin/auth/users', 'GET', '196.152.102.158', '[]', '2017-07-06 20:52:16', '2017-07-06 20:52:16');
INSERT INTO `admin_operation_log` VALUES ('970', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-07 01:59:25', '2017-07-07 01:59:25');
INSERT INTO `admin_operation_log` VALUES ('971', '2', 'admin/auth/doctor', 'GET', '196.152.102.158', '[]', '2017-07-07 01:59:36', '2017-07-07 01:59:36');
INSERT INTO `admin_operation_log` VALUES ('972', '2', 'admin/auth/doctor/11/edit', 'GET', '196.152.102.158', '{\"_pjax\":\"#pjax-container\"}', '2017-07-07 02:00:02', '2017-07-07 02:00:02');
INSERT INTO `admin_operation_log` VALUES ('973', '2', 'admin', 'GET', '41.236.206.81', '[]', '2017-07-08 01:01:52', '2017-07-08 01:01:52');
INSERT INTO `admin_operation_log` VALUES ('974', '2', 'admin/auth/doctor', 'GET', '41.236.206.81', '{\"_pjax\":\"#pjax-container\"}', '2017-07-08 01:01:59', '2017-07-08 01:01:59');
INSERT INTO `admin_operation_log` VALUES ('975', '2', 'admin/auth/doctor/11/edit', 'GET', '41.236.206.81', '{\"_pjax\":\"#pjax-container\"}', '2017-07-08 01:02:22', '2017-07-08 01:02:22');
INSERT INTO `admin_operation_log` VALUES ('976', '1', 'admin', 'GET', '154.137.242.143', '[]', '2017-07-10 23:17:21', '2017-07-10 23:17:21');
INSERT INTO `admin_operation_log` VALUES ('977', '1', 'admin/auth/doctor', 'GET', '45.99.242.169', '[]', '2017-07-10 23:22:06', '2017-07-10 23:22:06');
INSERT INTO `admin_operation_log` VALUES ('978', '1', 'admin/auth/permissions', 'GET', '45.99.242.169', '[]', '2017-07-10 23:22:06', '2017-07-10 23:22:06');
INSERT INTO `admin_operation_log` VALUES ('979', '1', 'admin/auth/users', 'GET', '45.99.242.169', '[]', '2017-07-10 23:22:06', '2017-07-10 23:22:06');
INSERT INTO `admin_operation_log` VALUES ('980', '1', 'admin/auth/menu', 'GET', '45.99.242.169', '[]', '2017-07-10 23:22:06', '2017-07-10 23:22:06');
INSERT INTO `admin_operation_log` VALUES ('981', '1', 'admin/auth/roles', 'GET', '45.99.242.169', '[]', '2017-07-10 23:22:06', '2017-07-10 23:22:06');
INSERT INTO `admin_operation_log` VALUES ('982', '2', 'admin', 'GET', '196.155.115.29', '[]', '2017-07-12 00:55:31', '2017-07-12 00:55:31');
INSERT INTO `admin_operation_log` VALUES ('983', '2', 'admin/auth/doctor', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:55:54', '2017-07-12 00:55:54');
INSERT INTO `admin_operation_log` VALUES ('984', '2', 'admin', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:56:53', '2017-07-12 00:56:53');
INSERT INTO `admin_operation_log` VALUES ('985', '2', 'admin', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:56:54', '2017-07-12 00:56:54');
INSERT INTO `admin_operation_log` VALUES ('986', '2', 'admin/auth/doctor', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:57:04', '2017-07-12 00:57:04');
INSERT INTO `admin_operation_log` VALUES ('987', '2', 'admin/auth/doctor', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:57:14', '2017-07-12 00:57:14');
INSERT INTO `admin_operation_log` VALUES ('988', '2', 'admin/auth/login', 'GET', '196.155.115.29', '[]', '2017-07-12 00:58:38', '2017-07-12 00:58:38');
INSERT INTO `admin_operation_log` VALUES ('989', '2', 'admin', 'GET', '196.155.115.29', '[]', '2017-07-12 00:58:46', '2017-07-12 00:58:46');
INSERT INTO `admin_operation_log` VALUES ('990', '2', 'admin/auth/setting', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:59:06', '2017-07-12 00:59:06');
INSERT INTO `admin_operation_log` VALUES ('991', '2', 'admin/auth/setting', 'GET', '196.155.115.29', '[]', '2017-07-12 00:59:12', '2017-07-12 00:59:12');
INSERT INTO `admin_operation_log` VALUES ('992', '2', 'admin/auth/setting', 'GET', '196.155.115.29', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 00:59:13', '2017-07-12 00:59:13');
INSERT INTO `admin_operation_log` VALUES ('993', '2', 'admin/auth/setting', 'GET', '196.155.115.29', '[]', '2017-07-12 00:59:16', '2017-07-12 00:59:16');
INSERT INTO `admin_operation_log` VALUES ('994', '2', 'admin', 'GET', '196.155.115.29', '[]', '2017-07-12 01:19:48', '2017-07-12 01:19:48');
INSERT INTO `admin_operation_log` VALUES ('995', '1', 'admin', 'GET', '197.246.9.12', '[]', '2017-07-12 14:31:43', '2017-07-12 14:31:43');
INSERT INTO `admin_operation_log` VALUES ('996', '1', 'admin/auth/users', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:31:49', '2017-07-12 14:31:49');
INSERT INTO `admin_operation_log` VALUES ('997', '1', 'admin/auth/users/2/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:31:54', '2017-07-12 14:31:54');
INSERT INTO `admin_operation_log` VALUES ('998', '2', 'admin/doctor', 'GET', '197.246.9.12', '[]', '2017-07-12 14:38:20', '2017-07-12 14:38:20');
INSERT INTO `admin_operation_log` VALUES ('999', '2', 'admin/auth/setting', 'GET', '197.246.9.12', '[]', '2017-07-12 14:38:44', '2017-07-12 14:38:44');
INSERT INTO `admin_operation_log` VALUES ('1000', '1', 'admin', 'GET', '105.40.39.13', '[]', '2017-07-12 14:43:44', '2017-07-12 14:43:44');
INSERT INTO `admin_operation_log` VALUES ('1001', '1', 'admin/auth/users', 'GET', '105.40.39.13', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:43:49', '2017-07-12 14:43:49');
INSERT INTO `admin_operation_log` VALUES ('1002', '1', 'admin/auth/users/2/edit', 'GET', '105.40.39.13', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:43:55', '2017-07-12 14:43:55');
INSERT INTO `admin_operation_log` VALUES ('1003', '1', 'admin/auth/users/2', 'PUT', '105.40.39.13', '{\"username\":\"Ahmed\",\"name\":\"Ahmed\",\"password\":\"eARH\'@3M\",\"password_confirmation\":\"eARH\'@3M\",\"roles\":[\"2\",null],\"permissions\":[\"2\",null],\"_token\":\"xpApNkzpqrngKoOjtSM5paXwKVFBy07s2pGbdxSv\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/docadmin.globalhomedoc.com\\/admin\\/auth\\/users\"}', '2017-07-12 14:44:04', '2017-07-12 14:44:04');
INSERT INTO `admin_operation_log` VALUES ('1004', '1', 'admin/auth/users', 'GET', '105.40.39.13', '[]', '2017-07-12 14:44:05', '2017-07-12 14:44:05');
INSERT INTO `admin_operation_log` VALUES ('1005', '2', 'admin/auth/setting', 'GET', '105.40.39.13', '[]', '2017-07-12 14:44:14', '2017-07-12 14:44:14');
INSERT INTO `admin_operation_log` VALUES ('1006', '2', 'admin/auth/logout', 'GET', '105.40.39.13', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:44:20', '2017-07-12 14:44:20');
INSERT INTO `admin_operation_log` VALUES ('1007', '2', 'admin', 'GET', '105.40.39.13', '[]', '2017-07-12 14:44:30', '2017-07-12 14:44:30');
INSERT INTO `admin_operation_log` VALUES ('1008', '1', 'admin/auth/logout', 'GET', '105.40.39.13', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:45:15', '2017-07-12 14:45:15');
INSERT INTO `admin_operation_log` VALUES ('1009', '2', 'admin', 'GET', '196.155.245.227', '[]', '2017-07-12 14:46:51', '2017-07-12 14:46:51');
INSERT INTO `admin_operation_log` VALUES ('1010', '2', 'admin', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:47:15', '2017-07-12 14:47:15');
INSERT INTO `admin_operation_log` VALUES ('1011', '2', 'admin', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:47:20', '2017-07-12 14:47:20');
INSERT INTO `admin_operation_log` VALUES ('1012', '2', 'admin/auth/doctor', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:47:24', '2017-07-12 14:47:24');
INSERT INTO `admin_operation_log` VALUES ('1013', '2', 'admin', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:48:17', '2017-07-12 14:48:17');
INSERT INTO `admin_operation_log` VALUES ('1014', '2', 'admin/auth/doctor', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:48:24', '2017-07-12 14:48:24');
INSERT INTO `admin_operation_log` VALUES ('1015', '2', 'admin/auth/doctor', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\",\"id\":\"22\"}', '2017-07-12 14:48:42', '2017-07-12 14:48:42');
INSERT INTO `admin_operation_log` VALUES ('1016', '2', 'admin/auth/doctor', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\",\"id\":null}', '2017-07-12 14:48:48', '2017-07-12 14:48:48');
INSERT INTO `admin_operation_log` VALUES ('1017', '2', 'admin/auth/doctor', 'GET', '196.155.245.227', '{\"_pjax\":\"#pjax-container\"}', '2017-07-12 14:48:49', '2017-07-12 14:48:49');
INSERT INTO `admin_operation_log` VALUES ('1018', '2', 'admin', 'GET', '196.154.81.228', '[]', '2017-07-25 15:21:05', '2017-07-25 15:21:05');
INSERT INTO `admin_operation_log` VALUES ('1019', '2', 'admin/auth/doctor', 'GET', '196.154.81.228', '{\"_pjax\":\"#pjax-container\"}', '2017-07-25 15:21:18', '2017-07-25 15:21:18');
INSERT INTO `admin_operation_log` VALUES ('1020', '2', 'admin/auth/logout', 'GET', '196.154.81.228', '{\"_pjax\":\"#pjax-container\"}', '2017-07-25 15:21:26', '2017-07-25 15:21:26');
INSERT INTO `admin_operation_log` VALUES ('1021', '2', 'admin', 'GET', '154.136.166.229', '[]', '2017-08-15 17:06:09', '2017-08-15 17:06:09');
INSERT INTO `admin_operation_log` VALUES ('1022', '2', 'admin', 'GET', '196.152.12.105', '[]', '2017-08-15 17:13:44', '2017-08-15 17:13:44');
INSERT INTO `admin_operation_log` VALUES ('1023', '2', 'admin', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:00', '2017-08-15 17:14:00');
INSERT INTO `admin_operation_log` VALUES ('1024', '2', 'admin', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:00', '2017-08-15 17:14:00');
INSERT INTO `admin_operation_log` VALUES ('1025', '2', 'admin/auth/doctor', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:03', '2017-08-15 17:14:03');
INSERT INTO `admin_operation_log` VALUES ('1026', '2', 'admin/auth/doctor', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:04', '2017-08-15 17:14:04');
INSERT INTO `admin_operation_log` VALUES ('1027', '2', 'admin', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:12', '2017-08-15 17:14:12');
INSERT INTO `admin_operation_log` VALUES ('1028', '2', 'admin/auth/doctor', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:17', '2017-08-15 17:14:17');
INSERT INTO `admin_operation_log` VALUES ('1029', '2', 'admin/auth/doctor', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:32', '2017-08-15 17:14:32');
INSERT INTO `admin_operation_log` VALUES ('1030', '2', 'admin/auth/doctor/create', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:44', '2017-08-15 17:14:44');
INSERT INTO `admin_operation_log` VALUES ('1031', '2', 'admin/auth/doctor', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:45', '2017-08-15 17:14:45');
INSERT INTO `admin_operation_log` VALUES ('1032', '2', 'admin/auth/doctor/create', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:46', '2017-08-15 17:14:46');
INSERT INTO `admin_operation_log` VALUES ('1033', '2', 'admin/auth/doctor/create', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:50', '2017-08-15 17:14:50');
INSERT INTO `admin_operation_log` VALUES ('1034', '2', 'admin/auth/doctor', 'GET', '154.136.166.229', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:14:59', '2017-08-15 17:14:59');
INSERT INTO `admin_operation_log` VALUES ('1035', '2', 'admin/auth/login', 'GET', '196.152.12.105', '[]', '2017-08-15 17:38:27', '2017-08-15 17:38:27');
INSERT INTO `admin_operation_log` VALUES ('1036', '2', 'admin', 'GET', '196.152.12.105', '[]', '2017-08-15 17:38:27', '2017-08-15 17:38:27');
INSERT INTO `admin_operation_log` VALUES ('1037', '2', 'admin/auth/doctor', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:38:31', '2017-08-15 17:38:31');
INSERT INTO `admin_operation_log` VALUES ('1038', '2', 'admin/auth/doctor/create', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:39:02', '2017-08-15 17:39:02');
INSERT INTO `admin_operation_log` VALUES ('1039', '2', 'admin', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:39:24', '2017-08-15 17:39:24');
INSERT INTO `admin_operation_log` VALUES ('1040', '2', 'admin/auth/doctor', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:39:28', '2017-08-15 17:39:28');
INSERT INTO `admin_operation_log` VALUES ('1041', '2', 'admin', 'GET', '196.152.12.105', '{\"_pjax\":\"#pjax-container\"}', '2017-08-15 17:47:49', '2017-08-15 17:47:49');
INSERT INTO `admin_operation_log` VALUES ('1042', '2', 'admin', 'GET', '196.145.126.129', '[]', '2017-08-16 13:27:06', '2017-08-16 13:27:06');
INSERT INTO `admin_operation_log` VALUES ('1043', '2', 'admin', 'GET', '45.107.20.219', '[]', '2017-08-16 13:51:56', '2017-08-16 13:51:56');
INSERT INTO `admin_operation_log` VALUES ('1044', '2', 'admin/auth/doctor', 'GET', '45.107.20.219', '{\"_pjax\":\"#pjax-container\"}', '2017-08-16 13:52:19', '2017-08-16 13:52:19');
INSERT INTO `admin_operation_log` VALUES ('1045', '2', 'admin', 'GET', '41.232.211.57', '[]', '2017-08-17 13:26:20', '2017-08-17 13:26:20');
INSERT INTO `admin_operation_log` VALUES ('1046', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:26:30', '2017-08-17 13:26:30');
INSERT INTO `admin_operation_log` VALUES ('1047', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:26:54', '2017-08-17 13:26:54');
INSERT INTO `admin_operation_log` VALUES ('1048', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 13:27:10', '2017-08-17 13:27:10');
INSERT INTO `admin_operation_log` VALUES ('1049', '2', 'admin', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:27:43', '2017-08-17 13:27:43');
INSERT INTO `admin_operation_log` VALUES ('1050', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:27:50', '2017-08-17 13:27:50');
INSERT INTO `admin_operation_log` VALUES ('1051', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 13:28:09', '2017-08-17 13:28:09');
INSERT INTO `admin_operation_log` VALUES ('1052', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2017-08-17 13:29:38', '2017-08-17 13:29:38');
INSERT INTO `admin_operation_log` VALUES ('1053', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 13:29:52', '2017-08-17 13:29:52');
INSERT INTO `admin_operation_log` VALUES ('1054', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2017-08-17 13:36:49', '2017-08-17 13:36:49');
INSERT INTO `admin_operation_log` VALUES ('1055', '2', 'admin/auth/doctor/268/edit', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:37:10', '2017-08-17 13:37:10');
INSERT INTO `admin_operation_log` VALUES ('1056', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 13:37:22', '2017-08-17 13:37:22');
INSERT INTO `admin_operation_log` VALUES ('1057', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 13:37:25', '2017-08-17 13:37:25');
INSERT INTO `admin_operation_log` VALUES ('1058', '2', 'admin/auth/doctor', 'GET', '41.232.211.57', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\",\"_export_\":\"1\"}', '2017-08-17 13:37:38', '2017-08-17 13:37:38');
INSERT INTO `admin_operation_log` VALUES ('1059', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\"}', '2017-08-17 15:37:16', '2017-08-17 15:37:16');
INSERT INTO `admin_operation_log` VALUES ('1060', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:37:25', '2017-08-17 15:37:25');
INSERT INTO `admin_operation_log` VALUES ('1061', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:37:27', '2017-08-17 15:37:27');
INSERT INTO `admin_operation_log` VALUES ('1062', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:37:36', '2017-08-17 15:37:36');
INSERT INTO `admin_operation_log` VALUES ('1063', '2', 'admin/auth/doctor/283/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:38:10', '2017-08-17 15:38:10');
INSERT INTO `admin_operation_log` VALUES ('1064', '2', 'admin', 'GET', '41.41.25.5', '[]', '2017-08-17 15:45:34', '2017-08-17 15:45:34');
INSERT INTO `admin_operation_log` VALUES ('1065', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:45:39', '2017-08-17 15:45:39');
INSERT INTO `admin_operation_log` VALUES ('1066', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:45:46', '2017-08-17 15:45:46');
INSERT INTO `admin_operation_log` VALUES ('1067', '2', 'admin/auth/doctor/283/edit', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:45:49', '2017-08-17 15:45:49');
INSERT INTO `admin_operation_log` VALUES ('1068', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:46:02', '2017-08-17 15:46:02');
INSERT INTO `admin_operation_log` VALUES ('1069', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2017-08-17 15:46:10', '2017-08-17 15:46:10');
INSERT INTO `admin_operation_log` VALUES ('1070', '2', 'admin/auth/doctor/11/edit', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:46:13', '2017-08-17 15:46:13');
INSERT INTO `admin_operation_log` VALUES ('1071', '2', 'admin', 'GET', '41.232.201.91', '[]', '2017-08-17 15:46:50', '2017-08-17 15:46:50');
INSERT INTO `admin_operation_log` VALUES ('1072', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:47:20', '2017-08-17 15:47:20');
INSERT INTO `admin_operation_log` VALUES ('1073', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:47:24', '2017-08-17 15:47:24');
INSERT INTO `admin_operation_log` VALUES ('1074', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:47:32', '2017-08-17 15:47:32');
INSERT INTO `admin_operation_log` VALUES ('1075', '2', 'admin/auth/doctor', 'GET', '41.41.25.5', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:47:34', '2017-08-17 15:47:34');
INSERT INTO `admin_operation_log` VALUES ('1076', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:47:36', '2017-08-17 15:47:36');
INSERT INTO `admin_operation_log` VALUES ('1077', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2017-08-17 15:47:47', '2017-08-17 15:47:47');
INSERT INTO `admin_operation_log` VALUES ('1078', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:49:06', '2017-08-17 15:49:06');
INSERT INTO `admin_operation_log` VALUES ('1079', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:49:09', '2017-08-17 15:49:09');
INSERT INTO `admin_operation_log` VALUES ('1080', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\",\"id\":\"Ahmed\"}', '2017-08-17 15:49:23', '2017-08-17 15:49:23');
INSERT INTO `admin_operation_log` VALUES ('1081', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 15:53:12', '2017-08-17 15:53:12');
INSERT INTO `admin_operation_log` VALUES ('1082', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\",\"page\":\"1\"}', '2017-08-17 15:53:17', '2017-08-17 15:53:17');
INSERT INTO `admin_operation_log` VALUES ('1083', '2', 'admin/auth/doctor/276/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:53:26', '2017-08-17 15:53:26');
INSERT INTO `admin_operation_log` VALUES ('1084', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"1\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:53:32', '2017-08-17 15:53:32');
INSERT INTO `admin_operation_log` VALUES ('1085', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 15:53:34', '2017-08-17 15:53:34');
INSERT INTO `admin_operation_log` VALUES ('1086', '2', 'admin/auth/doctor', 'GET', '196.154.40.248', '{\"page\":\"2\"}', '2017-08-17 16:06:51', '2017-08-17 16:06:51');
INSERT INTO `admin_operation_log` VALUES ('1087', '2', 'admin/auth/doctor/286/edit', 'GET', '196.154.40.248', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:07:06', '2017-08-17 16:07:06');
INSERT INTO `admin_operation_log` VALUES ('1088', '2', 'admin/auth/doctor/286/edit', 'GET', '196.154.40.248', '[]', '2017-08-17 16:10:07', '2017-08-17 16:10:07');
INSERT INTO `admin_operation_log` VALUES ('1089', '2', 'admin/auth/doctor', 'GET', '196.154.40.248', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:10:36', '2017-08-17 16:10:36');
INSERT INTO `admin_operation_log` VALUES ('1090', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:10:53', '2017-08-17 16:10:53');
INSERT INTO `admin_operation_log` VALUES ('1091', '2', 'admin/auth/doctor/285/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:11:03', '2017-08-17 16:11:03');
INSERT INTO `admin_operation_log` VALUES ('1092', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:11:13', '2017-08-17 16:11:13');
INSERT INTO `admin_operation_log` VALUES ('1093', '2', 'admin/auth/doctor/286/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:11:18', '2017-08-17 16:11:18');
INSERT INTO `admin_operation_log` VALUES ('1094', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:11:25', '2017-08-17 16:11:25');
INSERT INTO `admin_operation_log` VALUES ('1095', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '[]', '2017-08-17 16:13:18', '2017-08-17 16:13:18');
INSERT INTO `admin_operation_log` VALUES ('1096', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:13:25', '2017-08-17 16:13:25');
INSERT INTO `admin_operation_log` VALUES ('1097', '2', 'admin/auth/doctor/284/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:13:31', '2017-08-17 16:13:31');
INSERT INTO `admin_operation_log` VALUES ('1098', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:13:37', '2017-08-17 16:13:37');
INSERT INTO `admin_operation_log` VALUES ('1099', '2', 'admin/auth/doctor/286/edit', 'GET', '41.232.201.91', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:13:42', '2017-08-17 16:13:42');
INSERT INTO `admin_operation_log` VALUES ('1100', '2', 'admin/auth/doctor', 'GET', '41.232.201.91', '{\"page\":\"2\",\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:14:12', '2017-08-17 16:14:12');
INSERT INTO `admin_operation_log` VALUES ('1101', '2', 'admin/auth/doctor', 'GET', '41.232.174.117', '[]', '2017-08-17 16:53:05', '2017-08-17 16:53:05');
INSERT INTO `admin_operation_log` VALUES ('1102', '2', 'admin/auth/doctor', 'GET', '41.232.174.117', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 16:53:14', '2017-08-17 16:53:14');
INSERT INTO `admin_operation_log` VALUES ('1103', '2', 'admin/auth/doctor', 'GET', '45.106.231.24', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-17 16:55:23', '2017-08-17 16:55:23');
INSERT INTO `admin_operation_log` VALUES ('1104', '2', 'admin/auth/doctor/285/edit', 'GET', '45.106.231.24', '[]', '2017-08-17 16:55:46', '2017-08-17 16:55:46');
INSERT INTO `admin_operation_log` VALUES ('1105', '2', 'admin/auth/doctor/285/edit', 'GET', '45.106.231.24', '[]', '2017-08-17 16:55:56', '2017-08-17 16:55:56');
INSERT INTO `admin_operation_log` VALUES ('1106', '2', 'admin/auth/doctor', 'GET', '45.106.231.24', '{\"page\":\"2\"}', '2017-08-17 17:02:10', '2017-08-17 17:02:10');
INSERT INTO `admin_operation_log` VALUES ('1107', '2', 'admin/auth/doctor/285/edit', 'GET', '45.106.231.24', '{\"_pjax\":\"#pjax-container\"}', '2017-08-17 17:06:00', '2017-08-17 17:06:00');
INSERT INTO `admin_operation_log` VALUES ('1108', '2', 'admin', 'GET', '197.246.9.12', '[]', '2017-08-20 16:56:16', '2017-08-20 16:56:16');
INSERT INTO `admin_operation_log` VALUES ('1109', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-08-20 16:56:27', '2017-08-20 16:56:27');
INSERT INTO `admin_operation_log` VALUES ('1110', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-08-20 16:56:29', '2017-08-20 16:56:29');
INSERT INTO `admin_operation_log` VALUES ('1111', '2', 'admin/auth/doctor', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\",\"page\":\"2\"}', '2017-08-20 16:56:37', '2017-08-20 16:56:37');
INSERT INTO `admin_operation_log` VALUES ('1112', '2', 'admin/auth/doctor/287/edit', 'GET', '197.246.9.12', '{\"_pjax\":\"#pjax-container\"}', '2017-08-20 16:56:42', '2017-08-20 16:56:42');

-- ----------------------------
-- Table structure for `admin_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_permissions
-- ----------------------------
INSERT INTO `admin_permissions` VALUES ('1', 'admin', 'admin', '2017-06-03 07:17:38', '2017-07-05 18:09:03');
INSERT INTO `admin_permissions` VALUES ('2', 'call center', 'callcenter', '2017-07-05 18:09:17', '2017-07-05 18:09:17');
INSERT INTO `admin_permissions` VALUES ('3', 'Master', 'Master', '2017-07-05 20:24:07', '2017-07-05 20:24:07');

-- ----------------------------
-- Table structure for `admin_roles`
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES ('1', 'Administrator', 'administrator', '2017-06-03 05:53:16', '2017-06-03 05:53:16');
INSERT INTO `admin_roles` VALUES ('2', 'call center', 'callcenter', '2017-07-05 18:09:42', '2017-07-05 18:09:42');
INSERT INTO `admin_roles` VALUES ('3', 'Master', 'Master', '2017-07-05 20:24:28', '2017-07-05 20:24:28');

-- ----------------------------
-- Table structure for `admin_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES ('2', '15', null, null);
INSERT INTO `admin_role_menu` VALUES ('1', '2', null, null);
INSERT INTO `admin_role_menu` VALUES ('1', '8', null, null);

-- ----------------------------
-- Table structure for `admin_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES ('2', '1', null, null);
INSERT INTO `admin_role_permissions` VALUES ('2', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '1', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '2', null, null);
INSERT INTO `admin_role_permissions` VALUES ('3', '3', null, null);

-- ----------------------------
-- Table structure for `admin_role_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_role_users
-- ----------------------------
INSERT INTO `admin_role_users` VALUES ('1', '1', null, null);
INSERT INTO `admin_role_users` VALUES ('1', '1', null, null);
INSERT INTO `admin_role_users` VALUES ('2', '2', null, null);
INSERT INTO `admin_role_users` VALUES ('3', '1', null, null);

-- ----------------------------
-- Table structure for `admin_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES ('1', 'admin', '$2y$10$z3hOXAMcP8fpJqTZy8B8/OtWuiZLcUrqw9CQGjEdAz285B.IAerE.', 'Administrator', null, 'yGKjfcjHfFRatiLYo93J07e3rzCTobbL1QlJgqCE72BjOpBATfIBy1NfWIUl', '2017-06-03 05:53:15', '2017-06-03 05:53:15');
INSERT INTO `admin_users` VALUES ('2', 'Ahmed', '$2y$10$aWh8We3FoO8/qpCL/lz19uaSLZQH5VN518u7NVVm/gM/o5r.eNLGm', 'Ahmed', null, 'B8cd34iLs6ofyQpnh8DR7McgAPXzg61n0lFA0Kycj70SSz5hIwnNLv8KoPac', '2017-07-05 18:10:23', '2017-07-12 14:44:04');

-- ----------------------------
-- Table structure for `admin_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_user_permissions
-- ----------------------------
INSERT INTO `admin_user_permissions` VALUES ('2', '2', null, null);

-- ----------------------------
-- Table structure for `apps_cities`
-- ----------------------------
DROP TABLE IF EXISTS `apps_cities`;
CREATE TABLE `apps_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `city_ar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apps_cities
-- ----------------------------
INSERT INTO `apps_cities` VALUES ('1', 'EG', 'Cairo', 'القاهرة');
INSERT INTO `apps_cities` VALUES ('2', 'EG', 'Alexandria', 'الإسكندرية');
INSERT INTO `apps_cities` VALUES ('3', 'EG', 'Asyut', 'أسيوط');
INSERT INTO `apps_cities` VALUES ('4', 'EG', 'Beheira', 'البحيرة');
INSERT INTO `apps_cities` VALUES ('5', 'EG', 'Beheira', 'البحيرة');
INSERT INTO `apps_cities` VALUES ('6', 'EG', 'Beni Suef', 'بني سويف');
INSERT INTO `apps_cities` VALUES ('8', 'EG', 'Dakahlia', 'الدقهلية');
INSERT INTO `apps_cities` VALUES ('9', 'EG', 'Damietta', 'دمياط');
INSERT INTO `apps_cities` VALUES ('10', 'EG', 'Faiyum', 'الفيوم');
INSERT INTO `apps_cities` VALUES ('11', 'EG', 'Gharbia', 'الغربية');
INSERT INTO `apps_cities` VALUES ('12', 'EG', 'Giza', 'الجيزة');
INSERT INTO `apps_cities` VALUES ('13', 'EG', 'Ismailia', 'الإسماعيلية');
INSERT INTO `apps_cities` VALUES ('14', 'EG', 'Kafr el-Sheikh', 'كفر الشيخ');
INSERT INTO `apps_cities` VALUES ('15', 'EG', 'Luxor', 'الأقصر');
INSERT INTO `apps_cities` VALUES ('16', 'EG', 'Matrouh', 'مطروح');
INSERT INTO `apps_cities` VALUES ('17', 'EG', 'Minya', 'المنيا');
INSERT INTO `apps_cities` VALUES ('18', 'EG', 'Monufia', 'المنوفية');
INSERT INTO `apps_cities` VALUES ('19', 'EG', 'North Sinai', 'شمال سيناء');
INSERT INTO `apps_cities` VALUES ('20', 'EG', 'Port Said', 'بورسعيد');
INSERT INTO `apps_cities` VALUES ('21', 'EG', 'Qalyubia', 'القليوبية');
INSERT INTO `apps_cities` VALUES ('22', 'EG', 'Qena', 'قنا');
INSERT INTO `apps_cities` VALUES ('23', 'EG', 'Qena', 'قنا');
INSERT INTO `apps_cities` VALUES ('24', 'EG', 'Red Sea', 'البحر الاحمر');
INSERT INTO `apps_cities` VALUES ('25', 'EG', 'Sharqia', 'الشرقية');
INSERT INTO `apps_cities` VALUES ('26', 'EG', 'Sohag', 'سوهاج');
INSERT INTO `apps_cities` VALUES ('27', 'EG', 'Suez', 'السويس');
INSERT INTO `apps_cities` VALUES ('29', 'EG', 'Aswan', 'أسوان');

-- ----------------------------
-- Table structure for `centers`
-- ----------------------------
DROP TABLE IF EXISTS `centers`;
CREATE TABLE `centers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `map_location` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `serive_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of centers
-- ----------------------------
INSERT INTO `centers` VALUES ('1', 'فرع الهانوفيل', '      برج درة الخليل - ش الهانوفيل,اعلى سنترال الخليل العجمى, الاسكندرية', '29.918739,31.200092', '2017-06-03 18:09:33', '2017-06-03 18:09:33', '0');
INSERT INTO `centers` VALUES ('2', 'فرع  سموحة', '14 ش مصطفى كامل تقاطع ش بهاء الدين الغتورى,بجوار مستشفى اسكندرية الدولى سموحة, الاسكندرية', '29.918739,31.200092', '2017-06-03 18:09:33', '2017-06-03 18:09:33', '0');
INSERT INTO `centers` VALUES ('3', 'فرع ابراج السرايا', '16 ابراج السرايا - - الدور 1,بجوار المركز التخصصى العالمى لعلاج الاورام سموحة, الاسكندرية ', '29.918739,31.200092', '2017-06-03 18:09:33', '2017-06-03 18:09:33', '0');
INSERT INTO `centers` VALUES ('4', 'محرم بك', '58 ش الرصافة,خلف مستشفى النزهة الجديدة محرم بك, الاسكندرية ', '29.918739,31.200092', '2017-08-01 06:00:00', '2017-08-01 06:00:00', '0');
INSERT INTO `centers` VALUES ('5', 'فرع محطة الرمل', '8 ش كلية الطب,بجوار صيدلية اكسفورد محطة الرمل, الاسكندرية ', '29.918739,31.200092', '2017-08-01 06:00:00', '2017-08-01 06:00:00', '0');
INSERT INTO `centers` VALUES ('8', 'فرع محطة الرمل ش السلطان حسين', '                                     4 ش امين فكرى متفرع من ش السلطان حسين, محطة الرمل, الاسكندرية                                                                            ', '29.918739,31.200092', null, null, '0');

-- ----------------------------
-- Table structure for `center_phones`
-- ----------------------------
DROP TABLE IF EXISTS `center_phones`;
CREATE TABLE `center_phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `center_id` int(10) unsigned NOT NULL,
  `phone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `center_phones_center_id_foreign` (`center_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of center_phones
-- ----------------------------
INSERT INTO `center_phones` VALUES ('1', '8', '03-4878027', '2017-06-03 14:09:33', '2017-06-03 14:09:33');
INSERT INTO `center_phones` VALUES ('2', '2', '0166623581', '2017-06-03 14:09:33', '2017-06-03 14:09:33');
INSERT INTO `center_phones` VALUES ('3', '3', '0 3 4266656', '2017-06-03 14:09:34', '2017-06-03 14:09:34');
INSERT INTO `center_phones` VALUES ('4', '4', '01066623636', '2017-06-03 14:09:34', '2017-06-03 14:09:34');
INSERT INTO `center_phones` VALUES ('5', '5', '0 3 4266656', null, null);
INSERT INTO `center_phones` VALUES ('6', '8', '03-4878021', null, null);
INSERT INTO `center_phones` VALUES ('7', '1', '03-4390022 ', null, null);

-- ----------------------------
-- Table structure for `center_report`
-- ----------------------------
DROP TABLE IF EXISTS `center_report`;
CREATE TABLE `center_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `footer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of center_report
-- ----------------------------

-- ----------------------------
-- Table structure for `center_responses`
-- ----------------------------
DROP TABLE IF EXISTS `center_responses`;
CREATE TABLE `center_responses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_est_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_id` int(10) unsigned NOT NULL,
  `radiology_datetime` timestamp NULL DEFAULT NULL,
  `is_at_home` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `center_responses_request_id_foreign` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of center_responses
-- ----------------------------
INSERT INTO `center_responses` VALUES ('184', '2017-08-11 07:00:00', '69', '2017-08-03 07:00:00', '0', '2017-08-02 13:44:57', '2017-08-02 13:44:57');
INSERT INTO `center_responses` VALUES ('185', '2017-08-18 07:00:00', '69', '2017-08-03 09:00:00', '0', '2017-08-02 13:47:40', '2017-08-02 13:47:40');
INSERT INTO `center_responses` VALUES ('186', '2017-08-11 07:00:00', '69', '2017-08-03 07:00:00', '0', '2017-08-02 14:01:49', '2017-08-02 14:01:49');
INSERT INTO `center_responses` VALUES ('187', '2017-08-14 07:00:00', '70', '2017-08-03 07:00:00', '1', '2017-08-02 18:05:52', '2017-08-02 18:05:52');
INSERT INTO `center_responses` VALUES ('188', '2017-08-15 07:00:00', '72', '2017-08-08 07:00:00', '1', '2017-08-07 15:47:35', '2017-08-07 15:47:35');
INSERT INTO `center_responses` VALUES ('189', '2017-08-10 07:00:00', '73', '2017-08-08 07:00:00', '1', '2017-08-07 19:42:39', '2017-08-07 19:42:39');
INSERT INTO `center_responses` VALUES ('190', '2017-08-19 19:00:00', '74', '2017-08-08 07:00:00', '1', '2017-08-07 21:24:51', '2017-08-07 21:24:51');
INSERT INTO `center_responses` VALUES ('191', '2017-08-11 07:00:00', '78', '2017-08-09 07:00:00', '1', '2017-08-08 15:41:19', '2017-08-08 15:41:19');
INSERT INTO `center_responses` VALUES ('192', '2017-08-11 07:00:00', '79', '2017-08-09 07:00:00', '1', '2017-08-08 16:52:21', '2017-08-08 16:52:21');
INSERT INTO `center_responses` VALUES ('193', '2017-08-15 09:05:00', '80', '2017-08-09 09:02:00', '1', '2017-08-08 18:32:02', '2017-08-08 18:32:02');
INSERT INTO `center_responses` VALUES ('194', '2017-08-11 09:01:00', '80', '2017-08-09 08:00:00', '1', '2017-08-08 18:40:46', '2017-08-08 18:40:46');
INSERT INTO `center_responses` VALUES ('195', '2017-08-17 22:04:00', '81', '2017-08-09 10:05:00', '1', '2017-08-08 18:45:11', '2017-08-08 18:45:11');
INSERT INTO `center_responses` VALUES ('196', '2017-08-17 19:59:00', '82', '2017-08-16 19:59:00', '1', '2017-08-16 15:06:04', '2017-08-16 15:06:04');

-- ----------------------------
-- Table structure for `chat`
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES ('1', '16', '1', '0', '0', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('2', '16', '1', '0', '0', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('3', '16', '1', '0', '0', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('4', '16', '1', '0', '0', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('5', '16', '1', '0', '3', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('6', '16', '1', '0', '16', '0000-00-00 00:00:00');
INSERT INTO `chat` VALUES ('7', '16', '1', '0', '255', '2017-07-20 10:57:24');
INSERT INTO `chat` VALUES ('8', '128', '22', '0', '257', '2017-07-20 10:58:55');
INSERT INTO `chat` VALUES ('9', '16', '11', '0', '260', '2017-08-10 15:41:27');
INSERT INTO `chat` VALUES ('10', '16', '11', '0', '261', '2017-08-10 15:42:42');
INSERT INTO `chat` VALUES ('11', '16', '11', '0', '262', '2017-08-10 15:43:30');
INSERT INTO `chat` VALUES ('12', '16', '11', '0', '263', '2017-08-10 15:44:05');
INSERT INTO `chat` VALUES ('13', '16', '11', '0', '264', '2017-08-10 15:46:55');
INSERT INTO `chat` VALUES ('14', '16', '11', '0', '265', '2017-08-10 15:47:33');
INSERT INTO `chat` VALUES ('15', '16', '11', '0', '266', '2017-08-10 15:48:17');
INSERT INTO `chat` VALUES ('16', '128', '22', '0', '281', '2017-08-16 14:03:06');
INSERT INTO `chat` VALUES ('17', '3', '287', '0', '7', '2017-09-09 20:07:13');
INSERT INTO `chat` VALUES ('18', '3', '287', '0', '8', '2017-09-09 20:07:19');

-- ----------------------------
-- Table structure for `complaints`
-- ----------------------------
DROP TABLE IF EXISTS `complaints`;
CREATE TABLE `complaints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient` int(11) NOT NULL,
  `complaint_details` varchar(255) NOT NULL,
  `number_of_recordings` varchar(255) NOT NULL,
  `complain_recording` varchar(255) NOT NULL,
  `complaint_index` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of complaints
-- ----------------------------
INSERT INTO `complaints` VALUES ('1', '16', 'werwerwr', 'sffsfsdfetailssfgsfg', 'complain_recording_16_2017-08-0706:22:56.mp3', '1');
INSERT INTO `complaints` VALUES ('2', '16', 'werwerwr', 'sffsfsdfetails=sfgsfg', '', '');
INSERT INTO `complaints` VALUES ('3', '16', 'werwerwr', 'sffsfsdfetails=sfgsfg', '', '');
INSERT INTO `complaints` VALUES ('4', '16', 'werwerwr', 'sffsfsdfetails=sfgsfg', '', '');
INSERT INTO `complaints` VALUES ('5', '16', 'werwerwr', 'sffsfsdfetails=sfgsfg', '', '');
INSERT INTO `complaints` VALUES ('6', '128', 'test', '1', '', '');
INSERT INTO `complaints` VALUES ('7', '128', 'tttr', '1', '', '');
INSERT INTO `complaints` VALUES ('8', '128', 'rrr', '0', '', '');
INSERT INTO `complaints` VALUES ('9', '128', 'rttt', '1', '', '');
INSERT INTO `complaints` VALUES ('10', '16', 'werwerwr', '', '', '');
INSERT INTO `complaints` VALUES ('11', '161', 'tet', '', 'complaint_161_1504106297.3gp', '');
INSERT INTO `complaints` VALUES ('12', '170', '123', '', '', '');
INSERT INTO `complaints` VALUES ('13', '16', 'sfgsfg', '', '', '');
INSERT INTO `complaints` VALUES ('14', '16', 'sfgsfg', '', '', '');
INSERT INTO `complaints` VALUES ('15', '16', 'sfgsfg', '', '', '');
INSERT INTO `complaints` VALUES ('16', '16', 'sfgsfg', '', '', '');
INSERT INTO `complaints` VALUES ('17', '16', 'sfgsfg', '', '', '');
INSERT INTO `complaints` VALUES ('18', '160', '', '', 'complaint_160_1504783180.3gp', '');

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_working_version` varchar(45) DEFAULT NULL,
  `newest_version` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', '1.0', '1');

-- ----------------------------
-- Table structure for `contact_us`
-- ----------------------------
DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL DEFAULT '0',
  `message_details` varchar(255) NOT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contact_us
-- ----------------------------
INSERT INTO `contact_us` VALUES ('0', 'werwerwr', '16');

-- ----------------------------
-- Table structure for `demo`
-- ----------------------------
DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo
-- ----------------------------

-- ----------------------------
-- Table structure for `doctor`
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `user_state` varchar(45) DEFAULT NULL,
  `current_credit` varchar(45) DEFAULT NULL,
  `profile_img` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `graduation_year` varchar(45) DEFAULT NULL,
  `short_bio_arabic` mediumtext NOT NULL,
  `short_bio_english` mediumtext NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `neighborhood` varchar(255) NOT NULL,
  `doctor_level_id` int(11) NOT NULL,
  `doctor_speciality_id` int(11) NOT NULL,
  `front_card_id` varchar(255) NOT NULL,
  `back_card_id` varchar(255) NOT NULL,
  `master` varchar(255) NOT NULL,
  `phd` varchar(255) NOT NULL,
  `referral_number` varchar(255) NOT NULL,
  `practicing` varchar(255) NOT NULL,
  `doctor_identification_number` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone_confirmation` varchar(255) NOT NULL,
  `home_visit_price` varchar(255) NOT NULL,
  `chat_price` varchar(255) NOT NULL,
  `voice_call_price` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor
-- ----------------------------
INSERT INTO `doctor` VALUES ('11', 'asmaa', 'asmaa.kohla@gmail.com', 'M', '8', null, 'patiet_11_1502726935.jpg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'ar', 'en', '', '', '', '4', '39', 'doctor_11_1502793917.jpg', 'doctor_11_1502794017.jpg', 'doctor_11_1502794172.jpg', 'doctor_11_1502794347.jpg', 'asmaad123', '', '5652355', '0000-00-00 00:00:00', '2017-07-06 17:11:59', 'Not confirmed', '90', '77', '88');
INSERT INTO `doctor` VALUES ('22', 'Asmaa Ali', 'asmaa.kohla@yahoo.com', 'M', '2', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '', '', '', '', '', '1', '1', 'doc_22_2017-04-0414:23:04.jpg', '', '', '', '', '', '', '0000-00-00 00:00:00', '2017-07-05 18:33:18', 'Pending', '', '', '');
INSERT INTO `doctor` VALUES ('23', 'Asmaa Ali', 'asmaa@gmail.com', 'F', '2', null, 'doc_23_2017-04-0414:36:31.jpg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'ÙƒÙ„Ø§Ù… Ø¹Ø±Ø¨ÙŠ', 'english txt', '1', 'EG', 'agamy', '1', '1', 'doc_23_2017-04-0414:29:32.jpeg', '', '', '', '', '', '', '0000-00-00 00:00:00', '2017-07-06 17:11:22', 'Pending', '', '', '');
INSERT INTO `doctor` VALUES ('32', 'Mohamed Metwally', 'metwally@gmail.com', 'M', '3', null, 'doc_32_2017-04-2501:16:30.jpg', 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2000', 'Ø§Ù„Ø¯ÙƒØªÙˆØ± Ù…Ø­Ù…Ø¯ Ù…ØªÙˆÙ„ÙŠ', 'Dr. Mohamed Metwally', '1', 'EG', 'Shobra', '2', '30', 'doc_32_2017-04-2501:13:09.jpg', '', '', '', '', '', '', '0000-00-00 00:00:00', '2017-07-05 18:33:52', 'Confirmed', '', '', '');
INSERT INTO `doctor` VALUES ('250', 'mohamed shaban', 'mohammad.shabaan@gmail.com1', 'M', '7', null, null, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', '1', '0', '', '', '', '', 'mohamed5c3j1', '2234', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('251', 'mohamed shaban', 'mo@mo.com', 'm', '4', null, null, 'E+5/EWa+nTAxJpMU6AZv4idgMlXBalZ/a4+zYEC9Np8=', '', 'ar', 'en', '2', '1', '', '4', '39', '', '', '', '', '', '', '123', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('253', 'mohamed', 'm123@m.co', 'm', '3', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '', '', '', '2', '1', '', '0', '0', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('255', 'Rami Elbouhi', 'rami.elbouhi@gmail.com', 'M', '2', null, 'patiet_255_1502719068.jpeg', 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2011', 'aaa', 'ttt', '2', 'EG', '', '3', '51', 'doctor_11_1502794017.jpg', 'doctor_11_1502794017.jpg', 'doctor_11_1502794172.jpg', 'doctor_11_1502794347.jpg', '', 'hjkhk', '123456', null, null, 'Confirmed', '100', '100', '100');
INSERT INTO `doctor` VALUES ('256', 'mohamed', 'm12233@m.co', 'm', '3', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '', '', '', '2', '1', '', '0', '0', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('258', 'mohamed', 'm12@m.co', 'm', '3', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '', '', '', '2', '1', '', '0', '0', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('266', 'Asmaa Kohla', 'zeft@yahoo.com', 'M', '3', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', '', '', '2', 'EG', '', '0', '0', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('267', 'kljsdf', 'asdf@dfsdf.com', 'M', '4', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'asmaa+in+arabic', 'asmaa', '1', 'EG', '', '4', '46', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('268', 'kjhgkhg', 'jg@jhg.jh', 'M', '4', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'asdfasdf', 'adsfasdf', '1', 'EG', '', '4', '46', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('269', 'affiliate', 'sdf@sdf.com', 'M', '4', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'adsf', 'asdf', '1', 'EG', '', '4', '46', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('270', 'asdf', 'asd@d.sdf', 'M', '4', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'Arabic+bio', 'eng bio', '1', 'EG', '', '4', '46', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('271', 'giggly', 'high@fh.com', 'M', '2', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'dying', 'ruff', '1', 'EG', '', '4', '43', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('272', 'asdfghjkl', 'xyzz@xyzz.com', 'M', '2', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '1222', 'guard', 'shrug', '1', 'EG', '', '4', '46', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('273', 'dude', 'fug@dig.gu', 'M', '4', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', 'tight', 'thug', '2', 'EG', '', '4', '213', '', '', '', '', '', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('276', 'mohamed', 'm1299@m.co', 'm', '3', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '', '', '', '2', '1', '', '0', '0', '', '', '', '', 'mohamedpmuce', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('277', 'urban', 'joy@us.glh', 'M', '2', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', 'hdjfkg', 'mgkfig', '1', 'EG', '', '4', '46', '', '', '', '', 'urbanaqp7v', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('278', 'hydrogen', 'got@if.kf', 'M', '2', null, 'patiet_278_1502711938.jpg', 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '%D9%86%D8%AF%D8%AE%D9%84%D8%AE%D8%A7%D8%AE%D9%84', 'jditog', '1', 'EG', '', '4', '46', '', '', '', '', 'hydrogen6y9op', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('279', 'fight', 'ugit@gihg.vhnc', 'M', '4', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '%D8%AA%D9%86%D8%B2%D8%A7%D9%86%D9%8A%D8%A7', 'asdfghjkl', '2', 'EG', '', '4', '46', '', '', '', '', 'fightw9pl1', '', '56553555', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('280', 'rrrr', 'r@gmail.com', 'M', '6', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2011', '%D8%A7%D9%84%D8%A8%D8%AA%D8%AA%D8%A7%D8%A4%D8%A4%D8%A4', 'ffgfvv', '2', 'EG', '', '4', '209', 'doctor_280_1502803518.jpeg', 'doctor_280_1502803557.jpeg', 'doctor_280_1502803577.jpeg', 'doctor_280_1502803587.jpeg', 'rrrrl4ops', '', '123', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('281', 'fhhgg', 'dyd@fog.fig', 'F', '3', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '', '', '1', 'EG', '', '0', '0', '', '', '', '', 'fhhggnlypx', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('282', 'fugr', 'fug@gin.guj', 'M', '2', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'jacks', 'invite', '1', 'EG', '', '4', '46', 'doctor_282_1502806011.jpg', 'doctor_282_1502806051.jpg', 'doctor_282_1502806116.jpg', 'doctor_282_1502806165.jpg', 'fugrb5poq', '', '2685685', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('283', 'hdjfkg', 'xg@dyhg.fg', 'F', '4', null, null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '%D9%8A%D8%AA%D8%B1%D9%84', 'fjcd', '1', 'EG', '', '4', '46', '', '', '', '', 'hdjfkg8iaql', '', '5355', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('284', 'mohamed', 'm12@m.co123', 'm', '3', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '', '', '', '2', '1', '', '0', '0', '', '', '', '', 'mohamedatzbq', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('285', 'Amr Elsaid', 'Amrelsaid@yahoo.com', 'M', '2', null, null, 'HW+vpcS2MDyhhXQklMH77qQzVRgQl+uX1eR2W5PquIc=', '1985', '', '', '', '', '', '4', '113', '', '', '', '', 'Amrcesti', '29711', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('286', 'Ahmed Hafez', '7afez5ever@gmail.com ', 'M', '2', null, null, 'TEi0q8t1AIWRMXZBP3yzHws60zPZiJEvydKPYUFp4Dc=', '2012', '', '', '', '', '', '1', '0', '', '', '', '', 'Ahmeds0t1l', '5838468', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('287', 'Marwan Amr ', 'Marwan.a.abdulaziz@gmail.com', 'M', '2', null, null, '/ON9L8jrSb7Ilf2mUF59h3AaOfXOfL4DddUjj2UwhsQ=', '2013', '', '', '', '', '', '1', '0', '', '', '', '', 'Marwan66y9h', '268575', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('288', '      ', 'a@b.com', 'M', '4', null, null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '+++++++', '       ', '2', 'EG', '', '2', '18', 'doctor_288_1503281155.jpg', 'doctor_288_1503281198.jpg', '', '', 'y83qj', '', '123456', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('289', 'Jon', 'jonjon9080@yahoo.com', 'M', '6', null, null, 'QDL0tLfP1zqy5sPGzFmiKfuGqXM7i4Z2WrFyGYGrqfM=', '', 'running+', 'running ', '1', 'EG', '', '4', '46', 'doctor_289_1503261021.jpg', 'doctor_289_1503261025.jpg', 'doctor_289_1503261026.jpg', 'doctor_289_1503261027.jpg', 'Jondgw3k', '', '67567890', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('291', '       ', 'b@a.com', 'M', '4', null, null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '++++++', '     ', '20', 'EG', '', '2', '18', '', '', '', '', '0u533', '', '00000000', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('292', '    ', 'a@c.com', 'M', '4', null, null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '+', ' ', '1', 'EG', '', '4', '46', '', '', '', '', 'fqs3s', '', '865656868', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('293', 'ramy', 'test@g.com', 'M', '4', null, null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2011', '%D9%85%D8%A7%D8%A1%D9%86%D8%A4%D9%89%D8%A7', 'vvxbnv', '2', 'EG', '', '3', '181', '', '', '', '', 'ramy0zj8c', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('296', 'Ahmed', 'a@d.com', 'M', '3', null, null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '', '', '1', 'EG', '', '0', '0', '', '', '', '', 'Ahmedrw87j', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('297', 'Ali', 'ali@c.com', 'M', '4', null, null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '+++++', '     ', '1', 'EG', '', '4', '46', '', '', '', '', 'Ali9y0bn', '', '12345', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('298', 'john Teslie', 'flyflyerson@gmail.com', 'M', '4', null, null, '/i6B4WW0mmPDsg3Hr4xDNSAH4oOM2s+4QKR+18dMrQc=', '1987', 'test', 'test', '1', 'EG', '', '4', '46', '', '', '', '', 'johndvve2', '', '', null, null, '', '', '', '');
INSERT INTO `doctor` VALUES ('299', 'Asmaa Kohla', 'kohla@gmail.com', 'M', '2', null, null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'this+is+an+Arabic+short+bio', 'this is a short bio', '1', 'EG', '', '1', '0', 'doctor_299_1504809273.jpg', 'doctor_299_1504809549.jpg', '', '', 'Asmaarjlg9', '', '578754578', null, null, '', '', '', '');

-- ----------------------------
-- Table structure for `doctor_addresses`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_addresses`;
CREATE TABLE `doctor_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `doctor_id` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_addresses
-- ----------------------------
INSERT INTO `doctor_addresses` VALUES ('1', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('2', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('3', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('4', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('5', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('6', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('7', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('8', '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('9', '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('11', '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('15', '2134', 'alex', '31.1256852', '29.7836783', '66', 'testaddresses');
INSERT INTO `doctor_addresses` VALUES ('16', '', '', '31.0301109', '30.4590988', '202', '');
INSERT INTO `doctor_addresses` VALUES ('17', '', '', '899', '56756', '66', '');
INSERT INTO `doctor_addresses` VALUES ('18', '', '', '899', '56756', '11', '');
INSERT INTO `doctor_addresses` VALUES ('19', '', '', '899', '56756', '11', '');
INSERT INTO `doctor_addresses` VALUES ('20', '', '', '31.205753', '29.924526', '255', '');

-- ----------------------------
-- Table structure for `doctor_lara`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_lara`;
CREATE TABLE `doctor_lara` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `user_state` varchar(255) NOT NULL,
  `current_credit` varchar(255) NOT NULL,
  `profile_img` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `graduation_year` varchar(255) NOT NULL,
  `short_bio_arabic` text NOT NULL,
  `short_bio_english` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `neighborhood` varchar(255) NOT NULL,
  `doctor_level_id` int(11) NOT NULL,
  `doctor_speciality_id` int(11) NOT NULL,
  `front_card_id` varchar(255) NOT NULL,
  `back_card_id` varchar(255) NOT NULL,
  `referral_number` varchar(255) NOT NULL,
  `practicing` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_lara
-- ----------------------------

-- ----------------------------
-- Table structure for `doctor_login`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_login`;
CREATE TABLE `doctor_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `device` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key_UNIQUE` (`api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_login
-- ----------------------------
INSERT INTO `doctor_login` VALUES ('27', '11', '29357d2109fec1b0952b3c1f76832c90c4f55efdfe68ba210c10a241604d338f', null, '29357d2109fec1b0952b3c1f76832c90c4f55efdfe68ba210c10a241604d338f', '', 'apple');
INSERT INTO `doctor_login` VALUES ('50', '11', '9f2716a46a3f48813adb13a470362cf8', null, 'bf3845623db60d2319f117ed50c078b85710eb94ae4dd58920894fdf9cce0968', '', 'apple');
INSERT INTO `doctor_login` VALUES ('51', '11', '96403edfe6bda993d487e76a888af09d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('52', '11', 'a0b9f02cd4f30b6fccac097422cb0a22', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('53', '11', '0fa120b9288fee630afd0a8cdaa38b29', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('55', '11', 'ea91435448f16633d8da8a41dc161c42', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('56', '11', '1ac2f383b2f2ab30cb518972abc0b23a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('57', '11', '80efc8f1f780e36e976d22ac15474d00', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('58', '11', '2fc7960f6ca3301ee874bf28cc28b81f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('59', '11', 'e95cf2ff7679ae4456a6d0acc51fda3b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('60', '11', '502211b359d1fe960a633cb17b352a8a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('69', '20', '8ed1cf5708e743b01524c6e091d2a716', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('70', '20', 'd519159b048c9d79fb1ae7ea74a90542', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('71', '20', 'f12816f17c0a2253c703e682518b3a0b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('74', '21', 'e7552d3fdeefe3fb961d083fdf95e897', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('75', '21', 'c4b5e87234b799458f547d2a479f9fd3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('83', '24', 'ecb458a37c3344adc83b9d9aeec0c644', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('86', '11', 'cee74a88d8828a8f007ea1041bb3e97e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('87', '11', 'bf7c543f998763602a598dcc75fa1865', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('89', '11', '7401101ce0faf369ce930fc70d104e01', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('90', '11', '677fd77bb4b85cd738e9068a027abd97', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('92', '27', '19476ffdd923cba041712429106caf60', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('93', '29', 'c1f75ec772bc53875ce8c6f24dad86ae', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('94', '30', 'c7e460c94f39e6858121aaced7db3078', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('95', '32', '2fa22be8b3a1bae5fb5010737afa4a26', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('96', '33', 'ec8c2bff395d8a0f4e812569a53660ed', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('97', '35', '402e0a79f52ba8edd623476461946b55', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('98', '36', '310f4dd7d75944813cc9ccf37bf9aa74', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('99', '37', '26f01438169ffe5a1defd9e6e0ea657d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('100', '40', '56f3c6949e1e0b75b6228a57ddc726a2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('101', '41', 'aba6a33a6a6869a274d664924dadb3ae', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('102', '49', '86b11f913d55296bb0403c0d4b2653be', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('103', '50', '95243c36fb88f307a75319ddd3dee642', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('104', '51', '9425742a41b920201f8c3964afdaadd0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('105', '52', '2c5390e414d431f26a8555f7f87ea245', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('106', '53', '55704166cf1ccb275bf81ff30b7c87ea', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('107', '54', 'e496860f8f739ea981bbb73af55337b0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('108', '55', 'eff8b069a4a5806e868f8a9e0fb9ca78', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('109', '56', '5a656e8861ace2679cf7189fa6b5c8cf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('110', '56', 'af3e4a72f41be14a7cac24828c17ee5d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('111', '57', '6b69afb62a2e767e43e206595a9e4dbf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('112', '57', '88b8ba88284c407301f8b2a55416905d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('113', '58', '1c13c31915d4028b7a3ad8122f34692f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('114', '58', 'ffb374d235be5571a1c3c9e0e89c3fa4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('115', '59', '0b088b49cc05a0a15124b98b61578e4f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('116', '60', 'd5f2a0d7ab1154497882873b7e827006', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('117', '61', 'cf4216ec0f8f6f2c5187e3daa979109b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('118', '62', '833a0c4bd7af4d75b5cd8f4d6b47f5c4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('119', '64', '7366ab77f0cdf2ee7ffb0c01ec33da2e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('120', '64', '001d21eb2c5e01912dfc1cc8fcb0c2c4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('122', '66', 'dbdb8dab64b044ed55c45360f235a442', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('124', '67', '4b5655159ba4237e5d52dc3ab5b2ff4f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('125', '67', '96f6f974cf9d887cd8be2924464c73f5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('126', '68', '1b78b1870b94b2f4c5177e2b1638b8f4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('127', '69', 'a2201fb39c47d69ff6ae7b162fa8fc13', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('128', '70', 'd09df02888b9262d56713d79a2a5ea6e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('129', '71', '56d79e2ffa95b5876689669b797ef656', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('130', '71', '0575fc8a09f019e59e312df00f84a820', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('131', '71', '90a605447a98288376e2a3740e446fbd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('132', '71', '1fa809a1ee17e304d36ebc386d5d1835', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('134', '71', 'bdb44ec4af26b7fabca52f4322659a8e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('135', '71', 'd543b96457e433c7e1756835bfd43be7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('136', '72', '9d8e2b1e2598ff5e254a5c98cb0bb458', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('137', '74', '46782f7e8dc2a2c91b8839fdc0d9b4f1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('138', '75', 'f912de4d257180bf7fd03b88b19ba4f0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('139', '76', 'bdbf8f52d23399b619d8781b4de08084', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('140', '76', '1dfac892548705833c8b0a820ce550d0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('141', '77', '3a5278034a7d1d1bf8d731209a04d21b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('142', '78', '7517de60357fb39f453fd42a014ea21d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('143', '78', 'ae991bc7d0bbc7c31e56acd7e18df891', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('144', '79', '5b70495a01de235b4533cb9351e68744', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('145', '79', '2ab8120454af70c5cb1f4e208b06ddef', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('146', '80', '1e4fbf60575386905183dc574ae9e278', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('147', '82', '61a4ef8d601b38aab7ffbdd566c1d346', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('148', '82', '61b76a649d3fb82ef395bf41a81893e6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('149', '83', 'd197210c3b7c71e4029c6f3bf0e2a7d7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('150', '84', 'c189433bd921e1869df01de5b6779002', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('151', '85', '125efdd14a2cda20c7fbf4d109f6a0ab', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('152', '86', '8675bcf75418877e5f10eb3c48fe5a77', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('153', '87', '956b18badd8bc26d0540b874800c2383', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('154', '87', 'a8a606b685e465f955df8cea5a8945af', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('155', '88', 'edbc94c9883c50b3389c11c921b5e77e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('156', '88', '3a829fbc3cf6af06d5fae3efc8e9f050', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('157', '89', '4300952219a93704cb72ae833ae9eb52', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('158', '89', 'af5c86fdd6f8d9f3ae58819635cc3ea4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('159', '90', '85081e39419ce054dce23c4af79eeebe', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('160', '91', '245bceb814419a01561a4a79b526f372', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('161', '92', '0f551697ad52af98fd6b6b6b77aa0a3f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('162', '93', 'ceaa1e645dbe3458283bca77454af336', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('163', '94', 'e3b36339f5b94ae63c33c0b2594f2ad8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('164', '95', 'cd6d638a2f710de04f371d7e43845c5d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('165', '96', '393532aa12c8cc696374565446eb84c7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('166', '97', '794d551af8133c687f137fa6f4e7c34b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('168', '99', '1fa1bafc525e5dfa29c978885292f7f8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('169', '99', '331ed9799dedf5e7e99b5ddccd994036', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('170', '100', '6ff25dd3125d5ce43b63e5792c9fcb07', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('171', '100', '30148f16ca0dcef2e74872bb29a46183', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('172', '101', '5489a8012d7f6f564465268c559339c6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('173', '101', 'b3a2c2eacde44a3aa64b1e7cf1dad866', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('174', '102', '00eb65dd1a000d738e4c356f45af19ff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('175', '102', '02e9757390ec09975d80e5fbd9c8b584', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('176', '103', '6e8a6c590635d7f19059acecb914f547', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('177', '103', '5860004288c09f2f1db5f112f7f313cd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('178', '104', '30891612eeda0a55548c7b94ad19cec7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('179', '104', 'ff480d549bbd1dfc77e612af9f150230', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('180', '105', 'e20217150c0867c3b0da6e1c8501b6a0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('181', '105', 'e2ccca75738433a8252a2200eb127b68', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('182', '106', '9a9297b6a252d80e36f1b93442423a3b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('183', '106', 'e9cfafc5167855692c2b23ae40187fec', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('184', '107', '6ecc6ec6fc2082e2e692ad586710538d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('185', '107', 'e90bd23e92eb4befe48070f39a3b62cd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('186', '108', 'f060df6a68d33628642de373025509b3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('187', '108', '26956869fc2504e98700955f9a823d14', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('188', '108', '2090872486981ed2d433b2f8d72e1f33', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('189', '110', '9a3a0559382acb010bcb151b39691571', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('190', '113', 'a699e2c1d989c51e75542b8adca70143', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('191', '113', '824446eaf5dafe87be677fb021929cd7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('192', '114', 'e80c47154a92c54cac9c7b3a5c269cff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('193', '114', '76cccdfda30ee2aea8c651c1e7030943', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('194', '115', '389261af1fd8c14bc23b7e11ce7d7d00', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('195', '118', '5b8ce28e66b0fb72cffd6c0985f2e3e5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('196', '120', 'f76d094ea9fa9dd1c1fa3933c1a1c029', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('197', '120', '2106fec9a4f597f9a64996859dc7470f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('198', '121', '492c775ee279001056486be561c12407', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('199', '121', '83859640220024348cb6980ac1c0e0b6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('200', '121', 'ff69ded1bb460847cda7bfd55828f1b0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('201', '121', '673ca8f9dff88a0c13861e3c194b1a7a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('202', '121', '48ea59139ea6e991fa6ce741b03b1d8a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('203', '121', 'd02493bc6c785f0b009cf0ef3f95882a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('204', '121', 'ff424ab4f9ff873c7036725647eb3681', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('205', '122', 'f624c83d962cf466f7f0f87e13c38363', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('206', '122', 'e670026f68f07e9ce9d8d407535c4fb9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('207', '123', 'bc8b453ddb37c1eff0f7e2c6bced2e2e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('208', '123', '4ebad0b735eb8ccb11e830c17c40920d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('209', '124', '3ae17e3d9b8c7a651cf17900193b755d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('210', '124', '203ff2742723dc47c93d964488ec6ac1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('211', '125', '2b085b5a075ab497a3c0e80d5f2895dd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('212', '125', 'ad9f3ada13f17a472a3a59c20ee53d7e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('213', '126', '51719d69639381a275975ce6576dc458', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('214', '126', '1bea1d6553a2448c46d49debb3336179', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('215', '127', 'f4d689704a3b5d0348afa6cdcad1d1f3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('216', '127', '32a6f2b830ffddeab1fed609c3560346', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('217', '128', 'caec98c705a156e8ab60356ccd2e3d93', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('218', '128', '27a7453cb5fd7b20de4270f023d84a4e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('219', '129', '03b70bfe4fd036f743298bc05cb8b72b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('220', '130', 'ad5d3cfc63683818992892b6f35b7181', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('221', '131', '58be12725f6df82d53dd7bc2a4e998c5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('222', '132', '2369fbdb4077a5a9f1e23820f9b9814d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('223', '133', '08d3517187b577d523999dd6d6a7a93d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('224', '133', '93b58d027b3a29757db962708ee7698b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('225', '134', '58f41ea9f76c8153fd9587a64d8b3e89', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('226', '135', '79ba2908560eaf80430a4b87ee5ae64a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('227', '136', '57ebcde4e3e567af556485cab41b3062', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('228', '137', '5386e51aa9ffe5819d6edda80e93199d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('229', '138', '5a86598fe9abaf83d2c5aba892391879', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('230', '139', '390446c8997aaf61ee45279c2f767507', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('231', '140', '18064cda77d0add8c65222cdfd26b8bd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('232', '141', '7be8bd971e5ff907bb36a7d61a6c09ac', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('233', '141', '012c29be332c7dcd80c7518545ea224a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('234', '142', '151a02f06b96043604c57f701018a7e6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('235', '143', '37027281bc96d8ae5b63e07deeea7179', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('236', '144', '908c6aa95ca0a4d7fe9ecd0b65560ad8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('237', '145', 'e3a0215c296ec16ea93583f113e954cf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('238', '146', 'ff38559df3f68936f44581d7e94b53d4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('239', '147', '158fbbf1d3fb62a0ff796608beda0a50', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('240', '147', '79bd7d6928af040a14fc76ef436a4e7b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('241', '148', '90c0ff0698954bccf4913e9ebdca6bdf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('242', '149', '09e60fdf7f645030a20761713092f892', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('243', '151', 'd38cc4e5346b70e4d3d0d25bcaef25e1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('244', '151', 'e26754afa71a708e8dc9cdc29318f68e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('245', '152', '9d5cf00d7b70aa933dc73d2e82d04a3e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('246', '155', 'be3b8830ebfa58494cfd650aef2dedcb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('247', '155', 'c191caf23c8806b26534cc74356d56ba', null, 'sdsf', '123456', null);
INSERT INTO `doctor_login` VALUES ('248', '156', 'fb6e6fce86f01dfd7b074523a12aca76', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('249', '157', '39ec90cd303d7639c1ccacfda800ebf3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('250', '158', 'd4aff58d7f689df077102615b672449c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('251', '158', '2504d1be9359759075c3b726dce2dc04', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('252', '159', 'e402ebc2bb5d8b58842f41c1c9b83755', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('253', '159', '478b17b4d33286e183307157f2a9d926', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('254', '160', '6dc5837affd7cb7593decc36ccf82d77', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('255', '160', '188a2e67d128869f2fc7dac2aabdb462', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('256', '161', '6cd49cefc01e9b132682c91cc614f44e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('257', '161', '11ac9c321f3acc5c5e88b2133a20c99e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('258', '162', '133f24a3ccdb854b12955fb80e41f163', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('259', '162', '6ecf6845c1460fe04c35d1d9497a7e2f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('260', '163', '55e6b3666f12287708fc55f8a9897068', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('261', '163', 'af7a48fe9895ddccbf75fa1e29d9002c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('262', '71', '261135cef609c0edaee0a9aa0274d6af', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('263', '164', 'dc4598c8184439fd82a7607f7a1f81ec', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('264', '165', '03e8596ead311c650371ad0967991e89', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('265', '167', '941aa9357a3b442c4070b83f1bd1db1d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('266', '167', 'bc76da2bb064b60ee692c983cd5d7e03', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('267', '168', '1e0d6adba442e79aa92e2958cfa3f276', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('273', '169', '8bc8bf5faaa398564108841e69061aba', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('276', '170', '7ac0eacefc71c840a0b65837c7fd6bac', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('287', '11', '09b596ba5a205e9ec801fe03d7ac22fc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('288', '11', '29e4b07ba3840067a9d373a8a7388055', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('290', '11', '0c80cc6e8e79688633b16eccbafe891c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('292', '171', '7d347f571860f5e1bb3d18e9711ecd41', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('293', '173', '2d5b20d6fc5714a80ec395ae4af9430a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('294', '175', '0dfd4d92281932d124be47835b22e370', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('295', '181', '9f2c7f00d7bbdd5d28c15da4bbd52f30', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('296', '181', 'bef0af3d4c6563edeec93268e25b48c5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('301', '11', '41d029e62a151cb6373f9215a9da03e2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('302', '11', '2931e655cc552b124a46c665218230af', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('307', '182', 'fa2000a258e6731fc3ff7fddbf26118e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('314', '184', '0cb5d621566fa49382cfd242798cb75b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('322', '185', '2214042e29642f4d2902cd8ded996925', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('323', '186', '995c8e7372b839be82740e260f9f8c41', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('324', '186', 'ef2ba0864bdd8aa3569df028319e3730', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('329', '187', '22e9c8f7bd7f731b2138cc8b69db0f7d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('333', '188', '9b214a9c05d03d400c4729ce22e709e6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('334', '188', '999b3752da5c5f1b9e1238d51de0038a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('335', '188', '9a2ad6b2378bf3cb3f25cf60ddbfe5e3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('336', '189', '758cd9b84a0e3e86fe5368c0780023d1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('337', '188', '1ff386fbed0b492ccaf3d329601390ce', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('339', '189', '324b4e2936f8384008c5837ce5b587e2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('341', '188', '70f9225171141b3c5de62b7092e1a185', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('344', '188', 'cc8b1da83d010ea5f541ea5a0aa862ac', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('345', '181', '8b9cdc66085ba81fd6a51fdd41b6f6ab', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('346', '181', 'bc606489eae7472fa284c5c4b8cbd411', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('347', '189', '8b9cdc66085ba81fd6a51fdd41b6f6ab12', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('348', '50', '31c6f0a8d3cdf9db19d0b2e302c2e6b5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('349', '189', 'd44cdbf12679613dfe4c2db4285c5dfa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('351', '190', 'adcd49b9525a361ac44bf9bf1c81e4e3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('352', '190', 'e99b2e2ca8780ff4bb3ffcac0f9aa7a5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('354', '191', 'f83aee0b6771eb53bff3b093425e2ce4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('356', '192', 'f5572847df902c05840cec30dbe153ae', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('357', '192', 'a9bbe96944a7c5279e8854d38ec4380b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('358', '192', '45bd7c7323cd2026a223491ce9323a05', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('359', '192', '39468b544db2793b3c888b76f6427fee', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('361', '193', 'a7fa41d0d7b97fb87b3a6e696c9ab2f6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('362', '170', 'eef184e60929b0d2311cfc67e91d1c66', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('363', '170', '2b88d28fde7029940287c81dd9e85c45', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('364', '170', '37e674e33870100d86bcf1fed1886021', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('365', '193', '466df69aa63f6c7b7c91220c445af3f1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('366', '196', 'f8e7713d8e80e0f7665a43e01c9c834a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('367', '197', 'ffa31e5a897cc1e7ca372e1f249d72ba', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('368', '198', '2bde40b39a32eb9f47b42acd7450919b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('369', '199', 'a716841be8b52a422e5ea781d8af80f9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('370', '200', '17b8f54b2ad50aa8199283c63b490be2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('371', '193', '14c81a66dd54a87b17a8b15b0e763120', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('372', '193', '08840f27b40b1c9965bb3f480f81cfb0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('373', '193', 'e87a0433b725e6c6642da8abc0e3bdb8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('375', '201', '95ac2eb54a060791770345c2043f3575', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('376', '201', '643ba9ee5cb7417f7cc03cdaa096cf01', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('377', '202', '1a1831191baae1e3f6e604532189f590', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('378', '202', '7671cbadfa47e2b7f1d75ed1f83836ab', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('379', '202', '2b1596698695b565b8985ba85c3598b3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('380', '202', 'a4ca685d4c70f6faecaf494281af450b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('381', '202', 'ad5d0951babba14f38303880d19a706d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('382', '202', '2c573c77d7777ba16905e11decb0390b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('383', '202', '554018fa8df1ca2b3baaf33c07383ef4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('384', '202', '31edcd1089391c4fda75fc686d4ebdcc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('385', '202', '6d08b5ac4f9ae6f1c0f818ba27bd1868', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('386', '202', 'a1e49c61fb3a21a07231b2d4bf2fec1a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('387', '202', '41da9c00795002eb908d6f7800413b9b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('388', '202', '1ea6af525a4e396b0557830e6e97e17f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('389', '202', '8228c3d1c4346f6dfefa8a0dc26a0d41', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('390', '202', '497c7194fd10c0c970507fcf7f7ada1b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('391', '202', 'ce41698c5551a17271a7419873cc8cb4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('392', '201', '53e286b60f5d021fbf6bf78764c5b363', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('393', '201', 'b6e25e18337542111b3dbaad921677e3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('395', '203', 'e61d25b965b65019e38f057221b93290', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('396', '203', '6047f5756a4c27b6cf7ab0bc3fa05e34', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('397', '203', 'f3c2b3b883390c17e9958e5044129cd9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('398', '203', '00c256076a46a51c06b377af617116d1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('404', '205', 'e07bec264d7c9d28b9b6b1984fd02eb2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('405', '205', '4d82fce90f41714fea0327191d2a6195', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('406', '205', '84c1ffbfdf601029074636a912c9fb1c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('407', '205', '95159a1d8286ca2340c5e3922dc13c00', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('409', '206', '0a23a2618cccc55a687f4880710c29b7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('410', '206', '8675f5ade05aff3ee325412580aa6a82', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('412', '207', 'de61dbb2f11e5347164d5fdfc6c4c7ef', null, 'cZpe6shLitU:APA91bE1493y12FHCfxCXcOFzro9SIZxOIzZfbvVW4MJX76lItAAIoNvxrSPIc4ZbtRyYEFAB6Vri6Kk1s7RtgmuGcF9KD5hRKnbh-4xqVdyznSdkWp3zOwRp5D6d7ysRy92iHIiILqP', 'e90789852dbaa4f8', null);
INSERT INTO `doctor_login` VALUES ('413', '207', 'c6420201704d20ad497652c21185274a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('414', '207', 'b787ec8b8ecce5a2c7cb5d2c93d693f9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('415', '207', '816d3f9eca73baf9dbd79320f458763d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('416', '207', '21148b0eda4bbd51a5476af0458cd5d2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('417', '207', '1b312b6b5b2d9fe7c983536f86806679', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('418', '207', '33c9ba3310a93e444ebb8a8cf5edb388', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('420', '207', '450cb85af9d426596111351f0c416668', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('422', '207', 'f2e28c308b40d95c2b7871446f47febd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('423', '207', 'd4b23e0e6d7187ff6ceda1d67e6fb667', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('424', '207', '7d130cdc655b5784fcddbab28b92b1af', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('425', '207', '395ec0026e800b7609a7be1f8f48f5bb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('426', '207', '8122c34bdfcb352b6a97dba1df7d1903', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('428', '207', '7c5b01a0416cd2598bb96f8bf73b8cd0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('429', '207', 'bdc1f07ea9bfd834c0947bd7c0cfa204', null, 'cZpe6shLitU:APA91bE1493y12FHCfxCXcOFzro9SIZxOIzZfbvVW4MJX76lItAAIoNvxrSPIc4ZbtRyYEFAB6Vri6Kk1s7RtgmuGcF9KD5hRKnbh-4xqVdyznSdkWp3zOwRp5D6d7ysRy92iHIiILqP', 'e90789852dbaa4f8', null);
INSERT INTO `doctor_login` VALUES ('430', '207', '877174e6f7cdb29ddd05880aac72ce8c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('431', '207', '18b9296e0fe1f15944e52d326de8014d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('432', '208', 'da5fd5d9182e8ff62f811d12322dfd91', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('433', '209', 'fbc45eba6286a310ade608bdbefe4bfb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('434', '210', 'c2b391073642fd6f267c2a04ea2bd5e9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('435', '213', '8fe93fb6606f83af3b4e40e25300595b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('436', '219', '1271dff28fd0054eb992d6b6517ab797', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('437', '249', 'c642b51ba46b8e42114729e654eadb93', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('438', '250', '7ff3fda9cd46742ecea44b3a8679485f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('439', '202', '9b48ec804d492280d9050e6e9ebfdae0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('441', '253', 'a5b247f619dd47112e072ee6d2cda3cd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('442', '202', 'abf1b0bf0e5848f6a146464fc78bfe17', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('444', '251', '1873b2724976eebe1a0b82dd1b6969f0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('449', '250', 'eb62d35c24281c7dbd93463b0dcd8e96', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('450', '250', '87429ce810609ac43ba914c90bf79101', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('452', '11', '79a80823d08f4084a18a025b9ee7d92f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('453', '11', 'a53c009862af5ea7d54a5296a9e94c19', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('454', '11', 'bf44362468f65e5d5dad5692286ac101', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('461', '251', '8e57f954000a4429eb05b63c855898da', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('463', '11', '52e00d390958d89da98ab083ba982216', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('464', '251', '0a809e00bde5a28d762f61442376a3b3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('465', '11', '2bb37f090fbaaf94bd6977462289ec27', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('466', '11', 'ecdcd6da65675fe6f86086c277b3131c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('467', '11', '71329e36d6e6e7357e415ec78a620a8d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('468', '250', 'b2abac16ef1faf7556c714bff50eeb3d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('490', '256', '626df732eaef66593e8117276ea8856d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('492', '258', '01aa09d1b78357775dc225ca20b5ad7a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('493', '266', 'a9ce9ad8900578bcc3f9b1f9552268ef', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('494', '266', '63c05d670d7390e42c11884ac1f542df', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('495', '266', '7595e46c62ba329fb1fc799a24df69e5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('496', '266', '07d8b02a165aeb5dcf7b43710a129fd8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('498', '11', '21204d76327bfa416a130eff19005fb0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('499', '11', 'bb85e7a71285d3ebf95adcedeeded604', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('500', '267', '52fcdb71bd8750de65d9dcf82b69a4e4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('501', '267', '9d21d4651c35ad044ad80013309b1c8e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('503', '267', '3d687fb04e843326d03e9135f4806eea', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('504', '267', '607c5383e733b3afe0427ed4b96cbdaa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('505', '267', '6d411be3ad5df278eb83c0469a9b6345', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('506', '268', '455aa60cf0ff4ef1b01fbdcd7c445357', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('507', '268', 'e57450efc965b828c55f0965c1dc8106', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('508', '268', 'bfd8bd757180cecd84c20812eca0f71c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('509', '268', 'dcb9c0c91d68cbe7022b6016ea86d1ce', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('510', '268', '8120be9726a1cbcbff4e7d1ca0cbc0ef', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('511', '268', '774fcd644d84ef21af9e5f807e741eb2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('512', '268', '3a05f981322c64d8d0419936ac602c64', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('513', '268', '1481da27377b36b26f3b6b25b3815627', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('514', '269', '332f902212ff7cbd8966a7d5342a96a8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('515', '269', '2a6ca1906bf45d70c8e010379dd1e571', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('516', '269', '4d525e7ca981f7034b7deaa58e6d2ebc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('517', '269', 'f593bb9d1b96582de733a1ae61283363', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('518', '11', 'e53b78538fdf7ab06bc348a52c12a512', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('519', '11', '6ae6755faa637f7063b87f6b56f1c165', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('520', '11', '2bfaa85f82b3e74b4bce4ab6140b52e1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('521', '11', '24723b85620a1bb71dfffddde4ae44ba', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('522', '11', '9c45d8d5b00cd45c7f9893778d7e81d5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('523', '11', 'ff5d2631dcdb747be2f39de24113e912', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('524', '11', 'a8d032e057e6faf4fb8a54e29dc8d9d8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('525', '11', '3e5df245af7282a3f3f325c0c478112f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('526', '11', '332657421ab1c2132fd930b2d1841bf3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('527', '11', '503b695d0a012006e9cd556391ea4f38', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('528', '11', 'd4fb28935e09c6949bf6e72b878c8b2a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('529', '11', 'a035b0419127bc95323a3eb943174849', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('530', '11', '3f53d386d2b0b532584ec2dc8097b4da', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('531', '11', '170bb179b49160d246e2a7598f4468bb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('532', '11', '0a7a41241bae471d556a4b955a253d01', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('533', '11', '8bdcae6ed818ce5bd5db037225d6bf8c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('534', '11', '534a199f0cbceffe19eb617e20da4f2c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('535', '11', '75f2069fb3e3d7500478b2672d26118d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('537', '11', '4fdf064d89791a218553cca01bc609b4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('538', '11', '61e62da3776dd39d17f57e1094aa57aa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('539', '11', 'ec120d4aada1f42a79fc8b2f3857957e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('543', '270', 'e762c30272f846edbed86701129893fe', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('544', '270', '6b825f1797a8faa8180e3fbcfec90563', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('545', '270', 'aad1a125bf76e596824a225a4f625128', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('546', '270', '2fdbb9d789a567b433cb26cdae1c8800', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('547', '270', 'fdf286da25c59e495c0dab324a6ed464', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('549', '271', '24d1f50a6400e752127dab06e0e66fa4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('550', '271', '5f738702648fe504333674af016236ff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('551', '271', '2ba6c8dd90239dd98fff0aecd64392f1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('552', '271', '0e1e778f8fbf6500bcf823306dc1240c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('553', '271', 'e928497634c087bb7b068a8c969edce1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('554', '271', '545c3e40086d04be51bb03ec007ee462', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('555', '271', '3d2b9b800c64aaa0c1e2a6b9a87611b4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('556', '271', '333e84a8b010c517841824b26c1f53cc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('557', '271', '5fe30fe6f236155bdb27ea9ee9eeee38', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('558', '271', '31187dea5d01f0e7bd53d0c71305ad42', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('559', '271', 'f4a3241a34a5b3dc7ab37fbe6a6e0ac2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('560', '271', 'ada9a771c716986b1ee26e4833943d82', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('561', '271', '3cd63cdadfb6c8a23797c34e0b627747', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('562', '271', '8e4d4d9d6ed1b87e8c85346e24cc862b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('563', '271', '86b6a6cf130766b869ed9399293d0475', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('564', '271', '8b46dc8fafe797dfa861a02d4d2a3c26', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('565', '271', 'e9f008945bdbaa63d9dc2e9d98ddace1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('566', '271', '762424db25141455edfdf04965177e4a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('567', '271', '55009fee8b8450f4daca63d695f097f8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('568', '271', '8931ac271a26ea340a5c56186f511d02', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('569', '271', 'ec0f978ec5e0016dd99f569d9090df36', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('570', '11', '3a2c4b8be4ea1d5124e7e4bc49c5a0cc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('571', '271', '7b52337d2d6dfe77f96b884c9b2a3fae', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('572', '271', '5133354e045393972442e001ad65be10', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('573', '271', 'c4490455e4b07f0d1ede59a7e4546fff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('574', '271', '472a0efb3e9e6fb0df3526b41dbf49aa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('575', '271', '89479cbcc7cfde15c9ecef05ee3dabc4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('576', '271', 'd9901c930160507ec058bfcb2080fc71', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('577', '271', '2916ab680838295e1246f5c63129f542', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('578', '271', '8c111d9e6589a93e13796ff32b885508', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('579', '271', '99fb1ff25687c223acd7a21dcce9707f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('580', '271', '2e436047e24e6843daee3435f9071ef1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('583', '11', '5b7a20e8c71995cc85a96fe0885cbb21', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('587', '272', '15b5aa5ce0bce67115e04a449e5517cd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('588', '272', '0fe6ea5f3df9686c371afa11b5267106', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('589', '272', 'd01ccf4186de0f9383de01456f8b40b6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('590', '272', '263088396267b74734e5f31a219b2be6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('591', '272', '7b0008392db12e23f9662d097ddc7839', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('592', '272', '12d4052f1f3d3b1f78d494292e459ac2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('593', '273', '5ea3b31f6052046d79d3d1a1d6632b2b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('594', '273', 'ee0eea638776484afc796de3d6824189', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('595', '276', '8cf6a22c447ba71d020cf6e47d78d33c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('597', '277', '85d51d0289257876622a2df62934521a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('598', '277', '886d58b5165247dc4c38724940462090', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('601', '278', 'dc48490137300d66d2fdb44a3976107e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('602', '278', 'e1fa934e37a73671008e6c696a93c986', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('603', '278', '1fd859f51c8fc5614a5a3c951086d56b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('604', '278', '7469ef354e76208b82ddab329d29a0b6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('605', '278', '33219036697cde76805a42f9eaa9e958', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('606', '278', 'f9aa197ca00e41271fd24cc8b700ff90', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('607', '278', 'ef42fe6a7002aae1b52059a37d206672', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('609', '278', 'fecc9d78f934e585167b6efb211a2d6b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('610', '278', 'a0b02349d9f6056374cf35dc38030381', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('611', '278', '6eb1b12b2aae3b2588b6ddcf39745188', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('612', '278', '6e889627c133dbc0a3e6e7c8c300d2f9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('614', '278', '35b91749acfb0e7e6ba4d1301cbb36cf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('615', '278', '583ab69ada45af4ccc59eb05bcc146e2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('620', '278', '49f51f13512a5e39f925313f4783f1b7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('621', '278', '60de9c62fa8aad59037c0fe0a49c9911', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('622', '278', 'f0c64358e3d89ed284214f3010858447', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('623', '278', '3fa4868a3f6efc2c992bf45061bb1ee1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('624', '278', '2ef29e1fbf5fc71a777474442c2faf24', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('627', '278', 'e24b944368bb9ce1734d522986366f56', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('628', '279', '2d28c055d280156767a587fbc97f9897', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('629', '279', 'b60a7075412bce66e45e94bd454d071d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('631', '279', '8226caf287a8193eaa291ce9f2f5b68e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('635', '279', '8001fb30b2f18b0d971d3bce8d3f2fbd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('636', '279', 'a4ed6f442b0922f16daebb29f85fb196', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('637', '279', '7503760018531f6e4710fb232e91ced3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('638', '279', '30c726d2dafa8e261659138e6d72496e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('639', '279', '2fcb713ebafe77f0381a7867a89f4573', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('640', '279', '0c4d3a357a8bc8675c64781e8e817a86', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('641', '279', '53cd3b6381b965bf886175c6c16bd8f9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('642', '279', '19a66a5509cfe0a341e064ae1e4752dd', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('644', '11', 'f5f51e5650a75f4f53c1f22d5983c423', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('645', '11', '7076aba4be4d267344cb40a4cafbb939', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('646', '11', '45e48ed0648a8eb11609e96cd4279732', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('647', '11', '143b017394161561f6fcd4ec80cf6f30', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('648', '11', 'db69602d26ceb10b9019a86933a6e648', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('649', '11', '2c1e855a5c1cad244d62775f3be038bf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('650', '11', 'e200ac097653adf9066e1aabbe5696e6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('651', '11', 'f62e716683a5b767f393a2fbd91c5057', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('652', '11', 'e7e0276ba0f353a3313cc61eb9a0b854', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('653', '11', 'd416dc8a7b4e744666ac10376c762c63', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('654', '11', 'b0cd0f4f1ab733ca36d2d004e8250e7f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('655', '11', 'daa1164251769fc6a5c89629469177e3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('656', '11', 'e84b7b738e23e4002083d2445a76b687', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('658', '11', '8b8f5075709f1a12e7ac4205dfe79262', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('659', '11', '8549195120820085f302f0b9da26222a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('660', '11', '836a4a43cc5659093ad9e2e71ee7cae9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('661', '11', '848651046198df0645de7e76ca1d8581', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('662', '11', '36ac23dde04f1a3e01745c0e147e2a64', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('663', '11', '15d23c01fc820c97086c679cc7e5f6f3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('664', '11', 'a17a5c498352ed672fcbabe5fef693c3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('667', '11', '80f0d24ccfa7382fe707a1cc8eb406db', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('668', '11', 'bafd9fbf08f9366b80e7631008836f10', null, 'b7c8efe1e44ce19c1e9adc78ddaecc9284e9625569d3b649a86e24df104492b8', '', null);
INSERT INTO `doctor_login` VALUES ('669', '11', '52a632f10b7f07d059ea8cb385953fa9', null, '1908ee38ec5e8227dcf69ec344c728d10b87a4cb8ee2a9cb6108b899a50335cc', '', null);
INSERT INTO `doctor_login` VALUES ('671', '11', 'e876fa4f714ba07c947d7ccac64fef67', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('672', '11', '7a9500efe92fabbfc117a71270d00b49', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('673', '11', '6a6ebdd27d1bdf1388365e6a7db045ad', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('674', '11', 'a104d1674143eda07befcf6ff43dfac2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('675', '280', 'a06d4b811c8c972e1063bcb0ae919423', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('676', '280', 'f7ed963d87c32a9bbf8eb6ffd672c5e8', null, 'dDLtgs_rGvA:APA91bFBXOz3jCCCRlN_FI7vQsrOMoWZzXHXX4TOl0K9PRm7X-6_2BIEnVM_2wewBA_1fqN4bWILvkg9edCQsV3UCo3ZnQxDrirmww4niYLFylhDOf7QXLGIKczzzsNnfqFRz6839bLg', '915b302e5a1c117', null);
INSERT INTO `doctor_login` VALUES ('677', '280', '91a0a82561eb7ab1e9c34c14cfe30412', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('678', '280', 'a5c74b58ed11d8924fa48db9b6d5cef6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('679', '280', '9ad983aea8243b1e52a0fdf73ca22017', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('680', '280', '57f28adf81f3cad51d454b3321c0e99e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('681', '280', '5d8ab3d73300078f791299d60f155f34', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('682', '280', '58615a93ceda1b54ddf9f8e04d10b2de', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('683', '280', '82a72d4d2307b148dc82231ea6dc6e87', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('684', '280', 'f056e329ad66d71a5ac754a58a9c9b7d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('685', '280', '10721326619f44a0ecefac4714a6fe48', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('686', '280', '4391e316158ac36784f0f9d0b4554b9b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('687', '11', '51b7a1509044ead5204003daf04782e6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('688', '280', '6ddc08b0e12014ce225b08d8866cf3c4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('689', '281', '9834b2ff63fb569f77a1018d69332802', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('692', '282', 'e43f5b630ac0ed916c16bff39393d65e', null, '625c8e74f6ef9d57b7ffa8e21ee438ccc02b88ff14454918b6ac4c415ae70f96', '', null);
INSERT INTO `doctor_login` VALUES ('693', '282', 'd171af9e0a5f94f6337d535fe46124f1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('694', '282', '40b82f51931b00f001be87685984a5b3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('695', '282', '7163b097a180ff67ac550afe9ef62742', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('696', '282', '52020290e39cdcafb3abc79c0e49f0bf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('697', '282', 'c6a2ff29ab00885ebdec15cbd0a02ebb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('698', '282', 'cfb27282282c305a9f5902978f00bdf2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('699', '282', '6bd498d3eb815a61bc9e030adc452a96', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('700', '282', '1ec114a5120d9da1a773d950cef12e31', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('701', '282', '2d9c4c889324833797b1b63a92696466', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('702', '282', '3251435a83d465b7661a732908c74c5e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('703', '282', '097bff7f5cf950be8897397e8dbb4d71', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('704', '282', '44ad05e41863019cab61b98f47b30eb0', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('705', '282', 'ad5b9db1eb38c247ec3ee57df43182b5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('706', '282', '2a13e7f236e356356995cf05850c6c23', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('708', '11', 'd79ae4f56c1ca8eb6cc1ab3338028e3c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('709', '11', 'c9fe76b2141f04ab63cedaf8f0fcf04e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('710', '11', '3fc88be305dc6c17f17e47a070e172d5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('711', '11', '5d09c6c484192051ca21e7a4198bc594', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('712', '11', 'a467feee647f631b16acfb95644e578a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('713', '11', '680ae8f71255d9749d4bb050d63430d6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('714', '11', '8ff93059771f425f54d7dd08322092fa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('715', '11', '2e7ad2f3b2e4d39c6f0c9531c71b7f5f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('716', '283', '087383cc92ed7176755c07c4eb269422', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('717', '283', 'b04c7e1bf6dab7fa89d5b2c16571cbb7', null, '625c8e74f6ef9d57b7ffa8e21ee438ccc02b88ff14454918b6ac4c415ae70f96', '', null);
INSERT INTO `doctor_login` VALUES ('720', '284', 'd219ac06a65f45f31ee12ac94def112b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('721', '287', '5ee64af63d92cacb7dac3e6c6a00da62', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('722', '287', '661263bcb83bc53b4b5984b93bb35b30', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('724', '288', 'b2c5a3faf52ce535814bbc796088266f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('725', '288', '12704cb9a32e6a5cb6b7d9e5eeccd292', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('726', '288', '9faa0f11a9623b32b944c63708dd4af9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('727', '288', '9e22514c4e93cfe09cd358b139e83e82', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('728', '288', 'fb7f054ff2a0656f4f2c13b1b9a5caa7', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('729', '288', '4a6590c9ba7e3ff706bf4acb2f947091', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('730', '288', '58dd50c439e695ec72ec89521bcc1e08', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('731', '288', 'ec22ad8bf8c3d100ea522514be728b6c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('732', '288', '6fd2be70bf547afc49caf25c651e7068', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('733', '288', 'e5c6324a95552511048577773535ef33', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('734', '288', '8ee34ba68dca660e582c833c3e9f6705', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('735', '285', '67e978b3a7a30a253796d2c5f6c73e56', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('736', '285', '20ee51cf2222cf28d327d31aee290423', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('738', '289', '0a6da92ece051d966b517164029bd8ee', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('739', '289', '0d03ee7a51eecd2ce2e5a06af915d144', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('740', '289', 'e1976834096c49f1c9906fa171b9e112', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('741', '289', '2ec0f229a2c080857f3269755bc31b20', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('742', '289', 'd6729eadd713dbdbd58ff1d38d41e138', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('743', '289', '5705c8fbf2af79d072fff4cae507c2dc', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('744', '289', 'c818582cadc13036d2a510370df90345', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('745', '288', '77e6ef71ace9e8bc9643cf679523abb4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('746', '288', 'ad0495cafc636e5be9aa527dd471f48f', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('747', '288', '78b133dc1446267d23cdf6311755ea29', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('748', '288', 'ed962a6edeae0ecd23ffe99c95024ff9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('749', '288', 'e3ee7cd5ca45c2b531b6df0f94aef34e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('750', '288', '475d5bf5224f502ca25843d945e53b96', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('751', '288', '4060860f64aa8553d51d255105f1076a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('752', '291', '379df59ef3c3994977e73a6cd06925af', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('753', '291', 'ac5691e425306b9d19fdebf3a9c69b1a', null, '62a1c900c0e2ba3613e0ded63a2fc8951a752cdd2d449ff0a29e80c8dc556f5e', '', null);
INSERT INTO `doctor_login` VALUES ('754', '291', 'd171fbc933dbb36b930e461cc13976b9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('755', '291', 'd73a8f2060ed5305bf817c5e0ea6fb22', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('756', '292', '4b029fb5a76cb0134a5c53002673ff3d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('757', '292', '3b850e67456613bcc6ce059c4ff3494a', null, '20d0add409a6e0e3c27c838247a191faebcc8814726c4fd9b7c1c0ae882c4c7f', '', null);
INSERT INTO `doctor_login` VALUES ('758', '292', 'd5ea8561a6560b8c12ae2d1d469e3702', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('759', '285', '58a2edb9807bfcf18a20223bd9a7bd84', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('760', '285', 'bf630b0a6592c0d75185feb9b74109bf', null, '2130415e6ea1b0fea5b3a339793f352283215a8765b19aadfe8b24e203c064fa', '', null);
INSERT INTO `doctor_login` VALUES ('764', '283', 'a1d39075f0f22ef6eb9bb8a7cfc3a1b3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('765', '285', '63042f7ff8267f7fede9cf6b3b2432bf', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('766', '285', 'cd1b17d32a64dc89a113b16c8526a3b4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('774', '293', '9fb1013a1ef6e6c6a0af37409cafbb02', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('775', '293', '8b18ffe9e1911f51c39d85db63f382b8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('776', '293', 'ffd228e47821c87ec4d6f94b4ca8f527', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('777', '293', '1d93cef081c53779069b8088b919a4f8', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('779', '11', '9f97180e1167b3b0ab617c9981b319a6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('780', '11', '0fc310cc266bbcc344306081fd86c3fa', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('781', '11', 'e4edd7ebbc2babfd99e5fffa929393e2', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('782', '11', 'd42ded958616640ee6e6fb29ea883f97', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('783', '11', '4bf88daf4617020e121bd93d50da127b', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('784', '11', '555eee6d32b549fd05539d80f6181549', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('785', '11', '8544ad255a4b8b7a277571a95714c515', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('786', '11', '5412d38fe743ba70417ef9ffdd65959c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('789', '285', '0ef68abfcef005ad7923bd7cfbe90343', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('804', '128', '826a7be47e80c77e7d1e1335090bb9bb', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('856', '296', 'e14f32c65194357f2bc509b2101916f1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('857', '297', '383d5216dbd2053c1821516a8a72cd0a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('858', '297', '1f9d6ea0937d315413ee6f0c5226d9ce', null, '4bd6923c5726c0cdfe3b7550fe832b0c49f81a7460143498cb9431b106414aad', '', null);
INSERT INTO `doctor_login` VALUES ('859', '297', 'd21e4f58856099b8afadaa7b46973476', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('872', '255', '15a790114aba8f78a6aee21a2ce5f4ce', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('873', '255', 'f86f845fe21444de998074ebbff2d7ff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('874', '255', '8072b8aad1765f7aff5c1963613ef918', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('875', '255', '176e11ce6c1adca2666b23592d9c381c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('876', '255', '3b9c36e54d2a3cfe1509a3d20952e303', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('877', '255', '2b48da0271a6ede62319a644897929ac', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('878', '255', '0998c1fcc8e268ac4179a9facfd7f504', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('879', '255', '2ee2ab6c1bfcfe89364301097af63fc1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('880', '255', '94c21da6f5f109a0d4d3f4d6b46bc211', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('881', '255', '4d8d3e43408c0f29e4e843441881473e', null, 'egvxTTdfkEU:APA91bGaDrSyhgH8akTpWAhtUVhb3zo__tEZfnWcyP0DHFjv4iovGl512BikOYy-6twuPfvRQAbh-16q0dMqOK6oGLVAKdBFI0Ar6UGdfUKTpKpwXUYoleXiEOyMWKJcYnlUwcz9IUuk', '915b302e5a1c117', null);
INSERT INTO `doctor_login` VALUES ('882', '255', '30de223cac0ff3507d27f6338e1110e4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('883', '255', 'a3237e44c01604b6b3df66bcd582434d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('884', '255', '764ec2cdfc394fe5e7c62cb6a85ba5c4', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('885', '255', '8bccb95c8534a9951bf80da94cc82ef6', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('886', '255', 'e16ca7b2d9e55bff8bf2330d4dbcbc0a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('887', '255', '0370da02907ea71fc2ecef8e0b97d63d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('888', '298', 'c3bb023f929cb23894ee964a5eec2095', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('889', '298', '6af40f0963ec231d747042114b37bb88', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('890', '299', 'd030663f70ae76f6384634281aaf7fd3', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('891', '299', 'e334e9430e227f00b756d42adf3c4486', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('892', '299', '5ab37028399987f8c853ce889380092a', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('893', '299', 'fb1af23ef4a84adff8b09b1345fdebbe', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('894', '299', 'adbf687d8d23b814c9e964d70277abe9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('895', '299', '5c96340bcd779e6b72eb548936e3d1ff', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('896', '255', '18a57d09458b15381d53515aaed37093', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('897', '255', '387b95352fcac27f3349e37f1b4d17e1', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('898', '255', '2feb2e8d035fd068d31f0d3b6896216d', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('899', '255', '5b29ee1b074e04e85c6bd4416db78a7e', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('900', '255', 'f8e92891a425d400aa312b427ff60d03', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('901', '255', '171b555f1e61fed19f5f0adf20b5614c', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('902', '255', 'c31f300c7128fd994c1af016daf54df5', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('903', '255', '1669f4359786d1fcac1506a4c179f2c9', null, '', '', null);
INSERT INTO `doctor_login` VALUES ('904', '255', '90d06319c41d0989abe193a22ab3e339', null, '', '', null);

-- ----------------------------
-- Table structure for `doctor_offline_question`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_offline_question`;
CREATE TABLE `doctor_offline_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `doctor_speciality_id` int(11) NOT NULL,
  `doctor_sub_speciality_id` int(11) NOT NULL,
  `question_details` text NOT NULL,
  `in_hurry` int(11) NOT NULL,
  `images_number` varchar(255) NOT NULL,
  `recordings_number` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_offline_question
-- ----------------------------
INSERT INTO `doctor_offline_question` VALUES ('1', '127', '0', '0', '0', 'how to use doc', '0', '12', '456', '2', '0', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('2', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '0', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('3', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '0', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('4', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '0', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('5', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '4', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('6', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '5', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('7', '127', '2', '0', '0', 'how to use doc', '0', '12', '456', '', '6', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('8', '128', '57', '0', '0', 'tesr', '0', '0', '0', '', '51', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('9', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '52', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('10', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '53', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('11', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '54', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('12', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '55', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('13', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '56', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('14', '127', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '57', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('15', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '58', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('16', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '59', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('17', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '60', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('18', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '61', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('19', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '62', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('20', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '63', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('21', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '64', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('22', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '65', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('23', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '66', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('24', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '67', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('25', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '68', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('26', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '69', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('27', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '70', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('28', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '71', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('29', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '72', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('30', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '73', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('31', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '74', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('32', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '75', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('33', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '76', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('34', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '77', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('35', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '78', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('36', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '79', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('37', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '80', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('38', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '81', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('39', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '82', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('40', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '83', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('41', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '84', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('42', '16', '207', '0', '0', 'how to use doc', '0', '12', '456', '', '85', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('43', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '86', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('44', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '87', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('45', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '88', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('46', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '89', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('47', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '90', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('48', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '91', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('49', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '92', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('50', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '93', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('51', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '94', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('52', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '95', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('53', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '96', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('54', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '97', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('55', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '98', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('56', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '99', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('57', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '100', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('58', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '101', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('59', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '102', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('60', '16', '207', '0', '0', 'howtousedocsc', '0', '12', '456', '', '103', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('61', '16', '207', '0', '0', 'how to use docsc', '0', '12', '456', '', '104', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('62', '16', '207', '0', '0', 'how to use docsc', '0', '12', '456', '', '105', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('63', '16', '207', '0', '0', 'how to use docsc', '0', '12', '456', '', '106', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('64', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '107', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('65', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '108', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('66', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '109', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('67', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '110', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('68', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '111', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('69', '16', '207', '0', '0', 'how123tousedocsc', '0', '12', '456', '', '112', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('70', '16', '207', '0', '0', 'how123 tousedocsc', '0', '12', '456', '', '113', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('71', '16', '207', '0', '0', 'how123 to use docsc', '0', '12', '456', '', '114', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('72', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '116', '0000-00-00 00:00:00');
INSERT INTO `doctor_offline_question` VALUES ('73', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '236', '2017-07-19 13:00:32');
INSERT INTO `doctor_offline_question` VALUES ('74', '128', '22', '0', '0', 'test qusestion. ...', '0', '2', '1', '', '237', '2017-07-19 13:02:29');
INSERT INTO `doctor_offline_question` VALUES ('75', '128', '22', '0', '0', 'test qusestion. ...', '0', '2', '0', '', '240', '2017-07-19 13:03:06');
INSERT INTO `doctor_offline_question` VALUES ('76', '128', '22', '0', '0', 'test qusestion. ...', '0', '2', '0', '', '243', '2017-07-19 13:06:06');
INSERT INTO `doctor_offline_question` VALUES ('77', '128', '22', '0', '0', 'test 2', '0', '1', '1', '', '246', '2017-07-19 13:07:50');
INSERT INTO `doctor_offline_question` VALUES ('78', '16', '207', '0', '0', 'howtousedoc', '0', '12', '456', '', '249', '2017-07-19 13:10:10');
INSERT INTO `doctor_offline_question` VALUES ('79', '128', '22', '0', '0', 'test', '0', '0', '0', '', '251', '2017-07-20 10:04:29');
INSERT INTO `doctor_offline_question` VALUES ('80', '16', '11', '0', '0', 'howtousedoc', '0', '12', '456', '', '267', '2017-08-10 15:52:33');

-- ----------------------------
-- Table structure for `doctor_phone`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_phone`;
CREATE TABLE `doctor_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_phone
-- ----------------------------
INSERT INTO `doctor_phone` VALUES ('31', '20', '4877777777', null, '20');
INSERT INTO `doctor_phone` VALUES ('32', '20', '0126666666', null, '21');
INSERT INTO `doctor_phone` VALUES ('33', '20', '01226427786', null, '23');
INSERT INTO `doctor_phone` VALUES ('34', '20', '07538458', null, '24');
INSERT INTO `doctor_phone` VALUES ('41', '2', '01001420808', null, '32');
INSERT INTO `doctor_phone` VALUES ('43', '2', '01226427786', null, '78');
INSERT INTO `doctor_phone` VALUES ('44', '2', '01226427786', null, '79');
INSERT INTO `doctor_phone` VALUES ('45', '2', '01007893000', null, '82');
INSERT INTO `doctor_phone` VALUES ('46', '2', '01093103001', null, '87');
INSERT INTO `doctor_phone` VALUES ('47', '2', '01093103001', null, '88');
INSERT INTO `doctor_phone` VALUES ('48', '2', '01093103001', null, '89');
INSERT INTO `doctor_phone` VALUES ('49', '2', '01226427786', null, '99');
INSERT INTO `doctor_phone` VALUES ('50', '2', '01226427786', null, '100');
INSERT INTO `doctor_phone` VALUES ('51', '2', '01226427786', null, '101');
INSERT INTO `doctor_phone` VALUES ('52', '2', '01226427786', null, '102');
INSERT INTO `doctor_phone` VALUES ('53', '2', '01226427786', null, '103');
INSERT INTO `doctor_phone` VALUES ('54', '2', '01226427786', null, '104');
INSERT INTO `doctor_phone` VALUES ('55', '2', '01100222908', null, '105');
INSERT INTO `doctor_phone` VALUES ('56', '2', '01226427786', null, '106');
INSERT INTO `doctor_phone` VALUES ('57', '2', '01226427786', null, '107');
INSERT INTO `doctor_phone` VALUES ('58', '2', '01100222908', null, '108');
INSERT INTO `doctor_phone` VALUES ('59', '', '01100222908', null, '108');
INSERT INTO `doctor_phone` VALUES ('60', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('61', '2', '01100222908', null, '113');
INSERT INTO `doctor_phone` VALUES ('62', '2', '01226427786', null, '114');
INSERT INTO `doctor_phone` VALUES ('63', '2', '01226427786', null, '120');
INSERT INTO `doctor_phone` VALUES ('64', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('65', '2', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('66', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('67', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('68', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('69', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('70', '', '01100222908', null, '0');
INSERT INTO `doctor_phone` VALUES ('71', '', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('72', '', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('73', '', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('74', '', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('75', '', '01100222908', null, '121');
INSERT INTO `doctor_phone` VALUES ('76', '2', '01100222908', null, '122');
INSERT INTO `doctor_phone` VALUES ('77', '2', '01100222908', null, '123');
INSERT INTO `doctor_phone` VALUES ('78', '2', '01100222908', null, '124');
INSERT INTO `doctor_phone` VALUES ('79', '2', '01100222908', null, '125');
INSERT INTO `doctor_phone` VALUES ('80', '2', '01226427786', null, '126');
INSERT INTO `doctor_phone` VALUES ('81', '2', '01226427786', null, '127');
INSERT INTO `doctor_phone` VALUES ('82', '2', '01226427786', null, '128');
INSERT INTO `doctor_phone` VALUES ('83', '2', '01093103001', null, '133');
INSERT INTO `doctor_phone` VALUES ('84', '2', '01093103001', null, '141');
INSERT INTO `doctor_phone` VALUES ('85', '2', '01226427786', null, '147');
INSERT INTO `doctor_phone` VALUES ('88', '2', '01226427786', null, '158');
INSERT INTO `doctor_phone` VALUES ('90', '2', '01226427786', null, '160');
INSERT INTO `doctor_phone` VALUES ('92', '2', '01226427786', null, '162');
INSERT INTO `doctor_phone` VALUES ('93', '2', '01226427786', null, '163');
INSERT INTO `doctor_phone` VALUES ('95', '2', '01226427786', null, '181');
INSERT INTO `doctor_phone` VALUES ('96', '2', '01001420808', null, '186');
INSERT INTO `doctor_phone` VALUES ('102', '2', '01226427786', null, '189');
INSERT INTO `doctor_phone` VALUES ('104', '2', '01226427786', null, '190');
INSERT INTO `doctor_phone` VALUES ('105', '2', '01226427786', null, '191');
INSERT INTO `doctor_phone` VALUES ('106', '2', '01226427786', null, '192');
INSERT INTO `doctor_phone` VALUES ('107', '2', '01226427786', null, '193');
INSERT INTO `doctor_phone` VALUES ('108', '2', '01275471742', null, '202');
INSERT INTO `doctor_phone` VALUES ('109', '2', '01226427786', null, '201');
INSERT INTO `doctor_phone` VALUES ('113', '02', '01285819291', null, '7');
INSERT INTO `doctor_phone` VALUES ('114', '02', '01285819291', null, '7');
INSERT INTO `doctor_phone` VALUES ('115', '02', '01285819291', null, '7');
INSERT INTO `doctor_phone` VALUES ('118', '02', '01285819291', null, '11');
INSERT INTO `doctor_phone` VALUES ('119', '+2', '01226427786', null, '271');
INSERT INTO `doctor_phone` VALUES ('120', '+2', '01226427786', null, '272');
INSERT INTO `doctor_phone` VALUES ('121', '+2', '01226427786', null, '277');
INSERT INTO `doctor_phone` VALUES ('122', '+2', '01226427786', null, '278');
INSERT INTO `doctor_phone` VALUES ('123', '+2', '01226427786', null, '11');
INSERT INTO `doctor_phone` VALUES ('124', '+2', '01226427786', null, '282');
INSERT INTO `doctor_phone` VALUES ('125', '2', ' 01223117442', null, '285');
INSERT INTO `doctor_phone` VALUES ('126', '2', '01022217122', null, '286');
INSERT INTO `doctor_phone` VALUES ('127', '2', '01114627926', null, '287');
INSERT INTO `doctor_phone` VALUES ('128', '2', '01275471742', null, '255');
INSERT INTO `doctor_phone` VALUES ('129', '02', '01285819291', null, '11');
INSERT INTO `doctor_phone` VALUES ('130', '+2', '01226427786', null, '299');

-- ----------------------------
-- Table structure for `doctor_phone_temp`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_phone_temp`;
CREATE TABLE `doctor_phone_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `activation_code` varchar(45) DEFAULT NULL,
  `exp_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_phone_temp
-- ----------------------------
INSERT INTO `doctor_phone_temp` VALUES ('6', '2', '01274036897', null, '192', '24335', '2017-06-01 06:31');
INSERT INTO `doctor_phone_temp` VALUES ('16', '2', '01069416957', null, '208', '45683', '2017-06-24 09:36');
INSERT INTO `doctor_phone_temp` VALUES ('17', '2', '12345678912', null, '209', '97096', '2017-06-24 09:42');
INSERT INTO `doctor_phone_temp` VALUES ('18', '2', '12345678912', null, '210', '53109', '2017-06-24 10:07');
INSERT INTO `doctor_phone_temp` VALUES ('19', '2', '01285819291', null, '213', '62305', '2017-07-01 08:49');
INSERT INTO `doctor_phone_temp` VALUES ('20', '2', '01285819291', null, '219', '33437', '2017-07-01 09:46');
INSERT INTO `doctor_phone_temp` VALUES ('21', '2', '01285819291', null, '249', '87297', '2017-07-03 11:27');
INSERT INTO `doctor_phone_temp` VALUES ('22', '2', '01285819291', null, '250', '47758', '2017-07-12 04:51');
INSERT INTO `doctor_phone_temp` VALUES ('35', '+376', '55222', null, '270', '47978', '2017-08-08 12:35');
INSERT INTO `doctor_phone_temp` VALUES ('36', '+376', '5398', null, '271', '36781', '2017-08-09 07:43');
INSERT INTO `doctor_phone_temp` VALUES ('39', '+2', '01274036897', null, '11', '51275', '2017-08-15 06:52');
INSERT INTO `doctor_phone_temp` VALUES ('43', '+2', '57657778888', null, '289', '38924', '2017-08-20 16:30');

-- ----------------------------
-- Table structure for `doctor_rates`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_rates`;
CREATE TABLE `doctor_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_rates
-- ----------------------------
INSERT INTO `doctor_rates` VALUES ('1', '163', '1', '3', '', '2017-05-21 05:03:43');
INSERT INTO `doctor_rates` VALUES ('2', '163', '2', '5', '', '2017-05-21 05:03:43');
INSERT INTO `doctor_rates` VALUES ('3', '56', '1', '5', '', '2017-05-21 05:04:32');
INSERT INTO `doctor_rates` VALUES ('4', '57', '2', '4', '', '2017-05-21 05:04:32');
INSERT INTO `doctor_rates` VALUES ('5', '4', '105', '4', '', '2017-05-21 05:22:36');
INSERT INTO `doctor_rates` VALUES ('6', '4', '105', '47', '', '2017-05-21 05:22:57');
INSERT INTO `doctor_rates` VALUES ('7', '4', '105', '47', '', '2017-05-21 05:31:29');
INSERT INTO `doctor_rates` VALUES ('8', '4', '105', '47', '', '2017-05-21 05:31:30');
INSERT INTO `doctor_rates` VALUES ('9', '4', '105', '47', '', '2017-05-21 05:31:30');
INSERT INTO `doctor_rates` VALUES ('10', '4', '105', '4', '', '2017-05-21 05:35:51');
INSERT INTO `doctor_rates` VALUES ('11', '4', '105', '4', '', '2017-05-21 05:35:58');
INSERT INTO `doctor_rates` VALUES ('12', '4', '105', '4', '', '2017-05-21 05:35:58');

-- ----------------------------
-- Table structure for `doctor_referred`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_referred`;
CREATE TABLE `doctor_referred` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referring_doctor` int(11) NOT NULL,
  `referred_doctor` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `referred_doctor` (`referred_doctor`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_referred
-- ----------------------------
INSERT INTO `doctor_referred` VALUES ('1', '11', '143', '0000-00-00 00:00:00');
INSERT INTO `doctor_referred` VALUES ('2', '143', '144', '0000-00-00 00:00:00');
INSERT INTO `doctor_referred` VALUES ('3', '143', '145', '0000-00-00 00:00:00');
INSERT INTO `doctor_referred` VALUES ('4', '11', '146', '0000-00-00 00:00:00');
INSERT INTO `doctor_referred` VALUES ('5', '11', '147', '2017-05-09 15:51:07');
INSERT INTO `doctor_referred` VALUES ('7', '11', '148', '2017-05-09 16:37:33');
INSERT INTO `doctor_referred` VALUES ('8', '11', '149', '2017-05-10 14:12:06');
INSERT INTO `doctor_referred` VALUES ('9', '11', '151', '2017-05-10 14:14:08');
INSERT INTO `doctor_referred` VALUES ('10', '11', '152', '2017-05-10 17:16:44');
INSERT INTO `doctor_referred` VALUES ('11', '11', '155', '2017-05-10 17:22:03');
INSERT INTO `doctor_referred` VALUES ('12', '11', '156', '2017-05-11 09:31:01');
INSERT INTO `doctor_referred` VALUES ('13', '11', '157', '2017-05-11 10:22:39');
INSERT INTO `doctor_referred` VALUES ('14', '11', '158', '2017-05-11 16:17:37');
INSERT INTO `doctor_referred` VALUES ('15', '11', '159', '2017-05-11 16:26:22');
INSERT INTO `doctor_referred` VALUES ('16', '11', '160', '2017-05-11 17:11:33');
INSERT INTO `doctor_referred` VALUES ('17', '11', '161', '2017-05-11 17:17:45');
INSERT INTO `doctor_referred` VALUES ('18', '11', '162', '2017-05-11 17:47:44');
INSERT INTO `doctor_referred` VALUES ('19', '11', '163', '2017-05-11 17:55:38');
INSERT INTO `doctor_referred` VALUES ('20', '11', '165', '2017-05-21 16:30:15');
INSERT INTO `doctor_referred` VALUES ('21', '11', '167', '2017-05-21 16:34:33');
INSERT INTO `doctor_referred` VALUES ('22', '11', '168', '2017-05-23 17:13:31');
INSERT INTO `doctor_referred` VALUES ('23', '2', '169', '2017-05-24 13:35:03');
INSERT INTO `doctor_referred` VALUES ('24', '0', '171', '2017-05-28 04:30:36');
INSERT INTO `doctor_referred` VALUES ('25', '0', '173', '2017-05-28 04:31:33');
INSERT INTO `doctor_referred` VALUES ('26', '0', '175', '2017-05-28 04:33:29');
INSERT INTO `doctor_referred` VALUES ('27', '11', '181', '2017-05-30 12:58:15');
INSERT INTO `doctor_referred` VALUES ('28', '0', '182', '2017-05-30 14:09:58');
INSERT INTO `doctor_referred` VALUES ('29', '11', '184', '2017-05-30 15:07:28');
INSERT INTO `doctor_referred` VALUES ('30', '11', '185', '2017-05-31 10:30:10');
INSERT INTO `doctor_referred` VALUES ('31', '11', '186', '2017-05-31 10:36:41');
INSERT INTO `doctor_referred` VALUES ('32', '0', '187', '2017-05-31 12:52:34');
INSERT INTO `doctor_referred` VALUES ('33', '0', '188', '2017-05-31 13:50:05');
INSERT INTO `doctor_referred` VALUES ('34', '0', '189', '2017-05-31 15:29:07');
INSERT INTO `doctor_referred` VALUES ('35', '0', '190', '2017-06-01 11:57:03');
INSERT INTO `doctor_referred` VALUES ('36', '0', '191', '2017-06-01 12:00:50');
INSERT INTO `doctor_referred` VALUES ('37', '0', '192', '2017-06-01 12:24:05');
INSERT INTO `doctor_referred` VALUES ('38', '0', '193', '2017-06-01 13:44:42');
INSERT INTO `doctor_referred` VALUES ('39', '0', '196', '2017-06-05 14:31:52');
INSERT INTO `doctor_referred` VALUES ('40', '0', '197', '2017-06-05 14:46:09');
INSERT INTO `doctor_referred` VALUES ('41', '0', '198', '2017-06-05 17:13:09');
INSERT INTO `doctor_referred` VALUES ('42', '0', '199', '2017-06-05 17:19:02');
INSERT INTO `doctor_referred` VALUES ('43', '0', '200', '2017-06-05 17:40:32');
INSERT INTO `doctor_referred` VALUES ('44', '0', '201', '2017-06-06 14:18:54');
INSERT INTO `doctor_referred` VALUES ('45', '0', '202', '2017-06-06 15:02:12');
INSERT INTO `doctor_referred` VALUES ('46', '0', '203', '2017-06-08 15:02:55');
INSERT INTO `doctor_referred` VALUES ('47', '0', '204', '2017-06-11 04:23:53');
INSERT INTO `doctor_referred` VALUES ('48', '0', '205', '2017-06-11 04:36:11');
INSERT INTO `doctor_referred` VALUES ('49', '0', '206', '2017-06-11 12:37:05');
INSERT INTO `doctor_referred` VALUES ('50', '0', '207', '2017-06-11 12:51:02');
INSERT INTO `doctor_referred` VALUES ('51', '11', '208', '2017-06-24 15:36:33');
INSERT INTO `doctor_referred` VALUES ('52', '11', '209', '2017-06-24 15:42:57');
INSERT INTO `doctor_referred` VALUES ('53', '11', '210', '2017-06-24 16:07:17');
INSERT INTO `doctor_referred` VALUES ('54', '11', '213', '2017-07-01 14:49:13');
INSERT INTO `doctor_referred` VALUES ('55', '11', '219', '2017-07-01 15:46:57');
INSERT INTO `doctor_referred` VALUES ('56', '11', '249', '2017-07-03 17:27:09');
INSERT INTO `doctor_referred` VALUES ('57', '11', '250', '2017-07-12 10:51:25');
INSERT INTO `doctor_referred` VALUES ('58', '0', '251', '2017-08-04 14:21:53');
INSERT INTO `doctor_referred` VALUES ('59', '0', '253', '2017-08-04 14:22:46');
INSERT INTO `doctor_referred` VALUES ('60', '0', '255', '2017-08-06 10:21:13');
INSERT INTO `doctor_referred` VALUES ('61', '0', '256', '2017-08-06 20:14:31');
INSERT INTO `doctor_referred` VALUES ('62', '0', '258', '2017-08-07 12:38:18');
INSERT INTO `doctor_referred` VALUES ('63', '0', '266', '2017-08-07 13:39:18');
INSERT INTO `doctor_referred` VALUES ('64', '0', '267', '2017-08-07 16:00:37');
INSERT INTO `doctor_referred` VALUES ('65', '0', '268', '2017-08-07 16:56:00');
INSERT INTO `doctor_referred` VALUES ('66', '0', '269', '2017-08-07 17:58:06');
INSERT INTO `doctor_referred` VALUES ('67', '0', '270', '2017-08-08 17:56:56');
INSERT INTO `doctor_referred` VALUES ('68', '0', '271', '2017-08-08 18:48:06');
INSERT INTO `doctor_referred` VALUES ('69', '0', '272', '2017-08-09 16:58:04');
INSERT INTO `doctor_referred` VALUES ('70', '0', '273', '2017-08-10 16:16:06');
INSERT INTO `doctor_referred` VALUES ('71', '0', '276', '2017-08-10 16:38:57');
INSERT INTO `doctor_referred` VALUES ('72', '0', '277', '2017-08-10 16:40:30');
INSERT INTO `doctor_referred` VALUES ('73', '0', '278', '2017-08-10 17:19:11');
INSERT INTO `doctor_referred` VALUES ('74', '0', '279', '2017-08-14 15:50:46');
INSERT INTO `doctor_referred` VALUES ('75', '0', '280', '2017-08-15 15:05:16');
INSERT INTO `doctor_referred` VALUES ('76', '0', '281', '2017-08-15 15:48:32');
INSERT INTO `doctor_referred` VALUES ('77', '0', '282', '2017-08-15 16:05:08');
INSERT INTO `doctor_referred` VALUES ('78', '0', '283', '2017-08-16 10:03:11');
INSERT INTO `doctor_referred` VALUES ('79', '0', '284', '2017-08-17 11:54:10');
INSERT INTO `doctor_referred` VALUES ('80', '22', '287', '2017-08-20 12:01:09');
INSERT INTO `doctor_referred` VALUES ('81', '0', '288', '2017-08-20 20:15:52');
INSERT INTO `doctor_referred` VALUES ('82', '0', '289', '2017-08-20 22:28:43');
INSERT INTO `doctor_referred` VALUES ('83', '0', '291', '2017-08-21 04:29:52');
INSERT INTO `doctor_referred` VALUES ('84', '0', '292', '2017-08-21 04:52:31');
INSERT INTO `doctor_referred` VALUES ('85', '0', '293', '2017-08-24 11:25:17');
INSERT INTO `doctor_referred` VALUES ('86', '0', '296', '2017-09-01 10:31:12');
INSERT INTO `doctor_referred` VALUES ('87', '0', '297', '2017-09-01 10:42:31');
INSERT INTO `doctor_referred` VALUES ('88', '0', '298', '2017-09-07 20:25:21');
INSERT INTO `doctor_referred` VALUES ('89', '0', '299', '2017-09-07 20:26:35');

-- ----------------------------
-- Table structure for `doctor_services`
-- ----------------------------
DROP TABLE IF EXISTS `doctor_services`;
CREATE TABLE `doctor_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctor_services
-- ----------------------------
INSERT INTO `doctor_services` VALUES ('1', '71', '1', '2');
INSERT INTO `doctor_services` VALUES ('2', '202', '0', 'false');
INSERT INTO `doctor_services` VALUES ('3', '202', '2', 'false');
INSERT INTO `doctor_services` VALUES ('4', '251', '3', '1');
INSERT INTO `doctor_services` VALUES ('5', '255', '0', 'false');
INSERT INTO `doctor_services` VALUES ('6', '11', '3', '1');

-- ----------------------------
-- Table structure for `favorite_doctor`
-- ----------------------------
DROP TABLE IF EXISTS `favorite_doctor`;
CREATE TABLE `favorite_doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `temp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of favorite_doctor
-- ----------------------------
INSERT INTO `favorite_doctor` VALUES ('6', '16', '82', '');
INSERT INTO `favorite_doctor` VALUES ('7', '16', '81', '');
INSERT INTO `favorite_doctor` VALUES ('11', '120', '56', '');
INSERT INTO `favorite_doctor` VALUES ('12', '120', '66', '');
INSERT INTO `favorite_doctor` VALUES ('13', '120', '87', '');
INSERT INTO `favorite_doctor` VALUES ('14', '2', '66', '');
INSERT INTO `favorite_doctor` VALUES ('15', '124', '2', '');
INSERT INTO `favorite_doctor` VALUES ('16', '2', '87', '');
INSERT INTO `favorite_doctor` VALUES ('17', '2', '57', '');
INSERT INTO `favorite_doctor` VALUES ('18', '124', '66', '');
INSERT INTO `favorite_doctor` VALUES ('19', '124', '56', '');
INSERT INTO `favorite_doctor` VALUES ('21', '128', '56', '');
INSERT INTO `favorite_doctor` VALUES ('22', '134', '66', '');
INSERT INTO `favorite_doctor` VALUES ('25', '3', '22', '');
INSERT INTO `favorite_doctor` VALUES ('26', '128', '22', '');
INSERT INTO `favorite_doctor` VALUES ('28', '154', '22', '');
INSERT INTO `favorite_doctor` VALUES ('29', '154', '255', '');
INSERT INTO `favorite_doctor` VALUES ('30', '154', '285', '');

-- ----------------------------
-- Table structure for `galleries`
-- ----------------------------
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `key` varchar(191) NOT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `galleries_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of galleries
-- ----------------------------

-- ----------------------------
-- Table structure for `gallery_images`
-- ----------------------------
DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(10) unsigned NOT NULL,
  `source` varchar(191) NOT NULL,
  `path_source` varchar(191) NOT NULL,
  `thumb_source` varchar(191) NOT NULL,
  `mobile_source` varchar(191) NOT NULL,
  `order_number` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_images_gallery_id_foreign` (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gallery_images
-- ----------------------------

-- ----------------------------
-- Table structure for `home_visit`
-- ----------------------------
DROP TABLE IF EXISTS `home_visit`;
CREATE TABLE `home_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `is_now` int(11) NOT NULL,
  `visit_start_date` varchar(255) NOT NULL,
  `visit_starttime` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheduled` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of home_visit
-- ----------------------------
INSERT INTO `home_visit` VALUES ('1', '106', '26', '0', '6/6/2017', '12:46', '0', '12', '0000-00-00 00:00:00', '88/8/2017');
INSERT INTO `home_visit` VALUES ('2', '106', '26', '0', '6/6/2017', '12:46', '0', '13', '0000-00-00 00:00:00', '');
INSERT INTO `home_visit` VALUES ('3', '106', '26', '0', '6/6/2017', '12:46', '0', '14', '0000-00-00 00:00:00', '');
INSERT INTO `home_visit` VALUES ('4', '106', '26', '0', '6/6/2017', '12:46', '0', '15', '0000-00-00 00:00:00', '');
INSERT INTO `home_visit` VALUES ('5', '106', '26', '0', '2017-07-17', '07:27:06', '0', '157', '2017-07-17 13:27:06', '');
INSERT INTO `home_visit` VALUES ('6', '106', '26', '0', '2017-07-17', '07:28:09', '0', '159', '2017-07-17 13:28:09', '');
INSERT INTO `home_visit` VALUES ('7', '106', '26', '0', '2017-07-17', '08:08:05', '0', '165', '2017-07-17 14:08:05', '');
INSERT INTO `home_visit` VALUES ('8', '106', '26', '0', '2017-07-17', '08:08:25', '0', '166', '2017-07-17 14:08:25', '');
INSERT INTO `home_visit` VALUES ('9', '106', '26', '0', '2017-07-17', '08:08:25', '0', '167', '2017-07-17 14:08:25', '');
INSERT INTO `home_visit` VALUES ('10', '106', '26', '0', '2017-07-17', '08:08:26', '0', '168', '2017-07-17 14:08:26', '');
INSERT INTO `home_visit` VALUES ('11', '128', '26', '1', '2017-07-17', '08:10:47', '0', '179', '2017-07-17 14:10:47', '');
INSERT INTO `home_visit` VALUES ('12', '128', '26', '0', '2017-07-17', '08:12:01', '0', '182', '2017-07-17 14:12:01', '');
INSERT INTO `home_visit` VALUES ('13', '128', '26', '0', '2017-07-17', '08:12:34', '0', '183', '2017-07-17 14:12:34', '');
INSERT INTO `home_visit` VALUES ('14', '128', '26', '0', '2017-07-17', '08:12:46', '0', '184', '2017-07-17 14:12:46', '');
INSERT INTO `home_visit` VALUES ('15', '128', '26', '0', '2017-07-17', '08:13:53', '0', '185', '2017-07-17 14:13:53', '');
INSERT INTO `home_visit` VALUES ('16', '128', '26', '0', '2017-07-17', '08:14:19', '0', '186', '2017-07-17 14:14:19', '');
INSERT INTO `home_visit` VALUES ('17', '128', '26', '0', '2017-07-17', '08:14:23', '0', '187', '2017-07-17 14:14:23', '');
INSERT INTO `home_visit` VALUES ('18', '128', '26', '0', '2017-07-17', '08:14:47', '0', '188', '2017-07-17 14:14:47', '');
INSERT INTO `home_visit` VALUES ('19', '128', '26', '0', '2017-07-17', '08:15:23', '0', '189', '2017-07-17 14:15:23', '');
INSERT INTO `home_visit` VALUES ('20', '128', '26', '0', '2017-07-17', '08:15:43', '0', '190', '2017-07-17 14:15:43', '');
INSERT INTO `home_visit` VALUES ('21', '128', '26', '0', '2017-07-17', '08:15:53', '0', '191', '2017-07-17 14:15:53', '');
INSERT INTO `home_visit` VALUES ('22', '128', '26', '0', '2017-07-17', '08:16:04', '0', '192', '2017-07-17 14:16:04', '');
INSERT INTO `home_visit` VALUES ('23', '128', '26', '0', '2017-07-17', '08:16:14', '0', '193', '2017-07-17 14:16:14', '');
INSERT INTO `home_visit` VALUES ('24', '128', '26', '0', '2017-07-17', '08:21:55', '0', '199', '2017-07-17 14:21:55', '');
INSERT INTO `home_visit` VALUES ('25', '128', '26', '0', '2017-07-17', '08:24:00', '0', '204', '2017-07-17 14:24:00', '');
INSERT INTO `home_visit` VALUES ('26', '128', '22', '0', '2017-07-17', '08:27:57', '0', '208', '2017-07-17 14:27:57', '');
INSERT INTO `home_visit` VALUES ('27', '128', '26', '0', '2017-07-17', '08:28:21', '0', '209', '2017-07-17 14:28:21', '');
INSERT INTO `home_visit` VALUES ('28', '128', '26', '0', '2017-07-17', '08:28:50', '0', '210', '2017-07-17 14:28:50', '');
INSERT INTO `home_visit` VALUES ('29', '128', '22', '0', '2017-07-17', '08:29:52', '0', '211', '2017-07-17 14:29:52', '');
INSERT INTO `home_visit` VALUES ('30', '128', '26', '0', '2017-07-17', '08:41:04', '0', '212', '2017-07-17 14:41:04', '');
INSERT INTO `home_visit` VALUES ('31', '128', '26', '0', '2017-07-17', '10:07:53', '0', '219', '2017-07-17 16:07:53', '');
INSERT INTO `home_visit` VALUES ('32', '128', '22', '0', '2017-07-17', '10:09:36', '0', '224', '2017-07-17 16:09:36', '');
INSERT INTO `home_visit` VALUES ('33', '128', '26', '0', '20/07/2017', '16:1', '0', '227', '2017-07-17 16:13:01', '');
INSERT INTO `home_visit` VALUES ('34', '128', '26', '0', '20/07/2017', '16:1', '0', '228', '2017-07-17 16:13:25', '');
INSERT INTO `home_visit` VALUES ('35', '128', '22', '0', '20/07/2017', '16:14', '0', '229', '2017-07-17 16:15:34', '');
INSERT INTO `home_visit` VALUES ('36', '3', '22', '0', '7/31/17', '14:42', '0', '0', '2017-07-30 23:43:50', '');
INSERT INTO `home_visit` VALUES ('37', '106', '11', '0', '6/6/2017', '12:46', '0', '278', '2017-08-10 16:09:35', '');
INSERT INTO `home_visit` VALUES ('38', '106', '11', '0', '6/6/2017', '12:46', '0', '279', '2017-08-10 16:10:48', '');
INSERT INTO `home_visit` VALUES ('39', '128', '22', '0', '2017-08-17', '05:08:31', '0', '282', '2017-08-17 11:08:31', '');
INSERT INTO `home_visit` VALUES ('40', '2', '255', '0', '6/10/2017', '12:46', '0', '0', '2017-09-06 10:15:19', '');
INSERT INTO `home_visit` VALUES ('41', '2', '255', '0', '6/10/2017', '12:46', '0', '0', '2017-09-06 10:15:23', '');
INSERT INTO `home_visit` VALUES ('42', '2', '255', '0', '6/10/2017', '12:46', '0', '349', '2017-09-06 10:17:54', '');
INSERT INTO `home_visit` VALUES ('43', '2', '255', '0', '6/10/2017', '12:46', '0', '350', '2017-09-06 10:19:40', '');
INSERT INTO `home_visit` VALUES ('44', '2', '255', '0', '6/10/2017', '12:46', '0', '351', '2017-09-06 10:21:23', '');
INSERT INTO `home_visit` VALUES ('45', '2', '255', '0', '6/10/2017', '12:46', '0', '352', '2017-09-06 10:23:23', '');
INSERT INTO `home_visit` VALUES ('46', '2', '255', '0', '6/10/2017', '12:46', '0', '353', '2017-09-06 10:24:15', '');
INSERT INTO `home_visit` VALUES ('47', '2', '255', '0', '6/10/2017', '12:46', '0', '354', '2017-09-06 10:24:26', '');
INSERT INTO `home_visit` VALUES ('48', '2', '255', '0', '6/10/2017', '12:46', '0', '355', '2017-09-06 10:43:38', '');
INSERT INTO `home_visit` VALUES ('49', '160', '255', '0', '10/09/2017', '22:0', '0', '10', '2017-09-10 16:15:10', '');

-- ----------------------------
-- Table structure for `leads`
-- ----------------------------
DROP TABLE IF EXISTS `leads`;
CREATE TABLE `leads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(191) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leads
-- ----------------------------

-- ----------------------------
-- Table structure for `lead_maileds`
-- ----------------------------
DROP TABLE IF EXISTS `lead_maileds`;
CREATE TABLE `lead_maileds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) NOT NULL,
  `subject` varchar(191) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lead_maileds
-- ----------------------------

-- ----------------------------
-- Table structure for `lead_settings`
-- ----------------------------
DROP TABLE IF EXISTS `lead_settings`;
CREATE TABLE `lead_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mailer_name` varchar(191) DEFAULT NULL,
  `thank_you_subject` varchar(191) DEFAULT NULL,
  `thank_you_body` text,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lead_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `levels`
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_en` varchar(45) DEFAULT NULL,
  `title_ar` varchar(45) DEFAULT NULL,
  `offline_question` varchar(255) NOT NULL,
  `chat` varchar(255) NOT NULL,
  `call` varchar(255) NOT NULL,
  `visit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of levels
-- ----------------------------
INSERT INTO `levels` VALUES ('1', 'GP Doctor', 'ممارس عام', '5', '10', '15', '20');
INSERT INTO `levels` VALUES ('2', 'Specialist', 'اخصائي', '5', '10', '15', '20');
INSERT INTO `levels` VALUES ('3', 'Consultant', 'أستشاري', '5', '10', '15', '20');
INSERT INTO `levels` VALUES ('4', 'Professor', 'دكتور', '5', '10', '15', '20');

-- ----------------------------
-- Table structure for `locations`
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `url` varchar(191) NOT NULL,
  `key` varchar(191) NOT NULL,
  `map_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  `address` text,
  `latitude` double(10,8) NOT NULL,
  `longitude` double(10,8) NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `thumb_image` varchar(191) DEFAULT NULL,
  `marker_image` varchar(191) DEFAULT NULL,
  `order_number` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locations_key_unique` (`key`),
  KEY `locations_map_id_foreign` (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of locations
-- ----------------------------

-- ----------------------------
-- Table structure for `maps`
-- ----------------------------
DROP TABLE IF EXISTS `maps`;
CREATE TABLE `maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `key` varchar(191) NOT NULL,
  `description` text,
  `zoom` int(11) NOT NULL DEFAULT '10',
  `latitude` double(10,8) NOT NULL,
  `longitude` double(10,8) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `maps_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of maps
-- ----------------------------

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2016_06_23_150540_create_admins_table', '1');
INSERT INTO `migrations` VALUES ('2', '2016_08_15_131330_create_galleries_table', '1');
INSERT INTO `migrations` VALUES ('3', '2016_08_15_141821_create_gallery_images_table', '1');
INSERT INTO `migrations` VALUES ('4', '2016_08_17_094707_create_blog_posts_table', '1');
INSERT INTO `migrations` VALUES ('5', '2016_08_17_104428_create_blog_post_comments_table', '1');
INSERT INTO `migrations` VALUES ('6', '2016_08_17_121213_create_products_table', '1');
INSERT INTO `migrations` VALUES ('7', '2016_08_17_122159_create_product_categories_table', '1');
INSERT INTO `migrations` VALUES ('8', '2016_08_17_122513_create_product_comments_table', '1');
INSERT INTO `migrations` VALUES ('9', '2016_08_17_152152_create_orders_table', '1');
INSERT INTO `migrations` VALUES ('10', '2016_08_18_090228_create_order_items_table', '1');
INSERT INTO `migrations` VALUES ('11', '2016_08_18_090600_create_order_statuses_table', '1');
INSERT INTO `migrations` VALUES ('12', '2016_08_25_124505_create_pages_table', '1');
INSERT INTO `migrations` VALUES ('13', '2016_08_25_151518_create_page_element_types_table', '1');
INSERT INTO `migrations` VALUES ('14', '2016_08_25_151613_create_page_elements_table', '1');
INSERT INTO `migrations` VALUES ('15', '2016_08_30_091003_create_settings_table', '1');
INSERT INTO `migrations` VALUES ('16', '2016_08_30_124811_add_blog_category_id_in_blog_post_table', '1');
INSERT INTO `migrations` VALUES ('17', '2016_08_30_124811_create_blog_categories_table', '1');
INSERT INTO `migrations` VALUES ('18', '2016_09_01_124811_create_leads_table', '1');
INSERT INTO `migrations` VALUES ('19', '2016_09_01_174811_create_lead_settings_table', '1');
INSERT INTO `migrations` VALUES ('20', '2016_09_02_122154_create_lead_maileds_table', '1');
INSERT INTO `migrations` VALUES ('21', '2016_09_09_095636_add_elements_prefix_in_pages_table', '1');
INSERT INTO `migrations` VALUES ('22', '2016_09_28_135802_add_long_description_in_products', '1');
INSERT INTO `migrations` VALUES ('23', '2016_09_28_141609_add_key_to_galleries_table', '1');
INSERT INTO `migrations` VALUES ('24', '2016_09_29_090244_create_similar_products_table', '1');
INSERT INTO `migrations` VALUES ('25', '2016_09_29_132048_create_maps_table', '1');
INSERT INTO `migrations` VALUES ('26', '2016_09_29_132100_create_locations_table', '1');
INSERT INTO `migrations` VALUES ('27', '2016_10_04_144425_change_uri_key_and_url_id_to_slug', '1');
INSERT INTO `migrations` VALUES ('28', '2016_12_13_143350_add_url_to_locations_table', '1');
INSERT INTO `migrations` VALUES ('29', '2017_05_01_004700_change_page_keyword_to_keywords', '1');
INSERT INTO `migrations` VALUES ('30', '2014_10_12_000000_create_users_table', '2');
INSERT INTO `migrations` VALUES ('31', '2014_10_12_100000_create_password_resets_table', '2');
INSERT INTO `migrations` VALUES ('32', '2017_05_26_093125_create_patient_requests_table', '2');
INSERT INTO `migrations` VALUES ('33', '2017_05_30_070329_create_patient_uploads_table', '2');
INSERT INTO `migrations` VALUES ('34', '2017_05_30_121145_create_radiology_type_categories_table', '2');
INSERT INTO `migrations` VALUES ('35', '2017_05_30_121235_create_radiology_types_table', '2');
INSERT INTO `migrations` VALUES ('36', '2017_05_31_054338_create_centers_table', '2');
INSERT INTO `migrations` VALUES ('37', '2017_05_31_054525_create_center_phones_table', '2');
INSERT INTO `migrations` VALUES ('38', '2017_05_31_054923_create_center_responses_table', '2');
INSERT INTO `migrations` VALUES ('39', '2017_05_31_055005_create_response_radiology_items_table', '2');
INSERT INTO `migrations` VALUES ('40', '2017_05_31_055019_create_response_centers_table', '2');
INSERT INTO `migrations` VALUES ('41', '2017_05_31_115249_create_radiology_questions_table', '2');
INSERT INTO `migrations` VALUES ('42', '2017_05_31_121210_create_request_answers_table', '2');
INSERT INTO `migrations` VALUES ('43', '2017_06_03_081723_create_user_types_table', '2');
INSERT INTO `migrations` VALUES ('44', '2017_06_03_082026_Add_UserTypeId_To_Users_Table', '2');
INSERT INTO `migrations` VALUES ('45', '2017_06_03_125553_create_url_groups_table', '2');
INSERT INTO `migrations` VALUES ('46', '2017_06_03_125730_create_urls_table', '2');
INSERT INTO `migrations` VALUES ('47', '2017_06_03_131641_create_user_type_urls_table', '2');
INSERT INTO `migrations` VALUES ('48', '2014_10_09_000000_addQuestionTypeToRadiologyQuestion', '3');
INSERT INTO `migrations` VALUES ('49', '2014_10_10_000000_changeRequestAnswerToText', '3');
INSERT INTO `migrations` VALUES ('50', '2017_06_06_015834_add_request_status_to_patient_requests', '3');
INSERT INTO `migrations` VALUES ('51', '2017_06_06_021143_alter_request_status_to_patient_requests', '3');
INSERT INTO `migrations` VALUES ('52', '2017_06_06_022033_add_request_status_nullable_to_patient_requests', '3');
INSERT INTO `migrations` VALUES ('59', '2017_06_10_130244_create_View_v_patient', '4');
INSERT INTO `migrations` VALUES ('60', '2017_06_10_130316_create_View_v_patientrequestuploads', '4');
INSERT INTO `migrations` VALUES ('61', '2017_06_10_130347_create_View_v_patientanswers', '4');
INSERT INTO `migrations` VALUES ('62', '2017_06_10_131700_create_View_v_patientrequests', '4');
INSERT INTO `migrations` VALUES ('63', '2017_06_10_141431_add_column_comment_to_patient_requests', '5');
INSERT INTO `migrations` VALUES ('64', '2017_07_03_103528_radiology_confirmed', '5');
INSERT INTO `migrations` VALUES ('65', '2017_07_28_231738_add_report_to_patient_request_table', '6');
INSERT INTO `migrations` VALUES ('66', '2017_07_30_134238_AddUploadsToPatientRequestTable', '7');
INSERT INTO `migrations` VALUES ('67', '2017_07_31_071759_alter_view_v_patient_request', '7');
INSERT INTO `migrations` VALUES ('68', '2017_07_31_071842_alter_view_v_patient', '8');
INSERT INTO `migrations` VALUES ('69', '2017_07_31_074643_add_report_to_patient_requests_table', '9');
INSERT INTO `migrations` VALUES ('70', '2017_07_31_081359_add_definitaion_to_radiology_types_table', '10');
INSERT INTO `migrations` VALUES ('71', '2017_07_31_081420_add_notes_to_radiology_types_table', '10');
INSERT INTO `migrations` VALUES ('72', '2017_07_31_081545_add_preparation_to_radiology_types_table', '10');
INSERT INTO `migrations` VALUES ('73', '2017_07_31_082058_alter_preparation_response_radiology_items_table', '11');
INSERT INTO `migrations` VALUES ('74', '2017_07_31_082122_alter_definition_response_radiology_items_table', '12');
INSERT INTO `migrations` VALUES ('75', '2017_07_31_082140_alter_notes_response_radiology_items_table', '13');
INSERT INTO `migrations` VALUES ('76', '2017_07_31_113400_center_report', '14');
INSERT INTO `migrations` VALUES ('77', '2017_08_01_081228_add_price_to_radiology_types', '14');
INSERT INTO `migrations` VALUES ('78', '2017_08_01_113211_add_rate_column_PatientRequests', '14');
INSERT INTO `migrations` VALUES ('79', '2017_07_08_143230_create_Vendor_table', '15');
INSERT INTO `migrations` VALUES ('80', '2017_07_08_143246_create_VendorBranches_table', '15');
INSERT INTO `migrations` VALUES ('81', '2017_07_08_143301_create_VendorContacts_table', '15');
INSERT INTO `migrations` VALUES ('82', '2017_07_08_143151_create_ServiceTypes_table', '16');
INSERT INTO `migrations` VALUES ('83', '2017_07_08_143218_create_Services_table', '16');
INSERT INTO `migrations` VALUES ('84', '2017_07_08_143316_create_ServiceFiles_table', '16');
INSERT INTO `migrations` VALUES ('85', '2017_07_08_143331_create_VendorServices_table', '17');
INSERT INTO `migrations` VALUES ('86', '2017_07_08_143333_create_VendorUploads_table', '17');
INSERT INTO `migrations` VALUES ('87', '2017_07_11_142837_create_VendorServicesTypes_table', '17');
INSERT INTO `migrations` VALUES ('88', '2017_07_11_142920_create_VendorItems_table', '17');
INSERT INTO `migrations` VALUES ('89', '2017_07_11_142939_create_VendorItemBranch_table', '17');
INSERT INTO `migrations` VALUES ('90', '2017_07_11_143554_create_VendorItemBranches_table', '17');
INSERT INTO `migrations` VALUES ('91', '2017_07_13_135131_create_QuestionTypes_table', '18');
INSERT INTO `migrations` VALUES ('92', '2017_07_13_135300_create_VendorServiceQuestion_table', '18');
INSERT INTO `migrations` VALUES ('93', '2017_07_16_112945_add_column_gender_question', '18');
INSERT INTO `migrations` VALUES ('94', '2017_07_16_113055_add_column_vendor_location', '18');
INSERT INTO `migrations` VALUES ('95', '2017_07_13_121915_create_RequestHeader_table', '19');
INSERT INTO `migrations` VALUES ('96', '2017_07_13_121944_create_RequestDetail_table', '19');
INSERT INTO `migrations` VALUES ('97', '2017_07_13_122014_create_RequestUploads_table', '19');
INSERT INTO `migrations` VALUES ('98', '2017_07_13_135656_create_RequestQuestion_table', '19');
INSERT INTO `migrations` VALUES ('99', '2017_07_13_140741_create_RequestVendorBranches_table', '19');
INSERT INTO `migrations` VALUES ('100', '2017_07_13_150027_create_request_status_table', '19');
INSERT INTO `migrations` VALUES ('101', '2017_07_13_150029_create_response_request_status_table', '19');
INSERT INTO `migrations` VALUES ('102', '2017_07_13_150245_create_RequestLog_table', '19');
INSERT INTO `migrations` VALUES ('103', '2017_07_16_113304_add_column_vendor_request_review', '19');

-- ----------------------------
-- Table structure for `offline_question`
-- ----------------------------
DROP TABLE IF EXISTS `offline_question`;
CREATE TABLE `offline_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_degree_id` int(11) NOT NULL,
  `doctor_speciality_id` int(11) NOT NULL,
  `doctor_sub_speciality_id` int(11) NOT NULL,
  `question_details` text NOT NULL,
  `in_hurry` int(11) NOT NULL,
  `images_number` varchar(255) NOT NULL,
  `recordings_number` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `doctor_id` int(11) DEFAULT NULL,
  `from` varchar(11) DEFAULT NULL,
  `resource_type` varchar(255) NOT NULL DEFAULT '1',
  `resource` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of offline_question
-- ----------------------------
INSERT INTO `offline_question` VALUES ('1', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '1', '2017-09-06 13:03:28', '255', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('2', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '2', '2017-09-06 13:03:36', '255', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('3', '160', '0', '0', '0', 'test patient', '0', '1', '1', '', '3', '2017-09-06 13:27:30', '255', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('4', '160', '0', '0', '0', '', '0', '', '', '', '3', '2017-09-06 13:27:33', null, 'p', '2', 'reply_patiet_160_1504697253.jpeg');
INSERT INTO `offline_question` VALUES ('5', '160', '0', '0', '0', '', '0', '', '', '', '3', '2017-09-06 13:27:33', null, 'p', '3', 'reply_patiet_160_1504697254.3gp');
INSERT INTO `offline_question` VALUES ('6', '160', '0', '0', '0', 'test', '0', '0', '2', '', '4', '2017-09-06 13:51:56', '255', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('7', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-06 13:51:57', null, 'p', '3', 'reply_patiet_160_1504698717.3gp');
INSERT INTO `offline_question` VALUES ('8', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-06 13:51:57', null, 'p', '3', 'reply_patiet_160_1504698718.3gp');
INSERT INTO `offline_question` VALUES ('9', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '0', '2017-09-06 15:44:45', '207', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('10', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '0', '2017-09-06 15:44:48', '207', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('11', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '5', '2017-09-06 15:47:09', '207', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('12', '16', '0', '0', '0', 'howtousedoc', '0', '12', '456', '', '6', '2017-09-06 15:50:21', '207', 'p', '1', '');
INSERT INTO `offline_question` VALUES ('13', '160', '0', '0', '0', 'test', '0', '', '', '', '4', '2017-09-10 10:32:43', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('14', '160', '0', '0', '0', 't', '0', '', '', '', '4', '2017-09-10 10:54:51', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('15', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 10:59:26', null, 'p', '2', 'reply_patiet_160_1505033966.jpeg');
INSERT INTO `offline_question` VALUES ('16', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:03:04', null, 'p', '2', 'reply_patiet_160_1505034184.jpeg');
INSERT INTO `offline_question` VALUES ('17', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:03:37', null, 'p', '3', 'reply_patiet_160_1505034217.3gp');
INSERT INTO `offline_question` VALUES ('18', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:23:18', null, 'p', '2', 'reply_patiet_160_1505035399.jpg');
INSERT INTO `offline_question` VALUES ('19', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:27:09', null, 'p', '2', 'reply_patiet_160_1505035630.jpeg');
INSERT INTO `offline_question` VALUES ('20', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:39:19', null, 'p', '2', 'reply_patiet_160_1505036359.jpeg');
INSERT INTO `offline_question` VALUES ('21', '16', '0', '0', '0', '123message', '0', '', '', '', '1', '2017-09-10 11:53:41', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('22', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 11:54:09', null, 'p', '2', 'reply_patiet_160_1505037249.jpeg');
INSERT INTO `offline_question` VALUES ('23', '16', '0', '0', '0', '123message', '0', '', '', '', '1', '2017-09-10 11:55:08', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('24', '16', '0', '0', '0', '123message', '0', '', '', '', '1', '2017-09-10 11:57:49', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('25', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 12:01:22', null, 'p', '2', 'reply_patiet_160_1505037683.jpeg');
INSERT INTO `offline_question` VALUES ('26', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 13:07:16', null, 'p', '3', 'reply_patiet_160_1505041636.3gp');
INSERT INTO `offline_question` VALUES ('27', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 13:14:07', null, 'p', '3', 'reply_patiet_160_1505042048.3gp');
INSERT INTO `offline_question` VALUES ('28', '160', '0', '0', '0', 'rrrr', '0', '', '', '', '4', '2017-09-10 13:30:51', null, 'p', '1', '');
INSERT INTO `offline_question` VALUES ('29', '160', '0', '0', '0', 'test', '0', '', '', '', '4', '2017-09-10 13:40:38', '255', 'd', '1', '');
INSERT INTO `offline_question` VALUES ('30', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 13:41:31', '255', 'd', '2', 'reply_patiet_255_1505043691.jpeg');
INSERT INTO `offline_question` VALUES ('31', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 13:44:01', '255', 'd', '3', 'reply_patiet_255_1505043841.3gp');
INSERT INTO `offline_question` VALUES ('32', '160', '0', '0', '0', '', '0', '', '', '', '4', '2017-09-10 14:05:08', '255', 'd', '2', 'reply_patiet_255_1505045108.jpeg');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `patient`
-- ----------------------------
DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `user_state` varchar(45) DEFAULT NULL,
  `current_credit` varchar(45) DEFAULT '0',
  `profile_img` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `date_of_brith` date NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `login_type` int(11) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient
-- ----------------------------
INSERT INTO `patient` VALUES ('2', 'Asmaa Kohla', 'asmaa.kohla@gmail.com', 'F', '2', '0', 'pat_2_2017-04-1910:48:49.jpeg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2015-07-24', '', '', '1', '');
INSERT INTO `patient` VALUES ('3', 'Mohamed Metwally', 'metwally@gmail.com', 'M', '2', '0', null, 'sdA7D8/j8M5k+jQ9xMEYIO41JZf0oO2L+9Z6pWqoNRs=', '1976-09-18', '', '', '1', '');
INSERT INTO `patient` VALUES ('4', 'z', 'mohammad.shabaan@gmail.com12', 'M', '0', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '0000-00-00', '', '', '1', '');
INSERT INTO `patient` VALUES ('5', 'Samar', 'samaribrahimdoc@gmail.com', 'M', '0', '0', null, 'CR1dy637kT1v27d9+pNTdKpANJGZxp7/aXamZSZJWBA=', '0000-00-00', '', '', '1', '');
INSERT INTO `patient` VALUES ('8', 'samar Ibrahim ', 'samaribrahim@test.com', 'F', '2', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '0000-00-00', '', '', '1', '');
INSERT INTO `patient` VALUES ('39', 'asfhj', 'asmaa@gmail.com', 'M', '0', '0', null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '0000-00-00', '', '', '1', '');
INSERT INTO `patient` VALUES ('74', 'Soly', 'soli.abdallah@gmail.com', 'M', '0', '0', 'pat_74_2017-03-2810:19:51.jpg', 'ryLue5pTtekh4TLEBjLsTn/ZnaZ1Fcj3Z/IF3wXXXoY=', '0000-00-00', '', '', '1', '');
INSERT INTO `patient` VALUES ('154', 'asmaa.new', 'asmaa.new@gmail.com', 'F', '2', '0', null, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2001-01-01', '', '', '1', '');
INSERT INTO `patient` VALUES ('155', 'asmaaaa', 'asmaa@Sam.com', 'M', '2', '0', null, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '1989-06-15', '', '', '1', '');
INSERT INTO `patient` VALUES ('156', 'Asmaa Kohla', 'asmaa.kohla@yahoo.com', 'F', '2', '0', null, 'cygGLqlfhk6J7w7XuMGWgpQOJWizlAUFi2Yt5/Q68xM=', '1989-06-15', '10209781862355987', '', '1', '');
INSERT INTO `patient` VALUES ('159', 'mohamed', 'oovv12234090@m.co', 'm', '0', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2017-07-24', '', 'mohamedoniki', '1', '');
INSERT INTO `patient` VALUES ('160', 'Rami El bouhii', 'rami.elbouhi@gmail.com', 'M', '2', '0', 'pat_74_2017-03-2810:19:51.jpg', 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '1987-12-02', '', 'Ramii6a70', '1', '');
INSERT INTO `patient` VALUES ('161', 'Rami El-bouhi', 'r@gmail.com', 'M', '2', '0', 'patient_161_1504110875.jpg', 'cygGLqlfhk6J7w7XuMGWgpQOJWizlAUFi2Yt5/Q68xM=', '1988-12-02', '1267637473355866', 'Rami6czg8', '1', '');
INSERT INTO `patient` VALUES ('162', 'moha', 'moh.sha@gmail.com', 'M', '2', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '1990-10-27', '', 'mohac9mxb', '1', '');
INSERT INTO `patient` VALUES ('164', 'Asmaa Kohla', 'asmaaohla@yahoo.com', 'F', '0', '0', null, 'cygGLqlfhk6J7w7XuMGWgpQOJWizlAUFi2Yt5/Q68xM=', '1989-06-15', '10209781862355987', 'Asmaa8hm4i', '1', '');
INSERT INTO `patient` VALUES ('166', 'Asmaa Kohla', 'askohla@yahoo.com', 'F', '2', '0', null, 'cygGLqlfhk6J7w7XuMGWgpQOJWizlAUFi2Yt5/Q68xM=', '1989-06-15', '10209781862355987', 'Asmaa7ph6x', '1', '');
INSERT INTO `patient` VALUES ('169', 'mohamed', 'm12234090@m.co', 'm', '0', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2017-07-24', '', 'mohamedycm5t', '1', '');
INSERT INTO `patient` VALUES ('170', 'ahmed', 'ahmed1@mailinator.com', 'M', '2', '0', 'patient_170_1504343719.jpg', 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '1992-01-01', '', 'ahmed93en5', '1', '');
INSERT INTO `patient` VALUES ('171', '       ', 'a@b.com', 'F', '0', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2990-01-01', '', 'c77gq', '1', '');
INSERT INTO `patient` VALUES ('173', '    ', 'a@c.com', 'M', '0', '0', null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '1900-01-01', '', '5nqfw', '1', '');
INSERT INTO `patient` VALUES ('174', 'ahmed', 'a@d.com', 'M', '2', '0', null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2005-01-01', '', 'ahmedawg1c', '1', '');
INSERT INTO `patient` VALUES ('175', 'Ahmed', 'ahmed@mailinator.com', 'M', '2', '0', null, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '0000-00-00', '', 'Ahmedbtp03', '1', '');
INSERT INTO `patient` VALUES ('178', 'mohamed', 'm12234090888@m.co', 'm', '0', '0', null, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2017-07-24', '', 'mohamed60tnc', '0', '100462954337773576908');

-- ----------------------------
-- Table structure for `patient_addresses`
-- ----------------------------
DROP TABLE IF EXISTS `patient_addresses`;
CREATE TABLE `patient_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT 'EG',
  `city` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `patient_id` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_addresses
-- ----------------------------
INSERT INTO `patient_addresses` VALUES ('34', 'EG', '1', '31.1256852', '29.7836783', '78', 'data data');
INSERT INTO `patient_addresses` VALUES ('35', 'EG', '', '31.1256852', '29.7836783', '78', '');
INSERT INTO `patient_addresses` VALUES ('36', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `patient_addresses` VALUES ('37', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `patient_addresses` VALUES ('38', '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses');
INSERT INTO `patient_addresses` VALUES ('41', 'EG', '1', '31.1256852', '29.7836783', '105', 'xxx st');
INSERT INTO `patient_addresses` VALUES ('42', 'EG', '', '31.1256852', '29.7836783', '105', '');
INSERT INTO `patient_addresses` VALUES ('43', 'EG', '1', '31.1256852', '29.7836783', '105', 'xxxx st');
INSERT INTO `patient_addresses` VALUES ('44', 'EG', '1', '31.1256852', '29.7836783', '105', 'xxxxx ST');
INSERT INTO `patient_addresses` VALUES ('45', 'EG', '', '31.1256852', '29.7836783', '105', '');
INSERT INTO `patient_addresses` VALUES ('46', 'EG', '', '31.1256852', '29.7836783', '105', '');
INSERT INTO `patient_addresses` VALUES ('47', 'EG', '', '31.1256852', '29.7836783', '105', '');
INSERT INTO `patient_addresses` VALUES ('48', 'EG', '', '31.1256852', '29.7836783', '105', '');
INSERT INTO `patient_addresses` VALUES ('49', 'EG', '1', '', '', '114', 'test aa');
INSERT INTO `patient_addresses` VALUES ('50', 'EG', '1', '', '', '114', 'demo');
INSERT INTO `patient_addresses` VALUES ('51', 'EG', '', '31.1269407', '29.782796', '120', '');
INSERT INTO `patient_addresses` VALUES ('52', 'EG', '', '31.20212392772522', '29.96232323348522', '120', '');
INSERT INTO `patient_addresses` VALUES ('53', 'EG', '1', '', '', '2', 'addddress');
INSERT INTO `patient_addresses` VALUES ('54', 'EG', '', '31.1257769', '29.7833164', '2', '');
INSERT INTO `patient_addresses` VALUES ('55', 'EG', '1', '', '', '2', 'new address');
INSERT INTO `patient_addresses` VALUES ('59', 'EG', '', '31.19854689335236', '29.92978267371655', '3', '');
INSERT INTO `patient_addresses` VALUES ('60', 'EG', '', '31.2026622062055', '29.95438691228628', '2', '');
INSERT INTO `patient_addresses` VALUES ('62', 'EG', '1', '', '', '134', 'Ø¯Ø±Ø¨ØªØ¨');
INSERT INTO `patient_addresses` VALUES ('63', 'EG', '', '31.203018380266144', '29.963619075715542', '3', '');
INSERT INTO `patient_addresses` VALUES ('64', 'EG', '2', '', '', '3', 'Ø´Ø§Ø±Ø¹ Ø¬Ø±ÙŠÙ† Ø¨Ù„Ø§Ø²Ø§');
INSERT INTO `patient_addresses` VALUES ('70', 'EG', '', '31.206600694065106', '29.96519017964602', '3', '');
INSERT INTO `patient_addresses` VALUES ('71', 'EG', '', '31.206491723984254', '29.965147599577904', '3', 'Alexandria');
INSERT INTO `patient_addresses` VALUES ('73', 'EG', '2', '27.2845', '30.56598', '16', 'sidi gasbe');
INSERT INTO `patient_addresses` VALUES ('74', 'EG', '', '31.31047227942302', '30.06381567567587', '3', 'Al Akademia Al Arabia Al Baharia');
INSERT INTO `patient_addresses` VALUES ('75', 'EG', '', '31.203588198619286', '29.963379353284836', '3', 'El-Nasr');
INSERT INTO `patient_addresses` VALUES ('78', 'EG', '', '31.027716616300484', '30.457754395902157', '128', 'Al Shona, Qortasa, Qism Damanhour, El Beheira');
INSERT INTO `patient_addresses` VALUES ('79', 'EG', '', '31.2033288', '29.963559', '128', 'Green Towers, Elshohada Square Rd, Ezbet El-N');
INSERT INTO `patient_addresses` VALUES ('80', 'EG', '', '31.206383900833327', '29.967189431190494', '128', '14th of May Bridge Rd, Ezbet El-Nozha, Qism S');
INSERT INTO `patient_addresses` VALUES ('82', 'EG', '', '37.785834', '-122.406417', '147', '');
INSERT INTO `patient_addresses` VALUES ('87', 'EG', '', '37.785834', '-122.406417', '147', '');
INSERT INTO `patient_addresses` VALUES ('88', 'EG', '', '37.785834', '-122.406417', '147', '');
INSERT INTO `patient_addresses` VALUES ('89', 'EG', '1', '', '', '147', 'asdf asdfsaf');
INSERT INTO `patient_addresses` VALUES ('90', 'EG', '1', '', '', '154', 'address details');
INSERT INTO `patient_addresses` VALUES ('93', 'EG', '', '31.21153805405624', '29.94891688227653', '161', '15 Mostafa Kamel, Ezbet Saad, Qism Sidi Gabir');
INSERT INTO `patient_addresses` VALUES ('94', 'EG', '1', '', '', '166', 'fuhdt');
INSERT INTO `patient_addresses` VALUES ('99', 'EG', '', '31.2032866', '29.9634287', '160', 'Alexandria Agriculture Rd, Ezbet El-Nozha, Qi');

-- ----------------------------
-- Table structure for `patient_answer_questions`
-- ----------------------------
DROP TABLE IF EXISTS `patient_answer_questions`;
CREATE TABLE `patient_answer_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_answer_questions
-- ----------------------------
INSERT INTO `patient_answer_questions` VALUES ('118', '1', 'ans1', '106');
INSERT INTO `patient_answer_questions` VALUES ('119', '2', 'an2', '106');
INSERT INTO `patient_answer_questions` VALUES ('120', '3', '', '106');
INSERT INTO `patient_answer_questions` VALUES ('121', '0', '', '106');
INSERT INTO `patient_answer_questions` VALUES ('176', '1', 'Jun 2017', '128');
INSERT INTO `patient_answer_questions` VALUES ('177', '2', 'true', '128');
INSERT INTO `patient_answer_questions` VALUES ('178', '3', 'test', '128');
INSERT INTO `patient_answer_questions` VALUES ('179', '4', 'false', '128');
INSERT INTO `patient_answer_questions` VALUES ('180', '5', 'an2', '128');
INSERT INTO `patient_answer_questions` VALUES ('181', '6', 'true', '128');
INSERT INTO `patient_answer_questions` VALUES ('182', '7', 'an2', '128');
INSERT INTO `patient_answer_questions` VALUES ('183', '8', 'true', '128');
INSERT INTO `patient_answer_questions` VALUES ('184', '9', 'an2', '128');
INSERT INTO `patient_answer_questions` VALUES ('185', '10', 'false', '128');
INSERT INTO `patient_answer_questions` VALUES ('186', '11', 'false', '128');
INSERT INTO `patient_answer_questions` VALUES ('187', '0', '', '128');

-- ----------------------------
-- Table structure for `patient_device_data`
-- ----------------------------
DROP TABLE IF EXISTS `patient_device_data`;
CREATE TABLE `patient_device_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(45) DEFAULT NULL,
  `push_token` varchar(45) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `device_verision` varchar(45) DEFAULT NULL,
  `device_model` varchar(45) DEFAULT NULL,
  `data_updated` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_device_data
-- ----------------------------

-- ----------------------------
-- Table structure for `patient_login`
-- ----------------------------
DROP TABLE IF EXISTS `patient_login`;
CREATE TABLE `patient_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key_UNIQUE` (`api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2270 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_login
-- ----------------------------
INSERT INTO `patient_login` VALUES ('288', '16', '326ed0520be4ca971751758f4721d931', null, '', '', '');
INSERT INTO `patient_login` VALUES ('304', '80', '0d97943d347946d6319267d0f95d9db9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('305', '82', 'c8245e0e28eee203e62d186a3d1ba9c4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('306', '83', 'e121ebc7b9e343c4eaab67ebafc1b2b8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('307', '84', '6603da17372cf998bb385387cc294f91', null, '', '', '');
INSERT INTO `patient_login` VALUES ('308', '8', '710e294ee70f0686d2b9ee4ff4a4b16f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('309', '8', '0825a05eae409d40df57ba67e51df988', null, '', '', '');
INSERT INTO `patient_login` VALUES ('310', '85', 'cfe1856c6c2c325968a26939e534e083', null, '', '', '');
INSERT INTO `patient_login` VALUES ('311', '8', 'c631b33bd6bade98101a18d6894438de', null, '', '', '');
INSERT INTO `patient_login` VALUES ('312', '86', '3bd26a091eb170e130b40f2595341be6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('313', '87', '7c32e40f864d020c814a44a0eb8ebf7d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('314', '88', 'dc1a3b538b1a00609c940a9b5ab7a512', null, '', '', '');
INSERT INTO `patient_login` VALUES ('315', '89', 'ab439542d1bb724071faf670fa7d7bfb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('316', '90', '23dfb80fce7c376363384fa976172ee1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('317', '92', '4eed3073aabac98b5b4315c03559b2be', null, '', '', '');
INSERT INTO `patient_login` VALUES ('318', '93', '1d9b9b5557024c31314e7861b085b58f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('319', '94', '224ff842ffd316ff774ceca9cc8fe2ff', null, '', '', '');
INSERT INTO `patient_login` VALUES ('320', '95', '1a80a6f40cc67850d7911cea22ef1009', null, '', '', '');
INSERT INTO `patient_login` VALUES ('321', '96', '06be332552bb734fa67d37cfc7edf5be', null, '', '', '');
INSERT INTO `patient_login` VALUES ('322', '97', '129a50287a0effe27806e448e9ccc94a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('323', '98', 'c97aa67e804c42c25e411f720778de23', null, '', '', '');
INSERT INTO `patient_login` VALUES ('324', '1', '32febfeb1e8c4c9caf6b88725741fc79', null, '', '', '');
INSERT INTO `patient_login` VALUES ('325', '100', '74431e8c83092c7c6bc73a8a0e14ac2d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('326', '101', '5b8067e52b9f8d3d627abf27997c7357', null, '', '', '');
INSERT INTO `patient_login` VALUES ('330', '11', '5b43678fd08a446809cbf502c0bca5a7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('331', '11', '6cc05a914a24b8bdaf1fc03e725a3e42', null, '', '', '');
INSERT INTO `patient_login` VALUES ('332', '11', '348d8085251b0b18d78538e7886c43d5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('334', '102', '1379ae39a28d62c1ee644848381d50d7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('343', '105', 'd19194371c76f28ea6c5a659b74d2470', null, '', '', '');
INSERT INTO `patient_login` VALUES ('344', '105', '259c69aea7772a8f06aedc90cef7b0ce', null, '', '', '');
INSERT INTO `patient_login` VALUES ('345', '105', '37296d48de6d01f3a344246aa8236f17', null, '', '', '');
INSERT INTO `patient_login` VALUES ('346', '105', '73c207d95551139e4a7e85baf5f27e2b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('348', '106', 'e70f7c5ef19b75f91eb1c01f0c49d8a1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('349', '105', 'e9ec39df3580c5dbe671c141dcf8e50a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('350', '105', '0b0925242c7d51c0d757c10180271f0a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('351', '106', '3371de86f6475d4313fec9cac4e5538e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('352', '106', 'e04a503558ccd5909d5455f873697364', null, '', '', '');
INSERT INTO `patient_login` VALUES ('353', '105', '76dacbf1ab69e615bac8a57e73f3af5a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('354', '105', '66302d61962d8122e9b7cfc55eb4059a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('355', '105', 'a60fa812dbb5acea72e4323e1bab302b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('356', '105', '4a82fc9cc20922d4f0a40f69b0092a3c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('357', '105', 'ff78aba034b4b8711a92879a4bded46a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('358', '105', '79bbe6d3e3fd90e21fd50e24ee3e6237', null, '', '', '');
INSERT INTO `patient_login` VALUES ('359', '105', '1dd689e823b53b94a075956ca1fa1651', null, '', '', '');
INSERT INTO `patient_login` VALUES ('360', '105', '256ae4bd639bd47c7d4280dfa8aadb37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('361', '105', '3958a9afe66702833b898af9c3ad85cd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('362', '105', '82397ff9feef8fe28f52126c4f7effa4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('363', '105', '06512357045265118410478b5242c987', null, '', '', '');
INSERT INTO `patient_login` VALUES ('364', '106', '9a01f14678213a46ac34e9d993af0615', null, '', '', '');
INSERT INTO `patient_login` VALUES ('365', '105', '2bdbb2f4e42e05919a3ec54f4ea8d11a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('366', '105', '3480b793118bc11f58396fa2524b724c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('367', '105', '55c47c1933315fb2ac3b2bcee134717a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('368', '105', '0163bccfa4182652697767b7bfe1a624', null, '', '', '');
INSERT INTO `patient_login` VALUES ('369', '105', '142cfa8638bf043ccc1c40c422444ddc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('370', '105', 'e07f10789a6f18a2956f98c76d5939f4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('371', '105', 'e641e9c4b7ad1513a55c53add95eed0a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('372', '105', '4e4f14bacf20ef40a4606f01206da6d6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('373', '105', '201333bce1cf25be39e29b6f3e5e4c97', null, '', '', '');
INSERT INTO `patient_login` VALUES ('374', '105', '1f380c6440669803e18c2ce8add07a47', null, '', '', '');
INSERT INTO `patient_login` VALUES ('375', '105', '6be665efdf46323939d6292bf88c0d15', null, '', '', '');
INSERT INTO `patient_login` VALUES ('376', '105', 'b7d2e1012e950e9574f3f5d73eee1571', null, '', '', '');
INSERT INTO `patient_login` VALUES ('377', '106', '5e29f14f653cc0288a4a440ce3f1c7fb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('379', '106', '117e35fe4c7e719c128cae00dfae3e65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('381', '107', '78aa30f8eb3e7bb96a3f0a83d87049be', null, '', '', '');
INSERT INTO `patient_login` VALUES ('382', '107', '58e7bec7b0482949c98f8d82b5b895b1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('384', '108', '20e5995f9a1add2bda349e83e5af7677', null, '', '', '');
INSERT INTO `patient_login` VALUES ('389', '106', '592dbd055acd1a5c57b490a18f46b69b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('390', '0', 'fc1230bae352909a38cd4b6772451d3e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('391', '0', '81989455e4252839509005704a6989c8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('392', '0', '1ca79cd9b48b00091664292e0fb5d7a1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('393', '106', '5de8b3807887cbd9e29ef664d7f2979d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('394', '106', 'b6712eda82805718737917ccd0cdbdb6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('395', '106', '1731fa7751959237a68676f71bc809b4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('396', '106', 'd7c44a08fb4745d1f6ed1edb4e1d1f98', null, '', '', '');
INSERT INTO `patient_login` VALUES ('397', '8', 'c9171982a319fbb3e1148ee005a4e35f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('398', '8', '41ff27d61bdaaac10c71607eeda71538', null, '', '', '');
INSERT INTO `patient_login` VALUES ('399', '106', 'b9cb3ff7857bfdd07cfcd631f2aa8e2b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('403', null, null, null, '', '', '');
INSERT INTO `patient_login` VALUES ('405', '113', 'bbfd96763702f2de681bbd570ddcfbdc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('408', '114', '393c9209f70592f600024abc0eddecb7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('409', '114', '2154446d813a0786df1e212bc519057b', null, 'testtoken', '12', '');
INSERT INTO `patient_login` VALUES ('410', '114', '3aecabd957a8f098524d5a364ebe1b07', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('412', '114', '4895fceb79cb068e36afabea3b848d56', null, '', '', '');
INSERT INTO `patient_login` VALUES ('413', '114', '7f59c9da43e69d5fb4e18452a95ea279', null, '', '', '');
INSERT INTO `patient_login` VALUES ('414', '114', '07796899a6a1a1ca7aba0fbbd70676e5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('416', '106', '751c6f2a862197efb4d31250ea1523ee', null, '', '', '');
INSERT INTO `patient_login` VALUES ('418', '106', 'efe2b5dac08c0d7e4a349a67e3579fbd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('419', '106', '96e70ce15714576c7f1bb7169d4a7125', null, '', '', '');
INSERT INTO `patient_login` VALUES ('420', '106', 'b99846f258296f1ad37687412522d7de', null, '', '', '');
INSERT INTO `patient_login` VALUES ('425', '115', '1d25c93b15e9364ff62e2a40bf581509', null, '', '', '');
INSERT INTO `patient_login` VALUES ('426', '115', 'd0120c4e1822596b04b93af4685977b4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('428', '116', 'cd76cf3bfacfb7366ce00dd534683631', null, 'dIvNX5gF6NQ:APA91bE2duXJfHeSTwKD9Y9sxNiPiYy7ZyfARv1zWuDn98kvYZAUmX2zFF9ggOQePPNIoMj43OTuMJlMi8qmRvlCGRjzm4TSBGEfvr2o7LUvzO_7u-5NiGjHsn_FbRSg3dnFRzt0-Wl3', '8e0c6cf1aead475e', '');
INSERT INTO `patient_login` VALUES ('429', '116', '6c9824720c2d8ae0d84428e87bb853eb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('430', '116', 'aafc1c5584827d44474a4da32e2df4a8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('431', '116', 'f62527dea04fa9cd37118c6231d0947f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('432', '116', 'df5ced00f000aedf8795092d14ffa100', null, '', '', '');
INSERT INTO `patient_login` VALUES ('433', '117', '29af8f04bd1054c1f35595d5c5b767bc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('434', '117', 'b84e3c10e1da305a857558d93001dcd7', null, 'fAZimL3SM0g:APA91bF7Xl8oKDCaWNmeQjuxku3930PGWAxiqqrqlYqrusPCuT2EucZlFm4jBLwIUP_fXnjK83Fui4na6ECEOvXaHGyxofEYq_SUlgCoLrE8jzjL3XJXRNyF1L12zc_K1s9fqHXjHE9I', 'e90789852dbaa4f8', '');
INSERT INTO `patient_login` VALUES ('435', '117', 'a7a3b84102ec30c334a7b927d8f2b85f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('436', '117', '5200ae64805a1991725f82bd5ce53288', null, '', '', '');
INSERT INTO `patient_login` VALUES ('437', '117', 'a2a032b107f41134a719e0bb559abe12', null, '', '', '');
INSERT INTO `patient_login` VALUES ('438', '118', '25fd5277a4ac91cc00e379208646346b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('439', '118', 'ad2f01fe05a73a27fa04500b2129dd3b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('440', '118', '0f7559fda2a19e60b081aa6a98cf9192', null, '', '', '');
INSERT INTO `patient_login` VALUES ('441', '118', '9c32aefe76a8cfe0f12ac8076ae9021b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('442', '118', '718dca5c564313e18cf98ec40cd8c5b9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('443', '116', '757fdb701ff38bf83edf7bac13a2843e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('445', '119', '8cdbb1a9816363ef387de6c64ad0cb03', null, 'dIvNX5gF6NQ:APA91bE2duXJfHeSTwKD9Y9sxNiPiYy7ZyfARv1zWuDn98kvYZAUmX2zFF9ggOQePPNIoMj43OTuMJlMi8qmRvlCGRjzm4TSBGEfvr2o7LUvzO_7u-5NiGjHsn_FbRSg3dnFRzt0-Wl3', '8e0c6cf1aead475e', '');
INSERT INTO `patient_login` VALUES ('446', '119', 'bfb2d80506d364963741243b57a0da8f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('447', '119', '32987c3f253d0b2381168cef5bb17ee4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('448', '119', '6674b76d20f4c254cd97dc30fee19b19', null, '', '', '');
INSERT INTO `patient_login` VALUES ('449', '119', '05cf1af4978438a88f8397d7f94b5d4e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('450', '119', 'f3348d73057ab479e636297cc4bb31d7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('451', '119', 'fa45f478ccb437d20c8bc874c13cf3cd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('452', '119', '522e6b88beb3051c5b8657896d6597ea', null, '', '', '');
INSERT INTO `patient_login` VALUES ('453', '119', '5e4270f5fb7aa1317ed84aca2ef9d59b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('464', '120', '233beb34e430aeeee3266aa98e03282c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('465', '120', '591a61314ff2a3543e92d35f0cbdfb6b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('466', '120', '10b143e13d88cce5ea371534088d6273', null, '', '', '');
INSERT INTO `patient_login` VALUES ('467', '120', '255c69c297f02394db7b243099f36b09', null, '', '', '');
INSERT INTO `patient_login` VALUES ('468', '120', '8405d7c98c1c13c1cce03b2e59151f45', null, '', '', '');
INSERT INTO `patient_login` VALUES ('469', '120', '590a3968e5710d48765bcbbb510263f0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('470', '120', 'a2a9dcdbaee12b2aaae9f633957f9ed7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('471', '121', 'bed6461f87aaff15cd0baf3251572637', null, '', '', '');
INSERT INTO `patient_login` VALUES ('472', '121', 'aa5ac419ddb96cacdf5f42265c830885', null, '', '', '');
INSERT INTO `patient_login` VALUES ('473', '121', '8217d4446f730aa90009a130db2f8c15', null, '', '', '');
INSERT INTO `patient_login` VALUES ('474', '121', '2f8e64c2095431a9ff1699ffab52dbec', null, '', '', '');
INSERT INTO `patient_login` VALUES ('475', '121', 'f21da1c84249de6d9617f386b123c8c7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('476', '121', '96835ee79a4ac7f0e49f18996fd1a2e9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('477', '121', 'e69aba6441ece0fbb50d018daa6ce05c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('478', '121', 'ca57c96014ea5c203e68bac9fd019a37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('479', '121', 'f3bbb9411fa58f311a42f7a6dbdd546d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('480', '121', '9f1c3b4b9c7592fd93a2fbe87ec87a32', null, '', '', '');
INSERT INTO `patient_login` VALUES ('481', '121', '167fefb1df772a1865fde93edca00de6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('482', '121', '1813dec568ba68c9815264d7ebb65331', null, '', '', '');
INSERT INTO `patient_login` VALUES ('483', '121', 'b8260142d646532a2f30afb209055605', null, '', '', '');
INSERT INTO `patient_login` VALUES ('484', '120', 'f4dbfe80ef0e20fa4c74fb13613d76d5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('485', '120', '8db6a33e696f212dff2ad593e7fdb0dc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('486', '120', '9be4f268427e186ab515127a9886dccf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('487', '120', '02db02222ad69f90a3059c92c0244587', null, '', '', '');
INSERT INTO `patient_login` VALUES ('488', '120', '71047365ac5b59cc6e95747bcf3a44bc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('489', '122', '5735131e394b71d0c23c0e8365e52767', null, '', '', '');
INSERT INTO `patient_login` VALUES ('490', '122', '5a280b771a9c7246e6c2ec76880c4813', null, '', '', '');
INSERT INTO `patient_login` VALUES ('491', '122', '6c163e062edb9ab22475a1101a646f60', null, '', '', '');
INSERT INTO `patient_login` VALUES ('492', '122', '411e267d125b8d77c43492e4d79e5af1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('493', '120', '580a425f03e7a2382d890b0badb0beee', null, '', '', '');
INSERT INTO `patient_login` VALUES ('494', '122', '562a51f50d9b7238dc87a26ea8e620c2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('495', '120', 'd07839faf8e7bc6c6b0f5399ad8ca634', null, '', '', '');
INSERT INTO `patient_login` VALUES ('496', '122', '932a8ba1c6855a92e562c37287b372c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('497', '122', '6bccb62c724f2079b7f0062ddad6c341', null, '', '', '');
INSERT INTO `patient_login` VALUES ('498', '122', 'c98afd0189ea8ab16167cb2f18385f82', null, '', '', '');
INSERT INTO `patient_login` VALUES ('499', '122', '48ff59ab0b27aa965d429585b0e9636c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('500', '122', 'ffcb9be338c13cdec5adc4b996b7b7ca', null, '', '', '');
INSERT INTO `patient_login` VALUES ('501', '122', '01450195c43d58e5083fc1e094034d86', null, '', '', '');
INSERT INTO `patient_login` VALUES ('502', '122', '73e427322417ac88cdc6db67a099fdc0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('503', '122', 'b677162009cdc7a1ff52ed51b170a10f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('504', '122', '7c83cc25a5c19d6a6507837a32664bb9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('505', '122', 'ee6fb1b347a49b6091d2d36e77088311', null, '', '', '');
INSERT INTO `patient_login` VALUES ('506', '122', 'da7a78b128dc679d9dfa1db477d66dde', null, '', '', '');
INSERT INTO `patient_login` VALUES ('507', '122', '86530b6a89ad1bbc42cfbe5db1512e47', null, '', '', '');
INSERT INTO `patient_login` VALUES ('508', '122', '72f48087f86acdc60f786a7be752daa6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('509', '122', '6eda113533ed8d3014ce6a201fac60b9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('510', '122', 'f32b5bc95d9208345ed6c021fb6c0f24', null, '', '', '');
INSERT INTO `patient_login` VALUES ('511', '122', '6ee5d16b119f7d741a1f1f89c762c6bf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('512', '122', '9854d58512c1a8353b70a161672fc492', null, '', '', '');
INSERT INTO `patient_login` VALUES ('513', '122', '82cbde96effd478ad5229f373afe4c31', null, '', '', '');
INSERT INTO `patient_login` VALUES ('514', '120', '47124dfea03e238fa65390bad8c2a348', null, '', '', '');
INSERT INTO `patient_login` VALUES ('515', '122', '0a9e6c50ee94bda7eb95dca5f3542b9b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('516', '122', 'f2ff7d07d6f1e280bca7e97e87003e26', null, '', '', '');
INSERT INTO `patient_login` VALUES ('517', '122', '06bae13ca863027b26cea704d4a4ded1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('518', '122', 'f46140ccc2799d415542123228139ec1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('519', '122', 'f8f36768f7081965e1145a15b4bfe78a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('520', '120', '1910ad2b8e6614a37ca1190011bd6736', null, '', '', '');
INSERT INTO `patient_login` VALUES ('521', '122', '30854bd34b424c5c278befec90cdf07e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('522', '122', '9de3a66eb2ba48cf79151ee5635626e3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('523', '122', 'a67124e9d93794af77d65b82e6e61602', null, '', '', '');
INSERT INTO `patient_login` VALUES ('524', '120', '58c42356704ce0d397538924e914eddd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('525', '120', '84f54753ddd1d716043d98a3b58fe129', null, '', '', '');
INSERT INTO `patient_login` VALUES ('526', '122', 'c148dd9628a3890260b148a3710eae8d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('527', '122', '488e3c0237ea11279f4f694cee49b308', null, '', '', '');
INSERT INTO `patient_login` VALUES ('528', '122', '73a2779866cfd7620792806705df07cb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('529', '122', '451d17d39e6bb00b3a2837802c211f41', null, '', '', '');
INSERT INTO `patient_login` VALUES ('530', '120', '3f6649e6e93afa7062a87ccbc0dc86cf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('531', '122', 'ed22272a5f3100a9f4e18cd490fb6048', null, '', '', '');
INSERT INTO `patient_login` VALUES ('532', '120', '6fe637f87992957721ca77262456f840', null, '', '', '');
INSERT INTO `patient_login` VALUES ('533', '120', '6cb79c5c37d0e2200d1fbabd87d28618', null, '', '', '');
INSERT INTO `patient_login` VALUES ('534', '120', '212b888f33ced2bec5950a19f0cd316e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('535', '120', '88d408db49b55a48dd2e9e121c61459a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('536', '122', '3ca351dad30f931f47454dac89b42fa9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('537', '122', '2bd9f6f0ee1b6d391da46133c65fc627', null, '', '', '');
INSERT INTO `patient_login` VALUES ('538', '122', '0f1dacd52cf1b8b91b613eeafb7213a5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('539', '122', '23773a7b4ad84daf68cc0f3539dd3aa4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('540', '122', '295f1bb498cee780f450bf4204777907', null, '', '', '');
INSERT INTO `patient_login` VALUES ('541', '122', '0c8e69f409633c8b93770f1e529f2448', null, '', '', '');
INSERT INTO `patient_login` VALUES ('542', '120', 'af622d03af62c01122d5eec124dda16a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('543', '120', '0bd78b0468a097f697bff642781d48f3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('544', '122', '2e5b072633d290916b5fb07094c688d9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('545', '122', '20225772b8615f3c2cd5b8839244f4a1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('546', '122', 'e81ecee20c4ec6b45764b80d883bd485', null, '', '', '');
INSERT INTO `patient_login` VALUES ('547', '122', '316e545adfd25a66656234b7d465e3ea', null, '', '', '');
INSERT INTO `patient_login` VALUES ('548', '122', 'f283d4d9cf6fd7059a42b748209222a0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('549', '122', '3a6c49bc9e42c023522658094516eb65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('550', '122', 'fda1243350aeb34a8ffe796efdce2eba', null, '', '', '');
INSERT INTO `patient_login` VALUES ('551', '122', '9bd30c7915e0f3fecc961baf3413cfd6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('552', '122', 'e220c58cfb54aaf1341a80e0d0ad3997', null, '', '', '');
INSERT INTO `patient_login` VALUES ('553', '122', 'ee7bbfdea2e8ae1d08f3179b4eff1f11', null, '', '', '');
INSERT INTO `patient_login` VALUES ('554', '122', '9f0c8dc835882853c7db9e04fb1d6c85', null, '', '', '');
INSERT INTO `patient_login` VALUES ('555', '122', 'f8396ca0feb5108b55d440f0b3e12f57', null, '', '', '');
INSERT INTO `patient_login` VALUES ('556', '122', 'bab2e477a8878814039452792d044cf2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('557', '122', '1e4caa011cfb25448d6640e686f5100a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('558', '122', '8271898e988238bb5a46a81335a38741', null, '', '', '');
INSERT INTO `patient_login` VALUES ('559', '122', 'f6925f8a2f9889e6d9c5977fa431a605', null, '', '', '');
INSERT INTO `patient_login` VALUES ('560', '122', '5fc1779e22dd5d17d8337f0c2dad2034', null, '', '', '');
INSERT INTO `patient_login` VALUES ('561', '122', '85722e227bf27e5239a6f511c3a1b577', null, '', '', '');
INSERT INTO `patient_login` VALUES ('562', '122', '5eaf284f9ebe49ff80db28da8bced4b6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('563', '120', '7e58d7263e6921e035b222f0374dc550', null, '', '', '');
INSERT INTO `patient_login` VALUES ('564', '120', '9b9d846ec9b29ba8f5a8858d1c976c16', null, '', '', '');
INSERT INTO `patient_login` VALUES ('565', '120', 'f18b1fbe4358d5b949320471e7911307', null, '', '', '');
INSERT INTO `patient_login` VALUES ('566', '120', '03a83e30b286d30c4fa924fbfd8a9460', null, '', '', '');
INSERT INTO `patient_login` VALUES ('567', '120', '1b0548812cb09b74d53e87577692ff7c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('568', '120', 'aa5a0890f502a7bd3db04fcd2cb1584b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('569', '120', '5a790098b3519774f43ca3afc9e0941a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('570', '120', '64f34d64a2275fecfc3e731f61d7244e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('571', '120', 'f94b41b45dc7829dd1eae140a0216406', null, '', '', '');
INSERT INTO `patient_login` VALUES ('572', '120', '22baae51c8c6f6695bb45dc911e965fd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('573', '120', '9d07919824707addb18155b3e3b99380', null, '', '', '');
INSERT INTO `patient_login` VALUES ('574', '120', 'fe9ca7034edec7ba8ae124518fbc2781', null, '', '', '');
INSERT INTO `patient_login` VALUES ('575', '120', '73fe321ac6e428174295e9d0bb38f9dc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('576', '120', 'ead920030923082b3a7a370b6259b971', null, '', '', '');
INSERT INTO `patient_login` VALUES ('577', '120', '235f3ecd2b9b5fa6d0e8b30d38e8d2e0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('578', '120', '051d7e72fec1eee471e47f4d5bc0a323', null, '', '', '');
INSERT INTO `patient_login` VALUES ('579', '120', '47f233195388f7f935f5e0f66bf2f2aa', null, '', '', '');
INSERT INTO `patient_login` VALUES ('580', '120', 'b858f5a38abbae6655fd694bb5e3c20a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('581', '120', '7f3f8466333c53432bff52a76e1abd56', null, '', '', '');
INSERT INTO `patient_login` VALUES ('582', '120', 'd481383cbc3e4994094598d74975b997', null, '', '', '');
INSERT INTO `patient_login` VALUES ('583', '120', '3f49d3b8fe0c0e6eac1601e978861a32', null, '', '', '');
INSERT INTO `patient_login` VALUES ('584', '120', '3b6423956cefc638ec401ac4f150c8c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('585', '120', 'e4e0ebbe6d47c149443a0fbc80ea00f4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('586', '120', 'a17350724fed8fc43595405b317e73df', null, '', '', '');
INSERT INTO `patient_login` VALUES ('587', '120', 'c4a1560236be366a38c5c22cbe6c06c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('588', '105', '335be19f49fa4b82ba161378ada3f438', null, '', '', '');
INSERT INTO `patient_login` VALUES ('589', '105', '12d421a933dbe3d5f5042af1787627b1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('590', '105', '6cb3eb9e31f1db77188260933bbb93f0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('591', '105', 'd4985271b42a5c9f5152bea912413bb8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('592', '105', '53f0900a1996c5872952f4c0ca0c340d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('593', '105', '675b6f5c6e7beff3d66d0fe8968dd247', null, '', '', '');
INSERT INTO `patient_login` VALUES ('594', '105', 'eb52362f4130bba030987100b2e71cf9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('595', '105', '63105290338f16fa5c52a772789ac174', null, '', '', '');
INSERT INTO `patient_login` VALUES ('596', '124', '66bfb502c46438e4326e31073b11f3fb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('597', '124', '0d3fdd7b791585583ee93ce15c2c68e0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('598', '124', '12a778d360cdf8028bb5c3493aade8ee', null, '', '', '');
INSERT INTO `patient_login` VALUES ('599', '124', '0bdb7a1f91906910e38aad909a57eb18', null, '', '', '');
INSERT INTO `patient_login` VALUES ('600', '124', '042f35789897eb8b40ae1832433cb1db', null, '', '', '');
INSERT INTO `patient_login` VALUES ('601', '124', '7e8ec174a4ba685e9ae326d7ed8d43b0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('602', '124', '7954f5a901f70d986477df58d000100b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('603', '124', '7b885715fb405a2177f2224b56a0bd2a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('604', '124', '19c674f93fe7f04340e662d067ec7359', null, '', '', '');
INSERT INTO `patient_login` VALUES ('605', '124', 'bfea02ea4db0e6e5991e06996c164b58', null, '', '', '');
INSERT INTO `patient_login` VALUES ('606', '124', 'bba509ba181e6ced84a6f74b2c504bd5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('608', '124', '22c1073cfd2ae6822852794b561356cf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('609', '124', '3e614cbdf1a3b7e9467528a8868a074a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('611', '124', '68d1faf2d00ae61ceeaecddb17ffab5b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('612', '124', 'f8a0caa7b600f66c7b0be993ef05f754', null, '', '', '');
INSERT INTO `patient_login` VALUES ('613', '124', '8dc9337ba860fbe2a50edad324417b3e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('614', '124', '62872913d7bd8d0f829c3e9df61f7c37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('621', '126', '4887f9a1f1d1acf828540d3a3f24356d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('622', '126', 'e1ecfca72d4b51ca719593c313af3cd3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('623', '126', '828ca2562d2277be7294438eaf7ef117', null, '', '', '');
INSERT INTO `patient_login` VALUES ('624', '126', 'f527fadf673015de17a7364b18dbb204', null, '', '', '');
INSERT INTO `patient_login` VALUES ('625', '126', '09792e9e7f25a2810870469c53d8055d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('626', '126', 'a6881731137bfd44f011bae68a2bc1e4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('627', '126', '64e4fc68545e3addab886fd9f18e29f9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('628', '126', '60d0de3c4e8d8bab7e06d1b9b437332d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('629', '126', '48078fef83632b32680bc9ae2d6f9de5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('630', '126', 'c367952a1cfcf117232460baba0dcf67', null, '', '', '');
INSERT INTO `patient_login` VALUES ('631', '126', 'ef9c2f25c8bffaeb8a5cafbe56550e97', null, '', '', '');
INSERT INTO `patient_login` VALUES ('632', '126', '5f9a0606b2f43471ba32cc3a3c889621', null, '', '', '');
INSERT INTO `patient_login` VALUES ('633', '126', '436c933e4f0ea233f8cc35cd07d3cd69', null, '', '', '');
INSERT INTO `patient_login` VALUES ('634', '124', 'd147fbc22581a5073eade42075fcf4ec', null, '', '', '');
INSERT INTO `patient_login` VALUES ('635', '124', 'cb204e38cc482033eeaddcf2bb68b13b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('636', '124', '1c64c821421ebeaf5c80fc42c17d355c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('637', '124', '0b3a91733c00019d192ddea07c998e5d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('638', '124', 'dd1b5fd59549c33c2c5dbfab891229ca', null, '', '', '');
INSERT INTO `patient_login` VALUES ('639', '124', '9e956af86bd87065288db5bbcb6509b7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('645', '124', 'f0a6a0122ad35ebbd35b33f4210f67b3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('647', '124', 'aa3b6d3ef7a1f513be58d30cbf4477e2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('649', '124', '8c84dba8ecd0a02524e2af752938debe', null, '', '', '');
INSERT INTO `patient_login` VALUES ('651', '124', '0684672f1f91a19e8f5924ac78fb402e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('652', '124', 'b56212b67ef7f4dfb358aeb9df72bfb3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('653', '124', 'bba5a8e7ca067f128354f55f23cb7204', null, '', '', '');
INSERT INTO `patient_login` VALUES ('654', '124', '8eef0829c06870ce62ddbcb558347987', null, '', '', '');
INSERT INTO `patient_login` VALUES ('655', '124', '975dd74f6e244fdd6865325b6e03e5ae', null, '', '', '');
INSERT INTO `patient_login` VALUES ('656', '124', '0182669e6af7e209c205d956b7d317e2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('657', '124', 'a6d976038c1076d9c03eabaf0e76327c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('658', '124', '058364e5d68c0ed9448e254e5f6886ba', null, '', '', '');
INSERT INTO `patient_login` VALUES ('659', '124', '1954eca721587fc30f34f061678da65d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('660', '124', '0389259d2cbeb290dc33728b7fb84ecc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('661', '124', '1c654bad1a874075482579d82aeaf654', null, '', '', '');
INSERT INTO `patient_login` VALUES ('662', '124', 'b1c5ee0c63238c3051d7ea3a2e21d5e6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('663', '124', '291d6f850ed6188db1aaf55fed2dbd0f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('664', '124', 'cca1d4ccf3128b1c3e04da070fcbccdc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('665', '124', 'e5c5f1bd9f8c1e9d8616ab7985bcf500', null, '', '', '');
INSERT INTO `patient_login` VALUES ('666', '124', '6b14577568273ed08a92e2ac970e3cb0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('667', '124', 'e552fd99dbdc8b4eafb0c10bdf9358ee', null, '', '', '');
INSERT INTO `patient_login` VALUES ('668', '124', '0bd3f668ce67861651ec2126f984007a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('669', '124', 'c2775ae01a146329fb0111757ddc71fc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('670', '124', 'f3f443fce73bc0d0c307b5fa9eba98fd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('677', '126', '62553b2656ab0b2a0480caab31eee9e5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('688', '124', 'c5d8e0fcebdea48dbf840d460a6c5bd0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('689', '127', '9039d3a7b3417c787f66b45712e7ff65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('697', '122', '9ebbc0fb5ddb29970251cc8682e3f297', null, '', '', '');
INSERT INTO `patient_login` VALUES ('713', '126', '07297559b1e7f5ffc0dc609fe6f3dc5a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('722', '106', '63b3ce525c345f1ac4fa0cf95aa80da6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('745', '129', 'c0e8b4878a581b5d0ff79a8c3f0057a5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('746', '130', 'e0babc5c89952d3b686dc60db0d54306', null, '', '', '');
INSERT INTO `patient_login` VALUES ('747', '131', '6381deabcfffc804e133f8e0499c408a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('752', '132', '8228c3d1c4346f6dfefa8a0dc26a0d41', null, '', '', '');
INSERT INTO `patient_login` VALUES ('793', '126', '8348d381bbd54f355b436832dcd0c307', null, '', '', '');
INSERT INTO `patient_login` VALUES ('843', '126', '65785f0a8638894f996f6108d777357e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('844', '126', '18859898ea297baf4672707cbc851690', null, '', '', '');
INSERT INTO `patient_login` VALUES ('848', '126', '675f19620b37e639a36f3984225a4057', null, '', '', '');
INSERT INTO `patient_login` VALUES ('849', '126', 'f812462c790bef13f0c238ecfa4937f3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('850', '126', 'd443964cf4f8f335efaf29474ab27771', null, '', '', '');
INSERT INTO `patient_login` VALUES ('851', '126', 'cc6f7ffa1171b1a287bb0f4410a998ca', null, '', '', '');
INSERT INTO `patient_login` VALUES ('852', '126', 'e5bca4353c88af3c093cfb1a6838ec62', null, '', '', '');
INSERT INTO `patient_login` VALUES ('874', '106', '7542bdd218b6e3e938ee8a1ff52e9fcd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('875', '106', '2b6599cfa129c5b27f350a1f19c508e1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('876', '106', 'e37413f6f5a3bec4fa658d33c4fd20d9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('877', '106', '1071776817171e1bfe30a9a0b4b4a8ad', null, '', '', '');
INSERT INTO `patient_login` VALUES ('894', '126', '8c02d1980792d034182cf60e1b45b2d0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('957', '126', '5fb0338594289a4f1cf599d44d569af2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('958', '3', 'f78049e5a432f718c083ae5d795e5b8a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('959', '3', '63cbd6714672c85fbb896bbb9c9d8143', null, '', '', '');
INSERT INTO `patient_login` VALUES ('960', '3', 'a0b604ae689cf750f8cd4dcab5869f44', null, '', '', '');
INSERT INTO `patient_login` VALUES ('961', '3', 'ac8b6d4ff5451a035d7177dcc8f17c0c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('962', '3', '8f191e733fc34f93c2aae78dc4768d24', null, '', '', '');
INSERT INTO `patient_login` VALUES ('963', '3', 'd3b03a4fa98dd421c536280e857dd41a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1053', '134', 'e96cf3f88bea8ccb8d86d17757fa85fa', null, 'fCbxmleb8NE:APA91bGRniqkCL2f_WHfLcSDG48RPgO6dmp2fLEA2BUm-d-TE89D4pzpvSVnKHWaVGrvpYgZ5s1w-QkWv3e4KBrcAiktYciDBc6SLe_8jz22VJp5O6m_KL4aYOo9iEyhUiKkZ_6iDvRO', '8e0c6cf1aead475e', '');
INSERT INTO `patient_login` VALUES ('1054', '134', '9f38d22652c5eb87bb8a8c55baf8a77a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1055', '134', '55a7ccc03593eb711a5d13fc85dd48c8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1056', '134', '3d6922b82bce9d38d44cab84a11187d2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1067', '134', 'e5f3c5a086a6f975135f1e0995ccbba3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1068', '134', '10aee11ec9f7ac71f562ca5fda4da585', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1069', '134', 'ac2ebacaada411448cadffff9b48a5a4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1070', '134', 'fd6c83a378eea294c225d042f863d19f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1071', '134', '427ec6236f1956f917b7cbc5fd96efc0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1072', '134', 'abb8c2bf0ee071879dcfebde463c65b7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1073', '134', '5c3c772c211e48f0726aadb5284f2a88', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1082', '134', '14f0c98ee0884a6a76263ba4611341fd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1134', '114', 'd7500c2ee02c93d1e46fde11b04c4f0d', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1135', '114', 'cf0cef00480e648753a62b4ca304ed4a', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1136', '114', 'b3b769e6f85d34d548bd5a7d846fdb4b', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1137', '114', 'bbdd2a10a65f20fa80fc9e55ac798f90', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1138', '114', '9c7304a999c181e6ab6331042c6aed4a', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1172', '134', '78f33e38a945517c72f3cfb197320621', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1270', '3', '38aea1a62dd8fc11cb931deeced60747', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1271', '3', '0661bb35697075bf43e07618a903338a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1272', '3', '08331263ebc713ba472dfc5eda40c5b2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1275', '3', '9de30fb93de95a605f2b7abd9da74719', null, 'fP7CrSS5JYo:APA91bGms_C78ZQNTDqiDJzahWPzCuFTUfvg3sjy7JHQp_iYfvEx28S5aLMFIuLF4FWf4Rck4Qg-kG42lTxlQBYrV8PRTkdSLjDHP3xEt1cij6FHQeplzPN2Y8E1kiKCTEvPYS5s40u0', '893570908aac2617', '');
INSERT INTO `patient_login` VALUES ('1276', '3', '71213ddcd74d57ac34e4aac6c09a21c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1277', '3', 'f8b380b5be4de787255dcd50e05204ef', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1294', '108', '6736e21ed6ffbd4db079b2373c553c09', null, 'testtoken88', '12', '');
INSERT INTO `patient_login` VALUES ('1307', '3', 'c08711875f4325b49ab2f62da6afc039', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1311', '3', 'e4c784a45622925a928fdb98508e3af2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1312', '3', '9b1bdb86153f293825394cab5be498be', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1319', '3', 'f5d6ef26e99d2fbda1dcab07f9e81536', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1320', '3', 'd3e38f603f213e8c40b3cd37d80a747d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1321', '3', '0f5d5c099326c38cb8798f41dfdb9515', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1325', '3', '81a476e1da2d3d9039b706fcaa3ae918', null, 'djLsDs17Dqc:APA91bFI-3uOWMRVvawEASnN7nDymUj1jrUnl2pCIEhguvzIikCre7zr_lN-7WWXE8hrWG7byNI_5QBlQkoYKj9yzc3wEg5bRGPumq9y08bdQGgqqpbb201YYgh4Axp3DvmS0hFLNCFt', '893570908aac2617', '');
INSERT INTO `patient_login` VALUES ('1326', '3', '6b73190e522310433fa6ff653f5b4981', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1337', '3', 'a689a1f4c6e552b7bcc613f934b57726', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1338', '3', '1c3eca718bcfcd1b578ebcd9b87ee51e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1351', '3', '804a0e74909bdc41722707ff8ce7d212', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1352', '3', '6f9be61e429f57140ee76cb63c3e0bbd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1358', '106', '6e0bbe77976195340f3cd344f316333b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1359', '106', '05dca7ae7519810e90883c8b650a184f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1360', '106', '696e8c037cbd2cb62f22d7f5e69ca622', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1361', '106', '8de47bcf25354da3627d8ba03354155a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1362', '135', '1fba23c21578a2e459a2b50c9972c26c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1363', '106', '11c7dfef75fd68a0f4b2450bbf4d1334', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1364', '106', '06a41ad3fc22385c0f76d70313406aa4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1365', '106', '70affe1899e4d37f0f4b9302335553c2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1366', '106', '614cb24b0f216d7d25885fe6a9ce1b86', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1367', '106', '5ef210d41ee0624da91d3cc060c01d28', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1368', '106', 'e3d58fe9e064e78e41ec57e0385c9d72', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1373', '3', '6c3efcf7f8836f646618416a02551305', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1379', '141', '5486561a6d05d999317452f77dc52d53', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1420', '3', '3998bfdd59f95b3cd5f1862397080fc8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1482', '3', '4c2e7b47eb842713deb7ad04f84db340', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1483', '3', '0b6e582406099158ccacf6f12364546d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1484', '3', '4b7f96e7d2bd6e434a656cf770beb7d0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1486', '3', 'a62765a720144fbab421da6a1802c973', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1489', '3', '9b4fec7210d66a9ce3910f334260ebfe', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1490', '3', '341b34088a1c057a52d1a85e70539892', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1491', '3', 'b25e80597d4a19485f668da4c2d146f7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1492', '3', '461491c1f78cf27542b12171adf12b06', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1496', '3', 'e2e10c1eca2fd01ce3893518dfaaebe2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1512', '3', '312324e484587a20b01a73abe4747201', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1575', '2', 'fba137372427cfb0f8867645a29ef555', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1576', '2', '102ddbe3bd862db5e2fcf6922931c1df', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1577', '2', '92d93d03cbcc305a00142b7d8f31dd57', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1581', '3', '410b96fc65e63cff48a998514d56035f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1582', '3', 'cc6f36117360cd3793a091317ac6ae26', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1583', '3', '17c4d81791f0f85f3118e8f2a93c4d5f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1599', '3', 'f5e4a7ebd2cbda8c0128183aadd6ad58', null, 'fQF2Zyn_r-4:APA91bEmhNJrVEmZZJK0bWHSIv0To_XyORpCkwVpV2kewdQgPvquK6MQOWZW-tYNJaA1oToYbsxEMLjKPLt6Kr7l9gjm8k26P1subnoryAFpuUE82Zn7b4FmTYtAzp1WZeQ3P4SIc6rj', '893570908aac2617', '');
INSERT INTO `patient_login` VALUES ('1601', '3', '9f17171439fdda119ec8b727e04bf469', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1602', '3', '30aba8415eb32c59ce8189f64fbb314d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1603', '3', '3b3655bd713f6f52dfcea946d189c58c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1622', '3', '5cc52c2af6a7f4789c6ea9b19fdde461', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1626', '3', '877b50501738801c3a574a4a1d6042da', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1630', '3', '71c7ff2ea53fedaae449cc53fb8bfac0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1645', '2', '63c375cf83f26723cb1abc8f266af4c3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1646', '3', 'e09fb0a37e5c884a12ff0103d0016e56', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1685', '2', '7892caa28df7c2ed3dc262b662cf1621', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1689', '2', '206a5b84756aae5d00db855370c94041', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1690', '2', 'e6a02fe14c8d4d6cb418721cec10c462', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1691', '2', '32191d94cbcfecc5fc5dbbb9239b7d44', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1694', '2', 'a1d8927d725cc291f318ffd3c56b8276', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1696', '143', 'a2cebaef4b2ed9eb07aef4d9bf44ed1c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1698', '2', 'ca058be6d4603b6f5e0afc32dfa7d843', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1701', '2', '5595016d845415a97e7070a8df206c38', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1702', '2', '524d44788815a3454bac513a4937d60a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1703', '2', 'b30b633dee5591b8d5009a0edbb4a0fa', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1704', '2', 'c0ec981afbf7801a45749893d8336f55', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1705', '2', '6b275b2db375394805663b42230951ce', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1717', '3', '4360501706a9d80b2054a4de46836c3c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1720', '2', '521373b01d5f17e1a6912cde3536a396', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1721', '2', '8e99e5e4e40b8ade6eb63c2942e8aa09', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1722', '2', '66d3a8b4dc704bfdb525bbf39c9a53f3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1723', '2', 'ece8e1a98c58b3d371466f64f52eee63', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1725', '2', '961b62fa837628420157f211c74f1c47', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1726', '2', '6117a83133cbf348ccc1c3c67d774c0f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1727', '2', '925cb85640f488391e1f4e8b4d714878', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1728', '2', '94b2778db61e38682bf41c97f1c80ab5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1729', '2', '198a2de114f77def8b8f4a580ba2ce98', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1730', '2', '449d86cda942166e84910f664fbb72d9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1731', '2', 'f41f81a672236929bf075403fdec07b0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1732', '2', '19cdc3db1b3bed968d70ce7c19bc4eac', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1733', '2', '9711c765ea22c9af12289264f0ee6f15', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1734', '2', '5a6ad584f097140447a338720b649168', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1735', '2', '9df059d6fce0adc11f722a89ed4b9995', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1745', '2', 'e7e6407640570398ba486ba2094c0c8f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1746', '145', '14bc890d3b595e8acb8cffc4db670c75', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1747', '145', '019a02ef5c95eacaac166df3adc9e92e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1748', '145', 'da85db0769aa3ff3db940a7bbd100b35', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1749', '145', '4c6b105bf6dc8dcd595dc095055b6c43', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1750', '145', '68e23374212d035381297e1da81c9e57', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1751', '145', 'e98af502d7d5f35ecdf1a5501df52c5a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1753', '147', '22dc65da73127cd09dda6cc7a7a8fa02', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1754', '147', '23345fbf7b5c45a1e03c8d6f1869612e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1755', '147', '5fa3bb1ab4f0faa0997035f4b339dcd7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1756', '147', 'fdec4a46f0e656afb56476e71c8df18a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1757', '147', '6968c84b374280809821d1e71c194627', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1758', '147', '3ca12b35587a736c4cff9d4e26609a77', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1759', '147', 'ea41a2a26c62b212da6adeee0adc8420', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1760', '147', 'bf5216a0a9e5cfc25fe7f992efca5c57', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1761', '147', '29eb764f78c3150bf60bbc4362576f8a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1762', '147', 'eaf42092dde938cded23a786aa9cf30d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1763', '147', '5dc3537889704b077766ed472ab1808b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1764', '147', 'ecedfbba74fff00e807ea6c7a4e89148', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1765', '147', '0e6eb3c40eec0ffa8736c5f910351d38', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1766', '147', 'afb6a49de40171970b5f8f6c343e8e52', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1767', '147', '97d46809b67136a34a488546d7acd01f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1768', '147', '4d8602b325c457f03e6ba17a08132029', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1769', '147', '9241d5f6b04fbbafc726d190e8278c89', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1770', '147', '4d9f8599800f627f2dbb608ad4ecd20e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1771', '147', 'eb804fb3d394c633243b75ba5a92d2b2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1772', '147', '77c3becacd37fdce48d0a1a9743a105d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1773', '147', 'ad9d7d59f580c9c5b533b61b2e32ea93', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1774', '147', '8aca73445a97f6c588acdcc550fe1abc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1775', '147', '4ce7906a4da31b3bb7e35b3c928a2080', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1776', '147', 'b7efac97675293a80d97bb48322ffc45', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1777', '147', 'e2d12364f0a0dab74e94bd3153a26844', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1787', '150', '886c9d3a567d7836faacdabb81eb926a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1788', '150', 'e3c40201fe620b76c3a556b06979e7bc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1791', '147', 'd2f46373cf864c8fc38c8e038a9cc725', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1792', '147', '3809753bd16192224d6de1457cce1330', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1793', '147', '3ed88f989dd7d10ab208fdaf1d65dad0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1794', '147', 'b45c72783780f5ee2bec8793fe4d32d2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1795', '147', '3e0b55e6d522832c81dcaa7da8653f6d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1796', '147', 'f0f54fa396439c01b762303ef5de3d31', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1797', '150', '17c6fc45a04287230b97d9ff53a3e287', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1798', '147', '04c9bb023b156461e541dc2676e59105', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1799', '147', '78e94c882ab7589a7d6c47bf5005f94e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1800', '147', '6707e66bff60d90cd6d06586bb1223b5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1801', '147', '0a36da5c9c65c9595c4d510a853c55cd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1802', '147', '3e60401b185c6d84abc9cd07767b4c58', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1803', '150', '94e75a2a3f4a8e0834f3e353b88b860f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1804', '150', 'f125dab3da2c27b039b10bcd315e3324', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1805', '147', '53a88332672f09366d47a97a61ae9f21', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1806', '147', 'bbffcdf540a0e63606fbaeb1a713a034', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1807', '147', 'd4ee735aa8c46f90cff06150ec8be157', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1808', '147', '440efe6afb5c52c9519b35c46bc3ef15', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1809', '147', '18b7490abcff7fb40e688fc298f2fb1b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1810', '147', '649a8691e171654c6ededbe4a36680d5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1811', '147', 'b5bedf5ddbc8c8a20e7e1716524f92d0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1812', '147', 'eab059f846276fbda6ae8d07cb6b4648', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1813', '147', '6bd105fe277c580faa1df6513c69d2ce', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1814', '147', '8ca5649695fa0dc48d20a5b51b21282a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1815', '152', 'b1f57b40a5a0e68ddae7370d4ea68388', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1816', '147', '4b6ea10ee3f84caaccb129484e369b94', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1817', '147', '7d93db611c628c81482b55fafc5e52bf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1818', '153', 'bab62085c49fc8d44d5c1943ee6a3e82', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1819', '147', '1f54a8d07bbd16d6aae1ef96a5f6581c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1820', '147', '77c05f035ff59f04ed001393d00847c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1821', '150', '85f61ccf4f76ea34269b38ae90e740f8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1822', '150', '008c4d85c7316ddf6a5e3b56783b6eba', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1823', '147', 'ebe2c85280920b867cb187b2cd42cbf1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1824', '147', '3e92f087aafe6a7dcd346cd514787aa1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1825', '147', '7afc55d58760ebea0021d81679b12588', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1826', '147', '1c0ac4f5574b81ccd3b726fdee795f2c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1827', '147', 'c2122d40da851f3b4eef7d44d26e8336', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1828', '147', '1c192e24014212694b875272c530752b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1829', '147', 'b7aeb0b2990ad951d10606ea34db48df', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1830', '147', '7c8678b44f89623ae199d26c28d60eee', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1831', '147', '81889d50acb2054b2be85e1d1d076988', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1832', '147', '8288465eadc548afb8de442d1efb75c4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1833', '147', 'd37dcedead6078c5a4df435d39544f14', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1834', '147', '79b006c6aefea0adecc9aac2cf4f834f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1835', '147', '5fb3a53ed213f555bdc756ec0e9e3025', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1838', '147', '48e1d2a597f6c4e8301b385f2f3b9f12', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1839', '147', '50c9b25fcb370f5753b0acd342e7cd44', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1841', '2', 'dadaf04f2e0af489f6d202e282cb4689', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1843', '154', 'b60c8e08d6f99217f3c93ee27c61be07', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1844', '154', 'bba40e6ecc081be575178acb4b46ce55', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1845', '154', 'e6b8e7da1e082d81b30e5e1360f75351', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1846', '154', 'e977a8530959b043388581e5d2bce0e6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1847', '154', 'a71e5685a9f46152bb66725b9d6685d5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1849', '154', '6f3f33b85a71912dcf6756ea4efe69fd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1850', '154', '77728d29916a38a12fb9a97c372f3f77', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1851', '154', '05b4b63561eb3cba6cfe03224c57e059', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1852', '154', '0b5bf4a8f6ae52ee0fe43dea1b8b01b4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1856', '154', 'ab0cd71b1b91cbfa0f93de574ccf4954', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1857', '2', '477fc972dcd9ac198503ce9969a01c4b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1859', '155', 'ec2f59dd26adb0f301946a1c97362885', null, 'e3fbbff8fa4fc806e0b4ec490ae0ba5d94a4cb7f2c0a3d0f68e71174f07e7807', '', '');
INSERT INTO `patient_login` VALUES ('1860', '155', 'c7386accc4d156835013588a2f33be1b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1861', '155', '6f9d52630f915a3f719dd31d8ed8a8d7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1863', '155', 'b162b4fad39a2fed2d49687c065d3026', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1864', '156', '5b314032a0361ef1ec396686c54e3bd5', null, 'fCbxmleb8NE:APA91bGRniqkCL2f_WHfLcSDG48RPgO6dmp2fLEA2BUm-d-TE89D4pzpvSVnKHWaVGrvpYgZ5s1w-QkWv3e4KBrcAiktYciDBc6SLe_8jz22VJp5O6m_KL4aYOo9iEyhUiKkZ_6iDvRO', '8e0c6cf1aead475e', '');
INSERT INTO `patient_login` VALUES ('1865', '156', 'b71b3dc555efc30068251c0900893cd3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1866', '156', 'b142596e119edef96a51f32d3fa43e73', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1867', '156', 'f6e5978df3227530c58b958e7b175101', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1868', '156', '9bca45c521ac9cf0119e084a27122f5d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1874', '158', 'b0b8ae415e8f35249659bb1ed214b466', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1875', '159', '105d498efa7989f0b2bd3f21b0f3ae61', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1876', '160', '50fae3cb6f9bc64b9d8fe811dd6b2104', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1877', '160', '18704ce189cc448b96fc1e0e29d40b40', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1878', '160', 'b25e53acdbabb9329d0db6771e1944b6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1879', '160', '7febe1a880dd8d3e172546f9aa4768aa', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1880', '160', '728c7c7f134fc2abb8cd2cf89a3f480d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1881', '160', '31ee61aa7bfdc4b48f1728bd7d649978', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1882', '160', '0cf8ccc5265d7556f58a5ee593d2ca67', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1883', '160', '8578d612304b9a367385aa16aab06c04', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1886', '160', '871de2085a71f2ce0a849c4101b7fd44', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1887', '160', 'd7c6e698f09d44864bea1150573e67cb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1888', '160', '9c39c611c25e9f7282f736b3c59e909b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1889', '160', '536929124b677a9cfdd51fb87b9a0b59', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1891', '161', '6b40b4b0159ed49aec638215d702197d', null, 'cczV4PS2CaY:APA91bGe27W28ZhSG03xkqEowHfK8pc85rSnjU2GwyeH3S2__EnFY4f0LwHVnwgvlP0H3KnW26Z_U7qMZNyT-_riBYTJnXb2iAw5yKcrVcntip6L7QY-ts6_bBS-jb9gY1wyRyYEcds4', 'd24f420df3cb92', '');
INSERT INTO `patient_login` VALUES ('1892', '161', '5793453e0cdb76a31df575944b4fba23', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1893', '161', '76b8d066814b5caf1fc7defc73ce34f4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1894', '161', '2bbd1ad07ef3597641d7af94ba5c48ae', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1895', '161', '4a2cbe40ae51cdcc0895a6f354fa5673', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1896', '161', '70e6f42962d97ef30588caf6ce336e80', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1897', '161', 'f858c5ba468dfd3487b88812bb904507', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1898', '161', 'dd55fcda94202f1aa0bee5151c107b15', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1899', '161', 'd42cbcc95ff0166b99e7deaced0390f0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1900', '161', 'a0bb11dee578f19e9d397c3448d03c5f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1901', '161', '65ef5b6383124ca1dd1a6b644e657ffe', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1902', '161', 'c62a3180caf537563942089306c31c2d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1904', '161', 'feba950d6beeb7c98bf5b7630ea6c65b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1906', '160', 'd4c144c8d21aa9bd86bdf0ced1c0db1b', null, 'eaeA3oBtFJE:APA91bGuE8z2Onn7RIuO7WpxZ2Q9TLEKOHUqjhQaJx8E2fNxAtMSAx7F5wo1yp2X0S3xn-ES5u2pqEp64fD_0zZoobl5YZki1yKkO2hINxdKW9bse2Ezn3UHo4rSLiVN-Qn7jayUSpBI', 'e90789852dbaa4f8', '');
INSERT INTO `patient_login` VALUES ('1907', '161', 'd274901a4c17c3b8d088e37065ef2785', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1909', '162', '82d5be0248a6f03108c3994f67d4f140', null, 'dRanu04AOns:APA91bFYHQUfxx7PWOlHVDL21KWh-gPcwxDVJrQ6eaZxgiCNqG9yTVXyMDxDs80qoaHFxppb4Si4wIiqyYSHTWAvKwu5ALWcZyQZI7tZV4D8qRKtH8_NV-Rr5W08auyGlbssLBX8W0Qj', 'e90789852dbaa4f8', '');
INSERT INTO `patient_login` VALUES ('1910', '162', '960adab43eb72014bfdffddb8be51b46', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1911', '162', '2830a6424f09fef52be230f72f0e04d0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1912', '160', '2c990ac663944b79e1bc4b8882e75892', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1913', '162', 'b6f882a47eff69d755d14edcaef8279d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1914', '161', 'd0ec86e96c5c8f7608d978fbb279e303', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1915', '162', '1d2985f163ef37b541767762622a593e', null, 'dRanu04AOns:APA91bFYHQUfxx7PWOlHVDL21KWh-gPcwxDVJrQ6eaZxgiCNqG9yTVXyMDxDs80qoaHFxppb4Si4wIiqyYSHTWAvKwu5ALWcZyQZI7tZV4D8qRKtH8_NV-Rr5W08auyGlbssLBX8W0Qj', 'e90789852dbaa4f8', '');
INSERT INTO `patient_login` VALUES ('1916', '162', '9a278211e0e826ee0f5f8ea20edd7c96', null, 'dCr8qSiZbb8:APA91bHaCGIvRRbHDSiyP3LjHHNBPAz_focGu12w1JKvUT1zhuTBRMPQvs1xRf2nNE15GodYMA_9aKL1a5tvRJzdYm3ASs1WPA2DjJm6D27K1QkP3QwT4roQUZT5_5qLh3wwUITDMkwJ', '915b302e5a1c117', '');
INSERT INTO `patient_login` VALUES ('1917', '161', '14b1be368a57d74afe2a2ab035b5a012', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1918', '161', 'c5aa7d6c8930682fad94535295b93dde', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1919', '161', '934035b45f3aee06e745882fefc2874f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1920', '161', '4a0be33a7d55e42444e05b7a799cf936', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1921', '161', 'e18b379c02615c234d6fda9d6cb0c641', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1922', '162', 'd3ade7f2bd08c3f4881210684fac3c76', null, 'eJSGuUlYZQM:APA91bE8MbhuVq2G2fdo7-VZd1czdfQKAcMHHYSgRYJX7UCe2aVF7E2H9EuK1tI8GD_w6jwl6LXuJ8nIu6dz3bLuNWgu52KfjVKN8TSIIBK4E4FWgVvyIvqlnoIeHeJMbWsZiAWZdBy4', '915b302e5a1c117', '');
INSERT INTO `patient_login` VALUES ('1923', '161', 'abe3af2f12bd3c37f78a1d561cd9ac65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1924', '161', '7e503b8ec4b7e5023c6b7d60f330ea28', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1925', '161', '6fa6c941118991b05b4176aae8dc8fa3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1926', '161', '05b72709592b3eed5165a25af2cc2850', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1927', '162', 'f447526e3a933334c69da879c5e335c7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1928', '161', '54ee7436f610ab16e35c00716c1a0c83', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1929', '164', 'f9eaa7c3928c1256ef1a7b65d3f9ee77', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1930', '162', '5f95b7852e96a1e72f51921a9bde7b96', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1933', '166', '5a0b9cee0b4c1ff3a3408867cd0f4e52', null, 'e3fbbff8fa4fc806e0b4ec490ae0ba5d94a4cb7f2c0a3d0f68e71174f07e7807', '', '');
INSERT INTO `patient_login` VALUES ('1934', '166', 'e322cfbe6541a603370203d10688ffa3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1935', '166', '4efc080cbf74f5a6a61f6e46880f2da7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1936', '166', '7017d1751c7a758ef04e91139ef544d7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1937', '166', '84c2415261bb96316881bb01ed3a87d2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1938', '162', '32a8be90b4502f7056bd50c9a48a7a8b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1939', '162', '292416f726e543fd0ac6d3bc0629a5f2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1940', '162', '1377046a6d7f42fa9bdc3b3ab93934a5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1941', '162', 'f4f5f99df54c2e511371217b38a5c123', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1943', '161', '984fca831a245299c1dcbd07fd22a329', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1944', '161', 'd84a0b4096ed4022ef5f6f67d7720f0a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1946', '2', 'd55d57f6c20a63f5b05a6a42b16231c8', null, 'e3fbbff8fa4fc806e0b4ec490ae0ba5d94a4cb7f2c0a3d0f68e71174f07e7807', '', '');
INSERT INTO `patient_login` VALUES ('1947', '161', '6958a7b6962e55c1831d847ca942e394', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1948', '161', '6f2c531ed1c2e3110ca6c43c11b3d927', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1949', '161', '9f36f298228cc6a7192069b90bc6788d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1950', '161', 'ab31a72d5f251ef95822022366546403', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1951', '2', '1515bbb426e62c350953450a776e301a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1952', '2', '8374e001309332ef60f74ec2e3b4092c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1953', '2', 'c22ad510acd7d791d84b379cdabcbf32', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1954', '2', '859bceb0893d5846c44b6aaf4159443b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1955', '2', 'c26592c1ec92824fab6b2e4bcb12fc5e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1956', '2', '87ea7dfb67311f61b9bc31f2210995cc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1957', '2', 'f83bda2aeab3f1e26d8f84137d456a77', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1958', '161', '23bfdb80ebd1e2fcd837f6cc59620fe2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1959', '161', 'ae408368ce0a192b34f96aa86a58f895', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1960', '161', '94b893fe6f14d3374ed97b2d64e4df33', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1961', '161', '61cee2875e12b6ceb6dfb44f31ecf94d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1962', '161', 'c7401886c36f89deac506b844a19321c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1963', '161', 'be661313435d92dd132b91b22a195ec2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1964', '161', '657afedc499e33d8868a2313cd1124c6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1965', '161', 'e0f88156d3c82a91e445cedf3cf6ac72', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1966', '161', 'd4aed80e547d9123c87676fb770fd6a2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1967', '161', '36f1dffcacac0efc3c891092bb8bf169', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1968', '161', '9568f281566d4946aa0b07c84513e389', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1969', '161', 'cd0c512248b808f9943e91c37c8ab36e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1970', '161', 'd7bc6d42a5a4d9cffb264cfdff33fe99', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1971', '161', '0d98e0fd73e27ab188945cf50e6dd762', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1972', '161', '29b97acfb6d03d5fafacd51cc7bc8769', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1973', '169', 'c71630cc87e2026bfb5ff1d56d5c9874', null, '', '', '');
INSERT INTO `patient_login` VALUES ('1974', '161', '4417a90258c3fee3db5f9504fa40e9d3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2003', '171', 'ca07c98dbfd6437767af3f16887e4dfd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2004', '171', '218d5bfa59fe22c7258a7dda607aa401', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2005', '171', '89b78d0568cdfe693280833a26cb335e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2006', '171', 'e949f8d8e96fabdd33542c84fded8422', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2007', '173', '501f5aa28e36e162a794f6579c516ab9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2008', '173', 'b2482c89f51ebf4aedb2db8d5f7c1a52', null, 'fboA2oc2CBo:APA91bH5GNwLazgYwywsGZxrHScsXxzX9--INAUFFT-0BYDsZBw0PfpjMtZ8Qv_niwpHA_94oVEtX2xVu-SJCZcdsqRWmxkJMmt5g_r_24PwWHdOlTngR5zGaycdEoVRHausesKOuHky', 'e47aba74fd0e999b', '');
INSERT INTO `patient_login` VALUES ('2009', '173', '172a0124b32bd6b17484c58a28a314fb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2010', '173', '09390da90b9a75f4f28ebc47ad1116c9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2011', '173', 'fa837edc57e017eaff7a956b21234db9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2012', '173', '863a773ae43ccd981edc4096e93ba1fe', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2013', '173', '5f4e3abf27f8227ceb34e689244b190b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2020', '174', '765b3df5cc2ec4104b9de3fb721affb1', null, 'efnVkRS0beA:APA91bHcgtECfe1rIEixdn6YKiLY9C4l-TqPL2f3sHQHHmspI_Z8_yCDlWea84KB9pmjF6SA21FZkT50_ZDjHtZuTK5qq1WCggHKIfEckyzcz7iV18oUmv2xbZkBx9cnVUQ4_1VdP2Lz', 'e47aba74fd0e999b', '');
INSERT INTO `patient_login` VALUES ('2021', '174', '2ba93eb66fc4aac254cdd9c447862731', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2022', '174', '227ae604415ad5f4b1d156cf9d214731', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2023', '174', '98050cf6050712d1a9617b6eec4c8bf1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2025', '174', '8d1b36b1d27346d4cdd570b56a606d28', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2069', '170', 'ac7ddb93b859c80c1afbf6ec4464ff2c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2070', '170', '5f3c56fb4a466451812bf17c94dc0540', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2071', '170', '95d1e11a12cec174862b3140d1ba45fa', null, 'efnVkRS0beA:APA91bHcgtECfe1rIEixdn6YKiLY9C4l-TqPL2f3sHQHHmspI_Z8_yCDlWea84KB9pmjF6SA21FZkT50_ZDjHtZuTK5qq1WCggHKIfEckyzcz7iV18oUmv2xbZkBx9cnVUQ4_1VdP2Lz', 'e47aba74fd0e999b', '');
INSERT INTO `patient_login` VALUES ('2072', '170', '42d56abf04a56e0b45b6d994eb77748d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2073', '154', 'b8d07e6c71e03462bf3f88e7805ff7ec', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2074', '154', '2fd4777c9097bd889cd75f247784b459', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2075', '154', 'cbdfdcab552c4b18fc57c59dfc1cfb63', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2076', '170', '6e57c4d45c676f2dfff0b536a95a290d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2077', '170', '4dd9ba4e014b1b39ab54203a705f611a', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2078', '170', '41801e5f7b6b1da0c825fddbd8d6f1a6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2079', '170', '596dfdd2f69deb0436811703f54a8c0c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2080', '154', '87e1c2b3b1a5cc50626b2a4048e47c40', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2093', '170', '6f6b1fb65deb2bf10cfd7fa63e83205b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2103', '175', '22e34627b32c611f08aef6e3dd2c0b3f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2104', '175', 'de02e3c3fc82441a30a0b13582dfb019', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2105', '175', '5b35079111ba51bc811f654f11f7cf32', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2106', '175', '1c0823cb8bfa249a6819efed2e2c1492', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2107', '162', '67a96661ff0ece9c7fae1b8cc6804be2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2108', '162', '712085f2a2c155dd1b5ca8f20b49bf23', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2109', '162', 'd55971c96bd88fa7edeedb2c850aa3a8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2110', '162', 'c2f9a06969019fd3e6f3b3349c2c1af3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2112', '160', '82d71a4c3d74309b59443c9a677c300c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2113', '160', '27623dd54c9753dff8463ba097bdf451', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2116', '160', 'e8c8096cb917c864b41153623bc17f04', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2117', '160', '9c86b66aad650ddfcc7daf2e4eff9ff4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2118', '160', 'd2dfdf66c9ed19cdd6f3a415ebfec108', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2119', '160', 'a52f21e8005d81ba068a079743fe9144', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2120', '160', '347a57ef363a75a0ee9e20eda085a8a1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2121', '160', '21f6f60716278379ac8ec1bc1e726ef3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2122', '154', 'baf2bb70625907496af9d6e6ae641e9d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2123', '154', '9f4c2e46206bd08ad275e2f87df53c44', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2124', '160', '4978a9d2cf4d99da4b8d9fd64262aa8e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2125', '160', 'fb02acf73aa5b16c9e3957a6d73d9476', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2127', '160', '0ac9c4fd561f18c28b4636e4c3f158d8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2128', '160', '746c86ebb6336074ad3bd3974f9ccac1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2129', '160', '3e7767a366f89c916245ec3ca7a489f6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2130', '160', 'c8d3c0d78447f7b666c6a603ac785eaa', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2131', '160', 'ff01985f29c646be9c8ac2835e627eab', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2132', '160', 'b5a033fe9550fbe061417edc32eaff86', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2133', '160', 'bacf7e7e8aa50612e1df1a8a383674a4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2134', '160', 'd99bc434066b7455b69f6e0d036f41fc', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2135', '160', '34a2d24cdc5ff2a955060841e8210e70', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2136', '160', '870161217a18cf28bc8f707f389ca67d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2137', '160', '38fa78f8ba09942985a807b87e9a8a78', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2138', '0', 'e267f9200c6955f9e383b6d2368b8e64', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2139', '0', '1f29fcbb6632d9f1c2ac79eed3d5de65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2140', '2', 'e9e209086a53484dacc0c28d371d3a08', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2142', '160', '207d54b3a8779124bed9582169604183', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2143', '160', 'c2a80f54de15b6159bb34cf098ca8070', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2144', '160', '5c6aa15706f9174d7cd0089d5dba471b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2145', '2', '4bacae5eeab1596048c9bca207a4b5f5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2146', '160', '8e5aa083231f4a3f6c82ff57cb9e0d0e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2147', '160', '449d4d3c072d2ab37e23de4b5465831e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2148', '160', 'c6b32978c9d2e8bdf0fd959a17c61491', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2149', '160', 'e2cff4cf122dc6ce1ffb3aaa61158a37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2150', '160', 'fc423bd9ac7c2652df5636b6d4ded4fe', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2151', '160', 'b0318d9888532457af17261b74aa053b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2152', '160', 'd784a615f6705bd2f208cb6c34ee7860', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2153', '160', '483da234a621ac7bc2d9caab0a416449', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2154', '160', '800b398e202a860c84cdb555e08af62b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2155', '160', 'af55d21b0b3654d2a38e4dfb7e5d1778', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2156', '160', '18b07ecf9988f13e7b0c62911c7ddafb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2157', '160', 'e51cfe27239e0386d070b2ce9f04a470', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2158', '2', 'f62a903c8d750d5c4e7b760258f8137e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2159', '2', '63ba41bc73c5c6c6752fa27baf6cfc82', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2160', '2', 'e26afcaa6b7851bc563be346d541c8f7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2161', '2', 'ac28c789d7ec0f27bae85c52ab1e6ad5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2162', '2', '96f347120a025aeba3edf961575cd121', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2163', '160', 'd033271141431e3b602a3c150190d233', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2164', '160', 'c834f5b161c62381baf20182dd9d87c7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2165', '160', 'c53ebd5ee383205da6bf0410df075100', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2166', '160', 'a32c3a0bcd6cdeeaeac185f321392e68', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2167', '2', '925a8fae9854a1d8a523a4d1c888e49b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2168', '160', '33866e54c77de9842ccae017ac15b2f3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2169', '2', '2c41b0ec030b1a446bf53ab400a82fdb', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2170', '2', '369ea036fd480009588fbe0e1cf81f8b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2171', '2', '998fd151d90bb3213a61e56b7301fc86', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2172', '2', '5a21a3659e44e6c0d9e2074dce442a63', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2173', '2', 'ad94306218cb97ed19aac97de8931020', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2174', '2', 'a35b2dcad17daa7262b5ccfe8dc63581', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2175', '2', '76502d7383208b2f878428f659a8cf28', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2176', '2', '848febb8c0927fd99086fb17f102a237', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2177', '160', '10ab11814b6f56ad29368e8da41aec65', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2178', '160', 'd16141b8cee9792933201a4f56920603', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2179', '160', '46dda21a0ccb2812841650a3738dcdef', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2180', '2', '4067bbb4dfa17121d512aeae081a46f0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2181', '2', '0203c0c95d79a6983ac35961a5656e76', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2182', '160', 'be2b92aa7c7f141625693745ea71fe8c', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2184', '2', 'ab94dd98b52a441367504a82829e76a1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2185', '160', '284cfd2f09114d906df7f5daaf8c89b2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2186', '160', '1f951a93f23266aa876cc300f5806bec', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2187', '160', 'dd51bc882d2771fe08012c69e6ffd012', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2188', '160', '801040c24caa42158fedf2e78c1843b2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2189', '160', 'c265c355abf17ac1a6614ef253cd6a30', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2190', '160', 'beadbc5971327b7a76cb4fe55e0d9988', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2191', '160', '367419c93869c29e74e48cedf846ce37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2193', '2', '92679a003326fc34751079c28f9f456d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2194', '2', '3eb44c3290b92df8f7e6573dc81e1348', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2195', '2', 'd96a38c1d41bafb4b37d3c84a7cdc22f', null, 'fCbxmleb8NE:APA91bGRniqkCL2f_WHfLcSDG48RPgO6dmp2fLEA2BUm-d-TE89D4pzpvSVnKHWaVGrvpYgZ5s1w-QkWv3e4KBrcAiktYciDBc6SLe_8jz22VJp5O6m_KL4aYOo9iEyhUiKkZ_6iDvRO', '8e0c6cf1aead475e', '');
INSERT INTO `patient_login` VALUES ('2196', '160', '962aa51f0f82c3de5d43cdaf9911cea4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2197', '2', '4ebb2922f0e6e096f44c050497e901d1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2198', '160', '779d673901d07782990fa8f3aa63a764', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2199', '2', '43aecfe4d461ccdb15ba12e8004ea646', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2200', '2', '450ba95a1d7a63fa4b265867573fbeb8', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2201', '160', '97a19899e531ade1f69ccf1149ceec74', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2202', '160', 'c23ed2b0662f4627ac845801ddadbf0d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2203', '2', 'a8f7a2980584de09b93e25ce8fa511da', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2204', '2', '90aab1b8ca82db013e0f3524df56e277', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2205', '2', 'a6002803b3b38dcb7c709f8255af07a9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2206', '2', 'd841a521e8164def94e1920b99672754', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2207', '2', '2b0b1abd6f897a22c11db7de803e355e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2208', '2', 'a14000e76cba8ac12cac55f75080d51d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2209', '160', '4b95c556612cc618d6b98512e6fdac37', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2210', '160', 'df32aed4ec24f83c0b03e4e4106c133f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2211', '160', 'a7a06fe8e08abde18a59784ba2f2aa40', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2212', '160', '7ab375d1df34f93ce4870b369c39e3e4', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2214', '160', '91ef0543ff98d6fbbf8645e93fab8d89', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2215', '160', '74e703978cea43337f826dbd27294000', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2216', '160', '97b37c241a51658b736a9ea9d280e5bf', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2217', '2', 'cda9ddb05848d3a7e0b11b6dc9f178d0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2218', '160', '731d930db5311948f589f543a63b8d8e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2219', '160', '586309d924641001ca7f2598d32410f7', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2220', '160', 'eb39950e1ad5e2e306b9648f15cb6e42', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2221', '2', '27fc1d01e7afa71389d471127774b89f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2222', '160', '348d855d0d2537608f5b258cfd84e420', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2223', '160', '8844676da5daab25203bfb38e833d846', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2225', '160', 'edf92e0f1c92349539b11946e6aef5f3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2226', '160', 'abae9e3767714e4da0d8288cec948347', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2227', '160', '3c2fd519689f849e0fba9c2ec5478119', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2228', '160', 'f1bfed62b0d8aef51f5ecddd8e3241ef', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2229', '160', '34380a3e50766de72ff0396530fa868e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2230', '160', '11d171e2f123356ec0ead742cd48abf5', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2231', '160', 'aa0e48414ebc3b719d3e535e30ea984e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2232', '160', 'c547cf4fdf625b9bc99c0bf223ea9c93', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2233', '160', '87a9d4bceedc25aa92feb3800382df22', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2234', '178', 'f6a58b2d4de04b0451587f25c842f638', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2235', '3', 'e4885532c4efe6a50d02a1a1d5b6397e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2236', '160', '1cf7d3f51a5d98d5a20f719385869648', null, 'f8HPsOrU0R0:APA91bFxCo5CHxZJpwM3R_pi85F-14OlTZa6re81FkdwlY-nU0KP8f3H_eHUS5MNHadOdAQcLseCgI3Ofahobjcunf8qr_RwFu2VCfRrZRI7o0xfzs48p1sdc-UJUFeuHWALApu5OWw7', '915b302e5a1c117', '');
INSERT INTO `patient_login` VALUES ('2237', '160', 'e8f60cf0e697d68f47b9be1111bf128d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2238', '160', 'ffa7f91a37cfec1cbf3d84326cbade73', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2239', '2', 'a14ff5d423ea0b34adb6a73118dfd9d2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2240', '2', '7999d2d58463395ff83224902c186249', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2241', '2', '6f58fdb922a838f8cbff650dd1308917', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2242', '2', '33a1b9748257488aaf006b217b7eba79', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2243', '2', '9f6482f567cf4e3c29ac67219e9da93f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2244', '2', 'd344e57cfb8f7931e05f661891c68dde', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2245', '2', 'ee85cd6253d98e244a3b4f27f7548983', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2246', '2', '5b86c4b44f119d564f955381678ecbb3', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2247', '160', '5782b25f469a9a9a0a8ab1d306f2e3d1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2248', '160', '96dd6e54ee658dafcf469428690a5a72', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2249', '160', '946e32ec2896484fe7b5e23cffe775aa', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2250', '160', '3a08cde8b325eedfbf84503d0da3afd9', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2251', '160', 'd8bde5d70dea85f078e772afc674fa4f', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2252', '160', 'e9d0769c22ca675a0100f29ab79397f0', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2253', '160', '9415e79d24eebb2f4d679373086287c1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2254', '160', '4e336867a3ef3b6aee5666bc0781babd', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2255', '160', '6e4e0cf980d2cf40d3dbd4592fee66c6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2256', '160', 'eac0ddf69f78ce0e9aba5705cdbf509e', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2257', '160', 'aebd365ae3d0c3cac82241119a557e92', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2258', '2', '507f0d8b68700c5e7974f28d1274a4e6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2259', '2', '7f69ea29b1dc1e1295163ae8900cf3f6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2260', '2', '9bffe26d5d641d2bcdece5d9a6c2b098', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2261', '2', 'ad9accce774ab0f74f8df37bd7a221d1', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2262', '160', '6a7e81810591f9ef153cbc8a05654c3b', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2263', '160', '2ec32c75d31404e5c45b363ed2be33f2', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2264', '160', 'fc08f54c0ec92fadfed7affded8b7e66', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2265', '2', 'f1cf4b4c9737fc06525bf5e9eddb65f6', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2266', '2', '76a8a50260acee3a634bea99731497af', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2267', '2', '86e66947183938ac8fc469e2083df652', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2268', '160', '5820b4ac16fd971b9437cf67e7408a9d', null, '', '', '');
INSERT INTO `patient_login` VALUES ('2269', '2', '014c836d7cf8759a0c657c70b6e8bc99', null, '', '', '');

-- ----------------------------
-- Table structure for `patient_phone`
-- ----------------------------
DROP TABLE IF EXISTS `patient_phone`;
CREATE TABLE `patient_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_phone
-- ----------------------------
INSERT INTO `patient_phone` VALUES ('12', '02', '123', null, '4');
INSERT INTO `patient_phone` VALUES ('13', '+20', '0134567', null, '23');
INSERT INTO `patient_phone` VALUES ('14', ' 20', '0134567', null, '23');
INSERT INTO `patient_phone` VALUES ('15', ' 20', '0134567', null, '23');
INSERT INTO `patient_phone` VALUES ('16', '20', '01226427786', null, '39');
INSERT INTO `patient_phone` VALUES ('18', '20', '01274036897', null, '39');
INSERT INTO `patient_phone` VALUES ('30', '20', '0852365479', null, '39');
INSERT INTO `patient_phone` VALUES ('31', '20', '1234', null, '39');
INSERT INTO `patient_phone` VALUES ('33', '02', '123', null, '58');
INSERT INTO `patient_phone` VALUES ('34', '02', '12345', null, '58');
INSERT INTO `patient_phone` VALUES ('35', '02', '12345', null, '73');
INSERT INTO `patient_phone` VALUES ('36', '20', '0854658', null, '58');
INSERT INTO `patient_phone` VALUES ('37', '20', '02589654', null, '75');
INSERT INTO `patient_phone` VALUES ('38', '20', '085236', null, '76');
INSERT INTO `patient_phone` VALUES ('39', '02', '123456', null, '16');
INSERT INTO `patient_phone` VALUES ('40', '02', '1234567', null, '16');
INSERT INTO `patient_phone` VALUES ('41', '20', '08974563', null, '78');
INSERT INTO `patient_phone` VALUES ('43', '20', '78965422', null, '78');
INSERT INTO `patient_phone` VALUES ('44', '20', '456923', null, '78');
INSERT INTO `patient_phone` VALUES ('45', '20', '8525', null, '102');
INSERT INTO `patient_phone` VALUES ('48', '20', '868684607', null, '106');
INSERT INTO `patient_phone` VALUES ('53', '+376', '852147', null, '105');
INSERT INTO `patient_phone` VALUES ('54', '+376', '96321', null, '105');
INSERT INTO `patient_phone` VALUES ('55', '20', '86864834', null, '106');
INSERT INTO `patient_phone` VALUES ('56', '+376', '321456', null, '105');
INSERT INTO `patient_phone` VALUES ('57', '+376', '589632', null, '105');
INSERT INTO `patient_phone` VALUES ('58', '+376', '123654', null, '105');
INSERT INTO `patient_phone` VALUES ('59', '20', '1234', null, '107');
INSERT INTO `patient_phone` VALUES ('60', '2', '01147670025', null, '112');
INSERT INTO `patient_phone` VALUES ('61', '2', '01226427786', null, '2');
INSERT INTO `patient_phone` VALUES ('62', '2', '01285819291', null, '114');
INSERT INTO `patient_phone` VALUES ('63', '2', '01226427786', null, '115');
INSERT INTO `patient_phone` VALUES ('64', '2', '01226427786', null, '116');
INSERT INTO `patient_phone` VALUES ('65', '2', '01285819291', null, '117');
INSERT INTO `patient_phone` VALUES ('66', '2', '01225529997', null, '118');
INSERT INTO `patient_phone` VALUES ('67', '2', '01226427786', null, '119');
INSERT INTO `patient_phone` VALUES ('68', '2', '01226427786', null, '120');
INSERT INTO `patient_phone` VALUES ('69', '2', '01093103001', null, '124');
INSERT INTO `patient_phone` VALUES ('70', '2', '01275471742', null, '128');
INSERT INTO `patient_phone` VALUES ('71', '+2', '01226427786', null, '106');
INSERT INTO `patient_phone` VALUES ('72', '2', '01001420808', null, '3');
INSERT INTO `patient_phone` VALUES ('73', '2', '01226427786', null, '134');
INSERT INTO `patient_phone` VALUES ('74', '+2', '01226427786', null, '147');
INSERT INTO `patient_phone` VALUES ('75', '+2', '01226427786', null, '154');
INSERT INTO `patient_phone` VALUES ('76', '+2', '01226427786', null, '155');
INSERT INTO `patient_phone` VALUES ('77', '2', '01226427786', null, '156');
INSERT INTO `patient_phone` VALUES ('78', '2', '01285819291', null, '160');
INSERT INTO `patient_phone` VALUES ('79', '2', '01275471742', null, '161');
INSERT INTO `patient_phone` VALUES ('80', '2', '01285819291', null, '162');
INSERT INTO `patient_phone` VALUES ('81', '+2', '01226427786', null, '166');
INSERT INTO `patient_phone` VALUES ('84', '2', '01069416957', null, '174');
INSERT INTO `patient_phone` VALUES ('86', '+2', '01069416957', null, '175');

-- ----------------------------
-- Table structure for `patient_phone_temp`
-- ----------------------------
DROP TABLE IF EXISTS `patient_phone_temp`;
CREATE TABLE `patient_phone_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `activation_code` varchar(45) DEFAULT NULL,
  `exp_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_phone_temp
-- ----------------------------
INSERT INTO `patient_phone_temp` VALUES ('34', '+20', '01243587', null, '19', '1234', '2017-03-15 19:59');
INSERT INTO `patient_phone_temp` VALUES ('35', '+20', '909120932', null, '20', '1234', '2017-03-15 20:11');
INSERT INTO `patient_phone_temp` VALUES ('36', '+20', '019373', null, '21', '1234', '2017-03-16 07:23');
INSERT INTO `patient_phone_temp` VALUES ('38', '20', '01226427786', null, '26', '1234', '2017-03-22 13:41');
INSERT INTO `patient_phone_temp` VALUES ('39', '20', '01226427786', null, '26', '1234', '2017-03-22 13:42');
INSERT INTO `patient_phone_temp` VALUES ('40', '20', '01226427786', null, '27', '1234', '2017-03-22 13:56');
INSERT INTO `patient_phone_temp` VALUES ('41', '20', '1478', null, '28', '1234', '2017-03-22 14:03');
INSERT INTO `patient_phone_temp` VALUES ('46', '20', '01226427786', null, '33', '1234', '2017-03-22 17:50');
INSERT INTO `patient_phone_temp` VALUES ('48', '20', '01226427786', null, '34', '1234', '2017-03-23 19:31');
INSERT INTO `patient_phone_temp` VALUES ('49', '20', '05269', null, '35', '1234', '2017-03-23 19:47');
INSERT INTO `patient_phone_temp` VALUES ('50', '20', '8855', null, '36', '1234', '2017-03-23 20:48');
INSERT INTO `patient_phone_temp` VALUES ('51', '20', '9895#0#', null, '37', '1234', '2017-03-23 21:27');
INSERT INTO `patient_phone_temp` VALUES ('52', '20', '01226427786', null, '38', '1234', '2017-03-23 21:46');
INSERT INTO `patient_phone_temp` VALUES ('54', '20', '01252525', null, '39', '1234', '2017-03-24 19:42');
INSERT INTO `patient_phone_temp` VALUES ('70', '20', '123456789', null, '0', '1234', '2017-03-27 10:04');
INSERT INTO `patient_phone_temp` VALUES ('71', '20', '12345658', null, '0', '1234', '2017-03-27 10:05');
INSERT INTO `patient_phone_temp` VALUES ('73', '20', '1001420808', null, '72', '1234', '2017-03-27 12:31');
INSERT INTO `patient_phone_temp` VALUES ('74', '20', '1001420808', null, '72', '1234', '2017-03-27 12:31');
INSERT INTO `patient_phone_temp` VALUES ('75', '02', '123', null, '4', '1234', '2017-03-27 20:57');
INSERT INTO `patient_phone_temp` VALUES ('79', '20', '1007893000', null, '74', '1234', '2017-03-27 22:54');
INSERT INTO `patient_phone_temp` VALUES ('83', '02', '123', null, '4', '1234', '2017-03-28 12:09');
INSERT INTO `patient_phone_temp` VALUES ('84', '02', '123', null, '16', '1234', '2017-03-28 12:11');
INSERT INTO `patient_phone_temp` VALUES ('91', '20', '0875246', null, '2', '1234', '2017-03-29 14:59');
INSERT INTO `patient_phone_temp` VALUES ('93', '+376', '4678825 74', null, '103', '1234', '2017-04-10 17:58');
INSERT INTO `patient_phone_temp` VALUES ('108', '1234', '01285819291', null, '107', '1234', '2017-04-26 01:38');
INSERT INTO `patient_phone_temp` VALUES ('109', '1234', '01285819291', null, '107', '1234', '2017-04-26 01:39');
INSERT INTO `patient_phone_temp` VALUES ('110', '1234', '01285819291', null, '107', '1234', '2017-04-26 09:33');
INSERT INTO `patient_phone_temp` VALUES ('111', '1234', '01285819291', null, '107', '1234', '2017-04-26 09:33');
INSERT INTO `patient_phone_temp` VALUES ('112', '1234', '01285819291', null, '107', '1234', '2017-04-26 09:38');
INSERT INTO `patient_phone_temp` VALUES ('113', '1234', '01285819291', null, '107', '1234', '2017-04-26 09:39');
INSERT INTO `patient_phone_temp` VALUES ('114', '1234', '01285819291', null, '107', 'hkub', '2017-04-26 09:46');
INSERT INTO `patient_phone_temp` VALUES ('115', '1234', '201001420808', null, '107', '108n', '2017-04-26 09:52');
INSERT INTO `patient_phone_temp` VALUES ('116', '1234', '201285819291', null, '107', 'ccc8', '2017-04-26 10:03');
INSERT INTO `patient_phone_temp` VALUES ('117', '1234', '201285819291', null, '107', 'ievk', '2017-04-26 10:04');
INSERT INTO `patient_phone_temp` VALUES ('118', '1234', '201285819291', null, '107', 'qizf', '2017-04-26 10:05');
INSERT INTO `patient_phone_temp` VALUES ('119', '1234', '0201285819291', null, '107', 'vzpe', '2017-04-26 10:06');
INSERT INTO `patient_phone_temp` VALUES ('120', '1234', '01285819291', null, '107', 'w5h5', '2017-04-26 10:07');
INSERT INTO `patient_phone_temp` VALUES ('121', '2', '01285819291', null, '107', '8e63', '2017-04-26 10:07');
INSERT INTO `patient_phone_temp` VALUES ('122', '20', '01147670025', null, '110', '87754', '2017-04-27 13:54');
INSERT INTO `patient_phone_temp` VALUES ('123', '20', '01147670025', null, '111', '12935', '2017-04-27 14:12');
INSERT INTO `patient_phone_temp` VALUES ('126', '2', '01285819291', null, '113', '87552', '2017-05-11 12:10');
INSERT INTO `patient_phone_temp` VALUES ('127', '2', '01285819291', null, '113', '33240', '2017-05-11 12:11');
INSERT INTO `patient_phone_temp` VALUES ('128', '2', '01285819291', null, '113', '47053', '2017-05-11 12:12');
INSERT INTO `patient_phone_temp` VALUES ('137', '2', '01227447334', null, '121', '76220', '2017-05-16 09:44');
INSERT INTO `patient_phone_temp` VALUES ('138', '2', '01227447334', null, '121', '60985', '2017-05-16 09:52');
INSERT INTO `patient_phone_temp` VALUES ('139', '2', '01227447334', null, '121', '81551', '2017-05-16 09:53');
INSERT INTO `patient_phone_temp` VALUES ('140', '2', '01227447334', null, '121', '20226', '2017-05-16 09:54');
INSERT INTO `patient_phone_temp` VALUES ('141', '+376', '4656874543', null, '122', '19196', '2017-05-16 13:13');
INSERT INTO `patient_phone_temp` VALUES ('142', '+376', '12345654', null, '122', '99876', '2017-05-17 02:41');
INSERT INTO `patient_phone_temp` VALUES ('143', '+376', '123654789', null, '122', '67532', '2017-05-17 03:19');
INSERT INTO `patient_phone_temp` VALUES ('144', '+20', '01093103001', null, '122', '02011', '2017-05-17 03:24');
INSERT INTO `patient_phone_temp` VALUES ('145', '+376', '1324343', null, '122', '54614', '2017-05-17 04:06');
INSERT INTO `patient_phone_temp` VALUES ('146', '+20', '01226427786', null, '122', '69555', '2017-05-17 04:19');
INSERT INTO `patient_phone_temp` VALUES ('147', '+2', '01093103001', null, '122', '80345', '2017-05-17 05:09');
INSERT INTO `patient_phone_temp` VALUES ('148', '+2', '01093103001', null, '122', '34103', '2017-05-17 06:41');
INSERT INTO `patient_phone_temp` VALUES ('149', '+2', '01093103001', null, '122', '36138', '2017-05-17 06:50');
INSERT INTO `patient_phone_temp` VALUES ('150', '+2', '01226427786', null, '122', '00504', '2017-05-17 06:52');
INSERT INTO `patient_phone_temp` VALUES ('151', '2', '0109 310 3001', null, '124', '22738', '2017-05-21 05:06');
INSERT INTO `patient_phone_temp` VALUES ('153', '2', '01001420808', null, '126', '60532', '2017-05-22 13:05');
INSERT INTO `patient_phone_temp` VALUES ('154', '2', '01001420808', null, '126', '44091', '2017-05-22 13:06');
INSERT INTO `patient_phone_temp` VALUES ('155', '2', '60532', null, '126', '83427', '2017-05-22 13:06');
INSERT INTO `patient_phone_temp` VALUES ('157', '2', '01001420808', null, '126', '10443', '2017-06-13 22:24');
INSERT INTO `patient_phone_temp` VALUES ('163', '02', '0201285819291', null, '16', '09172', '2017-08-05 08:31');
INSERT INTO `patient_phone_temp` VALUES ('164', '02', '0201285819291', null, '16', '71021', '2017-08-05 08:33');
INSERT INTO `patient_phone_temp` VALUES ('165', '+2', '01226427786', null, '2', '16592', '2017-08-22 08:01');
INSERT INTO `patient_phone_temp` VALUES ('166', '+2', '543535', null, '2', '08278', '2017-08-22 08:14');
INSERT INTO `patient_phone_temp` VALUES ('167', '+2', '01226427786', null, '2', '81526', '2017-08-24 08:17');
INSERT INTO `patient_phone_temp` VALUES ('168', '+2', '01274036897', null, '2', '41361', '2017-08-24 08:18');
INSERT INTO `patient_phone_temp` VALUES ('169', '+2', '01274036897', null, '2', '67527', '2017-08-24 08:27');
INSERT INTO `patient_phone_temp` VALUES ('170', '+2', '01274036897', null, '2', '67949', '2017-08-24 08:29');
INSERT INTO `patient_phone_temp` VALUES ('171', '+2', '34534643534', null, '2', '25242', '2017-08-24 10:02');
INSERT INTO `patient_phone_temp` VALUES ('172', '+2', '0126427786', null, '145', '80911', '2017-08-27 09:57');
INSERT INTO `patient_phone_temp` VALUES ('173', '+2', '01226427786', null, '145', '24688', '2017-08-27 09:59');
INSERT INTO `patient_phone_temp` VALUES ('174', '+2', '01226427786', null, '145', '54074', '2017-08-27 10:29');
INSERT INTO `patient_phone_temp` VALUES ('175', '+2', '01226427786', null, '145', '75833', '2017-08-27 10:29');
INSERT INTO `patient_phone_temp` VALUES ('178', '+2', '43874', null, '147', '47705', '2017-08-27 11:53');
INSERT INTO `patient_phone_temp` VALUES ('186', '2', '01275471742', null, '160', '72989', '2017-08-30 07:12');
INSERT INTO `patient_phone_temp` VALUES ('187', '123456', '01274036897', null, '160', '86099', '2017-08-30 07:15');
INSERT INTO `patient_phone_temp` VALUES ('196', '2', '55656866', null, '170', '83061', '2017-08-31 07:02');
INSERT INTO `patient_phone_temp` VALUES ('197', '2', '01069416957', null, '170', '85948', '2017-08-31 07:02');
INSERT INTO `patient_phone_temp` VALUES ('198', '2', '01069416957', null, '170', '44500', '2017-08-31 07:03');
INSERT INTO `patient_phone_temp` VALUES ('199', '2', '01069416957', null, '170', '23398', '2017-08-31 07:04');
INSERT INTO `patient_phone_temp` VALUES ('200', '2', '01069416957', null, '170', '72953', '2017-08-31 07:05');
INSERT INTO `patient_phone_temp` VALUES ('202', '2', '01069416957', null, '170', '25426', '2017-08-31 07:10');
INSERT INTO `patient_phone_temp` VALUES ('204', '2', '01069416957', null, '170', '46103', '2017-08-31 07:14');
INSERT INTO `patient_phone_temp` VALUES ('205', '2', '01069416957', null, '170', '51802', '2017-08-31 07:15');
INSERT INTO `patient_phone_temp` VALUES ('206', '2', '01069416957', null, '173', '73666', '2017-08-31 07:56');
INSERT INTO `patient_phone_temp` VALUES ('207', '2', '01118278147', null, '173', '38390', '2017-08-31 07:58');
INSERT INTO `patient_phone_temp` VALUES ('208', '2', '01118278147', null, '173', '88978', '2017-08-31 07:58');
INSERT INTO `patient_phone_temp` VALUES ('209', '2', '01118278147', null, '173', '41119', '2017-08-31 07:59');
INSERT INTO `patient_phone_temp` VALUES ('211', '2', '010694169578', null, '170', '11502', '2017-09-02 04:18');
INSERT INTO `patient_phone_temp` VALUES ('212', '2', '01069416957', null, '170', '75281', '2017-09-02 04:18');
INSERT INTO `patient_phone_temp` VALUES ('213', '2', '01069416957', null, '170', '01213', '2017-09-02 04:19');
INSERT INTO `patient_phone_temp` VALUES ('214', '2', '010694169578', null, '170', '71048', '2017-09-02 04:23');
INSERT INTO `patient_phone_temp` VALUES ('216', '2', '01069416957', null, '170', '45597', '2017-09-02 04:33');
INSERT INTO `patient_phone_temp` VALUES ('217', '+2', '*', null, '175', '02181', '2017-09-04 04:29');
INSERT INTO `patient_phone_temp` VALUES ('218', '+2', '*#', null, '175', '03919', '2017-09-04 04:33');
INSERT INTO `patient_phone_temp` VALUES ('220', '2', '01275471742', null, '160', '34352', '2017-09-05 06:52');
INSERT INTO `patient_phone_temp` VALUES ('221', '2', '01275471742', null, '160', '47073', '2017-09-05 06:54');
INSERT INTO `patient_phone_temp` VALUES ('222', '2', '01285819291', null, '160', '59850', '2017-09-05 06:56');
INSERT INTO `patient_phone_temp` VALUES ('223', '2', '01275471742', null, '160', '06279', '2017-09-05 07:11');
INSERT INTO `patient_phone_temp` VALUES ('224', '2', '01285819291', null, '160', '31144', '2017-09-05 07:11');

-- ----------------------------
-- Table structure for `patient_questions`
-- ----------------------------
DROP TABLE IF EXISTS `patient_questions`;
CREATE TABLE `patient_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_ar` varchar(255) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `parent_question` int(11) DEFAULT NULL,
  `question_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_questions
-- ----------------------------
INSERT INTO `patient_questions` VALUES ('1', 'تاريخ الميلاد', '0', '0', 'Date of Birth?');
INSERT INTO `patient_questions` VALUES ('2', 'هل تعاني من أي مرض عضوي؟', '1', '0', ' Do you suffer from any organic disease?');
INSERT INTO `patient_questions` VALUES ('3', 'ما هي تلك الأمراضا؟', '0', '2', 'What are these diseases?');
INSERT INTO `patient_questions` VALUES ('4', 'هل تعاني من حساسية من أي دواء؟', '1', '0', 'Are you allergic to any medication?');
INSERT INTO `patient_questions` VALUES ('5', 'ما هي تلك الأدوية؟', '0', '4', 'What are these medications?');
INSERT INTO `patient_questions` VALUES ('6', 'هل تتناول أي نوع من الدواء؟', '1', '0', 'Do you take any medication?');
INSERT INTO `patient_questions` VALUES ('7', 'ما هي تلك الأدوية؟', '0', '6', 'What are these medications?');
INSERT INTO `patient_questions` VALUES ('8', 'هل يوجد تاريخ عائلي لأمراض مزمنة؟', '1', '0', 'Is there a family history of chronic diseases?');
INSERT INTO `patient_questions` VALUES ('9', 'ما هي تلك الأمراض؟', '0', '8', 'What are those diseases?');
INSERT INTO `patient_questions` VALUES ('10', 'هل انتي حامل؟', '1', '0', 'Are you pregnant?');
INSERT INTO `patient_questions` VALUES ('11', 'هل انتي مرضعة؟', '1', '0', 'Are you breastfeeding?');

-- ----------------------------
-- Table structure for `patient_requests`
-- ----------------------------
DROP TABLE IF EXISTS `patient_requests`;
CREATE TABLE `patient_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `patient_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `patient_age` int(10) unsigned NOT NULL,
  `patient_weight` decimal(8,2) NOT NULL,
  `patient_address_id` int(11) DEFAULT NULL,
  `is_at_home` tinyint(1) NOT NULL,
  `is_able_to_be_stable` tinyint(1) NOT NULL,
  `number_of_scanned_prescriptions` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `refuse_comment` varchar(255) DEFAULT NULL,
  `uploads` longtext,
  `report` varchar(255) DEFAULT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_requests_patient_id_foreign` (`patient_id`),
  KEY `patient_requests_patient_address_id_foreign` (`patient_address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_requests
-- ----------------------------
INSERT INTO `patient_requests` VALUES ('70', '3', 'id', '40', '55.00', '64', '1', '1', '1', '2017-08-02 18:00:02', '2017-08-02 18:14:15', 'pa', null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('72', '3', 'id', '40', '45.00', '59', '1', '1', '1', '2017-08-07 15:45:22', '2017-08-07 15:50:54', 'pa', null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('73', '3', 'id', '40', '55.00', '75', '1', '1', '1', '2017-08-07 19:41:24', '2017-08-14 22:28:23', 'rd', null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('81', '128', 'id', '28', '72.00', '67', '1', '1', '1', '2017-08-08 18:43:59', '2017-08-08 18:51:02', 'rd', null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('82', '128', 'id', '28', '72.00', '67', '1', '1', '1', '2017-08-16 15:02:01', '2017-08-16 15:22:14', 'rd', null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('83', '160', 'id', '28', '50.00', '99', '1', '1', '1', '2017-09-05 20:22:31', '2017-09-05 20:22:31', null, null, null, null, null, '0');
INSERT INTO `patient_requests` VALUES ('84', '160', 'id', '28', '70.00', '99', '1', '1', '1', '2017-09-05 20:23:43', '2017-09-05 20:23:43', null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for `patient_requests_log`
-- ----------------------------
DROP TABLE IF EXISTS `patient_requests_log`;
CREATE TABLE `patient_requests_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `changedat` datetime DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `old_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_requests_log
-- ----------------------------
INSERT INTO `patient_requests_log` VALUES ('1', '79', '2017-08-08 06:52:22', null, 'rw', null);
INSERT INTO `patient_requests_log` VALUES ('2', '79', '2017-08-08 06:52:41', null, 'qw', 'rw');
INSERT INTO `patient_requests_log` VALUES ('3', '79', '2017-08-08 06:53:21', null, 'qa', 'qw');
INSERT INTO `patient_requests_log` VALUES ('4', '79', '2017-08-08 06:53:48', null, 'pa', 'qa');
INSERT INTO `patient_requests_log` VALUES ('5', '79', '2017-08-08 06:54:19', null, 'pa', 'pa');
INSERT INTO `patient_requests_log` VALUES ('6', '79', '2017-08-08 06:54:19', null, 'rd', 'pa');
INSERT INTO `patient_requests_log` VALUES ('7', '79', '2017-08-08 06:54:19', null, 'rd', 'rd');
INSERT INTO `patient_requests_log` VALUES ('8', '79', '2017-08-08 06:55:29', null, 'rd', 'rd');
INSERT INTO `patient_requests_log` VALUES ('11', '80', '2017-08-08 08:33:27', null, null, 'qw');
INSERT INTO `patient_requests_log` VALUES ('12', '80', '2017-08-08 08:40:46', null, 'rw', null);
INSERT INTO `patient_requests_log` VALUES ('13', '81', '2017-08-08 08:45:11', null, 'rw', null);
INSERT INTO `patient_requests_log` VALUES ('14', '81', '2017-08-08 08:45:41', null, 'qw', 'rw');
INSERT INTO `patient_requests_log` VALUES ('15', '81', '2017-08-08 08:46:48', null, 'qa', 'qw');
INSERT INTO `patient_requests_log` VALUES ('16', '81', '2017-08-08 08:50:01', null, 'pa', 'qa');
INSERT INTO `patient_requests_log` VALUES ('17', '81', '2017-08-08 08:51:02', null, 'pa', 'pa');
INSERT INTO `patient_requests_log` VALUES ('18', '81', '2017-08-08 08:51:02', null, 'rd', 'pa');
INSERT INTO `patient_requests_log` VALUES ('19', '81', '2017-08-08 08:51:02', null, 'rd', 'rd');
INSERT INTO `patient_requests_log` VALUES ('20', '73', '2017-08-14 12:28:23', null, 'pa', 'pa');
INSERT INTO `patient_requests_log` VALUES ('21', '73', '2017-08-14 12:28:23', null, 'rd', 'pa');
INSERT INTO `patient_requests_log` VALUES ('22', '73', '2017-08-14 12:28:23', null, 'rd', 'rd');
INSERT INTO `patient_requests_log` VALUES ('23', '81', '2017-08-15 07:37:02', null, 'rd', 'rd');
INSERT INTO `patient_requests_log` VALUES ('24', '82', '2017-08-16 05:06:25', null, 'rw', null);
INSERT INTO `patient_requests_log` VALUES ('25', '82', '2017-08-16 05:13:30', null, 'qw', 'rw');
INSERT INTO `patient_requests_log` VALUES ('26', '82', '2017-08-16 05:16:39', null, 'qa', 'qw');
INSERT INTO `patient_requests_log` VALUES ('27', '82', '2017-08-16 05:19:43', null, 'pa', 'qa');
INSERT INTO `patient_requests_log` VALUES ('28', '82', '2017-08-16 05:22:14', null, 'pa', 'pa');
INSERT INTO `patient_requests_log` VALUES ('29', '82', '2017-08-16 05:22:14', null, 'rd', 'pa');
INSERT INTO `patient_requests_log` VALUES ('30', '82', '2017-08-16 05:22:14', null, 'rd', 'rd');

-- ----------------------------
-- Table structure for `patient_uploads`
-- ----------------------------
DROP TABLE IF EXISTS `patient_uploads`;
CREATE TABLE `patient_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `request_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_uploads_request_id_foreign` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patient_uploads
-- ----------------------------
INSERT INTO `patient_uploads` VALUES ('42', '6372058fc6082594c9b33756546f2be8.jpeg', '68', '2017-08-02 13:35:05', '2017-08-02 13:35:05');
INSERT INTO `patient_uploads` VALUES ('43', '2762eb248bf4569de8062d8083d494b9.jpeg', '69', '2017-08-02 13:38:50', '2017-08-02 13:38:50');
INSERT INTO `patient_uploads` VALUES ('44', '5ab808680f306cac13c17d40dc302044.jpeg', '70', '2017-08-02 18:00:35', '2017-08-02 18:00:35');
INSERT INTO `patient_uploads` VALUES ('45', '2762eb248bf4569de8062d8083d494b9.jpeg', '71', '2017-08-02 19:01:04', '2017-08-02 19:01:04');
INSERT INTO `patient_uploads` VALUES ('46', '18786feb45a7c3fb8afb71e527af297b.jpeg', '72', '2017-08-07 15:45:25', '2017-08-07 15:45:25');
INSERT INTO `patient_uploads` VALUES ('47', 'b613f8984cefe410c39d6acbbb7bd48a.jpeg', '73', '2017-08-07 19:41:30', '2017-08-07 19:41:30');
INSERT INTO `patient_uploads` VALUES ('48', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '74', '2017-08-07 20:54:09', '2017-08-07 20:54:09');
INSERT INTO `patient_uploads` VALUES ('49', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '75', '2017-08-07 21:42:47', '2017-08-07 21:42:47');
INSERT INTO `patient_uploads` VALUES ('50', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '76', '2017-08-07 21:43:30', '2017-08-07 21:43:30');
INSERT INTO `patient_uploads` VALUES ('51', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '77', '2017-08-07 21:47:48', '2017-08-07 21:47:48');
INSERT INTO `patient_uploads` VALUES ('52', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '78', '2017-08-08 15:37:35', '2017-08-08 15:37:35');
INSERT INTO `patient_uploads` VALUES ('53', 'f2e000dc1a8f5ed63a68e759443720ba.jpeg', '79', '2017-08-08 16:51:18', '2017-08-08 16:51:18');
INSERT INTO `patient_uploads` VALUES ('54', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '80', '2017-08-08 18:30:48', '2017-08-08 18:30:48');
INSERT INTO `patient_uploads` VALUES ('55', '79aebc94ff6d8b632137499eb90cb95c.jpeg', '81', '2017-08-08 18:44:02', '2017-08-08 18:44:02');
INSERT INTO `patient_uploads` VALUES ('56', '4bdf21d1ef3ea37315bd0f81b2a12f1d.jpeg', '82', '2017-08-16 15:02:08', '2017-08-16 15:02:08');
INSERT INTO `patient_uploads` VALUES ('57', '7d8ac9944ca4e2e8a3e77eb557a836a6.jpeg', '84', '2017-09-05 20:23:47', '2017-09-05 20:23:47');

-- ----------------------------
-- Table structure for `phone_confirmation`
-- ----------------------------
DROP TABLE IF EXISTS `phone_confirmation`;
CREATE TABLE `phone_confirmation` (
  `id` int(11) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_confirmation
-- ----------------------------
INSERT INTO `phone_confirmation` VALUES ('1', 'Not confirmed', null);
INSERT INTO `phone_confirmation` VALUES ('2', 'Pending', null);
INSERT INTO `phone_confirmation` VALUES ('3', 'Confirmed', null);

-- ----------------------------
-- Table structure for `questiontypes`
-- ----------------------------
DROP TABLE IF EXISTS `questiontypes`;
CREATE TABLE `questiontypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of questiontypes
-- ----------------------------

-- ----------------------------
-- Table structure for `radiologyconfirmed`
-- ----------------------------
DROP TABLE IF EXISTS `radiologyconfirmed`;
CREATE TABLE `radiologyconfirmed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned NOT NULL,
  `center_id` int(10) unsigned NOT NULL,
  `radiology_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `radiologyconfirmed_request_id_foreign` (`request_id`),
  KEY `radiologyconfirmed_center_id_foreign` (`center_id`),
  CONSTRAINT `radiologyconfirmed_center_id_foreign` FOREIGN KEY (`center_id`) REFERENCES `centers` (`id`),
  CONSTRAINT `radiologyconfirmed_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `patient_requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of radiologyconfirmed
-- ----------------------------

-- ----------------------------
-- Table structure for `radiology_questions`
-- ----------------------------
DROP TABLE IF EXISTS `radiology_questions`;
CREATE TABLE `radiology_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_for_women` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'boolean',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of radiology_questions
-- ----------------------------
INSERT INTO `radiology_questions` VALUES ('1', 'هل تم اجراء فحوصات سابقة بالمركز؟', 'Have the patient performed any other test at the center before?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('2', 'هل تم اجراء عمليات سابقة؟', 'Have the patient had any surgerys before?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('3', 'هل تم تركيب مسامير او شرائح او اجهزة تعويضية؟', 'have the patient had application of screws , plates or any Prosthetic devices?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('4', 'هل يوجد جهاز لتنظيم ضربات القلب؟', 'Do you have a pacemaker?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('5', 'هل يوجد تركيبات اسنان معدنية؟', 'Had the patient any metal teeth?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('6', 'هل المريض صائم؟', 'Is the patient fasting?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('7', 'هل تم عمل اشعة بالصبغة سابقا؟', 'did the patient undergo  a contrast radiography before?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('8', 'هل يوجد حساسية من اية عقاقير او مواد غذائية؟', 'Is the patient sensitive to any drug or food types?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('9', 'هل المريض يعاني من امراض ضغط الدم؟', 'Is the patient suffering from any of the blood pressure diseases?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('10', 'هل المريض يعاني من مرض السكر؟', 'Is the patient suffering from Diabetes?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('11', 'هل المريض يعاني من امراض بالكلي؟', 'Is the patient suffering from any of the renal diseases?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('12', 'هل المريض يعاني من امراض بالغدة الدرقية؟', 'Is the patient suffering from any of the thyroid gland diseases?', '0', '2017-07-10 17:16:03', '2017-07-10 17:16:03', 'boolean');
INSERT INTO `radiology_questions` VALUES ('13', 'هل المريض ياخذ علاج للغدة الدرقية؟', 'Is the patient taking any of the thyroid gland drugs?', '0', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'boolean');
INSERT INTO `radiology_questions` VALUES ('14', 'هل يتم اخذ علاج كيماوى؟', 'Is the patient taking Chemotherapy?', '0', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'boolean');
INSERT INTO `radiology_questions` VALUES ('15', 'ما هي ادوية السكر المستخدمة؟', 'what is the Diabetic drugs do the patient take?', '0', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'text');
INSERT INTO `radiology_questions` VALUES ('16', 'ما هو ميعاد اخر جلسة كيماوي؟', 'when was the last Chemotheraputic session?', '0', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'text');
INSERT INTO `radiology_questions` VALUES ('17', 'هل المريضة حامل؟', 'Is the patient pregnant?', '1', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'boolean');
INSERT INTO `radiology_questions` VALUES ('18', 'ما هو ميعاد اخر دورة؟', 'When was your last Menstruation?', '1', '2017-07-10 17:16:04', '2017-07-10 17:16:04', 'text');

-- ----------------------------
-- Table structure for `radiology_types`
-- ----------------------------
DROP TABLE IF EXISTS `radiology_types`;
CREATE TABLE `radiology_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `en_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type_group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `definition` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `preparation` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `radiology_types_type_group_id_foreign` (`type_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of radiology_types
-- ----------------------------
INSERT INTO `radiology_types` VALUES ('1', 'المخ', 'Brain', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('2', 'شرايين المخ', 'MRA', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('3', 'أوردة المخ بالصبغه', 'MRV', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('4', 'المخ و شرايين المخ', 'Brain & MRA', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('5', 'المخ و أوردة المخ بالصبغه', 'Brain & MRV', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('6', 'شرايين و أوردة المخ بالصبغه', 'MRA & MRV', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('7', 'المخ وشرايين المخ واوردة المخ بالصبغه', 'Brain & MRA & V', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('8', 'العمود الفقرى بالكامل', 'Whole Spine ', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('9', 'الغدة النخامية ( بالصبغة )', 'Sella Turcica', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('10', 'العين ( بالصبغة )', 'Orbits', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('11', 'الأذن ( بالصبغة )', 'Petrous Bone ( CPA )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('12', 'مفصلى الفك', 'Temporomandibular Joints ( TMJ )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('13', 'الجيوب الأنفية ( بالصبغة )', 'Paranasal Sinuses', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('14', 'العنق و الحنجرة ( بالصبغة )', 'Neck & Larynx', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('15', 'البلعوم الأنفى بالصبغه', 'Naso Pharynx', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('16', 'اللسان', 'Tongue', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('17', 'الفقرات العنقية', 'Cervical Spine ( CS )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('18', 'الفقرات الظهرية', 'Dorsal Spine ( DS )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('19', 'الفقرات القطنية الظهرية', 'Dorsolumbar', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('20', 'الفقرات القطنية', 'Lumbar Spine ( L.S.S )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('21', 'الكتف', 'Shoulder', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('22', 'مفصلى الحوض', 'Both Hips', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('23', 'الكاحل', 'Ankle', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('24', 'السمانة', 'Calf', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('25', 'القدم', 'Foot', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('26', 'أصابع اليد', 'Fingers', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('27', 'أصابع القدم', 'Toes', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('28', 'الساق', 'Leg', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('29', 'عظمة الفخذ', 'Femur', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('30', 'الفخذ', 'Thigh', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('31', 'اليد', 'Hand', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('32', 'الرسغ', 'Wrist', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('33', 'الكوع', 'Elbow', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('34', 'الساعد', 'Arm Or Forearm', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('35', 'العضد', 'Humerus', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('36', 'الحوض ( عظام )', 'Pelvis ( Bone ) - Both Hips', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('37', 'القلب ', 'Heart', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('38', 'الاطراف السفلية', 'Angio Lower Limbs', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('39', 'شرايين الرقبة', 'Angio Carotid', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('40', 'شرايين الكلى', 'Angio Renal', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('41', 'الشريان الرئوى ', 'Angio Palmonary', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('42', 'الطيف المخى', 'Spectroscopy', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('43', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصبيه', 'Tractography', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('44', 'القنوات المرارية', 'MRCP', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('45', 'المسالك البولية', 'MRU', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('46', 'البنكرياس', 'Pancreas', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('47', 'الكلى', 'Kidneys', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('48', 'الكبد', 'Triphasic Liver', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('49', 'الامعاء الدقيقة', 'Entrography', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('50', 'الثدى', 'Mammography', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('51', 'تدفق سائل النخاع الشوكي', 'Brain CSF Flow', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('52', 'نضح الدم بالمخ', 'Perfusion', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('53', 'نضح الدم بالمخ و المسح الطيفي', 'Perfusion & Spectroscopy', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('54', 'المسح الطيفي و المخ', 'Brain & Spectroscopy', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('55', 'نضح الدم ', 'Perfusion &Tractography', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('56', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصيبه و المخ', 'Brain &Tractography', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('57', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصيبه و المسح الطيفي', 'Tractography & Spectroscopy', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('58', 'فحص الحوض الديناميكي بالصبغه', 'Dynamic Pelvis', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('59', 'حزمه فحص السكته الدماغية', 'Stroke Package', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('60', 'فحص الضفيرة العضدية', 'Brachil Plexus', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('61', 'فحص تقاطع العنق مع الجمجمة', 'Cranio c-s', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('62', 'البروستاتا', 'Prostate', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('63', 'فحص المستقيم', 'Rectum', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('64', 'وعاء الخصيتين', 'Scrotum', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('65', 'فحص الكليتين', 'Renogram', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('66', 'جدار الصدر', 'Chest Wall', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('67', 'الحوض', 'Pelvis', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('68', 'ناصور', 'Fistula', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('69', 'العظم الكتفي', 'Scapula', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('70', 'فحص مفصل العجزي الحرقفي', 'Sacroilic', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('71', 'عظم العضد', 'Humerus', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('72', 'عظمة العجز', 'Sacrum', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('73', 'فحص مفصل الكتف بالصبغة ', 'Shoulder Arthrogram', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('74', 'فحص مفصل الرسغ بالصبغة', 'Wrist Arthrogram', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('75', 'التصوير العصبي الكمي', 'neuro Quant', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('76', 'سيلان سائل النخاع الشوكي من الانف', 'CSF Rhinorrhea', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('77', 'زرع قوقعة بالاذن', 'Cochlear Implant', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('78', ' فخص الورم الكوليسترولي بالاذن', 'Cholesteatoma', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('79', 'صبغه الجادولينيام', 'Contrast Medium ( Gadolinum )', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('80', 'مهدىء عام رنين مغناطيسى', '', '1', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('81', 'المخ', 'Brain', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('82', 'شرايين المخ', 'MRA', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('83', 'أوردة المخ', 'MRV ', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('84', 'المخ و شرايين المخ', 'Brain & MRA ', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('85', 'المخ و أوردة المخ بالصبغه', 'Brain&MRV ', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('86', 'شرايين و أوردة المخ بالصبغه', 'MRA&MRV', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('87', 'المخ وشرايين المخ واوردة المخ', 'Brain & MRA & V', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('88', 'العمود الفقرى بالكامل', 'Whole Spine', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('89', 'الغدة النخامية ', 'Sella trucica', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('90', 'العين ', 'Orbits', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('91', 'الفقرات العنقية', 'Cervical Spine', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('92', 'الفقرات القطنية', 'Lumbar Spine', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('93', 'مفصلى الحوض', 'Both Hips', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('94', 'الركبة', 'Knee ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('95', 'الكتف', 'Shoulder ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('96', 'الكاحل', 'Ankle ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('97', 'الساق او السمانة', 'Leg Or Calf ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('98', 'القدم او اصبع القدم', 'foot Or Toes ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('99', 'الفخذ ', 'Thigh Or Femur ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('100', 'الكوع', 'Elbow ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('101', 'الساعد', 'Forarm ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('102', 'الرسغ', 'Wrist ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('103', 'اليد او اصبع اليد', 'Hand Or Fingers ( One Side )', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('104', 'استكمال مرحلة واحدة', 'فحص اضافي T2 ', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('105', 'استخدام صبغة فى الفحص', 'contrast mri', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('106', 'مهدىء عام رنين مغناطيسى ', ' ', '2', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('107', 'المخ بدون صبغة ', 'Brain non contrast', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('108', 'المخ بالصبغة ', 'Brain with contrast', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('109', 'الغدة النخامية', 'Sella Turcica', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('110', 'العين', 'Orbits', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('111', 'البلعوم الأنفى', 'Naso - Pharynx', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('112', 'ثنائية المراحل على الرقبة', '2 Phase Neck', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('113', 'الجيوب الأنفية بالكامل', 'Paranasal Sinuses ( Axial and Coronal )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('114', 'الجيوب الأنفية بالكامل وبالصبغة', 'Paranasal Sinuses ( Axial and Coronal with contrast)', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('115', 'الجيوب الأنفية -  مقاطع', 'Paranasal Sinuses ( Coronal Only )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('116', 'العظمة الصخرية مقاطع أفقية ورأسية ', 'Petrous Bone ( Axial and Coronal )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('117', 'مفصلى الفك', 'Temporomandibular Joints ( TMJ )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('118', 'الفقرات القطنية والعجزية بالكامل', 'Lumbosacral Spine ( L1 -S1 )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('119', 'فقرات عنقية بدون صبغة', 'Cervical Spine ( Non Contrast )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('120', 'فقرات ظهرية - 6 فقرات فقط بدون صبغة', 'Dorsal Spine (6 only )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('121', 'الصدر', 'Chest', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('122', 'الصدر (بدون صبغة)', 'Chest ( Non Contrast )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('123', 'مقطعية على البطن فائقة الدقة( بدون صبغة)', 'Non contrast Abdomen thin cuts', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('124', 'البطن والحوض', 'Abdomen & Pelvis', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('125', 'البطن و الحوض ثلاثية المراحل (الكبد)', 'Abdomen& triphasic', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('126', 'ثلاثية المراحل على البطن(البنكرياس)', 'Abdomen& triphasic(pancreas)', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('127', ' ثلاثية المراحل على الكبد مع قياس حجم الكبد', 'Triphasic liver with volume', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('128', ' ثلاثية المراحل على الكبد مع الامعاء الدقيقة و الغليظة', 'Triphasic liver with entrocolonography', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('129', 'البطن و الحوض و النزيف الداخلى', 'GI Bleeding', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('130', 'البطن والحوض و الناصور ', 'CT Sinogram', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('131', 'ثنائية المراحل على البطن(الغدة فوق الكلوية)', '2 Phase Abdomen (Adrenal)', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('132', 'فحص  للمسالك البولية بالصبغة', 'CTA Urography Haematurie', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('133', 'فحص على القولون', 'CT Colonography', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('134', 'فحص على الامعاء الدقيقة', 'CT Enterography', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('135', 'متعددة المقاطع على الكلى ', 'Multi Phase Kidney', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('136', 'قياس طول الاطراف ', 'Scanogram', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('137', 'مفصلى الحوض', 'Both Hips', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('138', 'ركبة', 'Knee ( One Side )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('139', 'مفصل الكاحل', 'Ankle', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('140', 'الكتف', 'Shoulder ( One Side )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('141', 'عظام الأطراف', 'Limbs', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('142', 'فحص ثلاثى الابعاد على الوجه(بدون صبغة)', '3D face( Non Contrast )', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('143', 'فحص انجيو على شرايين الوجهة(بالصبغة)', 'CTA Angio Face(with contrast)', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('144', 'فحص انجيو على المخ', 'CTA Angio Brain', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('145', 'فحص انجيو على شرايين الرقبة', 'CTA Angio carotid', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('146', 'فحص انجيو على المخ و شرايين الرقبة', 'CTA Angio Brain and carotid', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('147', 'فحص انجيو على الاورطى البطنى و الصدرى', 'CTA Angio Aorta Chest & Abdomen', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('148', 'فحص انجيو على الاورطى و الاطراف السفلية', 'CTA Angio Aorta Chest & Abdomen & Both Lower Limbs', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('149', 'فحص انجيو على الاورطى و طرف علوى فقط ', 'CTA Angio Aorta Chest & Abdomen & one upper Limb', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('150', 'فحص انجيو على اوردة الاطراف السفلية', 'CTA Angio Venogram Both Lower Limbs', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('151', 'فحص انجيو على شرايين القلب', 'CTA angio Coronary', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('152', 'الشعب الهوائية بدون استخدام منظار', 'CTA Virtual Bronchoscopy', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('153', 'الشريان الرئوى ', 'CTA Pulmonary', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('154', 'فحص انجيو على شرايين الكلى', 'CTA Renal', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('155', 'فحص محدود + عينة', 'Limited CT Study + biopsy', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('156', 'عينة محدودة عن طريق الموجات الصوتية', 'Biopsy Under US', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('157', 'عينة من الغدة الدرقية', 'Thyroid Biopsy Under US', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('158', 'عينة محدودة من العظم', 'Bone Biopsy', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('159', ' ', 'Biopsy with big tale', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('160', 'عينة من الصدر عن طريق الأنبوب', 'Chest Tube Biopsy', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('161', 'تفريغ تجمع ', 'Drinage', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('162', 'الوزن ( 100 كجم واقل من 110 كجم )', 'Over wight from 100to 110', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('163', 'الوزن ( 110 كجم واقل من 120 كجم )', 'Over wight from 110to 120', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('164', 'الوزن ( 120 كجم واقل من 130 كجم )', 'Over wight from 120to 130', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('165', 'الوزن ( 130 كجم واقل من 140 كجم )', 'Over wight from 130to 140', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('166', 'الوزن ( 140 كجم واقل من 150 كجم )', 'Over wight from 140to 150', '3', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('167', 'عظام الجمجمة وضعين', 'Skull ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('168', 'النتوء الحلمى الأيمن والأيسر', 'Mastoids ( Both Sides )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('169', 'الجيوب الأنفية وضعين', 'Paranasal Sinuses ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('170', 'مفصلى الفك جانبين', 'T.M.J. Bilateral', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('171', 'البلعوم الأنفى', 'Nasopharynx ( 1 View )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('172', 'فقرات عنقية أربع أوضاع', 'Cervical Spine ( 4 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('173', 'فقرات عنقية  وضعين', 'Cervical Spine ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('174', 'فقرات قطنية وضعين', 'Lumbar Spine ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('175', 'فقرات قطنية  أربع أوضاع', 'Lumbar Spine ( 4 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('176', 'وضع اضافى على فقرة قطنية او ظهرية', 'Lumbar Spine Extra View', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('177', 'فقرات ظهرية', 'Dorsal Spine', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('178', 'مفصل الكتف', 'Shoulder', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('179', 'مفصل الكوع - وضعين', 'Elbow Joint ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('180', 'مفصل الرسغ - وضعين', 'Wrist Joint ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('181', 'العضد أو الساعد - وضعين', 'Forearm or Arm ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('182', 'اليد - وضعين', 'Hand ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('183', 'وضع اضافى ', 'Additional  View', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('184', 'الحوض - وضع واحد', 'Pelvis ( 1 View )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('185', 'مفصل الفخذ - وضعين', 'Hip Joint ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('186', 'عظام الفخذ - وضعين', 'Femur ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('187', 'مفصل الركبة - وضعين', 'Knee Joint ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('188', 'عظام الساق - وضعين', 'Leg ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('189', 'مفصل الكاحل - وضعين', 'Ankle Joint ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('190', 'عظام القدم - وضعين', 'Foot ( 2 Views )', '4', '2017-07-06 17:53:22', '2017-07-06 17:53:22', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('191', 'الكعبين', 'Both Heels', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('192', 'وضع اضافى', 'Additional View', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('193', 'الصدر والقلب وضع واحد', 'Chest & Heart ( 1 View )', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('194', 'الصدر والقلب وضعين', 'Chest & Heart ( 2 Views )', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('195', 'بلعوم أو مرئ بالباريوم', 'Hypopharynx, Esophagus ( Swallow )', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('196', 'أسفل المرئ والمعدة والاثنى عشر', 'Barium meal', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('197', 'القولون بالباريوم', 'Barium Enema', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('198', 'مسالك بولية عادية', 'U.T.', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('199', 'مسالك بولية بالصبغة', 'Intravenous Urography ( IVU )', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('200', 'قناة مجرى البول -  بالصبغة', 'Ascending Uretherography', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('201', 'المثانة وقناة مجرى البول', 'Cysto - Uretherography', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('202', 'أشعة بالصبغة على الناصور', 'Sinogram', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('203', 'اشعة بالصبغة على الرحم ( طبيبةأ&طبيب أشعة )', 'Hystro-salpingography ( HSG ) (Radiology doctor )', '4', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('204', 'البطن', 'Abdomen', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('205', 'الحوض', 'Pelvis', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('206', 'البطن والحوض', 'Abdomen & Pelvis', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('207', 'الغدة الدرقية', 'Thyroid Gland', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('208', 'الثدى', 'Breast', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('209', 'المفاصل (الركبة -الكوع -الرسغ ...)', 'Joints', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('210', 'متابعة تبويض', 'Follicular.Scanning', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('211', 'موجات صوتية عن طريق المهبل', 'Trans Vaginal', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('212', 'موجات صوتية عن طريق الشرج', 'Trans Rictal', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('213', 'عينة عن طريق الموجات الصوتية', 'Biopsy Under US', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('214', 'تفريغ تجمع بالصدر او البطن باستخدام الموجات الصوتية', 'Drainage of thoracic or abdominal collection', '5', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('215', 'دوبلر وريد كبدى', 'Portal Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('216', 'دوبلر شرايين الرقبة', 'Carotid Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('217', 'دوبلر شرايين الكلى', 'Renal Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('218', 'دوبلر خصيتين و كيس الصفن', 'Scrotal Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('219', 'دوبلر حمل', 'Obst Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('220', 'رباعية الابعاد على الحمل ', '4D', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('221', 'دوبلر اوردة طرف  واحد', 'Venous Doppler for one L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('222', 'دوبلر شرايين طرف  واحد', 'Arterial Doppler for one L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('223', 'دوبلر اوردة و شرايين طرف  واحد', 'Arterial and Venous Doppler for one L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('224', 'دوبلر اوردة طرفيين', 'Venous Doppler for both L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('225', 'دوبلر شرايين طرفيين ', 'Arterial Doppler for Both L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('226', 'دوبلر اوردة و شرايين طرفيين ', 'Arterial and Venous Doppler for Both L L', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('227', 'دوبلر العضو الذكرى', 'Penile Doppler', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('228', 'دوبلر على مفصل عظمى', 'Joints & Limbs', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('229', 'دوبلر على العضلات', 'Muscles', '6', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('230', 'كامل الجسم', 'Total Body Dexa', '7', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('231', 'الثديين', 'Both Breasts', '8', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('232', 'ثدى جانب واحد', 'Breast (One Side)', '8', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('233', 'بانوراما الاسنان الرقمى', 'Digital Panorama', '9', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('234', 'سيفالومترى ', 'Cephalometry', '9', '2017-07-06 17:53:23', '2017-07-06 17:53:23', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('235', 'المخ', 'Brain', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('236', 'شرايين المخ', 'MRA', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('237', 'أوردة المخ بالصبغه', 'MRV', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('238', 'المخ و شرايين المخ', 'Brain & MRA', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('239', 'المخ و أوردة المخ بالصبغه', 'Brain & MRV', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('240', 'شرايين و أوردة المخ بالصبغه', 'MRA & MRV', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('241', 'المخ وشرايين المخ واوردة المخ بالصبغه', 'Brain & MRA & V', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('242', 'العمود الفقرى بالكامل', 'Whole Spine ', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('243', 'الغدة النخامية ( بالصبغة )', 'Sella Turcica', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('244', 'العين ( بالصبغة )', 'Orbits', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('245', 'الأذن ( بالصبغة )', 'Petrous Bone ( CPA )', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('246', 'مفصلى الفك', 'Temporomandibular Joints ( TMJ )', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('247', 'الجيوب الأنفية ( بالصبغة )', 'Paranasal Sinuses', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('248', 'العنق و الحنجرة ( بالصبغة )', 'Neck & Larynx', '1', '2017-07-10 17:16:00', '2017-07-10 17:16:00', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('249', 'البلعوم الأنفى بالصبغه', 'Naso Pharynx', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('250', 'اللسان', 'Tongue', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('251', 'الفقرات العنقية', 'Cervical Spine ( CS )', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('252', 'الفقرات الظهرية', 'Dorsal Spine ( DS )', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('253', 'الفقرات القطنية الظهرية', 'Dorsolumbar', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('254', 'الفقرات القطنية', 'Lumbar Spine ( L.S.S )', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('255', 'الكتف', 'Shoulder', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('256', 'مفصلى الحوض', 'Both Hips', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('257', 'الكاحل', 'Ankle', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('258', 'السمانة', 'Calf', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('259', 'القدم', 'Foot', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('260', 'أصابع اليد', 'Fingers', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('261', 'أصابع القدم', 'Toes', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('262', 'الساق', 'Leg', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('263', 'عظمة الفخذ', 'Femur', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('264', 'الفخذ', 'Thigh', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('265', 'اليد', 'Hand', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('266', 'الرسغ', 'Wrist', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('267', 'الكوع', 'Elbow', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('268', 'الساعد', 'Arm Or Forearm', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('269', 'العضد', 'Humerus', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('270', 'الحوض ( عظام )', 'Pelvis ( Bone ) - Both Hips', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('271', 'القلب ', 'Heart', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('272', 'الاطراف السفلية', 'Angio Lower Limbs', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('273', 'شرايين الرقبة', 'Angio Carotid', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('274', 'شرايين الكلى', 'Angio Renal', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('275', 'الشريان الرئوى ', 'Angio Palmonary', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('276', 'الطيف المخى', 'Spectroscopy', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('277', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصبيه', 'Tractography', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('278', 'القنوات المرارية', 'MRCP', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('279', 'المسالك البولية', 'MRU', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('280', 'البنكرياس', 'Pancreas', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('281', 'الكلى', 'Kidneys', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('282', 'الكبد', 'Triphasic Liver', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('283', 'الامعاء الدقيقة', 'Entrography', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('284', 'الثدى', 'Mammography', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('285', 'تدفق سائل النخاع الشوكي', 'Brain CSF Flow', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('286', 'نضح الدم بالمخ', 'Perfusion', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('287', 'نضح الدم بالمخ و المسح الطيفي', 'Perfusion & Spectroscopy', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('288', 'المسح الطيفي و المخ', 'Brain & Spectroscopy', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('289', 'نضح الدم ', 'Perfusion &Tractography', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('290', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصيبه و المخ', 'Brain &Tractography', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('291', 'التصوير بالرنين المغناطيسي الانتشاري للالياف العصيبه و المسح الطيفي', 'Tractography & Spectroscopy', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('292', 'فحص الحوض الديناميكي بالصبغه', 'Dynamic Pelvis', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('293', 'حزمه فحص السكته الدماغية', 'Stroke Package', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('294', 'فحص الضفيرة العضدية', 'Brachil Plexus', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('295', 'فحص تقاطع العنق مع الجمجمة', 'Cranio c-s', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('296', 'البروستاتا', 'Prostate', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('297', 'فحص المستقيم', 'Rectum', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('298', 'وعاء الخصيتين', 'Scrotum', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('299', 'فحص الكليتين', 'Renogram', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('300', 'جدار الصدر', 'Chest Wall', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('301', 'الحوض', 'Pelvis', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('302', 'ناصور', 'Fistula', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('303', 'العظم الكتفي', 'Scapula', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('304', 'فحص مفصل العجزي الحرقفي', 'Sacroilic', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('305', 'عظم العضد', 'Humerus', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('306', 'عظمة العجز', 'Sacrum', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('307', 'فحص مفصل الكتف بالصبغة ', 'Shoulder Arthrogram', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('308', 'فحص مفصل الرسغ بالصبغة', 'Wrist Arthrogram', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('309', 'التصوير العصبي الكمي', 'neuro Quant', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('310', 'سيلان سائل النخاع الشوكي من الانف', 'CSF Rhinorrhea', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('311', 'زرع قوقعة بالاذن', 'Cochlear Implant', '1', '2017-07-10 17:16:01', '2017-07-10 17:16:01', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('312', ' فخص الورم الكوليسترولي بالاذن', 'Cholesteatoma', '1', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('313', 'صبغه الجادولينيام', 'Contrast Medium ( Gadolinum )', '1', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('314', 'مهدىء عام رنين مغناطيسى', '', '1', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('315', 'المخ', 'Brain', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('316', 'شرايين المخ', 'MRA', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('317', 'أوردة المخ', 'MRV ', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('318', 'المخ و شرايين المخ', 'Brain & MRA ', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('319', 'المخ و أوردة المخ بالصبغه', 'Brain&MRV ', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('320', 'شرايين و أوردة المخ بالصبغه', 'MRA&MRV', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('321', 'المخ وشرايين المخ واوردة المخ', 'Brain & MRA & V', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('322', 'العمود الفقرى بالكامل', 'Whole Spine', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('323', 'الغدة النخامية ', 'Sella trucica', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('324', 'العين ', 'Orbits', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('325', 'الفقرات العنقية', 'Cervical Spine', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('326', 'الفقرات القطنية', 'Lumbar Spine', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('327', 'مفصلى الحوض', 'Both Hips', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('328', 'الركبة', 'Knee ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('329', 'الكتف', 'Shoulder ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('330', 'الكاحل', 'Ankle ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('331', 'الساق او السمانة', 'Leg Or Calf ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('332', 'القدم او اصبع القدم', 'foot Or Toes ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('333', 'الفخذ ', 'Thigh Or Femur ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('334', 'الكوع', 'Elbow ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('335', 'الساعد', 'Forarm ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('336', 'الرسغ', 'Wrist ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('337', 'اليد او اصبع اليد', 'Hand Or Fingers ( One Side )', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('338', 'استكمال مرحلة واحدة', 'فحص اضافي T2 ', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('339', 'استخدام صبغة فى الفحص', 'contrast mri', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('340', 'مهدىء عام رنين مغناطيسى ', ' ', '2', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('341', 'المخ بدون صبغة ', 'Brain non contrast', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('342', 'المخ بالصبغة ', 'Brain with contrast', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('343', 'الغدة النخامية', 'Sella Turcica', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('344', 'العين', 'Orbits', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('345', 'البلعوم الأنفى', 'Naso - Pharynx', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('346', 'ثنائية المراحل على الرقبة', '2 Phase Neck', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('347', 'الجيوب الأنفية بالكامل', 'Paranasal Sinuses ( Axial and Coronal )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('348', 'الجيوب الأنفية بالكامل وبالصبغة', 'Paranasal Sinuses ( Axial and Coronal with contrast)', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('349', 'الجيوب الأنفية -  مقاطع', 'Paranasal Sinuses ( Coronal Only )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('350', 'العظمة الصخرية مقاطع أفقية ورأسية ', 'Petrous Bone ( Axial and Coronal )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('351', 'مفصلى الفك', 'Temporomandibular Joints ( TMJ )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('352', 'الفقرات القطنية والعجزية بالكامل', 'Lumbosacral Spine ( L1 -S1 )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('353', 'فقرات عنقية بدون صبغة', 'Cervical Spine ( Non Contrast )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('354', 'فقرات ظهرية - 6 فقرات فقط بدون صبغة', 'Dorsal Spine (6 only )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('355', 'الصدر', 'Chest', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('356', 'الصدر (بدون صبغة)', 'Chest ( Non Contrast )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('357', 'مقطعية على البطن فائقة الدقة( بدون صبغة)', 'Non contrast Abdomen thin cuts', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('358', 'البطن والحوض', 'Abdomen & Pelvis', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('359', 'البطن و الحوض ثلاثية المراحل (الكبد)', 'Abdomen& triphasic', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('360', 'ثلاثية المراحل على البطن(البنكرياس)', 'Abdomen& triphasic(pancreas)', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('361', ' ثلاثية المراحل على الكبد مع قياس حجم الكبد', 'Triphasic liver with volume', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('362', ' ثلاثية المراحل على الكبد مع الامعاء الدقيقة و الغليظة', 'Triphasic liver with entrocolonography', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('363', 'البطن و الحوض و النزيف الداخلى', 'GI Bleeding', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('364', 'البطن والحوض و الناصور ', 'CT Sinogram', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('365', 'ثنائية المراحل على البطن(الغدة فوق الكلوية)', '2 Phase Abdomen (Adrenal)', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('366', 'فحص  للمسالك البولية بالصبغة', 'CTA Urography Haematurie', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('367', 'فحص على القولون', 'CT Colonography', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('368', 'فحص على الامعاء الدقيقة', 'CT Enterography', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('369', 'متعددة المقاطع على الكلى ', 'Multi Phase Kidney', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('370', 'قياس طول الاطراف ', 'Scanogram', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('371', 'مفصلى الحوض', 'Both Hips', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('372', 'ركبة', 'Knee ( One Side )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('373', 'مفصل الكاحل', 'Ankle', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('374', 'الكتف', 'Shoulder ( One Side )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('375', 'عظام الأطراف', 'Limbs', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('376', 'فحص ثلاثى الابعاد على الوجه(بدون صبغة)', '3D face( Non Contrast )', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('377', 'فحص انجيو على شرايين الوجهة(بالصبغة)', 'CTA Angio Face(with contrast)', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('378', 'فحص انجيو على المخ', 'CTA Angio Brain', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('379', 'فحص انجيو على شرايين الرقبة', 'CTA Angio carotid', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('380', 'فحص انجيو على المخ و شرايين الرقبة', 'CTA Angio Brain and carotid', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('381', 'فحص انجيو على الاورطى البطنى و الصدرى', 'CTA Angio Aorta Chest & Abdomen', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('382', 'فحص انجيو على الاورطى و الاطراف السفلية', 'CTA Angio Aorta Chest & Abdomen & Both Lower Limbs', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('383', 'فحص انجيو على الاورطى و طرف علوى فقط ', 'CTA Angio Aorta Chest & Abdomen & one upper Limb', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('384', 'فحص انجيو على اوردة الاطراف السفلية', 'CTA Angio Venogram Both Lower Limbs', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('385', 'فحص انجيو على شرايين القلب', 'CTA angio Coronary', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('386', 'الشعب الهوائية بدون استخدام منظار', 'CTA Virtual Bronchoscopy', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('387', 'الشريان الرئوى ', 'CTA Pulmonary', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('388', 'فحص انجيو على شرايين الكلى', 'CTA Renal', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('389', 'فحص محدود + عينة', 'Limited CT Study + biopsy', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('390', 'عينة محدودة عن طريق الموجات الصوتية', 'Biopsy Under US', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('391', 'عينة من الغدة الدرقية', 'Thyroid Biopsy Under US', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('392', 'عينة محدودة من العظم', 'Bone Biopsy', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('393', ' ', 'Biopsy with big tale', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('394', 'عينة من الصدر عن طريق الأنبوب', 'Chest Tube Biopsy', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('395', 'تفريغ تجمع ', 'Drinage', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('396', 'الوزن ( 100 كجم واقل من 110 كجم )', 'Over wight from 100to 110', '3', '2017-07-10 17:16:02', '2017-07-10 17:16:02', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('397', 'الوزن ( 110 كجم واقل من 120 كجم )', 'Over wight from 110to 120', '3', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('398', 'الوزن ( 120 كجم واقل من 130 كجم )', 'Over wight from 120to 130', '3', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('399', 'الوزن ( 130 كجم واقل من 140 كجم )', 'Over wight from 130to 140', '3', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('400', 'الوزن ( 140 كجم واقل من 150 كجم )', 'Over wight from 140to 150', '3', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('401', 'عظام الجمجمة وضعين', 'Skull ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('402', 'النتوء الحلمى الأيمن والأيسر', 'Mastoids ( Both Sides )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('403', 'الجيوب الأنفية وضعين', 'Paranasal Sinuses ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('404', 'مفصلى الفك جانبين', 'T.M.J. Bilateral', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('405', 'البلعوم الأنفى', 'Nasopharynx ( 1 View )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('406', 'فقرات عنقية أربع أوضاع', 'Cervical Spine ( 4 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('407', 'فقرات عنقية  وضعين', 'Cervical Spine ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('408', 'فقرات قطنية وضعين', 'Lumbar Spine ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('409', 'فقرات قطنية  أربع أوضاع', 'Lumbar Spine ( 4 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('410', 'وضع اضافى على فقرة قطنية او ظهرية', 'Lumbar Spine Extra View', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('411', 'فقرات ظهرية', 'Dorsal Spine', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('412', 'مفصل الكتف', 'Shoulder', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('413', 'مفصل الكوع - وضعين', 'Elbow Joint ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('414', 'مفصل الرسغ - وضعين', 'Wrist Joint ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('415', 'العضد أو الساعد - وضعين', 'Forearm or Arm ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('416', 'اليد - وضعين', 'Hand ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('417', 'وضع اضافى ', 'Additional  View', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('418', 'الحوض - وضع واحد', 'Pelvis ( 1 View )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('419', 'مفصل الفخذ - وضعين', 'Hip Joint ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('420', 'عظام الفخذ - وضعين', 'Femur ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('421', 'مفصل الركبة - وضعين', 'Knee Joint ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('422', 'عظام الساق - وضعين', 'Leg ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('423', 'مفصل الكاحل - وضعين', 'Ankle Joint ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('424', 'عظام القدم - وضعين', 'Foot ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('425', 'الكعبين', 'Both Heels', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('426', 'وضع اضافى', 'Additional View', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('427', 'الصدر والقلب وضع واحد', 'Chest & Heart ( 1 View )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('428', 'الصدر والقلب وضعين', 'Chest & Heart ( 2 Views )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('429', 'بلعوم أو مرئ بالباريوم', 'Hypopharynx, Esophagus ( Swallow )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('430', 'أسفل المرئ والمعدة والاثنى عشر', 'Barium meal', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('431', 'القولون بالباريوم', 'Barium Enema', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('432', 'مسالك بولية عادية', 'U.T.', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('433', 'مسالك بولية بالصبغة', 'Intravenous Urography ( IVU )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('434', 'قناة مجرى البول -  بالصبغة', 'Ascending Uretherography', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('435', 'المثانة وقناة مجرى البول', 'Cysto - Uretherography', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('436', 'أشعة بالصبغة على الناصور', 'Sinogram', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('437', 'اشعة بالصبغة على الرحم ( طبيبةأ&طبيب أشعة )', 'Hystro-salpingography ( HSG ) (Radiology doctor )', '4', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('438', 'البطن', 'Abdomen', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('439', 'الحوض', 'Pelvis', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('440', 'البطن والحوض', 'Abdomen & Pelvis', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('441', 'الغدة الدرقية', 'Thyroid Gland', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('442', 'الثدى', 'Breast', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('443', 'المفاصل (الركبة -الكوع -الرسغ ...)', 'Joints', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('444', 'متابعة تبويض', 'Follicular.Scanning', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('445', 'موجات صوتية عن طريق المهبل', 'Trans Vaginal', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('446', 'موجات صوتية عن طريق الشرج', 'Trans Rictal', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('447', 'عينة عن طريق الموجات الصوتية', 'Biopsy Under US', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('448', 'تفريغ تجمع بالصدر او البطن باستخدام الموجات الصوتية', 'Drainage of thoracic or abdominal collection', '5', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('449', 'دوبلر وريد كبدى', 'Portal Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('450', 'دوبلر شرايين الرقبة', 'Carotid Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('451', 'دوبلر شرايين الكلى', 'Renal Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('452', 'دوبلر خصيتين و كيس الصفن', 'Scrotal Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('453', 'دوبلر حمل', 'Obst Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('454', 'رباعية الابعاد على الحمل ', '4D', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('455', 'دوبلر اوردة طرف  واحد', 'Venous Doppler for one L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('456', 'دوبلر شرايين طرف  واحد', 'Arterial Doppler for one L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('457', 'دوبلر اوردة و شرايين طرف  واحد', 'Arterial and Venous Doppler for one L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('458', 'دوبلر اوردة طرفيين', 'Venous Doppler for both L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('459', 'دوبلر شرايين طرفيين ', 'Arterial Doppler for Both L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('460', 'دوبلر اوردة و شرايين طرفيين ', 'Arterial and Venous Doppler for Both L L', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('461', 'دوبلر العضو الذكرى', 'Penile Doppler', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('462', 'دوبلر على مفصل عظمى', 'Joints & Limbs', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('463', 'دوبلر على العضلات', 'Muscles', '6', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('464', 'كامل الجسم', 'Total Body Dexa', '7', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('465', 'الثديين', 'Both Breasts', '8', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('466', 'ثدى جانب واحد', 'Breast (One Side)', '8', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('467', 'بانوراما الاسنان الرقمى', 'Digital Panorama', '9', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);
INSERT INTO `radiology_types` VALUES ('468', 'سيفالومترى ', 'Cephalometry', '9', '2017-07-10 17:16:03', '2017-07-10 17:16:03', null, null, null, null);

-- ----------------------------
-- Table structure for `radiology_type_categories`
-- ----------------------------
DROP TABLE IF EXISTS `radiology_type_categories`;
CREATE TABLE `radiology_type_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `en_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ar_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of radiology_type_categories
-- ----------------------------
INSERT INTO `radiology_type_categories` VALUES ('1', 'Closed MRI 1.5 Tesla', 'الرنين المغناطيسى المغلق 1.5 تسلا', '2017-07-10 17:16:00', '2017-07-10 17:16:00');
INSERT INTO `radiology_type_categories` VALUES ('2', 'MRI Open', 'فحوصات', '2017-07-10 17:16:02', '2017-07-10 17:16:02');
INSERT INTO `radiology_type_categories` VALUES ('3', 'Multi Slice CT', 'الاشعة المقطعية متعددة المقاطع', '2017-07-10 17:16:02', '2017-07-10 17:16:02');
INSERT INTO `radiology_type_categories` VALUES ('4', 'X-Ray', 'اﻷشعة العادية - أشعة أكس', '2017-07-10 17:16:03', '2017-07-10 17:16:03');
INSERT INTO `radiology_type_categories` VALUES ('5', 'Ultra Sound', 'الموجات فوق الصوتية', '2017-07-10 17:16:03', '2017-07-10 17:16:03');
INSERT INTO `radiology_type_categories` VALUES ('6', 'Doppler', 'الموجات فوق الصوتية الملونة', '2017-07-10 17:16:03', '2017-07-10 17:16:03');
INSERT INTO `radiology_type_categories` VALUES ('7', 'DEXA', 'قياس كثافة العظام', '2017-07-10 17:16:03', '2017-07-10 17:16:03');
INSERT INTO `radiology_type_categories` VALUES ('8', 'Mamogram + US', 'الماموجرام', '2017-07-10 17:16:03', '2017-07-10 17:16:03');
INSERT INTO `radiology_type_categories` VALUES ('9', 'Panorama', 'البانوراما', '2017-07-10 17:16:03', '2017-07-10 17:16:03');

-- ----------------------------
-- Table structure for `requestdetail`
-- ----------------------------
DROP TABLE IF EXISTS `requestdetail`;
CREATE TABLE `requestdetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_header_id` int(10) unsigned NOT NULL,
  `vendor_item_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `requestdetail_request_header_id_foreign` (`request_header_id`),
  KEY `requestdetail_vendor_item_id_foreign` (`vendor_item_id`),
  CONSTRAINT `requestdetail_request_header_id_foreign` FOREIGN KEY (`request_header_id`) REFERENCES `requestheader` (`id`),
  CONSTRAINT `requestdetail_vendor_item_id_foreign` FOREIGN KEY (`vendor_item_id`) REFERENCES `vendoritems` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `requestheader`
-- ----------------------------
DROP TABLE IF EXISTS `requestheader`;
CREATE TABLE `requestheader` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `requester_id` int(10) unsigned NOT NULL,
  `requester_type` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `is_home` tinyint(1) NOT NULL,
  `is_stable` tinyint(1) NOT NULL,
  `is_emergency` tinyint(1) NOT NULL,
  `weight` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `request_status_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `requestheader_requester_id_foreign` (`requester_id`),
  KEY `requestheader_request_status_id_foreign` (`request_status_id`),
  CONSTRAINT `requestheader_request_status_id_foreign` FOREIGN KEY (`request_status_id`) REFERENCES `requeststatus` (`id`),
  CONSTRAINT `requestheader_requester_id_foreign` FOREIGN KEY (`requester_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestheader
-- ----------------------------

-- ----------------------------
-- Table structure for `requestlog`
-- ----------------------------
DROP TABLE IF EXISTS `requestlog`;
CREATE TABLE `requestlog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_header_id` int(10) unsigned NOT NULL,
  `request_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requestlog_request_header_id_foreign` (`request_header_id`),
  KEY `requestlog_request_status_id_foreign` (`request_status_id`),
  CONSTRAINT `requestlog_request_header_id_foreign` FOREIGN KEY (`request_header_id`) REFERENCES `requestheader` (`id`),
  CONSTRAINT `requestlog_request_status_id_foreign` FOREIGN KEY (`request_status_id`) REFERENCES `requeststatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestlog
-- ----------------------------

-- ----------------------------
-- Table structure for `requestquestion`
-- ----------------------------
DROP TABLE IF EXISTS `requestquestion`;
CREATE TABLE `requestquestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request_id` int(10) unsigned NOT NULL,
  `vendor_service_question_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requestquestion_request_id_foreign` (`request_id`),
  KEY `requestquestion_vendor_service_question_id_foreign` (`vendor_service_question_id`),
  CONSTRAINT `requestquestion_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `requestheader` (`id`),
  CONSTRAINT `requestquestion_vendor_service_question_id_foreign` FOREIGN KEY (`vendor_service_question_id`) REFERENCES `vendorservicequestion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestquestion
-- ----------------------------

-- ----------------------------
-- Table structure for `requests`
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES ('3', '3', '160', '1', '3', '255', '0', '', '2017-09-06 15:42:40');
INSERT INTO `requests` VALUES ('4', '3', '160', '5', '3', '255', '1', '', '2017-09-06 15:42:40');
INSERT INTO `requests` VALUES ('5', '3', '16', '1', '3', '207', '0', '', '0000-00-00 00:00:00');
INSERT INTO `requests` VALUES ('6', '3', '16', '1', '3', '207', '0', '', '2017-09-06 15:50:21');
INSERT INTO `requests` VALUES ('7', '2', '3', '0', '2', '0', '0', '', '2017-09-09 20:07:13');
INSERT INTO `requests` VALUES ('8', '2', '3', '0', '2', '0', '0', '', '2017-09-09 20:07:19');
INSERT INTO `requests` VALUES ('9', '1', '3', '0', '1', '0', '0', '', '2017-09-09 20:07:25');
INSERT INTO `requests` VALUES ('10', '4', '160', '1', '4', '255', '0', '', '2017-09-10 16:15:10');

-- ----------------------------
-- Table structure for `requeststatus`
-- ----------------------------
DROP TABLE IF EXISTS `requeststatus`;
CREATE TABLE `requeststatus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requeststatus
-- ----------------------------

-- ----------------------------
-- Table structure for `requestuploads`
-- ----------------------------
DROP TABLE IF EXISTS `requestuploads`;
CREATE TABLE `requestuploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `upload` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request_header_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `requestuploads_request_header_id_foreign` (`request_header_id`),
  CONSTRAINT `requestuploads_request_header_id_foreign` FOREIGN KEY (`request_header_id`) REFERENCES `requestheader` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestuploads
-- ----------------------------

-- ----------------------------
-- Table structure for `requestvendorbranches`
-- ----------------------------
DROP TABLE IF EXISTS `requestvendorbranches`;
CREATE TABLE `requestvendorbranches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_details_id` int(10) unsigned NOT NULL,
  `vendor_item_branches_id` int(10) unsigned NOT NULL,
  `arrival_date_time` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requestvendorbranches_request_details_id_foreign` (`request_details_id`),
  KEY `requestvendorbranches_vendor_item_branches_id_foreign` (`vendor_item_branches_id`),
  CONSTRAINT `requestvendorbranches_request_details_id_foreign` FOREIGN KEY (`request_details_id`) REFERENCES `requestdetail` (`id`),
  CONSTRAINT `requestvendorbranches_vendor_item_branches_id_foreign` FOREIGN KEY (`vendor_item_branches_id`) REFERENCES `vendoritembranches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of requestvendorbranches
-- ----------------------------

-- ----------------------------
-- Table structure for `request_answers`
-- ----------------------------
DROP TABLE IF EXISTS `request_answers`;
CREATE TABLE `request_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_answers_request_id_foreign` (`request_id`),
  KEY `request_answers_question_id_foreign` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=382 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request_answers
-- ----------------------------
INSERT INTO `request_answers` VALUES ('1', '1', '1', '0', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('2', '1', '2', '0', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('3', '1', '3', '1', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('4', '1', '4', '1', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('5', '1', '5', '1', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('6', '1', '6', '1', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('7', '1', '7', '1', '2017-07-18 16:42:42', '2017-07-18 16:42:42');
INSERT INTO `request_answers` VALUES ('8', '1', '8', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('9', '1', '9', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('10', '1', '10', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('11', '27', '11', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('12', '27', '12', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('13', '27', '13', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('14', '27', '14', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('15', '27', '15', 'test', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('16', '27', '17', '1', '2017-07-18 16:42:43', '2017-07-18 16:42:43');
INSERT INTO `request_answers` VALUES ('17', '46', '1', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('18', '46', '2', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('19', '46', '3', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('20', '46', '4', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('21', '46', '5', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('22', '46', '6', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('23', '46', '7', '0', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('24', '46', '8', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('25', '46', '9', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('26', '46', '10', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('27', '46', '11', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('28', '46', '12', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('29', '46', '13', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('30', '46', '14', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('31', '46', '17', '1', '2017-07-25 15:28:42', '2017-07-25 15:28:42');
INSERT INTO `request_answers` VALUES ('32', '47', '1', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('33', '47', '2', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('34', '47', '3', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('35', '47', '4', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('36', '47', '5', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('37', '47', '6', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('38', '47', '7', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('39', '47', '8', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('40', '47', '9', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('41', '47', '10', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('42', '47', '11', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('43', '47', '12', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('44', '47', '13', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('45', '47', '14', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('46', '47', '17', '1', '2017-07-25 17:23:55', '2017-07-25 17:23:55');
INSERT INTO `request_answers` VALUES ('47', '64', '1', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('48', '64', '2', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('49', '64', '3', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('50', '64', '4', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('51', '64', '5', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('52', '64', '6', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('53', '64', '7', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('54', '64', '8', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('55', '64', '9', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('56', '64', '10', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('57', '64', '11', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('58', '64', '12', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('59', '64', '13', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('60', '64', '14', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('61', '64', '15', '\n', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('62', '64', '17', '1', '2017-07-30 23:53:28', '2017-07-30 23:53:28');
INSERT INTO `request_answers` VALUES ('63', '64', '1', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('64', '64', '2', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('65', '64', '3', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('66', '64', '4', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('67', '64', '5', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('68', '64', '6', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('69', '64', '7', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('70', '64', '8', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('71', '64', '9', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('72', '64', '10', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('73', '64', '11', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('74', '64', '12', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('75', '64', '13', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('76', '64', '14', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('77', '64', '17', '1', '2017-08-01 16:51:46', '2017-08-01 16:51:46');
INSERT INTO `request_answers` VALUES ('78', '64', '1', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('79', '64', '2', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('80', '64', '3', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('81', '64', '4', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('82', '64', '5', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('83', '64', '6', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('84', '64', '7', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('85', '64', '8', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('86', '64', '9', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('87', '64', '10', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('88', '64', '11', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('89', '64', '12', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('90', '64', '13', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('91', '64', '14', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('92', '64', '17', '1', '2017-08-01 16:52:13', '2017-08-01 16:52:13');
INSERT INTO `request_answers` VALUES ('93', '64', '1', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('94', '64', '2', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('95', '64', '3', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('96', '64', '4', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('97', '64', '5', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('98', '64', '6', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('99', '64', '7', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('100', '64', '8', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('101', '64', '9', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('102', '64', '10', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('103', '64', '11', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('104', '64', '12', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('105', '64', '13', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('106', '64', '14', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('107', '64', '17', '1', '2017-08-01 16:52:26', '2017-08-01 16:52:26');
INSERT INTO `request_answers` VALUES ('108', '69', '1', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('109', '69', '2', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('110', '69', '3', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('111', '69', '4', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('112', '69', '5', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('113', '69', '6', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('114', '69', '7', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('115', '69', '8', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('116', '69', '9', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('117', '69', '10', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('118', '69', '11', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('119', '69', '12', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('120', '69', '13', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('121', '69', '14', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('122', '69', '17', '1', '2017-08-02 14:18:15', '2017-08-02 14:18:15');
INSERT INTO `request_answers` VALUES ('123', '69', '1', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('124', '69', '2', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('125', '69', '3', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('126', '69', '4', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('127', '69', '5', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('128', '69', '6', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('129', '69', '7', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('130', '69', '8', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('131', '69', '9', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('132', '69', '10', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('133', '69', '11', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('134', '69', '12', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('135', '69', '13', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('136', '69', '14', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('137', '69', '17', '1', '2017-08-02 14:52:45', '2017-08-02 14:52:45');
INSERT INTO `request_answers` VALUES ('138', '70', '1', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('139', '70', '2', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('140', '70', '3', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('141', '70', '4', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('142', '70', '5', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('143', '70', '6', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('144', '70', '7', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('145', '70', '8', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('146', '70', '9', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('147', '70', '10', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('148', '70', '11', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('149', '70', '12', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('150', '70', '13', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('151', '70', '14', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('152', '70', '17', '1', '2017-08-02 18:08:10', '2017-08-02 18:08:10');
INSERT INTO `request_answers` VALUES ('153', '72', '1', '1', '2017-08-07 15:48:01', '2017-08-07 15:48:01');
INSERT INTO `request_answers` VALUES ('154', '72', '2', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('155', '72', '3', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('156', '72', '4', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('157', '72', '5', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('158', '72', '6', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('159', '72', '7', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('160', '72', '8', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('161', '72', '9', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('162', '72', '10', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('163', '72', '11', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('164', '72', '12', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('165', '72', '13', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('166', '72', '14', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('167', '72', '17', '1', '2017-08-07 15:48:02', '2017-08-07 15:48:02');
INSERT INTO `request_answers` VALUES ('168', '72', '1', '1', '2017-08-07 15:48:14', '2017-08-07 15:48:14');
INSERT INTO `request_answers` VALUES ('169', '72', '2', '1', '2017-08-07 15:48:14', '2017-08-07 15:48:14');
INSERT INTO `request_answers` VALUES ('170', '72', '3', '1', '2017-08-07 15:48:14', '2017-08-07 15:48:14');
INSERT INTO `request_answers` VALUES ('171', '72', '4', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('172', '72', '5', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('173', '72', '6', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('174', '72', '7', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('175', '72', '8', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('176', '72', '9', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('177', '72', '10', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('178', '72', '11', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('179', '72', '12', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('180', '72', '13', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('181', '72', '14', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('182', '72', '17', '1', '2017-08-07 15:48:15', '2017-08-07 15:48:15');
INSERT INTO `request_answers` VALUES ('183', '73', '1', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('184', '73', '2', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('185', '73', '3', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('186', '73', '4', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('187', '73', '5', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('188', '73', '6', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('189', '73', '7', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('190', '73', '8', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('191', '73', '9', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('192', '73', '10', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('193', '73', '11', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('194', '73', '12', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('195', '73', '13', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('196', '73', '14', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('197', '73', '17', '1', '2017-08-07 19:43:15', '2017-08-07 19:43:15');
INSERT INTO `request_answers` VALUES ('198', '73', '1', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('199', '73', '2', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('200', '73', '3', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('201', '73', '4', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('202', '73', '5', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('203', '73', '6', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('204', '73', '7', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('205', '73', '8', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('206', '73', '9', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('207', '73', '10', '1', '2017-08-07 19:44:14', '2017-08-07 19:44:14');
INSERT INTO `request_answers` VALUES ('208', '73', '11', '1', '2017-08-07 19:44:15', '2017-08-07 19:44:15');
INSERT INTO `request_answers` VALUES ('209', '73', '12', '1', '2017-08-07 19:44:15', '2017-08-07 19:44:15');
INSERT INTO `request_answers` VALUES ('210', '73', '13', '1', '2017-08-07 19:44:15', '2017-08-07 19:44:15');
INSERT INTO `request_answers` VALUES ('211', '73', '14', '1', '2017-08-07 19:44:15', '2017-08-07 19:44:15');
INSERT INTO `request_answers` VALUES ('212', '73', '17', '1', '2017-08-07 19:44:15', '2017-08-07 19:44:15');
INSERT INTO `request_answers` VALUES ('213', '73', '1', '1', '2017-08-07 19:44:40', '2017-08-07 19:44:40');
INSERT INTO `request_answers` VALUES ('214', '73', '2', '1', '2017-08-07 19:44:40', '2017-08-07 19:44:40');
INSERT INTO `request_answers` VALUES ('215', '73', '3', '1', '2017-08-07 19:44:40', '2017-08-07 19:44:40');
INSERT INTO `request_answers` VALUES ('216', '73', '4', '1', '2017-08-07 19:44:40', '2017-08-07 19:44:40');
INSERT INTO `request_answers` VALUES ('217', '73', '5', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('218', '73', '6', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('219', '73', '7', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('220', '73', '8', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('221', '73', '9', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('222', '73', '10', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('223', '73', '11', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('224', '73', '12', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('225', '73', '13', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('226', '73', '14', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('227', '73', '17', '1', '2017-08-07 19:44:41', '2017-08-07 19:44:41');
INSERT INTO `request_answers` VALUES ('228', '73', '1', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('229', '73', '2', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('230', '73', '3', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('231', '73', '4', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('232', '73', '5', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('233', '73', '6', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('234', '73', '7', '1', '2017-08-07 19:44:58', '2017-08-07 19:44:58');
INSERT INTO `request_answers` VALUES ('235', '73', '8', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('236', '73', '9', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('237', '73', '10', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('238', '73', '11', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('239', '73', '12', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('240', '73', '13', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('241', '73', '14', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('242', '73', '17', '1', '2017-08-07 19:44:59', '2017-08-07 19:44:59');
INSERT INTO `request_answers` VALUES ('243', '73', '1', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('244', '73', '2', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('245', '73', '3', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('246', '73', '4', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('247', '73', '5', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('248', '73', '6', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('249', '73', '7', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('250', '73', '8', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('251', '73', '9', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('252', '73', '10', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('253', '73', '11', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('254', '73', '12', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('255', '73', '13', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('256', '73', '14', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('257', '73', '15', 'لتباتااَت', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('258', '73', '17', '1', '2017-08-07 19:45:11', '2017-08-07 19:45:11');
INSERT INTO `request_answers` VALUES ('259', '73', '1', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('260', '73', '2', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('261', '73', '3', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('262', '73', '4', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('263', '73', '5', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('264', '73', '6', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('265', '73', '7', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('266', '73', '8', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('267', '73', '9', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('268', '73', '10', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('269', '73', '11', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('270', '73', '12', '1', '2017-08-07 19:46:18', '2017-08-07 19:46:18');
INSERT INTO `request_answers` VALUES ('271', '73', '13', '1', '2017-08-07 19:46:19', '2017-08-07 19:46:19');
INSERT INTO `request_answers` VALUES ('272', '73', '14', '1', '2017-08-07 19:46:19', '2017-08-07 19:46:19');
INSERT INTO `request_answers` VALUES ('273', '73', '16', 'ناانازوَزز', '2017-08-07 19:46:19', '2017-08-07 19:46:19');
INSERT INTO `request_answers` VALUES ('274', '73', '17', '1', '2017-08-07 19:46:19', '2017-08-07 19:46:19');
INSERT INTO `request_answers` VALUES ('275', '73', '18', 'اا. زروازون', '2017-08-07 19:46:19', '2017-08-07 19:46:19');
INSERT INTO `request_answers` VALUES ('276', '74', '1', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('277', '74', '2', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('278', '74', '3', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('279', '74', '4', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('280', '74', '5', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('281', '74', '6', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('282', '74', '7', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('283', '74', '8', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('284', '74', '9', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('285', '74', '10', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('286', '74', '11', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('287', '74', '12', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('288', '74', '13', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('289', '74', '14', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('290', '74', '17', '1', '2017-08-07 21:26:03', '2017-08-07 21:26:03');
INSERT INTO `request_answers` VALUES ('291', '74', '1', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('292', '74', '2', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('293', '74', '3', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('294', '74', '4', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('295', '74', '5', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('296', '74', '6', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('297', '74', '7', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('298', '74', '8', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('299', '74', '9', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('300', '74', '10', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('301', '74', '11', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('302', '74', '12', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('303', '74', '13', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('304', '74', '14', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('305', '74', '17', '1', '2017-08-07 21:28:06', '2017-08-07 21:28:06');
INSERT INTO `request_answers` VALUES ('306', '74', '1', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('307', '74', '2', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('308', '74', '3', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('309', '74', '4', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('310', '74', '5', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('311', '74', '6', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('312', '74', '7', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('313', '74', '8', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('314', '74', '9', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('315', '74', '10', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('316', '74', '11', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('317', '74', '12', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('318', '74', '13', '0', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('319', '74', '14', '1', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('320', '74', '17', '0', '2017-08-07 21:28:24', '2017-08-07 21:28:24');
INSERT INTO `request_answers` VALUES ('321', '78', '1', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('322', '78', '2', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('323', '78', '3', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('324', '78', '4', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('325', '78', '5', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('326', '78', '6', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('327', '78', '7', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('328', '78', '8', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('329', '78', '9', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('330', '78', '10', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('331', '78', '11', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('332', '78', '12', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('333', '78', '13', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('334', '78', '14', '1', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('335', '78', '17', '0', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('336', '78', '18', 'from 2 days', '2017-08-08 16:43:09', '2017-08-08 16:43:09');
INSERT INTO `request_answers` VALUES ('337', '79', '1', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('338', '79', '2', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('339', '79', '3', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('340', '79', '4', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('341', '79', '5', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('342', '79', '6', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('343', '79', '7', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('344', '79', '8', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('345', '79', '9', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('346', '79', '10', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('347', '79', '11', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('348', '79', '12', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('349', '79', '13', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('350', '79', '14', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('351', '79', '17', '1', '2017-08-08 16:53:01', '2017-08-08 16:53:01');
INSERT INTO `request_answers` VALUES ('352', '81', '1', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('353', '81', '2', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('354', '81', '3', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('355', '81', '4', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('356', '81', '5', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('357', '81', '6', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('358', '81', '7', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('359', '81', '8', '1', '2017-08-08 18:46:17', '2017-08-08 18:46:17');
INSERT INTO `request_answers` VALUES ('360', '81', '9', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('361', '81', '10', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('362', '81', '11', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('363', '81', '12', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('364', '81', '13', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('365', '81', '14', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('366', '81', '17', '1', '2017-08-08 18:46:18', '2017-08-08 18:46:18');
INSERT INTO `request_answers` VALUES ('367', '82', '1', '1', '2017-08-16 15:13:45', '2017-08-16 15:13:45');
INSERT INTO `request_answers` VALUES ('368', '82', '2', '1', '2017-08-16 15:13:46', '2017-08-16 15:13:46');
INSERT INTO `request_answers` VALUES ('369', '82', '3', '1', '2017-08-16 15:13:47', '2017-08-16 15:13:47');
INSERT INTO `request_answers` VALUES ('370', '82', '4', '1', '2017-08-16 15:13:47', '2017-08-16 15:13:47');
INSERT INTO `request_answers` VALUES ('371', '82', '5', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('372', '82', '6', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('373', '82', '7', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('374', '82', '8', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('375', '82', '9', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('376', '82', '10', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('377', '82', '11', '1', '2017-08-16 15:13:48', '2017-08-16 15:13:48');
INSERT INTO `request_answers` VALUES ('378', '82', '12', '1', '2017-08-16 15:13:49', '2017-08-16 15:13:49');
INSERT INTO `request_answers` VALUES ('379', '82', '13', '1', '2017-08-16 15:13:49', '2017-08-16 15:13:49');
INSERT INTO `request_answers` VALUES ('380', '82', '14', '1', '2017-08-16 15:13:49', '2017-08-16 15:13:49');
INSERT INTO `request_answers` VALUES ('381', '82', '17', '1', '2017-08-16 15:13:49', '2017-08-16 15:13:49');

-- ----------------------------
-- Table structure for `request_services`
-- ----------------------------
DROP TABLE IF EXISTS `request_services`;
CREATE TABLE `request_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_index` varchar(255) NOT NULL,
  `service_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request_services
-- ----------------------------
INSERT INTO `request_services` VALUES ('1', '16', '0', '0', 'patiet_16_1503321036.png', '1', '300', '0', '2017-08-21 15:10:36');
INSERT INTO `request_services` VALUES ('2', '16', '0', '0', 'patiet_16_1503321222.png', '1', '300', '0', '2017-08-21 15:13:42');
INSERT INTO `request_services` VALUES ('3', '16', '0', '0', 'patiet_16_1503321267.png', '1', '300', '0', '2017-08-21 15:14:26');
INSERT INTO `request_services` VALUES ('4', '128', '0', '0', 'patiet_128_1503328106.png', '0', '310', '0', '2017-08-21 17:08:25');
INSERT INTO `request_services` VALUES ('5', '128', '0', '0', 'patiet_128_1503328305.png', '0', '311', '0', '2017-08-21 17:11:45');
INSERT INTO `request_services` VALUES ('6', '128', '0', '0', 'patiet_128_1503403016.jpeg', '0', '314', '0', '2017-08-22 13:56:55');
INSERT INTO `request_services` VALUES ('7', '128', '0', '0', 'patiet_128_1503910909.jpg', '0', '316', '0', '2017-08-28 11:01:49');

-- ----------------------------
-- Table structure for `request_services_recording`
-- ----------------------------
DROP TABLE IF EXISTS `request_services_recording`;
CREATE TABLE `request_services_recording` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_index` varchar(255) NOT NULL,
  `service_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request_services_recording
-- ----------------------------
INSERT INTO `request_services_recording` VALUES ('1', '16', '0', '0', 'patiet_16_1503323572.mp4', '1', '303', '0', '2017-08-21 15:52:51');
INSERT INTO `request_services_recording` VALUES ('2', '16', '0', '0', 'patiet_16_1503323678.mp4', '1', '303', '0', '2017-08-21 15:54:37');
INSERT INTO `request_services_recording` VALUES ('3', '16', '0', '0', 'patiet_16_1503324457.mp4', '1', '303', '0', '2017-08-21 16:07:36');
INSERT INTO `request_services_recording` VALUES ('4', '16', '0', '0', 'patiet_16_1503324522.mp4', '1', '303', '0', '2017-08-21 16:08:41');
INSERT INTO `request_services_recording` VALUES ('5', '16', '0', '0', 'patiet_16_1503324618.mp4', '1', '303', '0', '2017-08-21 16:10:17');
INSERT INTO `request_services_recording` VALUES ('6', '128', '0', '0', 'patiet_128_1503395832.3gp', '0', '312', '0', '2017-08-22 11:57:11');
INSERT INTO `request_services_recording` VALUES ('7', '128', '0', '0', 'patiet_128_1503395922.3gp', '0', '313', '0', '2017-08-22 11:58:41');
INSERT INTO `request_services_recording` VALUES ('8', '128', '0', '0', 'patiet_128_1503403035.3gp', '0', '314', '0', '2017-08-22 13:57:15');
INSERT INTO `request_services_recording` VALUES ('9', '128', '0', '0', 'patiet_128_1503910934.3gp', '0', '316', '0', '2017-08-28 11:02:14');

-- ----------------------------
-- Table structure for `response_centers`
-- ----------------------------
DROP TABLE IF EXISTS `response_centers`;
CREATE TABLE `response_centers` (
  `center_id` int(10) unsigned NOT NULL,
  `response_id` int(10) unsigned NOT NULL,
  `arrive_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `response_centers_center_id_foreign` (`center_id`),
  KEY `response_centers_response_id_foreign` (`response_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of response_centers
-- ----------------------------
INSERT INTO `response_centers` VALUES ('2', '184', '2017-08-02 10:52:31', '1', '2017-08-02 13:44:57', '2017-08-02 14:52:31');
INSERT INTO `response_centers` VALUES ('3', '185', '2017-08-03 09:00:00', '0', '2017-08-02 13:47:40', '2017-08-02 13:47:40');
INSERT INTO `response_centers` VALUES ('5', '186', '2017-08-03 07:00:00', '0', '2017-08-02 14:01:49', '2017-08-02 14:01:49');

-- ----------------------------
-- Table structure for `response_radiology_items`
-- ----------------------------
DROP TABLE IF EXISTS `response_radiology_items`;
CREATE TABLE `response_radiology_items` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response_id` int(10) unsigned NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `preparation` longtext,
  `definition` longtext,
  `notes` longtext,
  KEY `response_radiology_items_type_id_foreign` (`type_id`),
  KEY `response_radiology_items_response_id_foreign` (`response_id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of response_radiology_items
-- ----------------------------
INSERT INTO `response_radiology_items` VALUES ('109', '184', '100.00', '1', '2017-08-02 13:44:57', '2017-08-02 14:52:31', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('181', '185', '120.00', '0', '2017-08-02 13:47:40', '2017-08-02 13:47:40', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('122', '186', '1524.00', '0', '2017-08-02 14:01:49', '2017-08-02 14:01:49', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('232', '187', '150.00', '1', '2017-08-02 18:05:52', '2017-08-02 18:07:54', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('86', '188', '500.00', '1', '2017-08-07 15:47:35', '2017-08-07 15:47:54', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('234', '189', '555.00', '1', '2017-08-07 19:42:39', '2017-08-07 19:43:11', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('231', '190', '250.00', '1', '2017-08-07 21:24:51', '2017-08-07 21:25:29', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('16', '191', '99.00', '0', '2017-08-08 15:41:20', '2017-08-08 15:41:20', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('90', '191', '22.00', '1', '2017-08-08 15:41:21', '2017-08-08 15:42:22', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('176', '191', '55.00', '1', '2017-08-08 15:41:21', '2017-08-08 15:42:22', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('81', '192', '222.00', '1', '2017-08-08 16:52:22', '2017-08-08 16:52:41', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('205', '193', '2344.00', '1', '2017-08-08 18:32:02', '2017-08-08 18:32:18', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('107', '194', '123123.00', '0', '2017-08-08 18:40:46', '2017-08-08 18:40:46', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('167', '195', '1222.00', '1', '2017-08-08 18:45:11', '2017-08-08 18:45:41', null, null, null);
INSERT INTO `response_radiology_items` VALUES ('228', '196', '600.00', '1', '2017-08-16 15:06:07', '2017-08-16 15:13:30', null, null, null);

-- ----------------------------
-- Table structure for `response_requests_items`
-- ----------------------------
DROP TABLE IF EXISTS `response_requests_items`;
CREATE TABLE `response_requests_items` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response_id` int(10) unsigned NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `definition` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `preparation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `response_radiology_items_type_id_foreign` (`type_id`),
  KEY `response_radiology_items_response_id_foreign` (`response_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of response_requests_items
-- ----------------------------

-- ----------------------------
-- Table structure for `search_sort_by`
-- ----------------------------
DROP TABLE IF EXISTS `search_sort_by`;
CREATE TABLE `search_sort_by` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of search_sort_by
-- ----------------------------
INSERT INTO `search_sort_by` VALUES ('1', 'Experience', '');
INSERT INTO `search_sort_by` VALUES ('3', 'nearest', '');

-- ----------------------------
-- Table structure for `servicefiles`
-- ----------------------------
DROP TABLE IF EXISTS `servicefiles`;
CREATE TABLE `servicefiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `servicefiles_service_id_foreign` (`service_id`),
  CONSTRAINT `servicefiles_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of servicefiles
-- ----------------------------

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `service_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `services_service_type_id_foreign` (`service_type_id`),
  CONSTRAINT `services_service_type_id_foreign` FOREIGN KEY (`service_type_id`) REFERENCES `servicetypes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of services
-- ----------------------------

-- ----------------------------
-- Table structure for `services_state`
-- ----------------------------
DROP TABLE IF EXISTS `services_state`;
CREATE TABLE `services_state` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of services_state
-- ----------------------------
INSERT INTO `services_state` VALUES ('0', 'home_visit', '', '0');
INSERT INTO `services_state` VALUES ('1', 'doctor_call', '', '0');
INSERT INTO `services_state` VALUES ('2', 'doctor_chat', '', '0');
INSERT INTO `services_state` VALUES ('3', 'doctor_offline_question', '', '0');
INSERT INTO `services_state` VALUES ('4', 'offline_question', '', '0');
INSERT INTO `services_state` VALUES ('5', 'doctor_video_call', '', '0');

-- ----------------------------
-- Table structure for `servicetypes`
-- ----------------------------
DROP TABLE IF EXISTS `servicetypes`;
CREATE TABLE `servicetypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of servicetypes
-- ----------------------------

-- ----------------------------
-- Table structure for `specialities`
-- ----------------------------
DROP TABLE IF EXISTS `specialities`;
CREATE TABLE `specialities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_en` varchar(45) DEFAULT NULL,
  `title_ar` varchar(45) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of specialities
-- ----------------------------
INSERT INTO `specialities` VALUES ('1', 'General Practice', 'ممارسة عامة', '0');
INSERT INTO `specialities` VALUES ('2', 'Dentistry (Teeth)', 'اسنان', '0');
INSERT INTO `specialities` VALUES ('3', 'Psychiatry (Mental, Emotional or Behavioral D', 'نفسي', '0');
INSERT INTO `specialities` VALUES ('4', 'Pediatrics and New Born (Child)', 'اطفال و حديثي الولادة', '0');
INSERT INTO `specialities` VALUES ('5', 'Neurology (Brain & Nerves)', 'مخ و اعصاب', '0');
INSERT INTO `specialities` VALUES ('6', 'Orthopedics (Bones)', 'عظام', '0');
INSERT INTO `specialities` VALUES ('7', 'Gynaecology and Infertility', 'نساء و توليد', '0');
INSERT INTO `specialities` VALUES ('8', 'Ear, Nose and Throat', 'انف و اذن و حنجرة', '0');
INSERT INTO `specialities` VALUES ('9', 'Cardiology and Vascular Disease (Heart)', 'قلب و اوعية دموية', '0');
INSERT INTO `specialities` VALUES ('10', 'Hematology', 'امراض دم', '0');
INSERT INTO `specialities` VALUES ('11', 'Oncology (Tumor)', 'اورام', '0');
INSERT INTO `specialities` VALUES ('12', 'Internal Medicine', 'باطنة', '0');
INSERT INTO `specialities` VALUES ('13', 'Dietitian and Nutrition', 'تخسيس و تغذية', '0');
INSERT INTO `specialities` VALUES ('14', 'Pediatric Surgery', 'جراحة اطفال', '0');
INSERT INTO `specialities` VALUES ('15', 'Vascular Surgery (Arteries and Vein Surgery)', 'جراحة اوعية دموية', '0');
INSERT INTO `specialities` VALUES ('16', 'Plastic Surgery', 'جراحة تجميل', '0');
INSERT INTO `specialities` VALUES ('17', 'Obesity and Laparoscopic Surgery', 'جراحة سمنة و مناظير', '0');
INSERT INTO `specialities` VALUES ('18', 'General Surgery', 'جراحة عامة', '0');
INSERT INTO `specialities` VALUES ('19', 'Spinal Surgery', 'جراحة عمود فقري', '0');
INSERT INTO `specialities` VALUES ('20', 'Cardiology and Thoracic Surgery (Heart & Ches', 'جراحة قلب و صدر', '0');
INSERT INTO `specialities` VALUES ('21', 'Neurosurgery (Brain & Nerves Surgery)', 'جراحة مخ و اعصاب', '0');
INSERT INTO `specialities` VALUES ('22', 'Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير', '0');
INSERT INTO `specialities` VALUES ('23', 'Allergy and Immunology (Sensitivity and Immun', 'حساسية و مناعة', '0');
INSERT INTO `specialities` VALUES ('24', 'IVF and Infertility', 'حقن مجهري و اطفال انابيب', '0');
INSERT INTO `specialities` VALUES ('25', 'Andrology and Male Infertility', 'ذكورة و عقم', '0');
INSERT INTO `specialities` VALUES ('26', 'Rheumatology', 'روماتيزم', '0');
INSERT INTO `specialities` VALUES ('27', 'Diabetes and Endocrinology', 'سكر و غدد صماء', '0');
INSERT INTO `specialities` VALUES ('28', 'Audiology', 'سمعيات', '0');
INSERT INTO `specialities` VALUES ('29', 'Chest and Respiratory', 'صدر و جهاز تنفسي', '0');
INSERT INTO `specialities` VALUES ('30', 'Elders (Old People Health)', 'طب مسنين', '0');
INSERT INTO `specialities` VALUES ('31', 'Pain Management', 'علاج الآلام', '0');
INSERT INTO `specialities` VALUES ('32', 'Physiotherapy and Sport Injuries', 'علاج طبيعي و اصابات ملاعب', '0');
INSERT INTO `specialities` VALUES ('33', 'Ophthalmology (Eyes)', 'عيون', '0');
INSERT INTO `specialities` VALUES ('34', 'Hepatology (Liver Doctor)', 'كبد', '0');
INSERT INTO `specialities` VALUES ('35', 'Nephrology', 'كلى', '0');
INSERT INTO `specialities` VALUES ('36', 'Urology (Urinary System)', 'مسالك بولية', '0');
INSERT INTO `specialities` VALUES ('38', 'Phoniatrics (Speech)', 'نطق و تخاطب', '0');
INSERT INTO `specialities` VALUES ('39', 'Dermatology (Skin12)', 'امراض جلدية و تناسلية   ', '0');
INSERT INTO `specialities` VALUES ('43', 'Adult Dermatology', 'جلدية بالغين', '39');
INSERT INTO `specialities` VALUES ('44', 'Pediatric Dermatology', 'جلدية اطفال', '39');
INSERT INTO `specialities` VALUES ('45', 'Cosmetic Dermatology and Laser', 'تجميل و ليزر', '39');
INSERT INTO `specialities` VALUES ('46', 'Genital Dermatology', 'امراض تناسلية', '39');
INSERT INTO `specialities` VALUES ('47', 'Adult Dentistry', 'اسنان بالغين', '2');
INSERT INTO `specialities` VALUES ('48', 'Pediatric Dentistry', 'اسنان اطفال', '2');
INSERT INTO `specialities` VALUES ('49', 'Elder Dentistry', 'اسنان مسنين', '2');
INSERT INTO `specialities` VALUES ('50', 'Orthodontics', 'تقويم اسنان', '2');
INSERT INTO `specialities` VALUES ('51', 'Endodontics', 'حشو و علاج الجذور و الاعصاب', '2');
INSERT INTO `specialities` VALUES ('52', 'Cosmetic Dentistry', 'تجميل اسنان', '2');
INSERT INTO `specialities` VALUES ('53', 'Implantology', 'زراعة اسنان', '2');
INSERT INTO `specialities` VALUES ('54', 'Oral and Maxillofacial Surgery', 'جراحة وجه و فكين', '2');
INSERT INTO `specialities` VALUES ('55', 'Periodontics', 'علاج اللثة', '2');
INSERT INTO `specialities` VALUES ('56', 'Prosthodontics', 'تركيبات اسنان', '2');
INSERT INTO `specialities` VALUES ('57', 'Oral Radiology', 'اشعة الاسنان', '2');
INSERT INTO `specialities` VALUES ('58', 'Adult Psychiatry', 'نفسي بالغين', '3');
INSERT INTO `specialities` VALUES ('59', 'Pediatric Psychiatry', 'نفسي اطفال', '3');
INSERT INTO `specialities` VALUES ('60', 'Addiction', 'علاج الادمان', '3');
INSERT INTO `specialities` VALUES ('69', 'Toxicology', 'علاج السموم', '3');
INSERT INTO `specialities` VALUES ('70', 'Family Counseling', 'استشارات اسرية', '3');
INSERT INTO `specialities` VALUES ('71', 'Pediatrics', 'اطفال', '4');
INSERT INTO `specialities` VALUES ('72', 'New Born', 'حديثي الولادة', '4');
INSERT INTO `specialities` VALUES ('73', 'Natural Breast Feeding', 'رضاعة طبيعية', '4');
INSERT INTO `specialities` VALUES ('74', 'Pediatric Neurology', 'مخ و اعصاب اطفال', '4');
INSERT INTO `specialities` VALUES ('75', 'Pediatric Cardiology', 'قلب اطفال', '4');
INSERT INTO `specialities` VALUES ('76', 'Pediatric Diabetes and Endocrinology', 'سكر و غدد صماء اطفال', '4');
INSERT INTO `specialities` VALUES ('77', 'Pediatric Hepatology', 'كبد اطفال', '4');
INSERT INTO `specialities` VALUES ('78', 'Pediatric Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير اطفال', '4');
INSERT INTO `specialities` VALUES ('79', 'Pediatric Nephrology', 'كلى اطفال', '4');
INSERT INTO `specialities` VALUES ('80', 'Pediatric Urology', 'مسالك بولية اطفال', '4');
INSERT INTO `specialities` VALUES ('81', 'Pediatric Allergy and Immunology', 'حساسية و مناعة اطفال', '4');
INSERT INTO `specialities` VALUES ('82', 'Pediatric Hematology', 'امراض دم اطفال', '4');
INSERT INTO `specialities` VALUES ('83', 'Pediatric Psychiatry', 'نفسي اطفال', '4');
INSERT INTO `specialities` VALUES ('84', 'Pediatric Rheumatology', 'روماتيزم اطفال', '4');
INSERT INTO `specialities` VALUES ('85', 'Pediatric Dietitian and Nutrition', 'تخسيس و تغذية اطفال', '4');
INSERT INTO `specialities` VALUES ('86', 'Adult Neurology', 'مخ و اعصاب بالغين', '5');
INSERT INTO `specialities` VALUES ('87', 'Pediatric Neurology', 'مخ و اعصاب اطفال', '5');
INSERT INTO `specialities` VALUES ('88', 'Adult Orthopedics', 'عظام بالغين', '6');
INSERT INTO `specialities` VALUES ('89', 'Pediatric Orthopedics', 'عظام اطفال', '6');
INSERT INTO `specialities` VALUES ('90', 'Adult Orthopedic Surgery', 'جراحة عظام بالغين', '6');
INSERT INTO `specialities` VALUES ('91', 'Pediatric Orthopedic Surgery', 'جراحة عظام اطفال', '6');
INSERT INTO `specialities` VALUES ('92', 'Bone Deformities', 'تشوهات عظام', '6');
INSERT INTO `specialities` VALUES ('93', 'Hand and Upper Limb Surgery', 'عظام اليد و الكتف', '6');
INSERT INTO `specialities` VALUES ('94', 'Foot and Ankle Orthopedics', 'عظام القدم و الكاحل', '6');
INSERT INTO `specialities` VALUES ('95', 'Joint Replacement', 'تغيير المفاصل', '6');
INSERT INTO `specialities` VALUES ('96', 'Microscopic Surgery', 'جراحة الاعصاب الطرفية', '6');
INSERT INTO `specialities` VALUES ('97', 'Osteopathic Medicine', 'تقويم عظام', '6');
INSERT INTO `specialities` VALUES ('98', 'Arthroscopy and Sports injuries', 'اصابات ملاعب و مناظير مفاصل', '6');
INSERT INTO `specialities` VALUES ('99', 'Gynaecology', 'امراض نساء و توليد', '7');
INSERT INTO `specialities` VALUES ('100', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '7');
INSERT INTO `specialities` VALUES ('101', 'Adult Ear, Nose and Throat', 'انف و اذن و حنجرة بالغين', '8');
INSERT INTO `specialities` VALUES ('102', 'Pediatric Ear, Nose and Throat', 'انف و اذن و حنجرة اطفال', '8');
INSERT INTO `specialities` VALUES ('103', 'Hearing and Balance Disorder', 'اضطراب السمع و التوازن', '8');
INSERT INTO `specialities` VALUES ('104', 'Adult Ear, Nose and throat Surgery', 'جراحة انف و اذن و حنجرة بالغين', '8');
INSERT INTO `specialities` VALUES ('105', 'Pediatric Ear, Nose and throat Surgery', 'جراحة انف و اذن و حنجرة اطفال', '8');
INSERT INTO `specialities` VALUES ('106', 'Adult Cardiology', 'قلب بالغين', '9');
INSERT INTO `specialities` VALUES ('107', 'Pediatric Cardiology', 'قلب اطفال', '9');
INSERT INTO `specialities` VALUES ('108', 'Adult Vascular Diseases', 'اوعية دموية بالغين', '9');
INSERT INTO `specialities` VALUES ('109', 'Pediatric Vascular Diseases', 'اوعية دموية اطفال', '9');
INSERT INTO `specialities` VALUES ('110', 'Adult Hematology', 'امراض دم بالغين', '10');
INSERT INTO `specialities` VALUES ('111', 'Pediatric Hematology', 'امراض دم اطفال', '10');
INSERT INTO `specialities` VALUES ('112', 'Bone Marrow Transplantation', 'زرع خلايا جزعية', '10');
INSERT INTO `specialities` VALUES ('113', 'Adult Oncology', 'اورام بالغين', '11');
INSERT INTO `specialities` VALUES ('114', 'Pediatric Oncology', 'اورام اطفال', '11');
INSERT INTO `specialities` VALUES ('115', 'Radiation Oncology', 'علاج اورام بالاشعاع', '11');
INSERT INTO `specialities` VALUES ('116', 'Adult Internal Medicine', 'باطنة عامة', '12');
INSERT INTO `specialities` VALUES ('117', 'Adult Dietitian and Nutrition', 'تخسيس و تغذية بالغين', '13');
INSERT INTO `specialities` VALUES ('118', 'Pediatric Dietitian and Nutrition', 'تخسيس و تغذية اطفال', '13');
INSERT INTO `specialities` VALUES ('119', 'Pediatric General Surgery', 'جراحة عامة اطفال', '14');
INSERT INTO `specialities` VALUES ('120', 'Pediatric Deformities and Birth Defects Surge', 'جراحة تشوهات اطفال و عيوب خلقية', '14');
INSERT INTO `specialities` VALUES ('121', 'Pediatric Urology Surgery', 'جراحة مسالك بولية اطفال', '14');
INSERT INTO `specialities` VALUES ('122', 'Pediatric Cardiology Surgery', 'جراحة قلب اطفال', '14');
INSERT INTO `specialities` VALUES ('123', 'Pediatric Thoracic Surgery', 'جراحة صدر اطفال', '14');
INSERT INTO `specialities` VALUES ('124', 'Pediatric Oncology Surgery', 'جراحة اورام اطفال', '14');
INSERT INTO `specialities` VALUES ('125', 'Pediatric Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير اطفال', '14');
INSERT INTO `specialities` VALUES ('126', 'Pediatric Neurosurgery', 'جراحة مخ و اعصاب اطفال', '14');
INSERT INTO `specialities` VALUES ('127', 'Adult Vascular Surgery', 'جراحة اوعية دموية بالغين', '15');
INSERT INTO `specialities` VALUES ('128', 'Pediatric Vascular Surgery', 'جراحة اوعية دموية اطفال', '15');
INSERT INTO `specialities` VALUES ('129', 'Diabetic Foot treatment', 'علاج قدم سكري', '15');
INSERT INTO `specialities` VALUES ('130', 'Facial Plastic Surgery', 'جراحة تجميل الوجه', '16');
INSERT INTO `specialities` VALUES ('131', 'Burn Surgery', 'جراحة تجميل الحروق', '16');
INSERT INTO `specialities` VALUES ('132', 'Eyes Cosmetic Surgery', 'جراحة تجميل العيون', '16');
INSERT INTO `specialities` VALUES ('133', 'Hand Surgery', 'جراحة تجميل اليد', '16');
INSERT INTO `specialities` VALUES ('134', 'Rhinoplastic Surgery', 'جراحة تجميل الانف', '16');
INSERT INTO `specialities` VALUES ('135', 'Cosmetic Dermatology and Laser', 'تجميل و ليزر', '16');
INSERT INTO `specialities` VALUES ('136', 'Obesity Surgery', 'جراحة سمنة و تخسيس', '17');
INSERT INTO `specialities` VALUES ('137', 'Adult General Surgery', 'جراحة عامة بالغين', '18');
INSERT INTO `specialities` VALUES ('138', 'Breast Tumor', 'جراحة اورام الثدي', '18');
INSERT INTO `specialities` VALUES ('139', 'Abdominal Surgery', 'جراحة بطن', '18');
INSERT INTO `specialities` VALUES ('140', 'Endocrinal Surgery', 'جراحة غدد صماء', '18');
INSERT INTO `specialities` VALUES ('141', 'Adult Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير بالغين', '18');
INSERT INTO `specialities` VALUES ('142', 'Pediatric Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير اطفال', '18');
INSERT INTO `specialities` VALUES ('143', 'Trauma and Accident Surgery', 'جراحة اصابات و حوادث', '18');
INSERT INTO `specialities` VALUES ('144', 'Adult Oncology Surgery', 'جراحة أورام بالغين', '18');
INSERT INTO `specialities` VALUES ('145', 'Brain Tumor', 'جراحة اورام المخ', '18');
INSERT INTO `specialities` VALUES ('146', 'Colon Tumor', 'جراحة اورام القولون', '18');
INSERT INTO `specialities` VALUES ('147', 'Liver Tumor', 'جراحة اورام الكبد', '18');
INSERT INTO `specialities` VALUES ('148', 'Lung Tumors', 'جراحة اورام الرئة', '18');
INSERT INTO `specialities` VALUES ('149', 'Orthopedics Tumor', 'جراحة اورام العظام', '18');
INSERT INTO `specialities` VALUES ('150', 'Pediatric Oncology Surgery', 'جراحة اورام اطفال', '18');
INSERT INTO `specialities` VALUES ('151', 'Prostate Tumor', 'جراحة اورام البروستاتا', '18');
INSERT INTO `specialities` VALUES ('152', 'Stomach Tumor', 'جراحة اورام المعدة', '18');
INSERT INTO `specialities` VALUES ('153', 'Pediatric General Surgery', 'جراحة عامة اطفال', '18');
INSERT INTO `specialities` VALUES ('154', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '18');
INSERT INTO `specialities` VALUES ('155', 'Adult Spine Surgery', 'جراحة عمود فقري بالغين', '19');
INSERT INTO `specialities` VALUES ('156', 'Pediatric Spine Surgery', 'جراحة عمود فقري اطفال', '19');
INSERT INTO `specialities` VALUES ('157', 'Adult Orthopedic Surgery', 'جراحة عظام بالغين', '19');
INSERT INTO `specialities` VALUES ('158', 'Pediatric Orthopedic Surgery', 'جراحة عظام اطفال', '19');
INSERT INTO `specialities` VALUES ('159', 'Bone Deformities', 'تشوهات عظام', '19');
INSERT INTO `specialities` VALUES ('160', 'Hand and Upper Limb Surgery', 'عظام اليد و الكتف', '19');
INSERT INTO `specialities` VALUES ('161', 'Foot and Ankle Orthopedics', 'عظام القدم و الكاحل', '19');
INSERT INTO `specialities` VALUES ('162', 'Joint Replacement', 'تغيير المفاصل', '19');
INSERT INTO `specialities` VALUES ('163', 'Microscopic Surgery', 'جراحة الاعصاب الطرفية', '19');
INSERT INTO `specialities` VALUES ('164', 'Osteopathic Medicine', 'تقويم عظام', '19');
INSERT INTO `specialities` VALUES ('165', 'Adult Cardiology Surgery', 'جراحة قلب بالغين', '20');
INSERT INTO `specialities` VALUES ('166', 'Pediatric Cardiology Surgery', 'جراحة قلب اطفال', '20');
INSERT INTO `specialities` VALUES ('167', 'Adult Thoracic Surgery', 'جراحة صدر بالغين', '20');
INSERT INTO `specialities` VALUES ('168', 'Pediatric Thoracic Surgery', 'جراحة صدر اطفال', '20');
INSERT INTO `specialities` VALUES ('169', 'Adult Neurosurgery', 'جراحة مخ و اعصاب بالغين', '21');
INSERT INTO `specialities` VALUES ('170', 'Pediatric Neurosurgery', 'جراحة مخ و اعصاب اطفال', '21');
INSERT INTO `specialities` VALUES ('171', 'Adult Spine Surgery', 'جراحة عمود فقري بالغين', '21');
INSERT INTO `specialities` VALUES ('172', 'Pediatric Spine Surgery', 'جراحة عمود فقري اطفال', '21');
INSERT INTO `specialities` VALUES ('173', 'Adult Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير بالغين', '22');
INSERT INTO `specialities` VALUES ('174', 'Pediatric Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير اطفال', '22');
INSERT INTO `specialities` VALUES ('175', 'Adult Allergy and Immunology', 'حساسية و مناعة بالغين', '23');
INSERT INTO `specialities` VALUES ('176', 'Pediatric Allergy and Immunology', 'حساسية و مناعة اطفال', '23');
INSERT INTO `specialities` VALUES ('177', 'Respiratory Tract Allergy', 'حساسية الجهاز التنفسي', '23');
INSERT INTO `specialities` VALUES ('178', 'Autoimmune Allergy', 'امراض المناعة الذاتية', '23');
INSERT INTO `specialities` VALUES ('179', 'Skin Allergy', 'حساسية الجلد', '23');
INSERT INTO `specialities` VALUES ('180', 'Ophthalmologic Allergy', 'حساسية العيون', '23');
INSERT INTO `specialities` VALUES ('181', 'IVF', 'حقن مجهري و عقم', '24');
INSERT INTO `specialities` VALUES ('182', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '24');
INSERT INTO `specialities` VALUES ('183', 'Andrology', 'امراض ذكورة', '25');
INSERT INTO `specialities` VALUES ('184', 'Adult Rheumatology', 'روماتيزم بالغين', '26');
INSERT INTO `specialities` VALUES ('185', 'Pediatric Rheumatology', 'روماتيزم اطفال', '26');
INSERT INTO `specialities` VALUES ('186', 'Adult Diabetes and Endocrinology', 'سكر و غدد صماء بالغين', '27');
INSERT INTO `specialities` VALUES ('187', 'Pediatric Diabetes and Endocrinology', 'سكر و غدد صماء اطفال', '27');
INSERT INTO `specialities` VALUES ('188', 'Adult Audiology', 'سمعيات بالغين', '28');
INSERT INTO `specialities` VALUES ('189', 'Pediatric Audiology', 'سمعيات اطفال', '28');
INSERT INTO `specialities` VALUES ('190', 'Hearing and Balance Disorder', 'اضطراب السمع و التوازن', '28');
INSERT INTO `specialities` VALUES ('191', 'Adult Chest and Respiratory', 'صدر و جهاز تنفسي بالغين', '29');
INSERT INTO `specialities` VALUES ('192', 'Pediatric Chest and Respiratory', 'صدر و جهاز تنفسي اطفال', '29');
INSERT INTO `specialities` VALUES ('193', 'Elder Memory', 'ذاكرة مسنين', '30');
INSERT INTO `specialities` VALUES ('194', 'Elder Movements', 'حركة مسنين', '30');
INSERT INTO `specialities` VALUES ('195', 'Elder Nutrition', 'تغذية مسنين', '30');
INSERT INTO `specialities` VALUES ('196', 'Adult Pain Management', 'علاج الالام بالغين', '31');
INSERT INTO `specialities` VALUES ('197', 'Pediatric Pain Management', 'علاج الالام اطفال', '31');
INSERT INTO `specialities` VALUES ('198', 'Adult Physiotherapy', 'علاج طبيعي بالغين', '32');
INSERT INTO `specialities` VALUES ('199', 'Pediatric Physiotherapy', 'علاج طبيعي اطفال', '32');
INSERT INTO `specialities` VALUES ('200', 'Sport Injuries', 'اصابات ملاعب', '32');
INSERT INTO `specialities` VALUES ('201', 'Adult Ophthalmology', 'عيون بالغين', '33');
INSERT INTO `specialities` VALUES ('202', 'Pediatric Ophthalmology', 'عيون اطفال', '33');
INSERT INTO `specialities` VALUES ('203', 'Lasik, Vision Correction and Cataract', 'ليزك و تصحيح الابصار و المياة البيضاء', '33');
INSERT INTO `specialities` VALUES ('204', 'Vitreous Body and Retinal Surgery', 'جراحة شبكية و جسم زجاجي', '33');
INSERT INTO `specialities` VALUES ('205', 'Visual Rehabilitation', 'تاهيل بصري', '33');
INSERT INTO `specialities` VALUES ('206', 'Adult Hepatology', 'كبد بالغين', '34');
INSERT INTO `specialities` VALUES ('207', 'Pediatric Hepatology', 'كبد اطفال', '34');
INSERT INTO `specialities` VALUES ('208', 'Adult Nephrology', 'كلى بالغين', '35');
INSERT INTO `specialities` VALUES ('209', 'Pediatric Nephrology', 'كلى اطفال', '35');
INSERT INTO `specialities` VALUES ('210', 'Adult Urology', 'مسالك بولية بالغين', '36');
INSERT INTO `specialities` VALUES ('211', 'Pediatric Urology', 'مسالك بولية اطفال', '36');
INSERT INTO `specialities` VALUES ('212', 'Adult Urology Surgery', 'جراحة مسالك بولية بالغين', '36');
INSERT INTO `specialities` VALUES ('213', 'Pediatric Urology Surgery', 'جراحة مسالك بولية اطفال', '36');
INSERT INTO `specialities` VALUES ('214', 'Adult General Practice', 'ممارسة عامة بالغين', '37');
INSERT INTO `specialities` VALUES ('215', 'Pediatric General Practice', 'ممارسة عامة اطفال', '37');
INSERT INTO `specialities` VALUES ('216', 'Adult Phoniatrics', 'نطق و تخاطب بالغين', '38');
INSERT INTO `specialities` VALUES ('217', 'Pediatric Phoniatrics', 'نطق و تخاطب اطفال', '38');

-- ----------------------------
-- Table structure for `statusmessages`
-- ----------------------------
DROP TABLE IF EXISTS `statusmessages`;
CREATE TABLE `statusmessages` (
  `sid` int(4) NOT NULL COMMENT 'Status id',
  `statusNumber` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - success, 1 - error',
  `statusMessage` varchar(400) NOT NULL COMMENT 'brief status message',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='status messages for the appl response';

-- ----------------------------
-- Records of statusmessages
-- ----------------------------
INSERT INTO `statusmessages` VALUES ('1', '1', 'Patient not logged in');
INSERT INTO `statusmessages` VALUES ('2', '1', 'Email Already Reigstered');
INSERT INTO `statusmessages` VALUES ('3', '0', 'Invalid Referral Code');
INSERT INTO `statusmessages` VALUES ('406', '1', 'Patient not logged in');

-- ----------------------------
-- Table structure for `sub_specialities`
-- ----------------------------
DROP TABLE IF EXISTS `sub_specialities`;
CREATE TABLE `sub_specialities` (
  `id` int(11) NOT NULL,
  `title_en` varchar(45) DEFAULT NULL,
  `title_ar` varchar(45) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sub_specialities
-- ----------------------------
INSERT INTO `sub_specialities` VALUES ('43', 'Adult Dermatology', 'جلدية بالغين', '39');
INSERT INTO `sub_specialities` VALUES ('44', 'Pediatric Dermatology', 'جلدية اطفال', '39');
INSERT INTO `sub_specialities` VALUES ('45', 'Cosmetic Dermatology and Laser', 'تجميل و ليزر', '39');
INSERT INTO `sub_specialities` VALUES ('46', 'Genital Dermatology', 'امراض تناسلية', '39');
INSERT INTO `sub_specialities` VALUES ('47', 'Adult Dentistry', 'اسنان بالغين', '2');
INSERT INTO `sub_specialities` VALUES ('48', 'Pediatric Dentistry', 'اسنان اطفال', '2');
INSERT INTO `sub_specialities` VALUES ('49', 'Elder Dentistry', 'اسنان مسنين', '2');
INSERT INTO `sub_specialities` VALUES ('50', 'Orthodontics', 'تقويم اسنان', '2');
INSERT INTO `sub_specialities` VALUES ('51', 'Endodontics', 'حشو و علاج الجذور و الاعصاب', '2');
INSERT INTO `sub_specialities` VALUES ('52', 'Cosmetic Dentistry', 'تجميل اسنان', '2');
INSERT INTO `sub_specialities` VALUES ('53', 'Implantology', 'زراعة اسنان', '2');
INSERT INTO `sub_specialities` VALUES ('54', 'Oral and Maxillofacial Surgery', 'جراحة وجه و فكين', '2');
INSERT INTO `sub_specialities` VALUES ('55', 'Periodontics', 'علاج اللثة', '2');
INSERT INTO `sub_specialities` VALUES ('56', 'Prosthodontics', 'تركيبات اسنان', '2');
INSERT INTO `sub_specialities` VALUES ('57', 'Oral Radiology', 'اشعة الاسنان', '2');
INSERT INTO `sub_specialities` VALUES ('58', 'Adult Psychiatry', 'نفسي بالغين', '3');
INSERT INTO `sub_specialities` VALUES ('59', 'Pediatric Psychiatry', 'نفسي اطفال', '3');
INSERT INTO `sub_specialities` VALUES ('60', 'Addiction', 'علاج الادمان', '3');
INSERT INTO `sub_specialities` VALUES ('69', 'Toxicology', 'علاج السموم', '3');
INSERT INTO `sub_specialities` VALUES ('70', 'Family Counseling', 'استشارات اسرية', '3');
INSERT INTO `sub_specialities` VALUES ('71', 'Pediatrics', 'اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('72', 'New Born', 'حديثي الولادة', '4');
INSERT INTO `sub_specialities` VALUES ('73', 'Natural Breast Feeding', 'رضاعة طبيعية', '4');
INSERT INTO `sub_specialities` VALUES ('74', 'Pediatric Neurology', 'مخ و اعصاب اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('75', 'Pediatric Cardiology', 'قلب اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('76', 'Pediatric Diabetes and Endocrinology', 'سكر و غدد صماء اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('77', 'Pediatric Hepatology', 'كبد اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('78', 'Pediatric Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('79', 'Pediatric Nephrology', 'كلى اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('80', 'Pediatric Urology', 'مسالك بولية اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('81', 'Pediatric Allergy and Immunology', 'حساسية و مناعة اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('82', 'Pediatric Hematology', 'امراض دم اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('83', 'Pediatric Psychiatry', 'نفسي اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('84', 'Pediatric Rheumatology', 'روماتيزم اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('85', 'Pediatric Dietitian and Nutrition', 'تخسيس و تغذية اطفال', '4');
INSERT INTO `sub_specialities` VALUES ('86', 'Adult Neurology', 'مخ و اعصاب بالغين', '5');
INSERT INTO `sub_specialities` VALUES ('87', 'Pediatric Neurology', 'مخ و اعصاب اطفال', '5');
INSERT INTO `sub_specialities` VALUES ('88', 'Adult Orthopedics', 'عظام بالغين', '6');
INSERT INTO `sub_specialities` VALUES ('89', 'Pediatric Orthopedics', 'عظام اطفال', '6');
INSERT INTO `sub_specialities` VALUES ('90', 'Adult Orthopedic Surgery', 'جراحة عظام بالغين', '6');
INSERT INTO `sub_specialities` VALUES ('91', 'Pediatric Orthopedic Surgery', 'جراحة عظام اطفال', '6');
INSERT INTO `sub_specialities` VALUES ('92', 'Bone Deformities', 'تشوهات عظام', '6');
INSERT INTO `sub_specialities` VALUES ('93', 'Hand and Upper Limb Surgery', 'عظام اليد و الكتف', '6');
INSERT INTO `sub_specialities` VALUES ('94', 'Foot and Ankle Orthopedics', 'عظام القدم و الكاحل', '6');
INSERT INTO `sub_specialities` VALUES ('95', 'Joint Replacement', 'تغيير المفاصل', '6');
INSERT INTO `sub_specialities` VALUES ('96', 'Microscopic Surgery', 'جراحة الاعصاب الطرفية', '6');
INSERT INTO `sub_specialities` VALUES ('97', 'Osteopathic Medicine', 'تقويم عظام', '6');
INSERT INTO `sub_specialities` VALUES ('98', 'Arthroscopy and Sports injuries', 'اصابات ملاعب و مناظير مفاصل', '6');
INSERT INTO `sub_specialities` VALUES ('99', 'Gynaecology', 'امراض نساء و توليد', '7');
INSERT INTO `sub_specialities` VALUES ('100', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '7');
INSERT INTO `sub_specialities` VALUES ('101', 'Adult Ear, Nose and Throat', 'انف و اذن و حنجرة بالغين', '8');
INSERT INTO `sub_specialities` VALUES ('102', 'Pediatric Ear, Nose and Throat', 'انف و اذن و حنجرة اطفال', '8');
INSERT INTO `sub_specialities` VALUES ('103', 'Hearing and Balance Disorder', 'اضطراب السمع و التوازن', '8');
INSERT INTO `sub_specialities` VALUES ('104', 'Adult Ear, Nose and throat Surgery', 'جراحة انف و اذن و حنجرة بالغين', '8');
INSERT INTO `sub_specialities` VALUES ('105', 'Pediatric Ear, Nose and throat Surgery', 'جراحة انف و اذن و حنجرة اطفال', '8');
INSERT INTO `sub_specialities` VALUES ('106', 'Adult Cardiology', 'قلب بالغين', '9');
INSERT INTO `sub_specialities` VALUES ('107', 'Pediatric Cardiology', 'قلب اطفال', '9');
INSERT INTO `sub_specialities` VALUES ('108', 'Adult Vascular Diseases', 'اوعية دموية بالغين', '9');
INSERT INTO `sub_specialities` VALUES ('109', 'Pediatric Vascular Diseases', 'اوعية دموية اطفال', '9');
INSERT INTO `sub_specialities` VALUES ('110', 'Adult Hematology', 'امراض دم بالغين', '10');
INSERT INTO `sub_specialities` VALUES ('111', 'Pediatric Hematology', 'امراض دم اطفال', '10');
INSERT INTO `sub_specialities` VALUES ('112', 'Bone Marrow Transplantation', 'زرع خلايا جزعية', '10');
INSERT INTO `sub_specialities` VALUES ('113', 'Adult Oncology', 'اورام بالغين', '11');
INSERT INTO `sub_specialities` VALUES ('114', 'Pediatric Oncology', 'اورام اطفال', '11');
INSERT INTO `sub_specialities` VALUES ('115', 'Radiation Oncology', 'علاج اورام بالاشعاع', '11');
INSERT INTO `sub_specialities` VALUES ('116', 'Adult Internal Medicine', 'باطنة عامة', '12');
INSERT INTO `sub_specialities` VALUES ('117', 'Adult Dietitian and Nutrition', 'تخسيس و تغذية بالغين', '13');
INSERT INTO `sub_specialities` VALUES ('118', 'Pediatric Dietitian and Nutrition', 'تخسيس و تغذية اطفال', '13');
INSERT INTO `sub_specialities` VALUES ('119', 'Pediatric General Surgery', 'جراحة عامة اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('120', 'Pediatric Deformities and Birth Defects Surge', 'جراحة تشوهات اطفال و عيوب خلقية', '14');
INSERT INTO `sub_specialities` VALUES ('121', 'Pediatric Urology Surgery', 'جراحة مسالك بولية اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('122', 'Pediatric Cardiology Surgery', 'جراحة قلب اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('123', 'Pediatric Thoracic Surgery', 'جراحة صدر اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('124', 'Pediatric Oncology Surgery', 'جراحة اورام اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('125', 'Pediatric Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('126', 'Pediatric Neurosurgery', 'جراحة مخ و اعصاب اطفال', '14');
INSERT INTO `sub_specialities` VALUES ('127', 'Adult Vascular Surgery', 'جراحة اوعية دموية بالغين', '15');
INSERT INTO `sub_specialities` VALUES ('128', 'Pediatric Vascular Surgery', 'جراحة اوعية دموية اطفال', '15');
INSERT INTO `sub_specialities` VALUES ('129', 'Diabetic Foot treatment', 'علاج قدم سكري', '15');
INSERT INTO `sub_specialities` VALUES ('130', 'Facial Plastic Surgery', 'جراحة تجميل الوجه', '16');
INSERT INTO `sub_specialities` VALUES ('131', 'Burn Surgery', 'جراحة تجميل الحروق', '16');
INSERT INTO `sub_specialities` VALUES ('132', 'Eyes Cosmetic Surgery', 'جراحة تجميل العيون', '16');
INSERT INTO `sub_specialities` VALUES ('133', 'Hand Surgery', 'جراحة تجميل اليد', '16');
INSERT INTO `sub_specialities` VALUES ('134', 'Rhinoplastic Surgery', 'جراحة تجميل الانف', '16');
INSERT INTO `sub_specialities` VALUES ('135', 'Cosmetic Dermatology and Laser', 'تجميل و ليزر', '16');
INSERT INTO `sub_specialities` VALUES ('136', 'Obesity Surgery', 'جراحة سمنة و تخسيس', '17');
INSERT INTO `sub_specialities` VALUES ('137', 'Adult General Surgery', 'جراحة عامة بالغين', '18');
INSERT INTO `sub_specialities` VALUES ('138', 'Breast Tumor', 'جراحة اورام الثدي', '18');
INSERT INTO `sub_specialities` VALUES ('139', 'Abdominal Surgery', 'جراحة بطن', '18');
INSERT INTO `sub_specialities` VALUES ('140', 'Endocrinal Surgery', 'جراحة غدد صماء', '18');
INSERT INTO `sub_specialities` VALUES ('141', 'Adult Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير بالغين', '18');
INSERT INTO `sub_specialities` VALUES ('142', 'Pediatric Gastroenterological Surgery', 'جراحة جهاز هضمي و مناظير اطفال', '18');
INSERT INTO `sub_specialities` VALUES ('143', 'Trauma and Accident Surgery', 'جراحة اصابات و حوادث', '18');
INSERT INTO `sub_specialities` VALUES ('144', 'Adult Oncology Surgery', 'جراحة أورام بالغين', '18');
INSERT INTO `sub_specialities` VALUES ('145', 'Brain Tumor', 'جراحة اورام المخ', '18');
INSERT INTO `sub_specialities` VALUES ('146', 'Colon Tumor', 'جراحة اورام القولون', '18');
INSERT INTO `sub_specialities` VALUES ('147', 'Liver Tumor', 'جراحة اورام الكبد', '18');
INSERT INTO `sub_specialities` VALUES ('148', 'Lung Tumors', 'جراحة اورام الرئة', '18');
INSERT INTO `sub_specialities` VALUES ('149', 'Orthopedics Tumor', 'جراحة اورام العظام', '18');
INSERT INTO `sub_specialities` VALUES ('150', 'Pediatric Oncology Surgery', 'جراحة اورام اطفال', '18');
INSERT INTO `sub_specialities` VALUES ('151', 'Prostate Tumor', 'جراحة اورام البروستاتا', '18');
INSERT INTO `sub_specialities` VALUES ('152', 'Stomach Tumor', 'جراحة اورام المعدة', '18');
INSERT INTO `sub_specialities` VALUES ('153', 'Pediatric General Surgery', 'جراحة عامة اطفال', '18');
INSERT INTO `sub_specialities` VALUES ('154', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '18');
INSERT INTO `sub_specialities` VALUES ('155', 'Adult Spine Surgery', 'جراحة عمود فقري بالغين', '19');
INSERT INTO `sub_specialities` VALUES ('156', 'Pediatric Spine Surgery', 'جراحة عمود فقري اطفال', '19');
INSERT INTO `sub_specialities` VALUES ('157', 'Adult Orthopedic Surgery', 'جراحة عظام بالغين', '19');
INSERT INTO `sub_specialities` VALUES ('158', 'Pediatric Orthopedic Surgery', 'جراحة عظام اطفال', '19');
INSERT INTO `sub_specialities` VALUES ('159', 'Bone Deformities', 'تشوهات عظام', '19');
INSERT INTO `sub_specialities` VALUES ('160', 'Hand and Upper Limb Surgery', 'عظام اليد و الكتف', '19');
INSERT INTO `sub_specialities` VALUES ('161', 'Foot and Ankle Orthopedics', 'عظام القدم و الكاحل', '19');
INSERT INTO `sub_specialities` VALUES ('162', 'Joint Replacement', 'تغيير المفاصل', '19');
INSERT INTO `sub_specialities` VALUES ('163', 'Microscopic Surgery', 'جراحة الاعصاب الطرفية', '19');
INSERT INTO `sub_specialities` VALUES ('164', 'Osteopathic Medicine', 'تقويم عظام', '19');
INSERT INTO `sub_specialities` VALUES ('165', 'Adult Cardiology Surgery', 'جراحة قلب بالغين', '20');
INSERT INTO `sub_specialities` VALUES ('166', 'Pediatric Cardiology Surgery', 'جراحة قلب اطفال', '20');
INSERT INTO `sub_specialities` VALUES ('167', 'Adult Thoracic Surgery', 'جراحة صدر بالغين', '20');
INSERT INTO `sub_specialities` VALUES ('168', 'Pediatric Thoracic Surgery', 'جراحة صدر اطفال', '20');
INSERT INTO `sub_specialities` VALUES ('169', 'Adult Neurosurgery', 'جراحة مخ و اعصاب بالغين', '21');
INSERT INTO `sub_specialities` VALUES ('170', 'Pediatric Neurosurgery', 'جراحة مخ و اعصاب اطفال', '21');
INSERT INTO `sub_specialities` VALUES ('171', 'Adult Spine Surgery', 'جراحة عمود فقري بالغين', '21');
INSERT INTO `sub_specialities` VALUES ('172', 'Pediatric Spine Surgery', 'جراحة عمود فقري اطفال', '21');
INSERT INTO `sub_specialities` VALUES ('173', 'Adult Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير بالغين', '22');
INSERT INTO `sub_specialities` VALUES ('174', 'Pediatric Gastroenterology and Endoscopy', 'جهاز هضمي و مناظير اطفال', '22');
INSERT INTO `sub_specialities` VALUES ('175', 'Adult Allergy and Immunology', 'حساسية و مناعة بالغين', '23');
INSERT INTO `sub_specialities` VALUES ('176', 'Pediatric Allergy and Immunology', 'حساسية و مناعة اطفال', '23');
INSERT INTO `sub_specialities` VALUES ('177', 'Respiratory Tract Allergy', 'حساسية الجهاز التنفسي', '23');
INSERT INTO `sub_specialities` VALUES ('178', 'Autoimmune Allergy', 'امراض المناعة الذاتية', '23');
INSERT INTO `sub_specialities` VALUES ('179', 'Skin Allergy', 'حساسية الجلد', '23');
INSERT INTO `sub_specialities` VALUES ('180', 'Ophthalmologic Allergy', 'حساسية العيون', '23');
INSERT INTO `sub_specialities` VALUES ('181', 'IVF', 'حقن مجهري و عقم', '24');
INSERT INTO `sub_specialities` VALUES ('182', 'Gynaecologic Oncological Surgery', 'جراحة اورام نسائية', '24');
INSERT INTO `sub_specialities` VALUES ('183', 'Andrology', 'امراض ذكورة', '25');
INSERT INTO `sub_specialities` VALUES ('184', 'Adult Rheumatology', 'روماتيزم بالغين', '26');
INSERT INTO `sub_specialities` VALUES ('185', 'Pediatric Rheumatology', 'روماتيزم اطفال', '26');
INSERT INTO `sub_specialities` VALUES ('186', 'Adult Diabetes and Endocrinology', 'سكر و غدد صماء بالغين', '27');
INSERT INTO `sub_specialities` VALUES ('187', 'Pediatric Diabetes and Endocrinology', 'سكر و غدد صماء اطفال', '27');
INSERT INTO `sub_specialities` VALUES ('188', 'Adult Audiology', 'سمعيات بالغين', '28');
INSERT INTO `sub_specialities` VALUES ('189', 'Pediatric Audiology', 'سمعيات اطفال', '28');
INSERT INTO `sub_specialities` VALUES ('190', 'Hearing and Balance Disorder', 'اضطراب السمع و التوازن', '28');
INSERT INTO `sub_specialities` VALUES ('191', 'Adult Chest and Respiratory', 'صدر و جهاز تنفسي بالغين', '29');
INSERT INTO `sub_specialities` VALUES ('192', 'Pediatric Chest and Respiratory', 'صدر و جهاز تنفسي اطفال', '29');
INSERT INTO `sub_specialities` VALUES ('193', 'Elder Memory', 'ذاكرة مسنين', '30');
INSERT INTO `sub_specialities` VALUES ('194', 'Elder Movements', 'حركة مسنين', '30');
INSERT INTO `sub_specialities` VALUES ('195', 'Elder Nutrition', 'تغذية مسنين', '30');
INSERT INTO `sub_specialities` VALUES ('196', 'Adult Pain Management', 'علاج الالام بالغين', '31');
INSERT INTO `sub_specialities` VALUES ('197', 'Pediatric Pain Management', 'علاج الالام اطفال', '31');
INSERT INTO `sub_specialities` VALUES ('198', 'Adult Physiotherapy', 'علاج طبيعي بالغين', '32');
INSERT INTO `sub_specialities` VALUES ('199', 'Pediatric Physiotherapy', 'علاج طبيعي اطفال', '32');
INSERT INTO `sub_specialities` VALUES ('200', 'Sport Injuries', 'اصابات ملاعب', '32');
INSERT INTO `sub_specialities` VALUES ('201', 'Adult Ophthalmology', 'عيون بالغين', '33');
INSERT INTO `sub_specialities` VALUES ('202', 'Pediatric Ophthalmology', 'عيون اطفال', '33');
INSERT INTO `sub_specialities` VALUES ('203', 'Lasik, Vision Correction and Cataract', 'ليزك و تصحيح الابصار و المياة البيضاء', '33');
INSERT INTO `sub_specialities` VALUES ('204', 'Vitreous Body and Retinal Surgery', 'جراحة شبكية و جسم زجاجي', '33');
INSERT INTO `sub_specialities` VALUES ('205', 'Visual Rehabilitation', 'تاهيل بصري', '33');
INSERT INTO `sub_specialities` VALUES ('206', 'Adult Hepatology', 'كبد بالغين', '34');
INSERT INTO `sub_specialities` VALUES ('207', 'Pediatric Hepatology', 'كبد اطفال', '34');
INSERT INTO `sub_specialities` VALUES ('208', 'Adult Nephrology', 'كلى بالغين', '35');
INSERT INTO `sub_specialities` VALUES ('209', 'Pediatric Nephrology', 'كلى اطفال', '35');
INSERT INTO `sub_specialities` VALUES ('210', 'Adult Urology', 'مسالك بولية بالغين', '36');
INSERT INTO `sub_specialities` VALUES ('211', 'Pediatric Urology', 'مسالك بولية اطفال', '36');
INSERT INTO `sub_specialities` VALUES ('212', 'Adult Urology Surgery', 'جراحة مسالك بولية بالغين', '36');
INSERT INTO `sub_specialities` VALUES ('213', 'Pediatric Urology Surgery', 'جراحة مسالك بولية اطفال', '36');
INSERT INTO `sub_specialities` VALUES ('214', 'Adult General Practice', 'ممارسة عامة بالغين', '37');
INSERT INTO `sub_specialities` VALUES ('215', 'Pediatric General Practice', 'ممارسة عامة اطفال', '37');
INSERT INTO `sub_specialities` VALUES ('216', 'Adult Phoniatrics', 'نطق و تخاطب بالغين', '38');
INSERT INTO `sub_specialities` VALUES ('217', 'Pediatric Phoniatrics', 'نطق و تخاطب اطفال', '38');

-- ----------------------------
-- Table structure for `temp_phones`
-- ----------------------------
DROP TABLE IF EXISTS `temp_phones`;
CREATE TABLE `temp_phones` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of temp_phones
-- ----------------------------

-- ----------------------------
-- Table structure for `urls`
-- ----------------------------
DROP TABLE IF EXISTS `urls`;
CREATE TABLE `urls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `url_group_id` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `urls_url_group_id_foreign` (`url_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of urls
-- ----------------------------
INSERT INTO `urls` VALUES ('1', 'Manage Urls', '/urls', '1', '0', null, null);
INSERT INTO `urls` VALUES ('5', 'User Roles', '/usertypes', '1', '0', null, null);
INSERT INTO `urls` VALUES ('6', 'Users', '/user', '1', '0', null, null);
INSERT INTO `urls` VALUES ('9', 'Add new user', '/user/create', '1', '0', '2017-08-02 03:28:03', '2017-08-02 03:28:46');
INSERT INTO `urls` VALUES ('10', 'Create new role', '/usertypes/create', '1', '0', '2017-08-02 03:30:25', '2017-08-02 03:35:18');
INSERT INTO `urls` VALUES ('11', 'Main Menu', '/urlgroups', '1', '0', '2017-08-02 03:32:12', '2017-08-02 03:32:36');
INSERT INTO `urls` VALUES ('12', 'Create new main menu', '/urlgroups/create', '1', '0', '2017-08-02 03:33:07', '2017-08-02 03:33:07');
INSERT INTO `urls` VALUES ('13', 'New Requests', '/patientrequests', '2', '0', '2017-08-02 03:39:51', '2017-08-02 03:39:51');
INSERT INTO `urls` VALUES ('14', 'Center Responses', '/centerresponses', '2', '0', '2017-08-02 03:41:05', '2017-08-02 03:41:05');
INSERT INTO `urls` VALUES ('15', 'Confirm Arrival', '/confirm', '2', '0', '2017-08-02 03:42:34', '2017-08-02 03:42:34');
INSERT INTO `urls` VALUES ('16', 'Radiology Report', '/result', '2', '0', '2017-08-02 03:43:00', '2017-08-02 04:33:47');
INSERT INTO `urls` VALUES ('17', 'All Branches', '/centers', '5', '0', '2017-08-02 03:51:35', '2017-08-02 03:51:35');
INSERT INTO `urls` VALUES ('18', 'Add new branch', '/centers/create', '5', '0', '2017-08-02 03:52:06', '2017-08-02 03:52:06');
INSERT INTO `urls` VALUES ('19', 'All Groups', '/radiologytypecategories', '3', '0', '2017-08-02 03:53:58', '2017-08-02 03:53:58');
INSERT INTO `urls` VALUES ('20', 'Add new Group', '/radiologytypecategories/create', '3', '0', '2017-08-02 03:54:20', '2017-08-02 03:55:09');
INSERT INTO `urls` VALUES ('21', 'All Services', '/radiologytypes', '3', '0', '2017-08-02 03:55:30', '2017-08-02 03:55:30');
INSERT INTO `urls` VALUES ('22', 'Add new service', '/radiologytypes/create', '3', '0', '2017-08-02 03:56:04', '2017-08-02 03:56:04');
INSERT INTO `urls` VALUES ('23', 'Request status', '/reports/statusgroup', '4', '0', '2017-08-02 03:59:48', '2017-08-02 03:59:48');

-- ----------------------------
-- Table structure for `url_groups`
-- ----------------------------
DROP TABLE IF EXISTS `url_groups`;
CREATE TABLE `url_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of url_groups
-- ----------------------------
INSERT INTO `url_groups` VALUES ('1', 'Administration', '#', '0', null, null);
INSERT INTO `url_groups` VALUES ('2', 'Requests', '#', '3', null, null);
INSERT INTO `url_groups` VALUES ('3', 'Services', '#', '1', null, null);
INSERT INTO `url_groups` VALUES ('4', 'Reports', '#', '4', null, null);
INSERT INTO `url_groups` VALUES ('5', 'Branches', '#', '2', '2017-08-02 03:50:33', '2017-08-02 03:50:33');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type_id` int(10) unsigned NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_type_id_foreign` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Radiology', 'admin@radiology.com', '$2y$10$YDKRykKXsqhkmK.XfSENk.2FRnzbG85JD6gXOmBkriDZJvbjo6XiW', 'FQZhkSAW9u6jX1j1cj3fH2DdaWzaCPEbFlmz7GsCaKw7FxOQgKejWpA4BMbf', null, '2017-08-27 21:05:33', '1', 'admin');

-- ----------------------------
-- Table structure for `users_old`
-- ----------------------------
DROP TABLE IF EXISTS `users_old`;
CREATE TABLE `users_old` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_old
-- ----------------------------

-- ----------------------------
-- Table structure for `user_types`
-- ----------------------------
DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_types_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_types
-- ----------------------------
INSERT INTO `user_types` VALUES ('1', 'Admin', null, null);

-- ----------------------------
-- Table structure for `user_type_url`
-- ----------------------------
DROP TABLE IF EXISTS `user_type_url`;
CREATE TABLE `user_type_url` (
  `user_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `user_type_url_url_id_foreign` (`url_id`),
  KEY `user_type_url_user_type_id_foreign` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_type_url
-- ----------------------------
INSERT INTO `user_type_url` VALUES ('1', '1', null, null);
INSERT INTO `user_type_url` VALUES ('1', '5', null, null);
INSERT INTO `user_type_url` VALUES ('1', '6', null, null);
INSERT INTO `user_type_url` VALUES ('1', '9', null, null);
INSERT INTO `user_type_url` VALUES ('1', '10', null, null);
INSERT INTO `user_type_url` VALUES ('1', '11', null, null);
INSERT INTO `user_type_url` VALUES ('1', '12', null, null);
INSERT INTO `user_type_url` VALUES ('1', '17', null, null);
INSERT INTO `user_type_url` VALUES ('1', '18', null, null);
INSERT INTO `user_type_url` VALUES ('1', '23', null, null);
INSERT INTO `user_type_url` VALUES ('1', '13', null, null);
INSERT INTO `user_type_url` VALUES ('1', '14', null, null);
INSERT INTO `user_type_url` VALUES ('1', '15', null, null);
INSERT INTO `user_type_url` VALUES ('1', '16', null, null);
INSERT INTO `user_type_url` VALUES ('1', '19', null, null);
INSERT INTO `user_type_url` VALUES ('1', '20', null, null);
INSERT INTO `user_type_url` VALUES ('1', '21', null, null);
INSERT INTO `user_type_url` VALUES ('1', '22', null, null);

-- ----------------------------
-- Table structure for `vendor`
-- ----------------------------
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `vat` text COLLATE utf8_unicode_ci NOT NULL,
  `reg` text COLLATE utf8_unicode_ci NOT NULL,
  `website` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendor
-- ----------------------------

-- ----------------------------
-- Table structure for `vendorbranches`
-- ----------------------------
DROP TABLE IF EXISTS `vendorbranches`;
CREATE TABLE `vendorbranches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` int(10) unsigned NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `lon` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `head_quarter` tinyint(1) NOT NULL,
  `map_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorbranches_vendor_id_foreign` (`vendor_id`),
  CONSTRAINT `vendorbranches_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorbranches
-- ----------------------------

-- ----------------------------
-- Table structure for `vendorcontacts`
-- ----------------------------
DROP TABLE IF EXISTS `vendorcontacts`;
CREATE TABLE `vendorcontacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `level` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8_unicode_ci NOT NULL,
  `ex` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vendor_branch_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorcontacts_vendor_branch_id_foreign` (`vendor_branch_id`),
  CONSTRAINT `vendorcontacts_vendor_branch_id_foreign` FOREIGN KEY (`vendor_branch_id`) REFERENCES `vendorbranches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorcontacts
-- ----------------------------

-- ----------------------------
-- Table structure for `vendoritembranches`
-- ----------------------------
DROP TABLE IF EXISTS `vendoritembranches`;
CREATE TABLE `vendoritembranches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `vendor_item_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendoritembranches_vendor_item_id_foreign` (`vendor_item_id`),
  KEY `vendoritembranches_branch_id_foreign` (`branch_id`),
  CONSTRAINT `vendoritembranches_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `vendorbranches` (`id`),
  CONSTRAINT `vendoritembranches_vendor_item_id_foreign` FOREIGN KEY (`vendor_item_id`) REFERENCES `vendoritems` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendoritembranches
-- ----------------------------

-- ----------------------------
-- Table structure for `vendoritems`
-- ----------------------------
DROP TABLE IF EXISTS `vendoritems`;
CREATE TABLE `vendoritems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_name` text COLLATE utf8_unicode_ci NOT NULL,
  `en_name` text COLLATE utf8_unicode_ci NOT NULL,
  `vendor_service_type_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendoritems_vendor_service_type_id_foreign` (`vendor_service_type_id`),
  CONSTRAINT `vendoritems_vendor_service_type_id_foreign` FOREIGN KEY (`vendor_service_type_id`) REFERENCES `vendorservicestypes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendoritems
-- ----------------------------

-- ----------------------------
-- Table structure for `vendorservicequestion`
-- ----------------------------
DROP TABLE IF EXISTS `vendorservicequestion`;
CREATE TABLE `vendorservicequestion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_service_id` int(10) unsigned NOT NULL,
  `question_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorservicequestion_vendor_service_id_foreign` (`vendor_service_id`),
  KEY `vendorservicequestion_question_type_id_foreign` (`question_type_id`),
  CONSTRAINT `vendorservicequestion_question_type_id_foreign` FOREIGN KEY (`question_type_id`) REFERENCES `questiontypes` (`id`),
  CONSTRAINT `vendorservicequestion_vendor_service_id_foreign` FOREIGN KEY (`vendor_service_id`) REFERENCES `vendorservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorservicequestion
-- ----------------------------

-- ----------------------------
-- Table structure for `vendorservices`
-- ----------------------------
DROP TABLE IF EXISTS `vendorservices`;
CREATE TABLE `vendorservices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorservices_vendor_id_foreign` (`vendor_id`),
  KEY `vendorservices_service_id_foreign` (`service_id`),
  CONSTRAINT `vendorservices_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  CONSTRAINT `vendorservices_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorservices
-- ----------------------------

-- ----------------------------
-- Table structure for `vendorservicestypes`
-- ----------------------------
DROP TABLE IF EXISTS `vendorservicestypes`;
CREATE TABLE `vendorservicestypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_name` text COLLATE utf8_unicode_ci NOT NULL,
  `en_name` text COLLATE utf8_unicode_ci NOT NULL,
  `vendor_service_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendorservicestypes_vendor_service_id_foreign` (`vendor_service_id`),
  CONSTRAINT `vendorservicestypes_vendor_service_id_foreign` FOREIGN KEY (`vendor_service_id`) REFERENCES `vendorservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendorservicestypes
-- ----------------------------

-- ----------------------------
-- Table structure for `vendoruploads`
-- ----------------------------
DROP TABLE IF EXISTS `vendoruploads`;
CREATE TABLE `vendoruploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_files_id` int(10) unsigned NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `vendorservices_id` int(10) unsigned NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendoruploads_service_files_id_foreign` (`service_files_id`),
  KEY `vendoruploads_vendorservices_id_foreign` (`vendorservices_id`),
  CONSTRAINT `vendoruploads_service_files_id_foreign` FOREIGN KEY (`service_files_id`) REFERENCES `servicefiles` (`id`),
  CONSTRAINT `vendoruploads_vendorservices_id_foreign` FOREIGN KEY (`vendorservices_id`) REFERENCES `vendorservices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vendoruploads
-- ----------------------------

-- ----------------------------
-- Table structure for `voice-requests`
-- ----------------------------
DROP TABLE IF EXISTS `voice-requests`;
CREATE TABLE `voice-requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `cur_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voice-requests
-- ----------------------------
INSERT INTO `voice-requests` VALUES ('1', '16', '11', '0', '270', '2017-08-10 15:58:18');
INSERT INTO `voice-requests` VALUES ('2', '16', '11', '0', '271', '2017-08-10 15:58:27');
INSERT INTO `voice-requests` VALUES ('3', '16', '11', '0', '274', '2017-08-10 16:00:49');
INSERT INTO `voice-requests` VALUES ('4', '16', '11', '0', '280', '2017-08-10 16:31:28');
INSERT INTO `voice-requests` VALUES ('5', '3', '287', '0', '9', '2017-09-09 20:07:25');

-- ----------------------------
-- View structure for `v_patient`
-- ----------------------------
DROP VIEW IF EXISTS `v_patient`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_patient` AS select `pd`.`id` AS `PatientCode`,`pd`.`name` AS `PatientName`,`pd`.`gender` AS `PatientGender`,`pd`.`email` AS `email`,group_concat(trim(concat(`pp`.`phone`)) separator ',') AS `Phones`,group_concat(trim(`pa`.`address`) separator ',') AS `Addresses`,group_concat(concat('(',`pa`.`latitude`,',',`pa`.`longitude`,')') separator ',') AS `MapAddresses` from ((`patient` `pd` left join `patient_phone` `pp` on((`pd`.`id` = `pp`.`patient_id`))) left join `patient_addresses` `pa` on((`pd`.`id` = `pa`.`patient_id`))) group by `pd`.`id`,`pd`.`name`,`pd`.`gender`,`pd`.`email` ;

-- ----------------------------
-- View structure for `v_patientanswers`
-- ----------------------------
DROP VIEW IF EXISTS `v_patientanswers`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_patientanswers` AS select `a`.`request_id` AS `RequestNo`,`q`.`en_name` AS `EnQusestion`,`a`.`answer` AS `QuestionAnswerBool`,(case when (`a`.`answer` = 0) then 'No' else 'Yes' end) AS `QuestionAnswerStr` from (`request_answers` `a` left join `radiology_questions` `q` on((`a`.`question_id` = `q`.`id`))) ;

-- ----------------------------
-- View structure for `v_patientrequest`
-- ----------------------------
DROP VIEW IF EXISTS `v_patientrequest`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_patientrequest` AS select `p`.`PatientCode` AS `PatientCode`,`p`.`PatientName` AS `PatientName`,`p`.`PatientGender` AS `PatientGender`,`p`.`email` AS `email`,`p`.`Phones` AS `Phones`,(case when (`r`.`patient_address_id` is not null) then `a`.`address` else `p`.`Addresses` end) AS `Addresses`,`p`.`MapAddresses` AS `MapAddresses`,`r`.`patient_age` AS `Age`,concat(`r`.`patient_weight`,' KG') AS `Weight`,`r`.`is_at_home` AS `is_at_home`,(case when (`r`.`is_at_home` = 0) then 'At Center' else 'At Home' end) AS `Location`,(case when (`r`.`is_able_to_be_stable` = 0) then 'No' else 'Yes' end) AS `Stable`,date_format(`r`.`created_at`,'%d-%m-%Y %h:%i %p') AS `RequestDate`,`u`.`PrescriptionImages` AS `Uploads`,`r`.`id` AS `RequestNo`,`r`.`status` AS `status`,`r`.`report` AS `report` from (((`patient_requests` `r` left join `v_patient` `p` on((`r`.`patient_id` = `p`.`PatientCode`))) left join `v_patientrequestuploads` `u` on((`r`.`id` = `u`.`request_id`))) left join `patient_addresses` `a` on((`a`.`id` = `r`.`patient_address_id`))) ;

-- ----------------------------
-- View structure for `v_patientrequestuploads`
-- ----------------------------
DROP VIEW IF EXISTS `v_patientrequestuploads`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_patientrequestuploads` AS select `patient_uploads`.`request_id` AS `request_id`,group_concat(trim(concat(`patient_uploads`.`image_url`)) separator ',') AS `PrescriptionImages` from `patient_uploads` group by `patient_uploads`.`request_id` ;
DROP TRIGGER IF EXISTS `before_patient_requests_update`;
DELIMITER ;;
CREATE TRIGGER `before_patient_requests_update` AFTER UPDATE ON `patient_requests` FOR EACH ROW BEGIN
    INSERT INTO patient_requests_log
    SET 
     request_id = OLD.id,
        old_status = OLD.status,
        status = NEW.status,
        changedat = NOW(); 
END
;;
DELIMITER ;
